#!/usr/bin/env python
# license removed for brevity
import rospy
import numpy as np
import copy
import csv
import yaml
from tf.transformations import quaternion_matrix, quaternion_from_matrix


def read_aruco_transformation():

    with open('/home/amir/catkin_ws/src/ergo-hri/ll4ma_posture_estimation/config/aruco_S01.yaml') as f:
        aruco = list(yaml.safe_load_all(f))
    webcam_T_aruco=quaternion_matrix([aruco[0]["webcam_orientation_x"], aruco[0]["webcam_orientation_y"], aruco[0]["webcam_orientation_z"], aruco[0]["webcam_orientation_w"] ])
    webcam_T_aruco[0,3]=aruco[0]["webcam_position_x"]
    webcam_T_aruco[1,3]=aruco[0]["webcam_position_y"]
    webcam_T_aruco[2,3]=aruco[0]["webcam_position_z"]

    kinect_T_aruco=quaternion_matrix([aruco[1]["kinect_orientation_x"], aruco[1]["kinect_orientation_y"], aruco[1]["kinect_orientation_z"], aruco[1]["kinect_orientation_w"] ])
    kinect_T_aruco[0,3]=aruco[1]["kinect_position_x"]
    kinect_T_aruco[1,3]=aruco[1]["kinect_position_y"]
    kinect_T_aruco[2,3]=aruco[1]["kinect_position_z"]

    return webcam_T_aruco, kinect_T_aruco

def read_mocap_T_frame0():

    with open('/home/amir/catkin_ws/src/ergo-hri/ll4ma_posture_estimation/config/study_config_S19.yaml') as f:
        stool = yaml.safe_load(f)
    mocap_T_frame0=np.array([[-1, 0, 0, stool["seat_x"]+.12],
    [0, 0, 1, stool["seat_y"]-stool["seat_height"]-0.05],
    [0, 1, 0, stool["seat_z"]+0.13],
    [0, 0, 0, 1]])
    return mocap_T_frame0


webcam_T_topic, kinect_T_aruco= read_aruco_transformation()

aruco_T_mocap=np.array([[0, 0, 1, 0],
                        [1, 0, 0, 0],
                        [0, 1, 0, 0],
                        [0, 0, 0, 1]])
topic_T_aruco=np.array([[-1, 0, 0, 0],
                        [0, 0, 1, 0],
                        [0, 1, 0, 0],
                        [0, 0, 0, 1]])
camera_T_frame0_1=np.dot(topic_T_aruco, aruco_T_mocap)
camera_T_frame0_2=np.dot(webcam_T_topic, camera_T_frame0_1)
# kinect_T_frame0=kinect_T_aruco*aruco_T_mocap*mocap_T_frame0
mocap_T_frame0=read_mocap_T_frame0()
camera_T_frame0=np.dot(camera_T_frame0_2, mocap_T_frame0)
# camera_T_frame0=np.linalg.inv(np.dot(camera_T_frame0_1, mocap_T_frame0))
camera_T_frame0=camera_T_frame0.tolist()
quat_camera_T_frame0=quaternion_from_matrix(camera_T_frame0)
quat_camera_T_frame0=quat_camera_T_frame0.tolist()

yaml_camera_T_frame0=dict(
position_x=  camera_T_frame0[0][3],
position_y=  camera_T_frame0[1][3],
position_z=  camera_T_frame0[2][3],
orientation_x= quat_camera_T_frame0[0],
orientation_y= quat_camera_T_frame0[1],
orientation_z= quat_camera_T_frame0[2],
orientation_w= quat_camera_T_frame0[3]
)
yaml.dump(yaml_camera_T_frame0, default_flow_style=False)
with open('/home/amir/catkin_ws/src/ergo-hri/ll4ma_posture_estimation/config/camera_T_frame0_S01.yaml' , 'w') as ff:
        ff.write(yaml.dump(yaml_camera_T_frame0, default_flow_style=False))
