#!/usr/bin/env python


# Use this file to test kdl functions

import numpy as np
from human_model import human_model
import math

# from rospkg import RosPack
# rp=RosPack()
# rp.list()
# path_urdf=rp.get_path('urlg_robots_description')+'/robots/'

def test_human_model():
    # import class:
    file_="/home/amir/catkin_ws/src/ll4ma_posture_estimation/ll4ma_pose_estimation/urdf/sticky_man_v3_one_arm_sitting.urdf"


    human=human_model(file_)
    j=np.ones(11)
    j[1]+=np.pi/2
    j[3]+=np.pi/2
    j[4]+=3*np.pi/2
    j[6]+=np.pi/2
    j[7]+=np.pi/2
    j[8]+=np.pi/2
    j[10]=0
    print j
    # print human.Jacobian(j)
    # print np.linalg.pinv(human.Jacobian(j))
    print human.FK(j)
def mat2euler(M, cy_thresh=None):

    # The convention of rotation around ``z``, followed by rotation around
    # ``y``, followed by rotation around ``x``, is known (confusingly) as
    # "xyz", pitch-roll-yaw, Cardan angles, or Tait-Bryan angles.


    _FLOAT_EPS_4 = np.finfo(float).eps * 4.0
    M = np.asarray(M)
    if cy_thresh is None:
        try:
            cy_thresh = np.finfo(M.dtype).eps * 4
        except ValueError:
            cy_thresh = _FLOAT_EPS_4
    r11, r12, r13, r21, r22, r23, r31, r32, r33 = M.flat
    # cy: sqrt((cos(y)*cos(z))**2 + (cos(x)*cos(y))**2)
    cy = math.sqrt(r33*r33 + r23*r23)
    if cy > cy_thresh: # cos(y) not close to zero, standard form
        z = math.atan2(-r12,  r11) # atan2(cos(y)*sin(z), cos(y)*cos(z))
        y = math.atan2(r13,  cy) # atan2(sin(y), cy)
        x = math.atan2(-r23, r33) # atan2(cos(y)*sin(x), cos(x)*cos(y))
    else: # cos(y) (close to) zero, so x -> 0.0 (see above)
        # so r21 -> sin(z), r22 -> cos(z) and
        z = math.atan2(r21,  r22)
        y = math.atan2(r13,  cy) # atan2(sin(y), cy)
        x = 0.0
    return [x, y, z]


def DH_def(particle, length):

    DH_right=[[particle[0], 0, 0, -np.pi/2],
    [np.pi/2+particle[1], 0, 0, np.pi/2],
    [particle[2], length[0], length[1]/2, np.pi/2],
    [np.pi/2+particle[3], 0, 0, np.pi/2 ],
    [3*np.pi/2+particle[4], 0, 0, np.pi/2],
    [particle[5], length[2], 0, -np.pi/2],
    [np.pi/2+particle[6], 0, 0, np.pi/2],
    [np.pi/2+particle[7], length[3], 0, np.pi/2],
    [np.pi/2+particle[8], 0, 0, np.pi/2],
    [particle[9], 0, length[4], 0]]

    return DH_right

def fkp(particle):
    length=[0.355, 0.457, 0.327, 0.256, 0.08] #link length for body segments based on ANSUR II model
    # T=human.FK(particle.theta)
    # x_dot=np.dot(human.Jacobian(particle.theta),particle.theta_dot)
    # orientation=[[T[0,0],T[0,1],T[0,2]],
    #                    [T[1,0],T[1,1],T[1,2]],
    #                    [T[2,0],T[2,1],T[2,2]]]
    # ee_orientation=mat2euler(orientation)
    # x= [T[0,3], T[1,3], T[2,3], ee_orientation[0], ee_orientation[1], ee_orientation[2]]
    # return [x,x_dot.tolist()[0]]
    DH=DH_def(particle,length)
    T=np.eye(4)
    for i in range(10):
        T=np.dot(T, [[np.cos(DH[i][0]), -np.sin(DH[i][0])*np.cos(DH[i][3]), np.sin(DH[i][0])*np.sin(DH[i][3]), DH[i][2]*np.cos(DH[i][0])],
        [np.sin(DH[i][0]), np.cos(DH[i][0])*np.cos(DH[i][3]), -np.cos(DH[i][0])*np.sin(DH[i][3]), DH[i][2]*np.sin(DH[i][0])],
        [0, np.sin(DH[i][3]), np.cos(DH[i][3]), DH[i][1]],
        [0, 0, 0, 1]])
    arm_ee_position=[T[0][3], T[1][3], T[2][3]]
    arm_ee_position=[T[1][3] ,T[2][3], T[0][3]+0.81] #convert to frame0
    arm_ee_orientation=[[T[0][0],T[0][1],T[0][2]],
                       [T[1][0],T[1][1],T[1][2]],
                       [T[2][0],T[2][1],T[2][2]]]


    arm_ee_orientation=np.dot([[0,1,0],[0,0,1],[1,0,0]], arm_ee_orientation)        #convert to frame0
    print arm_ee_orientation
    arm_ee_orientation=mat2euler(arm_ee_orientation)
    return arm_ee_position+arm_ee_orientation

test_human_model()
j=np.ones(10)
print fkp(j)
