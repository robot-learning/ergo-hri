import numpy as np
from tf.transformations import euler_from_matrix


a_vec = np.array([7, 0, 1])/np.linalg.norm(np.array([7, 0, 1]))
b_vec = np.array([7, -3, 3.5])/np.linalg.norm(np.array([7, -3, 3.5]))
# print a_vec
# print b_vec

cross = np.cross(a_vec, b_vec)
cross_len = np.linalg.norm(cross)
cross = cross/cross_len
angle = np.arccos(np.dot(a_vec,b_vec))

# Alias the axis coordinates.
x = cross[0]
y = cross[1]
z = cross[2]

# Trig functions (only need to do this maths once!).
ca = np.cos(angle)
sa = np.sin(angle)
R = np.zeros([3,3])
R[0,0] = 1.0 + (1.0 - ca)*(x**2 - 1.0)
R[0,1] = -z*sa + (1.0 - ca)*x*y
R[0,2] = y*sa + (1.0 - ca)*x*z
R[1,0] = z*sa+(1.0 - ca)*x*y
R[1,1] = 1.0 + (1.0 - ca)*(y**2 - 1.0)
R[1,2] = -x*sa+(1.0 - ca)*y*z
R[2,0] = -y*sa+(1.0 - ca)*x*z
R[2,1] = x*sa+(1.0 - ca)*y*z
R[2,2] = 1.0 + (1.0 - ca)*(z**2 - 1.0)

# vx = np.array([[0,-cross[2],cross[1]],[cross[2],0,-cross[0]],[-cross[1],cross[0],0]])
# R = np.identity(3)*np.cos(ab_angle) + (1-np.cos(ab_angle))*np.outer(cross,cross) + np.sin(ab_angle)*vx



validation=np.matmul(R,a_vec)
# print  validation
print R

euler = euler_from_matrix(R, 'rxyz')

print euler
