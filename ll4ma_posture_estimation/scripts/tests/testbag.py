#!/usr/bin/env python
# license removed for brevity



import rospy
import rosbag
from std_msgs.msg import Int32, String

bag = rosbag.Bag('test.bag', 'w')

try:
    str = String()
    str.data = 'foo'

    i = Int32()
    i.data = 2
    ii = Int32()
    ii.data = 4

    bag.write('human', i)
    bag.write('hd2', str)
    bag.write('hd2', str)
    bag.write('human', ii)
finally:
    bag.close()

bag = rosbag.Bag('test.bag')
for topic, msg, t in bag.read_messages(topics=['hd2', 'human']):
    print topic, ":", msg.data
bag.close()
