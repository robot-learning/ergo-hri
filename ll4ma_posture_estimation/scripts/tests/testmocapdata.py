#!/usr/bin/env python
# license removed for brevity
import rospy
import csv
import numpy as np
import math
from ll4ma_posture_estimation.msg import observation, state
from geometry_msgs.msg import Pose, PoseWithCovarianceStamped, Point, Quaternion, Twist, PoseStamped, PointStamped
from nav_msgs.msg import Path
from visualization_msgs.msg import Marker
from tf.transformations import quaternion_from_euler, euler_from_quaternion
import pandas as pd
from sensor_msgs.msg import JointState

if __name__ == '__main__':
    try:

        rospy.init_node('testmocapdata', anonymous=True)

        # ground_truth_data = pd.read_csv("/home/amir/catkin_ws/src/ll4ma_posture_estimation/ll4ma_pose_estimation/data/roya1/pose18.csv")
        ground_truth_data = pd.read_csv("/home/amir/working_dataset/natural_pose1 (copy).csv")
        pub = rospy.Publisher('joint_states', JointState, queue_size=1)
        pose=JointState()
        pose.header.stamp = rospy.Time.now()
        pose.header.frame_id = 'frame_0'
        pose.name =  [
        "1_waist_flextion",
        "1_waist_lateral_bending",
        "1_waist_rotation",
        "1_right_shoulder_abduction",
        "1_right_shoulder_ver_flextion",
        "1_right_shoulder_hor_flextion",
        "1_right_elbow_flextion",
        "1_right_elbow_supination",
        "1_right_hand_rad_deviation",
        "1_right_hand_flextion"]
        # rospy.loginfo(ground_truth_data.loc[2,'hip_x'])
        observ_freq=80
        rate = rospy.Rate(observ_freq)
        # M=50
        while not rospy.is_shutdown():

            for i in range(ground_truth_data['hip_x'].count()):
                # rospy.loginfo(i)
                eul_hip=euler_from_quaternion([
                ground_truth_data.loc[i,'hip_x'],
                ground_truth_data.loc[i,'hip_y'],
                ground_truth_data.loc[i,'hip_z'],
                ground_truth_data.loc[i,'hip_w']],axes='szyx')

                eul_abb=euler_from_quaternion([
                ground_truth_data.loc[i,'abb_x'],
                ground_truth_data.loc[i,'abb_y'],
                ground_truth_data.loc[i,'abb_z'],
                ground_truth_data.loc[i,'abb_w']],axes='szyx')

                eul_chest=euler_from_quaternion([
                ground_truth_data.loc[i,'chest_x'],
                ground_truth_data.loc[i,'chest_y'],
                ground_truth_data.loc[i,'chest_z'],
                ground_truth_data.loc[i,'chest_w']],axes='szyx')

                eul_Rshoulder=euler_from_quaternion([
                ground_truth_data.loc[i,'Rshoulder_x'],
                ground_truth_data.loc[i,'Rshoulder_y'],
                ground_truth_data.loc[i,'Rshoulder_z'],
                ground_truth_data.loc[i,'Rshoulder_w']],axes='szyx')

                eul_RUarm=euler_from_quaternion([
                ground_truth_data.loc[i,'RUarm_x'],
                ground_truth_data.loc[i,'RUarm_y'],
                ground_truth_data.loc[i,'RUarm_z'],
                ground_truth_data.loc[i,'RUarm_w']],axes='syzx')

                eul_RFarm=euler_from_quaternion([
                ground_truth_data.loc[i,'RFarm_x'],
                ground_truth_data.loc[i,'RFarm_y'],
                ground_truth_data.loc[i,'RFarm_z'],
                ground_truth_data.loc[i,'RFarm_w']],axes='szyx')

                eul_Rhand=euler_from_quaternion([
                ground_truth_data.loc[i,'Rhand_x'],
                ground_truth_data.loc[i,'Rhand_y'],
                ground_truth_data.loc[i,'Rhand_z'],
                ground_truth_data.loc[i,'Rhand_w']],axes='szyx')
                # rospy.loginfo(eul_Rhand[0])
                pose.position = len(pose.name) * [0.0]
                pose.position[0]=0 - eul_hip[2]
                pose.position[1]=np.pi/2 + eul_hip[1]
                pose.position[2]=0 - eul_hip[0] + eul_Rshoulder[0]+ eul_chest[0]+ eul_abb[0]
                # rospy.loginfo(eul_hip)
                pose.position[3]=np.pi+eul_RUarm[0]
                pose.position[4]=3*np.pi/2+eul_RUarm[1]
                pose.position[5] = -np.pi/2-eul_RUarm[2]

                pose.position[6]=eul_RFarm[0]
                pose.position[7]=-np.pi/2+eul_RFarm[2]
                pose.position[8]=np.pi/2-eul_Rhand[0]
                pose.position[9]=0-eul_Rhand[1]
                pose.header.stamp = rospy.Time.now()
                pose.header.frame_id = 'frame_0'
                pub.publish(pose)

                # rospy.loginfo("publish")
                # rospy.loginfo(pose.position[0])
                rate.sleep()
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
