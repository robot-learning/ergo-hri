#!/usr/bin/env python
# license removed for brevity
import rospy
import copy
import math
import numpy as np
from ll4ma_pose_estimation.msg import particle
from human_model import human_model




def mat2euler(M, cy_thresh=None):

    # The convention of rotation around ``z``, followed by rotation around
    # ``y``, followed by rotation around ``x``, is known (confusingly) as
    # "xyz", pitch-roll-yaw, Cardan angles, or Tait-Bryan angles.


    _FLOAT_EPS_4 = np.finfo(float).eps * 4.0
    M = np.asarray(M)
    if cy_thresh is None:
        try:
            cy_thresh = np.finfo(M.dtype).eps * 4
        except ValueError:
            cy_thresh = _FLOAT_EPS_4
    r11, r12, r13, r21, r22, r23, r31, r32, r33 = M.flat
    # cy: sqrt((cos(y)*cos(z))**2 + (cos(x)*cos(y))**2)
    cy = math.sqrt(r33*r33 + r23*r23)
    if cy > cy_thresh: # cos(y) not close to zero, standard form
        z = math.atan2(-r12,  r11) # atan2(cos(y)*sin(z), cos(y)*cos(z))
        y = math.atan2(r13,  cy) # atan2(sin(y), cy)
        x = math.atan2(-r23, r33) # atan2(cos(y)*sin(x), cos(x)*cos(y))
    else: # cos(y) (close to) zero, so x -> 0.0 (see above)
        # so r21 -> sin(z), r22 -> cos(z) and
        z = math.atan2(r21,  r22)
        y = math.atan2(r13,  cy) # atan2(sin(y), cy)
        x = 0.0
    return [x, y, z]


def DH_def(particle, length):

    DH_right=[[particle.theta[0], 0, 0, -np.pi/2],
    [np.pi/2+particle.theta[1], 0, 0, np.pi/2],
    [particle.theta[2], length[0], length[1]/2, np.pi/2],
    [np.pi/2+particle.theta[3], 0, 0, np.pi/2 ],
    [3*np.pi/2+particle.theta[4], 0, 0, np.pi/2],
    [particle.theta[5], length[2], 0, -np.pi/2],
    [np.pi/2+particle.theta[6], 0, 0, np.pi/2],
    [np.pi/2+particle.theta[7], length[3], 0, np.pi/2],
    [np.pi/2+particle.theta[8], 0, 0, np.pi/2],
    [particle.theta[9], 0, length[4], 0]]

    return DH_right

def fkp(human,particle):
    length=[0.355, 0.457, 0.327, 0.256, 0.08] #link length for body segments based on ANSUR II model
    T=human.FK(particle.theta)
    x_dot=np.dot(human.Jacobian(particle.theta),particle.theta_dot)
    orientation=[[T[0,0],T[0,1],T[0,2]],
                       [T[1,0],T[1,1],T[1,2]],
                       [T[2,0],T[2,1],T[2,2]]]
    ee_orientation=mat2euler(orientation)
    x= [T[0,3], T[1,3], T[2,3], ee_orientation[0], ee_orientation[1], ee_orientation[2]]
    return [x,x_dot.tolist()[0]]
    # DH=DH_def(particle,length)
    # T=np.eye(4)
    # for i in range(10):
    #     T=np.dot(T, [[np.cos(DH[i][0]), -np.sin(DH[i][0])*np.cos(DH[i][3]), np.sin(DH[i][0])*np.sin(DH[i][3]), DH[i][2]*np.cos(DH[i][0])],
    #     [np.sin(DH[i][0]), np.cos(DH[i][0])*np.cos(DH[i][3]), -np.cos(DH[i][0])*np.sin(DH[i][3]), DH[i][2]*np.sin(DH[i][0])],
    #     [0, np.sin(DH[i][3]), np.cos(DH[i][3]), DH[i][1]],
    #     [0, 0, 0, 1]])
    # arm_ee_position=[T[0][3], T[1][3], T[2][3]]
    # arm_ee_position=[T[1][3] ,T[2][3], T[0][3]+0.81] #convert to frame0
    # arm_ee_orientation=[[T[0][0],T[0][1],T[0][2]],
    #                    [T[1][0],T[1][1],T[1][2]],
    #                    [T[2][0],T[2][1],T[2][2]]]
    #
    # arm_ee_orientation=np.dot([[0,1,0],[0,0,1],[1,0,0]], arm_ee_orientation)        #convert to frame0
    # arm_ee_orientation=mat2euler(arm_ee_orientation)
    # return arm_ee_position+arm_ee_orientation


file_="/home/amir/catkin_ws/src/ll4ma_posture_estimation/ll4ma_pose_estimation/urdf/sticky_man_v3_one_arm_sitting.urdf"
human=human_model(file_)
particle = particle()
particle.theta = [-2.4074328384545627, 2.1398549660970625, -2.5040860578568394, 2.533087173189375, -1.9134344034713024, 1.0851656345141754, -2.7232268599094933, -0.5786183705287211, -1.572052091436418, 2.2321892662476897]
particle.theta_dot = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]

sigma_kinematic =[
            [0.5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0.5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0.5, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0.2, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0.2, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0.2, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0.01, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0.01, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0.01, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0.01, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.01, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.01],
        ]
[x,x_dot]=fkp(human,particle)

x_diff=[observation[j]-x[j] for j in range(6)]
x_dot_diff=[observation[j+6]-x_dot[j] for j in range(6)]
diff=x_diff+x_dot_diff

weights=float(np.power(np.linalg.det(np.multiply(2*3.14,sigma_kinematic)),-0.5)*np.exp(-0.5*np.dot(np.dot(diff,np.linalg.inv(sigma_kinematic)),np.transpose(diff))))


print weights
