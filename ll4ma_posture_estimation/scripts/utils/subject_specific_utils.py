#!/usr/bin/env python

import rospy
import numpy as np

def compensate_grasp_error():
    '''
    compensating the error on the grasp of user on stylus by adding the offset we
    calculated from the dataset
    '''

    subj = rospy.get_param("subject_num")
    task = rospy.get_param("task_num")
    if subj == 1:
        offset = [0.04, 0.02, 0.02]

    elif subj == 2:
        if task == 1:
            offset = [0.06, -0.02, 0.02]
        elif task == 2:
            offset = [0.09, -0.05, 0.02]
        elif task == 3:
            offset = [0.09, -0.02, 0.02]
        else:
            offset = [0.06, -0.02, 0.02]

    elif subj == 3:
        if task == 1:
            offset = [0.04, 0.02, 0.02]
        elif task == 3:
            offset = [0.07, 0.02, 0.02]
        else:
            offset = [0.04, 0.01, 0.02]

    elif subj == 4:
        if task == 1:
            offset = [0.07, 0, 0.05]
        elif task == 2:
            offset = [0.07, -0.03, 0.08]
        else:
            offset = [0.09, -0.01, 0.08]

    elif subj == 5:
        if task == 1 or task==2:
            offset = [0.01, 0.02, 0.03]
        else:
            offset = [0.01, 0.02, 0.03]

    elif subj == 6:
        if task == 1:
            offset = [0.09, 0.08, 0.04]
        elif task == 2:
            offset = [0.10, 0.08, 0.01]
        else:
            offset = [0.05, 0.10, 0.02]

    elif subj == 7:
        if task == 1:
            offset = [0.02, 0.10, 0.02]
        elif task == 2:
            offset = [0.03, 0.09, 0.05]
        else:
            offset = [0.03, 0.09, 0.04]

    elif subj == 8:
        if task == 1:
            offset = [0.01, 0.02, 0.04]
        elif task == 2:
            offset = [0.04, -0.03, 0.05]
        else:
            offset = [0.02, 0.01, 0.05]

    else:
        rospy.loginfo("Wrong subject number in --subject_specific_utils/compensate_grasp_error--")

    return offset

def human_joint_limit():
    '''
    compensating the error on the grasp of user on stylus by adding the offset we
    calculated from the dataset
    '''

    subj = rospy.get_param("subject_num")
    task = rospy.get_param("task_num")
    trial = rospy.get_param("trial_num")
    joint_limits_type = rospy.get_param("joint_limit")

    if joint_limits_type == "full":
            # self.joint_limits_deg = [[-30, 75], [-35, 35], [-35,35], [-90, 135], [-90, 90], [-45, 135], [0, 150], [-70, 70], [-30, 20], [-70, 80]]   # full human join limits in degrees
            joint_limits_deg =        [[-5, 10],  [-2, 2],   [-5,10],   [0, 90],   [-90, 90], [-45, 135], [0, 150], [-70, 70], [-30, 20], [-45, 45]]   # join limits in degrees ( reduces limits for torso)
    else:
        if subj == 1:
            if joint_limits_type == "modified_torso":
                if task == 1 and trial == 2:
                    joint_limits_deg = [[5, 10], [-2, 2], [-5,5], [75, 86], [-90, 90], [-45, 135], [0, 150], [-70, 70], [-30, 20], [-45, 45]] #subj1 task1 trial 2
                elif (task == 2 or task == 3) and trial == 2:
                    joint_limits_deg = [[5, 10], [-2, 2], [-5,5], [70, 80], [-90, 90], [-45, 135], [0, 150], [-70, 70], [-30, 20], [-45, 45]] #subj1 task2,3 trial 2
                elif task == 4 and trial == 1:
                    joint_limits_deg = [[5, 10], [-2, 2], [-5,5], [60, 80], [-90, 90], [-45, 135], [0, 150], [-70, 70], [-30, 20], [-45, 45]] #subj1 task 4 trial 1
                else:
                    rospy.loginfo("===================================")
                    rospy.loginfo("Wrong trial number from the dataset in --subject_specific_utils/human_joint_limit")
                    rospy.loginfo("===================================")

            else: # subject stpecfic
                if task == 1 and trial == 2:
                    joint_limits_deg = [[5, 10], [-2, 2], [-5,5], [75, 86], [-90, 90], [-45, 135], [0, 150], [-70, 70], [-30, 20], [-70, 30]] #subj1 task1 trial 2
                elif (task == 2 or task == 3) and trial == 2:
                    joint_limits_deg = [[5, 10], [-2, 2], [-5,5], [60, 80], [-90, 90], [-45, 135], [0, 150], [-70, 70], [-30, 20], [-70, 30]] #subj1 task2,3 trial 2
                elif task == 4 and trial == 1:
                    joint_limits_deg = [[5, 10], [-2, 2], [-5,5], [60, 80], [-90, 90], [-45, 135], [0, 150], [-70, 70], [-30, 20], [-70, 30]] #subj1 task 4 trial 1
                else:
                    rospy.loginfo("===================================")
                    rospy.loginfo("Wrong trial number from the dataset in --subject_specific_utils/human_joint_limit")
                    rospy.loginfo("===================================")

        elif subj == 2:
            if joint_limits_type == "modified_torso":
                if task == 1 and trial == 1:
                    joint_limits_deg = [[5, 8], [-2, 2], [10,20], [45, 86],   [-90, 90], [-45, 135], [0, 150], [-70, 70], [-30, 20], [-45, 45]] #subj2 task1 trial 1
                elif task == 2 and trial == 1:
                    joint_limits_deg = [[5, 10], [-2, 2], [10,20], [45, 86],   [-90, 90], [-45, 135], [0, 150], [-70, 70], [-30, 20], [-45, 45]] #subj2 task2 trial 1
                elif (task == 3 or task == 4) and trial == 2:
                    joint_limits_deg = [[5, 8], [-2, 2], [0,15], [45, 86],   [-90, 90], [-45, 135], [0, 150], [-70, 70], [-30, 20], [-45, 45]] #subj2 task3 trial 2
                else:
                    rospy.loginfo("===================================")
                    rospy.loginfo("Wrong trial number from the dataset in --subject_specific_utils/human_joint_limit")
                    rospy.loginfo("===================================")
            else:
                if task == 1 and trial == 1:
                    joint_limits_deg = [[5, 8], [-2, 2], [10,20], [45, 86], [-90, 0], [0, 135], [0, 150], [-70, 70], [-30, 20], [-20, 0]] #subj2 task1 trial 1
                elif task == 2 and trial == 1:
                    joint_limits_deg = [[5, 10], [-2, 2], [10,20], [45, 86], [-90, -10], [-10, 135], [0, 150], [-70, 70], [-30, 20], [-20, 0]] #subj2 task2 trial 1
                elif (task == 3 or task == 4) and trial == 2:
                    joint_limits_deg = [[5, 8], [-2, 2], [0,15], [45, 86], [-90, -10], [-10, 135], [0, 150], [-70, 70], [-30, 20], [-20, 0]] #subj2 task3 trial 2
                else:
                    rospy.loginfo("===================================")
                    rospy.loginfo("Wrong trial number from the dataset in --subject_specific_utils/human_joint_limit")
                    rospy.loginfo("===================================")

        elif subj == 3:
            if joint_limits_type == "modified_torso":
                if task == 1 and trial == 2:
                    joint_limits_deg = [[7, 13], [-3, 0], [-1,2], [65, 86],   [-90, 90], [-45, 135], [0, 150], [-70, 70], [-30, 20], [-45, 45]] #subj3 task1 trial 2
                elif task == 2 and trial == 2:
                    joint_limits_deg = [[7, 13], [-3, 0], [1,10], [60, 86],   [-90, 90], [-45, 135], [0, 150], [-70, 70], [-30, 20], [-45, 45]] #subj3 task2 trial 2
                elif task == 3  and trial == 2:
                    joint_limits_deg = [[7, 13], [-1, 2], [1,10], [45, 80],   [-90, 90], [-45, 135], [0, 150], [-70, 70], [-30, 20], [-45, 45]] #subj3 task3 & 4 trial 2
                elif task == 4 and trial == 2:
                    joint_limits_deg = [[7, 13], [-1, 2], [1,10], [60, 86],   [-90, 90], [-45, 135], [0, 150], [-70, 70], [-30, 20], [-45, 45]] #subj3 task3 & 4 trial 2
                else:
                    rospy.loginfo("===================================")
                    rospy.loginfo("Wrong trial number from the dataset in --subject_specific_utils/human_joint_limit")
                    rospy.loginfo("===================================")
            else:
                if task == 1 and trial == 2:
                    joint_limits_deg = [[7, 13], [-5, 1], [-1, 2], [45, 86], [-90, 90], [-45, 135], [0, 150], [-70, 70], [-30, 20], [0, 20]] #subj3 task1 trial 2
                elif task == 2 and trial == 2:
                    joint_limits_deg = [[7, 13], [-3, 0], [1, 10], [45, 86], [5, 90], [-45, 135], [0, 150], [-70, 70], [-30, 20], [-0, 20]] #subj3 task2 trial 2
                elif (task == 3 or task == 4) and trial == 2:
                    joint_limits_deg = [[7, 13], [-2, 3], [1, 10], [45, 86], [-5, 90], [-45, 135], [0, 150], [-70, 70], [-30, 20], [-0, 20]] #subj3 task 3&4 trial 2
                else:
                    rospy.loginfo("===================================")
                    rospy.loginfo("Wrong trial number from the dataset in --subject_specific_utils/human_joint_limit")
                    rospy.loginfo("===================================")

        elif subj == 4:
            if joint_limits_type == "modified_torso":
                if task == 1 and trial == 2:
                    joint_limits_deg = [[2, 5], [-2, 2], [5, 10], [60, 86], [-90, 90], [-45, 135], [0, 150], [-70, 70], [-30, 20], [-45, 45]] #subj4 task1 trial 2
                elif task == 2 and trial == 1:
                    joint_limits_deg = [[2, 8], [-2, 2], [5, 15], [70, 85], [-90, 90], [-45, 135], [0, 150], [-70, 70], [-30, 20], [-45, 45]] #subj4 task2 trial 1
                elif task == 3 and trial == 2:
                    joint_limits_deg = [[2, 8], [-8, -2], [5, 15], [60, 90], [-90, 90], [-45, 135], [0, 150], [-70, 70], [-30, 20], [-45, 45]] #subj4 task3 trial 2
                elif task == 4 and trial == 2:
                    joint_limits_deg = [[2, 8], [-8, -2], [5, 15], [70, 85], [-90, 90], [-45, 135], [0, 150], [-70, 70], [-30, 20], [-70, 30]] #subj4 task3 trial 2

                else:
                    rospy.loginfo("===================================")
                    rospy.loginfo("Wrong trial number from the dataset in --subject_specific_utils/human_joint_limit")
                    rospy.loginfo("===================================")
            else:
                if task == 1 and trial == 2:
                    joint_limits_deg = [[2, 5], [-2, 2], [5, 15], [60, 86], [0, 90], [-45, 135], [0, 150], [-70, 70], [-30, 20], [-70, 30]] #subj5 task1 trial 2
                elif task == 2 and trial == 1:
                    joint_limits_deg = [[2, 8], [-2, 2], [5, 15], [70, 85], [-90, 90], [-45, 135], [0, 150], [-70, 70], [-30, 20], [-70, 30]] #subj5 task2 trial 1
                elif (task == 3 or task == 4) and trial == 2:
                    joint_limits_deg = [[2, 8], [-8, -2], [5, 15], [70, 85], [-90, 90], [-45, 135], [0, 150], [-70, 70], [-30, 20], [-70, 30]] #subj5 task3 trial 2
                else:
                    rospy.loginfo("===================================")
                    rospy.loginfo("Wrong trial number from the dataset in --subject_specific_utils/human_joint_limit")
                    rospy.loginfo("===================================")

        elif subj == 5:
            if joint_limits_type == "modified_torso":
                if task == 1 and trial == 1:
                    joint_limits_deg = [[-2, 2], [-1, 3], [-5, 5], [75, 84], [-90, 90], [-45, 135], [0, 150], [-70, 70], [-30, 20], [-45, 45]] #subj5 task1 trial 1
                elif task == 2 and trial == 1:
                    joint_limits_deg = [[-1, 3], [-1, 3], [-5, 5], [75, 90], [-90, 90], [-45, 135], [0, 150], [-70, 70], [-30, 20], [-45, 45]] #subj5 task2 trial 1
                elif (task == 3 or task == 4) and trial == 2:
                    joint_limits_deg = [[1, 4], [-3, 1], [-5, 5], [45, 95], [-90, 90], [-45, 135], [0, 150], [-70, 70], [-30, 20], [-45, 45]] #subj5 task3 trial 2
                else:
                    rospy.loginfo("===================================")
                    rospy.loginfo("Wrong trial number from the dataset in --subject_specific_utils/human_joint_limit")
                    rospy.loginfo("===================================")
            else:
                if task == 1 and trial == 1:
                    joint_limits_deg = [[-2, 2], [-1, 3], [-5, 5], [45, 86], [-90, 90], [-0, 135], [0, 150], [-70, 70], [-30, 20], [0, 20]] #subj5 task1 trial 1
                elif task == 2 and trial == 1:
                    joint_limits_deg = [[-1, 3], [-1, 3], [-5, 5], [45, 86], [-90, 90], [-0, 135], [0, 150], [-70, 70], [-30, 20], [0, 20]] #subj5 task2 trial 1
                elif (task == 3 or task == 4) and trial == 2:
                    joint_limits_deg = [[1, 4], [-3, 1], [-5, 5], [45, 95], [-90, 90], [-0, 135], [0, 150], [-70, 70], [-30, 20], [-10, 20]] #subj5 task3 trial 2
                else:
                    rospy.loginfo("===================================")
                    rospy.loginfo("Wrong trial number from the dataset in --subject_specific_utils/human_joint_limit")
                    rospy.loginfo("===================================")

        elif subj == 6:
            if joint_limits_type == "modified_torso":
                if task == 1 and trial == 1:
                    joint_limits_deg = [[2, 8], [-1, 5], [5, 15], [70, 90], [-90, 90], [-45, 135], [0, 150], [-70, 70], [-30, 20], [-45, 45]] #subj6 task1 trial 1
                elif (task ==2 or task == 3 or task == 4) and trial == 1:
                    joint_limits_deg = [[5, 10], [-1, 3], [5, 15], [70, 85], [-90, 90], [-45, 135], [0, 150], [-70, 70], [-30, 20], [-45, 45]] #subj6 task2&3 trial 1
                else:
                    rospy.loginfo("===================================")
                    rospy.loginfo("Wrong trial number from the dataset in --subject_specific_utils/human_joint_limit")
                    rospy.loginfo("===================================")
            else:
                if task == 1 and trial == 1:
                    joint_limits_deg = [[2, 8], [-1, 5], [5,15], [70, 90], [-90, 90], [-45, 135], [0, 150], [-70, 70], [-10, 10], [0, 45]] #subj6 task1 trial 1
                elif (task ==2 or task == 3 or task == 4) and trial == 1:
                    joint_limits_deg = [[5, 10], [-1, 3], [5,15], [70, 85], [-90, 90], [-45, 135], [0, 150], [-70, 70], [-10, 10], [-5, 45]] #subj6 task2&3 trial 1
                else:
                    rospy.loginfo("===================================")
                    rospy.loginfo("Wrong trial number from the dataset in --subject_specific_utils/human_joint_limit")
                    rospy.loginfo("===================================")

        elif subj == 7:
            if joint_limits_type == "modified_torso":
                if task == 1 and trial == 1:
                    joint_limits_deg = [[0, 5], [-2, 2], [-5,5], [80, 100], [-90, 90], [-45, 135], [0, 150], [-70, 70], [-30, 20], [-45, 45]] #subj7 task1 trial 1
                elif task == 2 and trial == 2:
                    joint_limits_deg = [[2, 8], [-2, 2], [-3,3], [75, 90], [-90, 90], [-45, 135], [0, 150], [-70, 70], [-30, 20], [-45, 45]] #subj7 task2 trial 2
                elif (task == 3 or task == 4) and trial == 2:
                    joint_limits_deg = [[2, 8], [-2, 2], [-3,1], [45, 93], [-90, 90], [-45, 135], [0, 150], [-70, 70], [-30, 20], [-45, 45]] #subj7 task3 trial 2
                else:
                    rospy.loginfo("===================================")
                    rospy.loginfo("Wrong trial number from the dataset in --subject_specific_utils/human_joint_limit")
                    rospy.loginfo("===================================")
            else:
                if task == 1 and trial == 1:
                    joint_limits_deg = [[0, 5], [-2, 2], [-5,5], [80, 130], [-90, 90], [-0, 135], [0, 150], [-70, 70], [-5, 30], [-10, 10]] #subj7 task1 trial 1
                elif task == 2 and trial == 2:
                    joint_limits_deg = [[2, 8], [-2, 2], [-3,3], [80, 100], [-90, 90], [-10, 135], [0, 150], [-70, 70], [-15, 30], [-10, 10]] #subj7 task2 trial 2
                elif (task == 3 or task == 4) and trial == 2:
                    joint_limits_deg = [[2, 8], [-2, 2], [-3,1], [45, 100], [-90, 90], [-10, 135], [0, 150], [-70, 70], [-15, 30], [-10, 10]] #subj7 task3 trial 2

                else:
                    rospy.loginfo("===================================")
                    rospy.loginfo("Wrong trial number from the dataset in --subject_specific_utils/human_joint_limit")
                    rospy.loginfo("===================================")

        elif subj == 8:
            if joint_limits_type == "modified_torso":
                if task == 1 and trial == 1:
                    joint_limits_deg = [[5, 10], [-2, 2], [2,7], [45, 75], [-90, 90], [-45, 135], [0, 150], [-70, 70], [-30, 20], [-45, 45]] #subj8 task1 trial 1
                elif task == 2 and trial == 1:
                    joint_limits_deg = [[5, 10], [-2, 2], [2,7], [45, 60], [-90, 90], [-45, 135], [0, 150], [-70, 70], [-30, 20], [-45, 45]] #subj8 task2 trial 1
                elif task == 3 and trial == 1:
                    joint_limits_deg = [[5, 12], [-2, 2], [2,7], [45, 60], [-90, 90], [-45, 135], [0, 150], [-70, 70], [-30, 20], [-45, 45]] #subj8 task3 trial 1
                elif task == 4 and trial == 1:
                    joint_limits_deg = [[5, 12], [-2, 2], [2,7], [45, 60], [-90, 90], [-45, 135], [0, 150], [-70, 70], [-30, 20], [-45, 45]] #subj8 task4 trial 1
                else:
                    rospy.loginfo("===================================")
                    rospy.loginfo("Wrong trial number from the dataset in --subject_specific_utils/human_joint_limit")
                    rospy.loginfo("===================================")
            else:
                if task == 1 and trial == 1:
                    joint_limits_deg = [[5, 10], [-2, 2], [2,7], [45, 75], [-90, 90], [-0, 135], [0, 150], [-90, 90], [-30, 20], [-10, 20]] #subj8 task1 trial 1
                elif task == 2 and trial == 1:
                    joint_limits_deg = [[5, 10], [-2, 2], [2,7], [45, 60], [-90, 0], [-0, 135], [0, 150], [-90, 90], [-30, 20], [-10, 20]] #subj8 task2 trial 1
                elif task == 3 and trial == 1:
                    joint_limits_deg = [[5, 12], [-2, 2], [2,7], [45, 60], [-90, 90], [-0, 135], [0, 150], [-90, 90], [-30, 20], [-10, 10]] #subj8 task3 trial 1
                elif task == 4 and trial == 1:
                    joint_limits_deg = [[5, 12], [-2, 2], [2,7], [45, 60], [-90, 90], [-0, 135], [0, 150], [-90, 90], [-30, 30], [-30, 30]] #subj8 task4 trial 1
                else:
                    rospy.loginfo("===================================")
                    rospy.loginfo("Wrong trial number from the dataset in --subject_specific_utils/human_joint_limit")
                    rospy.loginfo("===================================")

        else:
            rospy.loginfo("===================================")
            rospy.loginfo("Wrong subject number from the dataset in --subject_specific_utils/human_joint_limit")
            rospy.loginfo("===================================")

    # converting to radians
    joint_limits = [[joint_limits_deg[i][0]*np.pi/180, joint_limits_deg[i][1]*np.pi/180] for i in range(len(joint_limits_deg))]
    # calculating the lengths of the joint limit
    joint_limits_len=[joint_limits[i][1]-joint_limits[i][0] for i in range(10)]

    return joint_limits, joint_limits_len
