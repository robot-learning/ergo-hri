#!/usr/bin/env python

import rospy
from tf.transformations import quaternion_from_euler, euler_from_quaternion, quaternion_matrix,  euler_from_matrix
from visualization_msgs.msg import Marker

def Set_Arrow_Marker(point):
    '''
    showing an arrow in rviz showing the pose of the robot stylus from the
    observation data
    '''
    P = Marker()
    P.header.frame_id = "frame0"
    P.header.stamp    = rospy.get_rostime()
    P.ns = "robot"
    P.id = 0
    P.type = 3 # arrow
    P.action = 0
    P.pose.position.x = point.x
    P.pose.position.y = point.y
    P.pose.position.z = point.z
    quaternion = quaternion_from_euler(point.theta, point.phi ,point.psi)
    P.pose.orientation.x = quaternion[0]
    P.pose.orientation.y = quaternion[1]
    P.pose.orientation.z = quaternion[2]
    P.pose.orientation.w = quaternion[3]

    P.scale.x = 0.02
    P.scale.y = 0.02
    P.scale.z = 0.2

    P.color.r = 0
    P.color.g = 0
    P.color.b = 1
    P.color.a = 1.0

    P.lifetime =  rospy.Duration(0)
    return P
