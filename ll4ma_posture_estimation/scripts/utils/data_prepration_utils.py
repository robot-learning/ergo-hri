#!/usr/bin/env python

import rospy
from tf.transformations import quaternion_from_euler, euler_from_quaternion, quaternion_matrix,  euler_from_matrix
from human_model_17_2_10dof import human_model as human_model_17_to_10dof
from utils.robot_utils import *
from utils.visualization_utils import *
from utils.subject_specific_utils import *
import numpy as np
from geometry_msgs.msg import Pose, PoseWithCovarianceStamped, Point, Quaternion, Twist, PoseStamped, PointStamped
from ll4ma_posture_estimation.msg import observation, state


def stylus_pose_maker(pose):
    '''
    Generates the pose of the robot stylus as two type of variables
    - posestamped : to be used for stylus path from observation containing the last 10 steps
    - location : to be used for a marker showing the pose of stylus from observation

    input: pose array from observation data
    output: posestamped
            location

    '''
    stylus1_marker_pub = rospy.Publisher('stylus1_marker', Marker, queue_size = 100)
    location = state()
    x =  pose[0]
    y =  pose[1]
    z =  pose[2]
    location.x = x
    location.y = y
    location.z = z
    location.theta = pose[3]
    location.phi = pose[4]
    location.psi = pose[5]
    posestamped = PoseStamped()   #we generate two types of data for stylus pose
    point = Pose()
    point.position.x = x
    point.position.y = y
    point.position.z = z
    posestamped.pose = point
    stylus1_marker = Set_Arrow_Marker(location)

    stylus1_marker_pub.publish(stylus1_marker)

    return posestamped, location


def calculate_euler_angles_from_mocap_data(mocap_data,i):
    _R0_ = np.array([[-1,0,0,0],[0,-1,0,0],[0,0,1,0],[0,0,0,1]])  # transform coordinate of a bone in urdf system into coordiante of the bone in mocap system
    _R1_ = np.array([[0,1,0,0],[-1,0,0,0],[0,0,1,0],[0,0,0,1]])  # transform coordinate of a bone in urdf system into coordiante of the bone in mocap system
    _R2_ = np.array([[0,-1,0,0],[1,0,0,0],[0,0,1,0],[0,0,0,1]])  # transform actual(not zero angle) coordinate of a bone into zero-angle coordinate of its child bone (check onenote!)

    # hip segment
    foo = np.dot(_R0_,quaternion_matrix([
                mocap_data.loc[i,'hip_x'],
                mocap_data.loc[i,'hip_y'],
                mocap_data.loc[i,'hip_z'],
                mocap_data.loc[i,'hip_w']]))
    u0_R_u0 = np.dot(foo,_R1_)
    eul_hip = euler_from_matrix(u0_R_u0, 'ryxz')   #ryxz because we want it to be realet to the previous one
    # and 1st joing is around y, 2nd joint is around x and the 3rd joint is around z

    # ab segment
    foo = np.dot(_R2_,quaternion_matrix([
                    mocap_data.loc[i,'abb_x'],
                    mocap_data.loc[i,'abb_y'],
                    mocap_data.loc[i,'abb_z'],
                    mocap_data.loc[i,'abb_w']]))
    u0_R_u0 = np.dot(foo,_R1_)
    eul_ab = euler_from_matrix(u0_R_u0, 'ryxz')   #ryxz because we want it to be realet to the previous one
    # and 1st joing is around y, 2nd joint is around x and the 3rd joint is around z

    # chest segment
    foo = np.dot(_R2_,quaternion_matrix([
                    mocap_data.loc[i,'chest_x'],
                    mocap_data.loc[i,'chest_y'],
                    mocap_data.loc[i,'chest_z'],
                    mocap_data.loc[i,'chest_w']]))
    u0_R_u0 = np.dot(foo,_R1_)
    eul_chest = euler_from_matrix(u0_R_u0, 'ryxz')   #ryxz because we want it to be realet to the previous one
    # and 1st joing is around y, 2nd joint is around x and the 3rd joint is around z

    #right shoulder segment
    foo = np.dot(_R2_,quaternion_matrix([
                    mocap_data.loc[i,'Rshoulder_x'],
                    mocap_data.loc[i,'Rshoulder_y'],
                    mocap_data.loc[i,'Rshoulder_z'],
                    mocap_data.loc[i,'Rshoulder_w']]))
    u0_R_u0 = np.dot(foo,_R1_)
    eul_Rshoulder = euler_from_matrix(u0_R_u0, 'ryxz')   #ryxz because we want it to be realet to the previous one
    # and 1st joing is around y, 2nd joint is around x and the 3rd joint is around z

    #right upper arm segment
    foo = np.dot(_R2_,quaternion_matrix([
                    mocap_data.loc[i,'RUarm_x'],
                    mocap_data.loc[i,'RUarm_y'],
                    mocap_data.loc[i,'RUarm_z'],
                    mocap_data.loc[i,'RUarm_w']]))
    u0_R_u0 = np.dot(foo,_R1_)
    eul_RUarm = euler_from_matrix(u0_R_u0, 'rxyz')   #ryxz because we want it to be realet to the previous one
    # and 1st joing is around x, 2nd joint is around y and the 3rd joint is around z

    # right forearm segment
    foo = np.dot(_R2_,quaternion_matrix([
                    mocap_data.loc[i,'RFarm_x'],
                    mocap_data.loc[i,'RFarm_y'],
                    mocap_data.loc[i,'RFarm_z'],
                    mocap_data.loc[i,'RFarm_w']]))
    u0_R_u0 = np.dot(foo,_R1_)
    eul_RFarm = euler_from_matrix(u0_R_u0, 'rzyx')   #ryxz because we want it to be realet to the previous one
    # and 1st joing is around z, 2nd joint is around y and the 3rd joint is around x

    # right hand segment
    foo = np.dot(_R2_,quaternion_matrix([
                    mocap_data.loc[i,'Rhand_x'],
                    mocap_data.loc[i,'Rhand_y'],
                    mocap_data.loc[i,'Rhand_z'],
                    mocap_data.loc[i,'Rhand_w']]))
    u0_R_u0 = np.dot(foo,_R1_)
    eul_Rhand = euler_from_matrix(u0_R_u0, 'rzxy')   #ryxz because we want it to be realet to the previous one
    # and 1st joing is around z, 2nd joint is around x and the 3rd joint is around y

    mocap_df_rviz = [
                        eul_hip[0],
                        eul_hip[1],
                        eul_hip[2],
                        eul_ab[0],
                        eul_ab[1],
                        eul_ab[2],
                        eul_chest[0],
                        eul_chest[1],
                        eul_chest[2],
                        0,
                        eul_Rshoulder[2],
                        eul_RUarm[0],
                        eul_RUarm[1]+eul_RFarm[2],
                        eul_RUarm[2],
                        eul_RFarm[0],
                        eul_RFarm[1]+eul_Rhand[2],
                        eul_Rhand[0],
                        eul_Rhand[1]]
    # fixing the wrist issue
    mocap_df_rviz=fix_mocap_wrist(mocap_df_rviz,size=18)
    # checking the angles passing 2*pi
    mocap_df_rviz=angle_2pi_check(mocap_df_rviz)

    return mocap_df_rviz, eul_hip, eul_ab, eul_chest, eul_Rshoulder

def fix_mocap_wrist(mocap_df,size):
    '''
    This function adds the proper rigid body transformation to fix the issue of
    mocao data at the wrist joints
    for more info, see the paper
    '''
    subj = rospy.get_param("subject_num")
    task = rospy.get_param("task_num")
    if subj == 1:
        hand_transform = [-0.6, -0.2 , 0.5]
        mocap_df[size-3] += hand_transform[0]
        mocap_df[size-2] += hand_transform[1]
        mocap_df[size-1] += hand_transform[2]

    elif subj == 2:
        hand_transform = [-0.4, -0.2 , 1.4]
        mocap_df[size-3] += hand_transform[0]
        mocap_df[size-2] += hand_transform[1]
        mocap_df[size-1] += hand_transform[2]

    elif subj == 3:
        hand_transform = [-0.1, 0.1 , 0.50]
        mocap_df[size-3] += hand_transform[0]
        mocap_df[size-2] += hand_transform[1]
        mocap_df[size-1] += hand_transform[2]

    elif subj == 4:
        if task == 1:
            hand_transform = [-0.4, -0.0 , 0.50]
            mocap_df[size-3] += hand_transform[0]
            mocap_df[size-2] += hand_transform[1]
            mocap_df[size-1] += hand_transform[2]
        else:
            hand_transform = [-0.4, -0.0 , 0.70]
            mocap_df[size-3] += hand_transform[0]
            mocap_df[size-2] += hand_transform[1]
            mocap_df[size-1] += hand_transform[2]

    elif subj == 5:
        if task ==1 or task == 2:
            hand_transform = [-0.8, -0.4 , 0.7]
            mocap_df[size-3] += hand_transform[0]
            mocap_df[size-2] += hand_transform[1]
            mocap_df[size-1] += hand_transform[2]
        else:
            hand_transform = [-0.8, -0.3 , 0.7]
            mocap_df[size-3] += hand_transform[0]
            mocap_df[size-2] += hand_transform[1]
            mocap_df[size-1] += hand_transform[2]

    elif subj == 6:
        if task == 1:
            hand_transform = [-0.1, 0.0 , 0.7]
            mocap_df[size-3] += hand_transform[0]
            mocap_df[size-2] += hand_transform[1]
            mocap_df[size-1] += hand_transform[2]
        else:
            hand_transform = [-0.1, -0.2 , 0.3]
            mocap_df[size-3] += hand_transform[0]
            mocap_df[size-2] += hand_transform[1]
            mocap_df[size-1] += hand_transform[2]
    elif subj == 7:
            hand_transform = [-0.4, 0.1 , 0.5]
            mocap_df[size-3] += hand_transform[0]
            mocap_df[size-2] += hand_transform[1]
            mocap_df[size-1] += hand_transform[2]

    elif subj == 8:
            hand_transform = [-0.4, 0.2 , 0.9]
            mocap_df[size-3] += hand_transform[0]
            mocap_df[size-2] += hand_transform[1]
            mocap_df[size-1] += hand_transform[2]

    else:
        rospy.loginfo("subject number is not correct --> in the data_prepration_utils/fix_mocap_wrist")

    return mocap_df

def equivalent_torso_for_mocap(mocap_df_rviz, eul_hip, eul_ab, eul_chest, eul_Rshoulder):
    '''
    reducing the mocap data joints by generating an equivalent torso segment
    '''
    # setting the human model urdf file
    urdf_file = "/home/amir/catkin_ws/src/ergo-hri/ll4ma_posture_estimation/urdf/rviz_gt_v2.urdf.xacro"
    # building human model up to the chest
    human_chest = human_model_17_to_10dof(urdf_file, 'mocap_full_waist', 'mocap_full_chest_shoulder_sphere1')   # generate a kdl model from waist to chest
    # building human model up to the shoulder
    human_shoulder = human_model_17_to_10dof(urdf_file, 'mocap_full_waist', 'mocap_full_right_shoulder_abduction')  # generate a kdl model from waist to shoulder
    #calculating neck and shoulder position
    neck_point = fk_position(human_chest,[eul_hip[0], eul_hip[1], eul_hip[2], eul_ab[0], eul_ab[1], eul_ab[2], eul_chest[0], eul_chest[1], eul_chest[2], 0])
    shoulder_point = fk_position(human_shoulder,[eul_hip[0], eul_hip[1], eul_hip[2], eul_ab[0], eul_ab[1], eul_ab[2], eul_chest[0], eul_chest[1], eul_chest[2], eul_Rshoulder[2], 0])
    # projectinng shoulder and neck into sagital, frontal and top planes
    neck_point_projected_sagital = [neck_point[0], neck_point[2]]
    neck_point_projected_frontal = [neck_point[1], neck_point[2]]
    shoulder_point_projected_top = [shoulder_point[0]- neck_point[0], shoulder_point[1]-neck_point[1]]
    # Euler angles of equivalent torso segment
    theta_1_10DOF = np.arctan2(neck_point_projected_sagital[0], neck_point_projected_sagital[1])
    theta_2_10DOF = np.arctan2(neck_point_projected_frontal[1], neck_point_projected_frontal[0])-np.pi/2
    theta_3_10DOF = np.arctan2(shoulder_point_projected_top[1], shoulder_point_projected_top[0])+np.pi/2
    # building the mocap_df for reduces joint mocap data
    mocap_df = mocap_df_rviz[8:]
    mocap_df[0] = theta_1_10DOF
    mocap_df[1] = theta_2_10DOF
    mocap_df[2] = theta_3_10DOF
    # fixing the wrist issue
    mocap_df=fix_mocap_wrist(mocap_df,size=10)
    # checking the angles passing 2*pi
    mocap_df=angle_2pi_check(mocap_df)

    return mocap_df



def transfer_robotframe_to_frame0(np_df_robot, grasp_offset):
    '''
    converting stylus pose and veloctity from robot frame into frame0
    '''
    transformation = [rospy.get_param("frame0_T_robot0_x") + grasp_offset[0],
                      rospy.get_param("frame0_T_robot0_y") + grasp_offset[1],
                      rospy.get_param("frame0_T_robot0_z") + grasp_offset[2]]

    stylus1_pose_array =  [-np_df_robot[6] + transformation[0],
                            np_df_robot[5] + transformation[1],
                            np_df_robot[7] + transformation[2],
                            -np_df_robot[9],
                            np_df_robot[8],
                            np_df_robot[10]]

    stylus1_vel_array =  [-np_df_robot[12],
                         np_df_robot[11],
                         np_df_robot[13],
                         -np_df_robot[15],
                         np_df_robot[14],
                         np_df_robot[16]]
    return stylus1_pose_array, stylus1_vel_array

def calculate_stylus_dtime(np_df_robot,Prev_Robot_data_frame):
    '''
    Calculating the timestamps and delta_time from the stylus motion data
    '''
    timestamp = [np_df_robot[2],np_df_robot[4]]

    if np_df_robot[1] == Prev_Robot_data_frame[1]:
        dtime = (np_df_robot[3]-Prev_Robot_data_frame[3])/100 #in sec
    else:
        dtime = (100+np_df_robot[3]-Prev_Robot_data_frame[3])/100 # in sec

    return timestamp, dtime

def prepare_stylus_data(observed_data, second, hund_second, Prev_Robot_data_frame):
    '''

    '''
    #find the frames from observe_data that have same second as the selected data_frame in mocap_data
    data_frame_robot = observed_data.loc[observed_data['second'].isin([second])]
    #find the frame from observe_data that have same mili_second as the selected data_frame in mocap_data
    np_df_robot = (data_frame_robot.loc[data_frame_robot['hund_second'].isin([hund_second])]).as_matrix()[0] #find the frame from observe_data
    # that have same mili_second as the selected data_frame in mocap_data
    # taking the first dataframe of the matching dataframes (it can be substituted with the average values)

    ## Calculating the Timestamp and dtime from the stylus pose dataset
    robot_time_stamp, dtime = calculate_stylus_dtime(np_df_robot , Prev_Robot_data_frame)

    ## compensating the error in grasp point on the stylus
    grasp_offset = compensate_grasp_error()

    ## convert stylus pose and veloctity from robot frame into frame0
    stylus1_pose_array, stylus1_vel_array = transfer_robotframe_to_frame0(np_df_robot, grasp_offset)

    # make the pose stampt and pose state version of stylus pose
    stylus1_pose_posestamped, stylus1_pose_state = stylus_pose_maker(stylus1_pose_array)

    return stylus1_pose_array, stylus1_vel_array, robot_time_stamp, dtime, stylus1_pose_posestamped, stylus1_pose_state, np_df_robot
