#!/usr/bin/env python

import rospy
import numpy as np
from tf.transformations import quaternion_from_euler, euler_from_quaternion, quaternion_matrix,  euler_from_matrix
import copy
import math



# TODO: merge two Fk together into a generalized version

def fk_position(human,posture):
    '''
    returns the position of human hand based on the joint hip_joint_angles
    input:
        human: model of human from human class
        posture: joint angles for Posture
    output:
        x : position of hand
    '''
    T = human.FK(posture)
    x = [T[0,3], T[1,3], T[2,3]]
    return x

def fkp(human,particle):
    kdl_theta=copy.copy(particle.theta)
    kdl_theta.append(0)
    T=human.FK(kdl_theta)
    x_dot=np.dot(human.Jacobian(kdl_theta),particle.theta_dot+[0])
    orientation=[[T[0,0],T[0,1],T[0,2]],
                       [T[1,0],T[1,1],T[1,2]],
                       [T[2,0],T[2,1],T[2,2]]]
    ee_orientation=mat2euler(orientation)
    x= [T[0,3], T[1,3], T[2,3], ee_orientation[0], ee_orientation[1], ee_orientation[2]]

    return [x,x_dot.tolist()[0][0:11]]

def fk_ls(human,theta):
    kdl_theta=copy.copy(theta.tolist())
    kdl_theta.append(0)
    T=human.FK(kdl_theta)
    orientation=[[T[0,0],T[0,1],T[0,2]],
                       [T[1,0],T[1,1],T[1,2]],
                       [T[2,0],T[2,1],T[2,2]]]
    ee_orientation=mat2euler(orientation)
    x= np.array([T[0,3], T[1,3], T[2,3], ee_orientation[0], ee_orientation[1], ee_orientation[2]])

    return x

def angle_2pi_check(posture):
    '''
    refining the angles if they pass 2*pi
    '''
    for k in range(len(posture)):
                    if posture[k]>2*np.pi:
                        posture[k] -= 2*np.pi
                    if posture[k]<-2*np.pi:
                        posture[k] += 2*np.pi
    return posture

def mat2euler(M, cy_thresh=None):

    # The convention of rotation around ``z``, followed by rotation around
    # ``y``, followed by rotation around ``x``, is known (confusingly) as
    # "xyz", pitch-roll-yaw, Cardan angles, or Tait-Bryan angles.


    _FLOAT_EPS_4 = np.finfo(float).eps * 4.0
    M = np.asarray(M)
    if cy_thresh is None:
        try:
            cy_thresh = np.finfo(M.dtype).eps * 4
        except ValueError:
            cy_thresh = _FLOAT_EPS_4
    r11, r12, r13, r21, r22, r23, r31, r32, r33 = M.flat
    # cy: sqrt((cos(y)*cos(z))**2 + (cos(x)*cos(y))**2)
    cy = math.sqrt(r33*r33 + r23*r23)
    if cy > cy_thresh: # cos(y) not close to zero, standard form
        z = math.atan2(-r12,  r11) # atan2(cos(y)*sin(z), cos(y)*cos(z))
        y = math.atan2(r13,  cy) # atan2(sin(y), cy)
        x = math.atan2(-r23, r33) # atan2(cos(y)*sin(x), cos(x)*cos(y))
    else: # cos(y) (close to) zero, so x -> 0.0 (see above)
        # so r21 -> sin(z), r22 -> cos(z) and
        z = math.atan2(r21,  r22)
        y = math.atan2(r13,  cy) # atan2(sin(y), cy)
        x = 0.0
    return [x, y, z]
