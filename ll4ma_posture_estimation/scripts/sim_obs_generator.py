#!/usr/bin/env python
# license removed for brevity
import rospy
import copy
import csv
import numpy as np
import math
from ll4ma_posture_estimation.msg import observation, state
from geometry_msgs.msg import Pose, PoseWithCovarianceStamped, Point, Quaternion, Twist, PoseStamped, PointStamped
from nav_msgs.msg import Path
from visualization_msgs.msg import Marker
from tf.transformations import quaternion_from_euler, euler_from_quaternion
# import pandas as pd
from human_model_sim import human_model


def mat2euler(M, cy_thresh=None):

    # The convention of rotation around ``z``, followed by rotation around
    # ``y``, followed by rotation around ``x``, is known (confusingly) as
    # "xyz", pitch-roll-yaw, Cardan angles, or Tait-Bryan angles.


    _FLOAT_EPS_4 = np.finfo(float).eps * 4.0
    M = np.asarray(M)
    if cy_thresh is None:
        try:
            cy_thresh = np.finfo(M.dtype).eps * 4
        except ValueError:
            cy_thresh = _FLOAT_EPS_4
    r11, r12, r13, r21, r22, r23, r31, r32, r33 = M.flat
    # cy: sqrt((cos(y)*cos(z))**2 + (cos(x)*cos(y))**2)
    cy = math.sqrt(r33*r33 + r23*r23)
    if cy > cy_thresh: # cos(y) not close to zero, standard form
        z = math.atan2(-r12,  r11) # atan2(cos(y)*sin(z), cos(y)*cos(z))
        y = math.atan2(r13,  cy) # atan2(sin(y), cy)
        x = math.atan2(-r23, r33) # atan2(cos(y)*sin(x), cos(x)*cos(y))
    else: # cos(y) (close to) zero, so x -> 0.0 (see above)
        # so r21 -> sin(z), r22 -> cos(z) and
        z = math.atan2(r21,  r22)
        y = math.atan2(r13,  cy) # atan2(sin(y), cy)
        x = 0.0
    return [x, y, z]


# def DH_def(particle, length):
#
#     DH_right=[[particle.theta[0], 0, 0, -np.pi/2],
#     [np.pi/2+particle.theta[1], 0, 0, np.pi/2],
#     [particle.theta[2], length[0], length[1]/2, np.pi/2],
#     [np.pi/2+particle.theta[3], 0, 0, np.pi/2 ],
#     [3*np.pi/2+particle.theta[4], 0, 0, np.pi/2],
#     [particle.theta[5], length[2], 0, -np.pi/2],
#     [np.pi/2+particle.theta[6], 0, 0, np.pi/2],
#     [np.pi/2+particle.theta[7], length[3], 0, np.pi/2],
#     [np.pi/2+particle.theta[8], 0, 0, np.pi/2],
#     [particle.theta[9], 0, length[4], 0]]
#
#     return DH_right

def fkp(human,posture,posture_vel):
    length=[0.355, 0.457, 0.327, 0.256, 0.08] #link length for body segments based on ANSUR II model
    kdl_theta=posture.tolist()
    kdl_theta[1]+=np.pi/2
    kdl_theta[3]+=np.pi/2
    kdl_theta[4]+=3*np.pi/2
    kdl_theta[6]+=np.pi/2
    kdl_theta[7]+=np.pi/2
    kdl_theta[8]+=np.pi/2
    kdl_theta.append(0)
    T=human.FK(kdl_theta)
    x_dot=np.dot(human.Jacobian(kdl_theta),posture_vel+[0])
    orientation=[[T[0,0],T[0,1],T[0,2]],
                       [T[1,0],T[1,1],T[1,2]],
                       [T[2,0],T[2,1],T[2,2]]]
    ee_orientation=mat2euler(orientation)
    x= [T[0,3], T[1,3], T[2,3], ee_orientation[0], ee_orientation[1], ee_orientation[2]]
    return [x,x_dot.tolist()[0][0:10]]


def Set_Arrow_Marker(point):
        P = Marker()
        P.header.frame_id = "frame0"
        P.header.stamp    = rospy.get_rostime()
        P.ns = "robot"
        P.id = 0
        P.type = 0 # arrow
        P.action = 0
        P.pose.position.x = point.x
        P.pose.position.y = point.y
        P.pose.position.z = point.z
        quaternion = quaternion_from_euler(point.theta, point.phi ,point.psi)
        P.pose.orientation.x = quaternion[0]
        P.pose.orientation.y = quaternion[1]
        P.pose.orientation.z = quaternion[2]
        P.pose.orientation.w = quaternion[3]
        P.scale.x = 0.1
        P.scale.y = 0.02
        P.scale.z = 0.02

        P.color.r = 1
        P.color.g = 0
        P.color.b = 0
        P.color.a = 1.0

        P.lifetime =  rospy.Duration(0)
        return P

def ee_pose_generator(pose):
    location = state()#moved in to for loop
    # t = rospy.Time.now().to_sec() * math.pi
    x = pose[0]
    y = pose[1]
    z = pose[2]

    location.x = x
    location.y = y
    location.z = z
    location.theta = pose[3]
    location.phi = pose[4]
    location.psi = pose[5]
    posestamped = PoseStamped()   #we generate two types of data for stylus pose
    point = Pose()
    point.position.x = x
    point.position.y = y
    point.position.z = z
    posestamped.pose = point
    stylus1_marker=Set_Arrow_Marker(location)
    stylus1_marker_pub.publish(stylus1_marker)

    return posestamped, location


if __name__ == '__main__':
    try:

        rospy.init_node('sim_posture_and_observation_gen', anonymous=True)
        observ_pub = rospy.Publisher('observation', observation, queue_size=100)
        stylus1_marker_pub = rospy.Publisher('stylus1_marker', Marker, queue_size=100)
        stylus1_path_pub = rospy.Publisher("stylus1_path", Path, queue_size=1)


        stylus1_path = Path()
        stylus1_path.header.frame_id = 'frame0'
        global observ_freq

        observ_freq=5#Hz
        data_freq=50 #Hz
        dtime=float(1)/data_freq
        rate = rospy.Rate(observ_freq)

        file_="/home/amir/catkin_ws/src/ll4ma_posture_estimation/ll4ma_posture_estimation/urdf/one_arm/upperbody/sim/kdl.urdf.xacro"
        human=human_model(file_)
        # Prev_observation=np.zeros(12)
        # pre_gt_posture=[(10*np.pi/180), 0, (10*np.pi/180), (5*np.pi/180), (5*np.pi/180), (5*np.pi/180), 0, 0, 0, 0]
        pre_gt_posture=[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]




        gt_posture=np.zeros(10)
        # gt_posture += [(10*np.pi/180), 0, (10*np.pi/180), (5*np.pi/180), (5*np.pi/180), (5*np.pi/180), 0, 0, 0, 0]
        gt_posture_vel=np.zeros(10)
        # prev_obs_pose_and_vel=np.zeros(12)
        t=0.0
        observation_data = observation()
        gt_posture_vel=[(gt_posture[k]-pre_gt_posture[k])/(dtime) for k in range(10)]

        stylus1_pose_and_vel=fkp(human,gt_posture,gt_posture_vel)



        wait_time=10 #iteration
        data_motion_preriod = 2 #sec
        data_motion_omega=float(1)/(2*np.pi*data_motion_preriod)  #Hz

        while not rospy.is_shutdown():
            if t>wait_time:
                #  theta=theta_0=a*cos(omega(t-t_0))+a    a=amplitute of motion
                ########## trajectory (1) #########################
                # gt_posture[0]=0-(10*np.pi/180)*np.cos(data_motion_omega*(t-wait_time))+(10*np.pi/180)
                # gt_posture[1]=0+(5*np.pi/180)*np.cos(data_motion_omega*(t-wait_time))-(5*np.pi/180)
                # gt_posture[2]=0-(10*np.pi/180)*np.cos(data_motion_omega*(t-wait_time))+(10*np.pi/180)
                # gt_posture[3]=0+(5*np.pi/180)*np.cos(data_motion_omega*(t-wait_time))-(5*np.pi/180)
                # gt_posture[4]=0-(15*np.pi/180)*np.cos(data_motion_omega*(t-wait_time))+(15*np.pi/180)
                # gt_posture[5]=0+(15*np.pi/180)*np.cos(data_motion_omega*(t-wait_time))-(15*np.pi/180)
                # gt_posture[6]=0+(10*np.pi/180)*np.cos(data_motion_omega*(t-wait_time))-(10*np.pi/180)
                # gt_posture[7]=0#+(10*np.pi/180)*np.cos(data_motion_omega*(t-wait_time))-(10*np.pi/180)
                # gt_posture[8]=0#-(10*np.pi/180)*np.cos(data_motion_omega*(t-wait_time))+(10*np.pi/180)
                # gt_posture[9]=0#-(10*np.pi/180)*np.cos(data_motion_omega*(t-wait_time))+(10*np.pi/180)


                ########## trajectory (2) #########################
                # gt_posture[0]=0-(10*np.pi/180)*np.cos(data_motion_omega*(t-wait_time))+(10*np.pi/180)
                # gt_posture[1]=0-(5*np.pi/180)*np.cos(data_motion_omega*(t-wait_time))+(5*np.pi/180)
                # gt_posture[2]=0-(5*np.pi/180)*np.cos(data_motion_omega*(t-wait_time))+(10*np.pi/180)
                # gt_posture[3]=0+(5*np.pi/180)*np.cos(data_motion_omega*(t-wait_time))-(5*np.pi/180)
                # gt_posture[4]=0-(40*np.pi/180)*np.cos(data_motion_omega*(t-wait_time))+(40*np.pi/180)
                # gt_posture[5]=0+(35*np.pi/180)*np.cos(data_motion_omega*(t-wait_time))-(35*np.pi/180)
                # gt_posture[6]=0+(10*np.pi/180)*np.cos(data_motion_omega*(t-wait_time))-(10*np.pi/180)
                # gt_posture[7]=0+(25*np.pi/180)*np.cos(data_motion_omega*(t-wait_time))-(25*np.pi/180)
                # gt_posture[8]=0-(10*np.pi/180)*np.cos(data_motion_omega*(t-wait_time))+(10*np.pi/180)
                # gt_posture[9]=0-(25*np.pi/180)*np.cos(data_motion_omega*(t-wait_time))+(25*np.pi/180)


                ########## trajectory (3) #########################
                gt_posture[0]=0-(5*np.pi/180)*np.cos(data_motion_omega*(t-wait_time))+(5*np.pi/180)
                gt_posture[1]=0+(5*np.pi/180)*np.cos(data_motion_omega*(t-wait_time))-(5*np.pi/180)
                gt_posture[2]=0-(5*np.pi/180)*np.cos(data_motion_omega*(t-wait_time))+(5*np.pi/180)
                gt_posture[3]=0+(15*np.pi/180)*np.cos(data_motion_omega*(t-wait_time))-(5*np.pi/180)
                gt_posture[4]=0-(20*np.pi/180)*np.cos(data_motion_omega*2*(t-wait_time))+(20*np.pi/180)
                gt_posture[5]=0+(10*np.pi/180)*np.cos(data_motion_omega*(t-wait_time))-(1*np.pi/180)
                gt_posture[6]=0+(15*np.pi/180)*np.cos(data_motion_omega*2*(t-wait_time))-(15*np.pi/180)
                gt_posture[7]=0+(10*np.pi/180)*np.cos(data_motion_omega*(t-wait_time))-(10*np.pi/180)
                gt_posture[8]=0-(2*np.pi/180)*np.cos(data_motion_omega*(t-wait_time))+(2*np.pi/180)
                gt_posture[9]=0-(5*np.pi/180)*np.cos(data_motion_omega*(t-wait_time))+(5*np.pi/180)


                ########## trajectory (4) #########################
                # gt_posture[0]=0#-(10*np.pi/180)*np.cos(data_motion_omega*(t-wait_time))+(10*np.pi/180)
                # gt_posture[1]=0#-(5*np.pi/180)*np.cos(data_motion_omega*(t-wait_time))+(5*np.pi/180)
                # gt_posture[2]=0#-(5*np.pi/180)*np.cos(data_motion_omega*(t-wait_time))+(10*np.pi/180)
                # gt_posture[3]=0+(10*np.pi/180)*np.cos(data_motion_omega*.5*(t-wait_time))-(10*np.pi/180)
                # gt_posture[4]=0-(10*np.pi/180)*np.cos(data_motion_omega*.5*(t-wait_time))+(10*np.pi/180)
                # gt_posture[5]=0#+(20*np.pi/180)*np.cos(data_motion_omega*2*(t-wait_time))-(20*np.pi/180)
                # gt_posture[6]=0+(20*np.pi/180)*np.cos(data_motion_omega*1*(t-wait_time))-(20*np.pi/180)
                # gt_posture[7]=0+(3*np.pi/180)*np.cos(data_motion_omega*(t-wait_time))-(3*np.pi/180)
                # gt_posture[8]=0-(3*np.pi/180)*np.cos(data_motion_omega*(t-wait_time))+(3*np.pi/180)
                # gt_posture[9]=0-(10*np.pi/180)*np.cos(data_motion_omega*(t-wait_time))+(10*np.pi/180)

                gt_posture_vel=[(gt_posture[k]-pre_gt_posture[k])/(dtime) for k in range(10)]

                stylus1_pose_and_vel=fkp(human,gt_posture,gt_posture_vel)

                stylus1_pose_posestamped, stylus1_pose_state = ee_pose_generator(stylus1_pose_and_vel[0])

                pre_gt_posture=copy.copy(gt_posture)
            stylus1_pose_posestamped, stylus1_pose_state = ee_pose_generator(stylus1_pose_and_vel[0])
            stylus1_path.poses.append(stylus1_pose_posestamped)
            # rospy.loginfo(stylus1_path)
            if len(stylus1_path.poses) > 10:  #only take the last 10 poses of stylus
                    stylus1_path.poses.pop(0)

            observation_data.stylus1_pose=stylus1_pose_state
            observation_data.stylus1_vel.x=stylus1_pose_and_vel[1][0]
            observation_data.stylus1_vel.y=stylus1_pose_and_vel[1][1]
            observation_data.stylus1_vel.z=stylus1_pose_and_vel[1][2]
            observation_data.stylus1_vel.theta=stylus1_pose_and_vel[1][3]
            observation_data.stylus1_vel.phi=stylus1_pose_and_vel[1][4]
            observation_data.stylus1_vel.psi=stylus1_pose_and_vel[1][5]
            # rospy.loginfo(ground_truth_df)
            observation_data.stylus1_pose_gt=gt_posture
            observation_data.stylus1_vel_gt=gt_posture_vel
            observ_pub.publish(observation_data)
            stylus1_path_pub.publish(stylus1_path)


            t+=1

            rate.sleep()

    except rospy.ROSInterruptException:
        pass
