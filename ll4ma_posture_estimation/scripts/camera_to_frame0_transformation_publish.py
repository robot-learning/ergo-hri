#!/usr/bin/env python
import os
import rospy
import tf
import tf2_ros
import yaml
import rospkg
from geometry_msgs.msg import TransformStamped
import numpy as np
import copy
import csv
from tf.transformations import quaternion_matrix, quaternion_from_matrix




def read_aruco_transformation():

    with open('/home/amir/catkin_ws/src/ergo-hri/ll4ma_posture_estimation/config/aruco_S01.yaml') as f:
        aruco = list(yaml.safe_load_all(f))
    webcam_T_aruco=quaternion_matrix([aruco[0]["webcam_orientation_x"], aruco[0]["webcam_orientation_y"], aruco[0]["webcam_orientation_z"], aruco[0]["webcam_orientation_w"] ])
    webcam_T_aruco[0,3]=aruco[0]["webcam_position_x"]
    webcam_T_aruco[1,3]=aruco[0]["webcam_position_y"]
    webcam_T_aruco[2,3]=aruco[0]["webcam_position_z"]

    kinect_T_aruco=quaternion_matrix([aruco[1]["kinect_orientation_x"], aruco[1]["kinect_orientation_y"], aruco[1]["kinect_orientation_z"], aruco[1]["kinect_orientation_w"] ])
    kinect_T_aruco[0,3]=aruco[1]["kinect_position_x"]
    kinect_T_aruco[1,3]=aruco[1]["kinect_position_y"]
    kinect_T_aruco[2,3]=aruco[1]["kinect_position_z"]

    return webcam_T_aruco, kinect_T_aruco

def read_mocap_T_frame0():
    seat_x = float(rospy.get_param("seat_x"))
    seat_y = float(rospy.get_param("seat_y"))
    seat_z = float(rospy.get_param("seat_z"))
    if rospy.get_param("seat")=="high":
        seat_height=0.609
    else:
        seat_height=0.445
    # with open('/home/amir/catkin_ws/src/ergo-hri/ll4ma_posture_estimation/config/study_config_S19.yaml') as f:
    #     stool = yaml.safe_load(f)
    mocap_T_frame0=np.array([[-1, 0, 0, seat_x+0.28],
    [0, 0, 1, seat_y-seat_height-0.24],
    [0, 1, 0, seat_z+0.38],
    [0, 0, 0, 1]])     #Subj 1

    # mocap_T_frame0=np.array([[-1, 0, 0, seat_x+0.0],
    # [0, 0, 1, seat_y-seat_height-0.00],
    # [0, 1, 0, seat_z+0.05],
    # [0, 0, 0, 1]])     #Subj 4
    return mocap_T_frame0


def calculate_transformation():
    webcam_T_topic, kinect_T_aruco= read_aruco_transformation()
    aruco_T_mocap=np.array([[0, 0, 1, 0],
                            [1, 0, 0, 0],
                            [0, 1, 0, 0],
                            [0, 0, 0, 1]])
    topic_T_aruco=np.array([[-1, 0, 0, 0],
                            [0, 0, 1, 0],
                            [0, 1, 0, 0],
                            [0, 0, 0, 1]])
    camera_T_frame0_1=np.dot(topic_T_aruco, aruco_T_mocap)
    camera_T_frame0_2=np.dot(webcam_T_topic, camera_T_frame0_1)
    # kinect_T_frame0=kinect_T_aruco*aruco_T_mocap*mocap_T_frame0
    mocap_T_frame0=read_mocap_T_frame0()
    camera_T_frame0=np.dot(camera_T_frame0_2, mocap_T_frame0)
    # camera_T_frame0=np.linalg.inv(np.dot(camera_T_frame0_1, mocap_T_frame0))
    camera_T_frame0=camera_T_frame0.tolist()
    quat_camera_T_frame0=quaternion_from_matrix(camera_T_frame0)
    quat_camera_T_frame0=quat_camera_T_frame0.tolist()
    return quat_camera_T_frame0, camera_T_frame0





if __name__ == '__main__':

    # rospy.loginfo("Helooooooooooooooooo")

    rospy.init_node("aruco_calibration_tf_publisher")

    parent_link = "usb_cam"
    child_link  = "frame0"

    broadcaster = tf2_ros.StaticTransformBroadcaster()
    #
    quat_camera_T_frame0, camera_T_frame0 = calculate_transformation()
    #
    #
    name = "{}__to__{}".format(parent_link, child_link)
    base_tf_stmp = TransformStamped()
    base_tf_stmp.header.stamp            = rospy.Time.now()
    base_tf_stmp.header.frame_id         = parent_link
    base_tf_stmp.child_frame_id          = child_link
    base_tf_stmp.transform.translation.x = camera_T_frame0[0][3]
    base_tf_stmp.transform.translation.y = camera_T_frame0[1][3]
    base_tf_stmp.transform.translation.z = camera_T_frame0[2][3]
    base_tf_stmp.transform.rotation.x    = quat_camera_T_frame0[0]
    base_tf_stmp.transform.rotation.y    = quat_camera_T_frame0[1]
    base_tf_stmp.transform.rotation.z    = quat_camera_T_frame0[2]
    base_tf_stmp.transform.rotation.w    = quat_camera_T_frame0[3]
    #
    # rospy.loginfo(camera_T_frame0)

    # filename = "/home/amir/catkin_ws/src/ergo-hri/ll4ma_posture_estimation/config/camera_T_frame0_S01.yaml"
    # with open(filename, 'r') as f:
    #     poses = yaml.load(f)
    #
    # name = "{}__to__{}".format(parent_link, child_link)
    # base_tf_stmp = TransformStamped()
    # base_tf_stmp.header.stamp            = rospy.Time.now()
    # base_tf_stmp.header.frame_id         = parent_link
    # base_tf_stmp.child_frame_id          = child_link
    # base_tf_stmp.transform.translation.x = poses["position_x"]
    # base_tf_stmp.transform.translation.y = poses["position_y"]
    # base_tf_stmp.transform.translation.z = poses["position_z"]
    # base_tf_stmp.transform.rotation.x    = poses["orientation_x"]
    # base_tf_stmp.transform.rotation.y    = poses["orientation_y"]
    # base_tf_stmp.transform.rotation.z    = poses["orientation_z"]
    # base_tf_stmp.transform.rotation.w    = poses["orientation_w"]
    # rospy.loginfo(poses["position_x"])
    rospy.loginfo("Sending base transform...")
    broadcaster.sendTransform(base_tf_stmp)
    rospy.spin()
