#!/usr/bin/env python
# license removed for brevity
import rosbag
import numpy as np
import matplotlib.pyplot as plt
from pylab import plot, show, savefig, xlim, figure, \
                hold, ylim, legend, boxplot, setp, axes
from time import time
from rula import Task, Rula
import csv


def setBoxColors(bp, edge_color, fill_color, mid_color):
		for element in ['boxes', 'whiskers', 'fliers', 'caps']:
			plt.setp(bp[element], color=edge_color)
		plt.setp(bp['medians'], color=mid_color)
		for patch in bp['boxes']:
			patch.set(facecolor=fill_color)


def plot_rula(rula_scores):


    plt.rcParams.update({'font.size':26})
    plt.rcParams['text.usetex'] = True
    plt.rcParams['text.latex.preamble'] = [r'\usepackage{amsmath}']

    T = []

    #
    # plt.rcParams.update({'font.size':26})
    # plt.rcParams['text.usetex'] = True
    # plt.rcParams['text.latex.preamble'] = [r'\usepackage{amsmath}']



    num_tasks = [4, 4, 4, 4, 4, 4, 4, 4]  #number of tasks for rach subject



#***********************************plot of rula scores for all tasks per mode per subject********************************************
    i = 0
    # m = 400 #number of data points
    # for j in range(m):
    #     T.append(j*0.1)
    # for sub in [4]:
    #     for mode in [0]:
    #         i += 1
    #         plt.figure(i, figsize=(10,10))
    #         ax1 = plt.subplot(4,1,1)
    #         # print len(T[0:m])
    #         # print len(rula_scores[sub][mode][0][0])
    #         ax1.plot(T[0:m],rula_scores[sub][mode][0][0][0:400], 'r', linewidth=2 , label="$\mathrm{Our~Approach}$")
    #         ax1.plot(T[0:m],rula_scores[sub][mode][0][1][0:400], 'b--', linewidth=2 , label="$\mathrm{Motion~Capture}$")
    #         ax1.set_ylim([1, 7])
    #         ax1.tick_params(axis='both', which='major', labelsize=20)
    #         ax1.set_ylabel(r'$\mathrm{Task~1}$', fontsize=22)
    #         ax1.yaxis.set_ticks(np.arange(1, 8, 1))
    #         # ax1.set_xlabel(r'$\mathrm{Time~(s)}', fontsize=22)
    #
    #         ax2 = plt.subplot(4,1,2)
    #         ax2.plot(T[0:m],rula_scores[sub][mode][1][0][0:400], 'r', linewidth=2)
    #         ax2.plot(T[0:m],rula_scores[sub][mode][1][1][0:400], 'b--', linewidth=2)
    #         ax2.set_ylim([1, 7])
    #         ax2.set_ylabel(r'$\mathrm{Task~2}$', fontsize=22)
    #         ax2.tick_params(axis='both', which='major', labelsize=20)
    #         ax2.yaxis.set_ticks(np.arange(1, 8, 1))
    #         # ax2.set_xlabel(r'$\mathrm{Time~(s)}', fontsize=22)
    #
    #         ax3 = plt.subplot(4,1,3)
    #         ax3.plot(T[0:m],rula_scores[sub][mode][2][0][0:400], 'r', linewidth=2)
    #         ax3.plot(T[0:m],rula_scores[sub][mode][2][1][0:400], 'b--', linewidth=2)
    #         ax3.set_ylim([1, 7])
    #         ax3.tick_params(axis='both', which='major', labelsize=20)
    #         ax3.yaxis.set_ticks(np.arange(1, 8, 1))
    #         ax3.set_ylabel(r'$\mathrm{Task~3}$', fontsize=22)
    #         # ax2.set_xlabel(r'$\mathrm{Time~(s)}', fontsize=22)
    #
    #         ax4 = plt.subplot(4,1,4)
    #         ax4.plot(T[0:m],rula_scores[sub][mode][3][0][0:400], 'r', linewidth=2)
    #         ax4.plot(T[0:m],rula_scores[sub][mode][3][1][0:400], 'b--', linewidth=2)
    #         ax4.set_ylim([1, 7])
    #         ax4.tick_params(axis='both', which='major', labelsize=20)
    #         ax4.set_ylabel(r'$\mathrm{Task~4}$', fontsize=22)
    #         ax4.set_xlabel(r'$\mathrm{Time~(s)}$', fontsize=22)
    #         ax4.yaxis.set_ticks(np.arange(1, 8, 1))
    #
    #         ax1.legend(loc='upper center',ncol=2,bbox_to_anchor=(0.5, 1), prop={'size': 20})
    #         plt.text(-4,15, r'$\mathrm{RULA~Score}$', ha="center", va="center", rotation=90, fontsize=24)
    #
    #         plt.savefig("/home/amir/iros_result_files/rula-score-time.pdf", dpi =300, bbox_inches='tight')



#***********************************plot of rula scores for one tasks per 1 mode per one********************************************
    # i = 0
    # m = 400 #number of data points
    # for j in range(m):
    #     T.append(j*0.1)
    # for sub in [4]:
    #     for mode in [0]:
    #         i += 1
    #         plt.figure(i, figsize=(10,2))
    #
    #         ax3 = plt.subplot(1,1,1)
    #         ax3.plot(T[0:m],rula_scores[sub][mode][2][0][0:400], 'tomato', linewidth=3, label="$\mathrm{Our~Approach}$")
    #         ax3.plot(T[0:m],rula_scores[sub][mode][2][1][0:400], 'b--', linewidth=3 , label="$\mathrm{Motion~Capture}$")
    #         ax3.set_ylim([1, 7])
    #         ax3.tick_params(axis='both', which='major', labelsize=20)
    #         ax3.yaxis.set_ticks(np.arange(1, 8, 1))
    #         ax3.set_ylabel(r'$\mathrm{RULA~Score}$', fontsize=22)
    #         ax3.set_xlabel(r'$\mathrm{Time~(s)}$', fontsize=22)
    #
    #         ax3.legend(loc='upper center',ncol=2,bbox_to_anchor=(0.5, 1), prop={'size': 20})
    #         # plt.text(-4,15, r'$\mathrm{RULA~Score}$', ha="center", va="center", rotation=90, fontsize=24)
    #
    #         plt.savefig("/home/amir/iros_result_files/rula-score-time-1task.pdf", dpi =300, bbox_inches='tight')

#***********************************plot of max of rula scores for all tasks per mode per subject********************************************
    # i += 1
    # plt.figure(i, figsize=(10,10))
    # plt.rcParams["legend.edgecolor"] = '0.8'
    # plt.rcParams["axes.edgecolor"] = 'black'
    # plt.rcParams["legend.scatterpoints"] = '1'
    #
    #
    # ax1 = plt.subplot(4,1,1)
    # r1=ax1.axhspan(1, 2.5, alpha=0.2, color='limegreen')
    # # r1.set_edgecolor('red')
    # r2=ax1.axhspan(2.5, 4.5, alpha=0.6, color='salmon')
    # # r2.set_edgecolor('red')
    # r3=ax1.axhspan(4.5, 6.5, alpha=0.5, color='red')
    # # r3.set_edgecolor('red')
    # r4=ax1.axhspan(6.5, 7.5, alpha=0.6, color='darkred')
    # # r4.set_edgecolor('red')
    # ax1.scatter([sub+1 for sub in range(8)], [max(rula_scores[sub][0][0][0]) for sub in range(8)], c='g', marker='^', s=200, alpha=1, edgecolors='none', label=r'$\mathrm{Our~Approach}$' )
    # ax1.scatter([sub+1 for sub in range(8)], [max(rula_scores[sub][0][0][1]) for sub in range(8)], c='m', marker='o', s=150, alpha=1, edgecolors='none', label=r'$\mathrm{Motion~Capture}$')
    # ax1.set_ylim([1, 7.5])
    # # ax1.set_xlim([1, 8])
    # ax1.tick_params(axis='both', which='major', labelsize=20)
    # ax1.set_ylabel(r'$\mathrm{Task~1}$', fontsize=22)
    # ax1.yaxis.set_ticks(np.arange(1, 8, 1))
    # ax1.margins(x=0.05)
    #
    #
    #
    #
    # # ax1.grid(True)
    # #
    # ax2 = plt.subplot(4,1,2)
    # r1=ax2.axhspan(1, 2.5, alpha=0.2, color='limegreen')
    # # r1.set_edgecolor('red')
    # r2=ax2.axhspan(2.5, 4.5, alpha=0.6, color='salmon')
    # # r2.set_edgecolor('red')
    # r3=ax2.axhspan(4.5, 6.5, alpha=0.5, color='red')
    # # r3.set_edgecolor('red')
    # r4=ax2.axhspan(6.5, 7.5, alpha=0.6, color='darkred')
    # r0=ax2.scatter([sub+1 for sub in range(8)], [max(rula_scores[sub][0][1][0]) for sub in range(8)], c='g', marker='^', s=200, edgecolors='none')
    # r00=ax2.scatter([sub+1 for sub in range(8)], [max(rula_scores[sub][0][1][1]) for sub in range(8)], c='m', marker='o', s=150, edgecolors='none')
    # ax2.set_ylim([1, 7.5])
    # # ax1.set_xlim([1, 8])
    # ax2.tick_params(axis='both', which='major', labelsize=20)
    # ax2.set_ylabel(r'$\mathrm{Task~2}$', fontsize=22)
    # ax2.yaxis.set_ticks(np.arange(1, 8, 1))
    # ax2.margins(x=0.05)
    # # ax1.grid(True)
    #
    # ax3 = plt.subplot(4,1,3)
    # r1=ax3.axhspan(1, 2.5, alpha=0.2, color='limegreen')
    # # r1.set_edgecolor('red')
    # r2=ax3.axhspan(2.5, 4.5, alpha=0.6, color='salmon')
    # # r2.set_edgecolor('red')
    # r3=ax3.axhspan(4.5, 6.5, alpha=0.5, color='red')
    # # r3.set_edgecolor('red')
    # r4=ax3.axhspan(6.5, 7.5, alpha=0.6, color='darkred')
    # ax3.scatter([sub+1 for sub in range(8)], [max(rula_scores[sub][0][2][0]) for sub in range(8)], c='g', marker='^', s=200, edgecolors='none')
    # ax3.scatter([sub+1 for sub in range(8)], [max(rula_scores[sub][0][2][1]) for sub in range(8)], c='m', marker='o', s=150, edgecolors='none')
    # ax3.set_ylim([1, 7.5])
    # # ax1.set_xlim([1, 8])
    # ax3.tick_params(axis='both', which='major', labelsize=20)
    # ax3.set_ylabel(r'$\mathrm{Task~3}$', fontsize=22)
    # ax3.yaxis.set_ticks(np.arange(1, 8, 1))
    # ax3.margins(x=0.05)
    # # ax1.grid(True)
    #
    #
    # ax4 = plt.subplot(4,1,4)
    # r1=ax4.axhspan(1, 2.5, alpha=0.2, color='limegreen')
    # # r1.set_edgecolor('red')
    # r2=ax4.axhspan(2.5, 4.5, alpha=0.6, color='salmon')
    # # r2.set_edgecolor('red')
    # r3=ax4.axhspan(4.5, 6.5, alpha=0.5, color='red')
    # # r3.set_edgecolor('red')
    # r4=ax4.axhspan(6.5, 7.5, alpha=0.6, color='darkred')
    # lable_flag=False
    # for sub in range(8):
    #         ax4.scatter(sub+1, max(rula_scores[sub][0][3][0]), c='g', marker='^', s=200, edgecolors='none')
    #         ax4.scatter(sub+1, max(rula_scores[sub][0][3][1]), c='m', marker='o', s=150, edgecolors='none')
    # ax4.set_ylim([1, 7.5])
    # # ax1.set_xlim([1, 8])
    # ax4.tick_params(axis='both', which='major', labelsize=20)
    # ax4.set_ylabel(r'$\mathrm{Task~4}$', fontsize=22)
    # ax4.yaxis.set_ticks(np.arange(1, 8, 1))
    # ax4.margins(x=0.05)
    # ax4.set_xlabel(r'$\mathrm{Subject~Number}$', fontsize=22)
    #
    # plt.text(-0.3, 16, r'$\mathrm{Max~of~RULA~Score}$', ha="center", va="center", rotation=90, fontsize=24)
    # ax1.legend(ncol=1, bbox_to_anchor=(0.3,2), fontsize=18, frameon=False)
    # plt.legend([r4, r3, r2, r1], [r'$\mathrm{Investigate,~and~implement~change}$',
    #  r'$\mathrm{Further~investigations,~change~soon}$', r'$\mathrm{Further~investigations,~change~may~be~needed}$',
    #  r'$\mathrm{Acceptable~posture}$' ], ncol=1, bbox_to_anchor=(1.05, 5.6), fontsize=18, frameon=False)
    #
    # plt.savefig("/home/amir/working_dataset/final_posture_estimation_results/rula-max_score-subj.pdf", dpi =300, bbox_inches='tight')




#*********************************** box plot of rula scores accuracy for all tasks********************************************

    blue_circle = dict(markerfacecolor='deepskyblue', marker='o')
    peru_circle = dict(markerfacecolor='olivedrab', marker='o')
    red_circle = dict(markerfacecolor='red', marker='o')
    N = 5
    ind = np.arange(N)
    fig,ax = plt.subplots(1, figsize=(12,6))
    width = 0.2

    bp0 = [[],[],[],[]]
    accuracy_tasks = []


    accuracy_tasks = [[],[],[],[]]
    for sub in range(8):
        for task in range(num_tasks[sub]):
            num_true = 0
            # print rula_scores[sub][0][task][0][0]
            for i in range(len(rula_scores[sub][0][task][0])):
                if rula_scores[sub][0][task][0][i] == rula_scores[sub][0][task][1][i]:
                    num_true += 1
            accuracy = 100*float(num_true)/len(rula_scores[sub][0][task][0])
            accuracy_tasks[task].append(accuracy)

    # print(accuracy_tasks)
    for i in range(4):
        bp0[i] = ax.boxplot(accuracy_tasks[i], positions = [i+1], widths = width, patch_artist=True, flierprops=blue_circle, showfliers=False)
        setBoxColors(bp0[i], 'black', '#00AFBB', 'black')



    ax.set_ylim((50, 100))
    ax.set_xlim((0.5,4.5))

    ax.set_xticklabels([r'$\mathrm{Task~1}$', r'$\mathrm{Task~2}$', r'$\mathrm{Task~3}$', r'$\mathrm{Task~4}$'], fontsize=26)
    # ax.set_yticklabels([r'$\mathrm{0}$', r'$\mathrm{20}$', r'$\mathrm{40}$', r'$\mathrm{60}$', r'$\mathrm{80}$', r'$\mathrm{100}$'], fontsize=26)

    ax.set_xticks([1, 2, 3, 4])
    ax.set_yticks([50, 60, 70, 80, 90, 100])

    ax.set_ylabel(r'$\mathrm{RULA~Score~Accuracy~(\%)}$', fontsize=28)


    plt.savefig("/home/amir/working_dataset/final_posture_estimation_results/rula-score-accuracy.pdf", dpi =300)



    ax.yaxis.grid(True)

    plt.show()





if __name__ == '__main__':
    error_mocap = []
    estimates = []
    mocap = []

# *******************************Real******************************************************************************************************
    num_tasks = 4
    num_subs = 8 #num of subjects
    num_tasks = [4, 4, 4, 4, 4, 4, 4, 4]

    T = 1  # to start from task T
    S = 1  # start from subject S
    # for mode in ['cpa', 'full_measure','height_measure']:
    for mode in ['cpa']:
        for j in range(S, num_subs+1):
            for i in range(T, num_tasks[j-1]+1):
                globals()["estimates_subject{0}_task{1}_{2}".format(j, i, mode)] = [[],[],[],[],[],[],[],[],[],[]]
                globals()["mocap_subject{0}_task{1}_{2}".format(j, i, mode)] = [[],[],[],[],[],[],[],[],[],[]]
                globals()["error_mocap_subject{0}_task{1}_{2}".format(j, i, mode)] = [[],[],[],[],[],[],[],[],[],[]]
                print j,i,mode
                globals()["bag_subject{0}_task{1}_{2}".format(j, i, mode)] = rosbag.Bag("/home/amir/working_dataset/final_posture_estimation_results/subject{0}_task{1}_{2}_PF+LS_modified_torso_trajsample10.bag".format(j, i, mode))

                for topic, msg, t in globals()["bag_subject{0}_task{1}_{2}".format(j, i, mode)].read_messages(topics=['PF_solution']):
                    for k in range(10):  # number of DOF
                        globals()["estimates_subject{0}_task{1}_{2}".format(j, i, mode)][k].append(msg.theta[k])    # adding the angles of each joint into a list

                for topic, msg, t in globals()["bag_subject{0}_task{1}_{2}".format(j, i, mode)].read_messages(topics=['sensory_data']):
                    for k in range(10):
                        globals()["mocap_subject{0}_task{1}_{2}".format(j, i, mode)][k].append(msg.mocap.reduced_posture[k])

                for k in range(10):
                    len_estimate = len(globals()["estimates_subject{0}_task{1}_{2}".format(j, i, mode)][0])
                    globals()["error_mocap_subject{0}_task{1}_{2}".format(j, i, mode)][k] = [abs(globals()["estimates_subject{0}_task{1}_{2}".format(j, i, mode)][k][l]-globals()["mocap_subject{0}_task{1}_{2}".format(j, i, mode)][k][l]) for l in range(len_estimate)]

                globals()["bag_subject{0}_task{1}_{2}".format(j, i, mode)].close()

        #---------------------- convert data into a nested list: mode>Subj>Task ----#
        estimates.append([[globals()["estimates_subject{0}_task{1}_{2}".format(j, i, mode)] for i in range(T,num_tasks[j-1]+1)] for j in range(S,num_subs+1)])
        mocap.append([[globals()["mocap_subject{0}_task{1}_{2}".format(j, i, mode)] for i in range(T,num_tasks[j-1]+1)] for j in range(S,num_subs+1)])
        error_mocap.append([[globals()["error_mocap_subject{0}_task{1}_{2}".format(j, i, mode)] for i in range(T,num_tasks[j-1]+1)] for j in range(S,num_subs+1)])


    estimates_without_outliers_for_all = []
    mocap_without_outliers_for_all = []

    errors = [[[],[],[],[],[],[],[],[],[],[]],[[],[],[],[],[],[],[],[],[],[]],[[],[],[],[],[],[],[],[],[],[]]]
    quantile_75 = [[[],[],[],[],[],[],[],[],[],[]],[[],[],[],[],[],[],[],[],[],[]],[[],[],[],[],[],[],[],[],[],[]]]
    quantile_25 = [[[],[],[],[],[],[],[],[],[],[]],[[],[],[],[],[],[],[],[],[],[]],[[],[],[],[],[],[],[],[],[],[]]]
    IQ = [[[],[],[],[],[],[],[],[],[],[]],[[],[],[],[],[],[],[],[],[],[]],[[],[],[],[],[],[],[],[],[],[]]]

    # for mode in range(3):
    for mode in range(1):
        for joint in range(10):
            for sub in range(4):
                for task in range(num_tasks[sub]):
                    for t in range(400):
                        errors[mode][joint].append(error_mocap[mode][sub][task][joint][t])
            err = errors[mode][joint]
            quantile_75[mode][joint] = np.quantile(err, .75)
            quantile_25[mode][joint] = np.quantile(err, .25)
            IQ[mode][joint] = quantile_75[mode][joint] - quantile_25[mode][joint]


    for sub in range(num_subs):
        estimates_without_outliers = [[[[],[],[],[],[],[],[],[],[],[]], [[],[],[],[],[],[],[],[],[],[]], [[],[],[],[],[],[],[],[],[],[]]], [[[],[],[],[],[],[],[],[],[],[]], [[],[],[],[],[],[],[],[],[],[]], [[],[],[],[],[],[],[],[],[],[]]], [[[],[],[],[],[],[],[],[],[],[]], [[],[],[],[],[],[],[],[],[],[]], [[],[],[],[],[],[],[],[],[],[]]], [[[],[],[],[],[],[],[],[],[],[]], [[],[],[],[],[],[],[],[],[],[]], [[],[],[],[],[],[],[],[],[],[]]]]
        mocap_without_outliers = [[[[],[],[],[],[],[],[],[],[],[]], [[],[],[],[],[],[],[],[],[],[]], [[],[],[],[],[],[],[],[],[],[]]], [[[],[],[],[],[],[],[],[],[],[]], [[],[],[],[],[],[],[],[],[],[]], [[],[],[],[],[],[],[],[],[],[]]], [[[],[],[],[],[],[],[],[],[],[]], [[],[],[],[],[],[],[],[],[],[]], [[],[],[],[],[],[],[],[],[],[]]], [[[],[],[],[],[],[],[],[],[],[]], [[],[],[],[],[],[],[],[],[],[]], [[],[],[],[],[],[],[],[],[],[]]]]
        # for mode in range(3):
        for mode in range(1):
            for task in range(num_tasks[sub]):
                for t in range(len(error_mocap[mode][sub][task][0])):
                    valid = True
                    for joint in range(10):
                        print mode,sub,task,joint,t
                        if (error_mocap[mode][sub][task][joint][t] < quantile_25[mode][joint] - 1.5 * IQ[mode][joint]) or (error_mocap[mode][sub][task][joint][t] > quantile_75[mode][joint] + 1.5 *IQ[mode][joint]):
                            valid = False
                    if valid:
                        for joint in range(10):
                            estimates_without_outliers[task][mode][joint].append(estimates[mode][sub][task][joint][t])
                            mocap_without_outliers[task][mode][joint].append(mocap[mode][sub][task][joint][t])
                # print(sub,task,mode)
                # print(len(estimates_without_outliers[task][mode][joint]))
        estimates_without_outliers_for_all.append(estimates_without_outliers)
        mocap_without_outliers_for_all.append(mocap_without_outliers)
    print("OK")
    # print(estimates_without_outliers_for_all)



    rula_scores = [[[], [], []], [[], [], []], [[], [], []], [[], [], []], [[], [], []], [[], [], []], [[], [], []], [[], [], []]]

    writer = csv.writer(open("/home/amir/working_dataset/final_posture_estimation_results/RULA_scores_subject_study", 'w'))
    # writer = csv.writer(open("/home/amir/working_dataset/RULA_scores_sim", 'w'))

    row = ["subject","task","max","min","mean","std","mc_max","mc_min","mc_mean","mc_std"]
    writer.writerow(row)
    task1=Task()

    for sub in range(num_subs):
        for mode in range(1):
            for task in range(num_tasks[sub]):
                RULA_ScoreSet=[]
                for t in range(len(estimates[mode][sub][task][0])):
                    posture = [estimates[mode][sub][task][i][t] for i in range(10)]
                    RULA_1= Rula(posture,task1)
                    RULA_ScoreSet.append(RULA_1.rula_score())
                    # if sub==4 and task==2 and t==170:
                    #     print("estimate:")
                    #     print(RULA_1.upper_arm_index)
                    #     print(RULA_1.lower_arm_index)
                    #     print(RULA_1.wrist_index)
                    #     print(RULA_1.wrist_twist_index)
                    #     print(RULA_1.joint_angles)

                max_RULA = max(RULA_ScoreSet)
                min_RULA = min(RULA_ScoreSet)
                mean_RULA = np.mean(RULA_ScoreSet)
                std_RULA = np.std(RULA_ScoreSet)

                RULA_ScoreSet_mocap = []
                for t in range(len(mocap[mode][sub][task][0])):
                    posture_mocap = [mocap[mode][sub][task][i][t] for i in range(10)]
                    RULA_1_mocap= Rula(posture_mocap,task1)
                    RULA_ScoreSet_mocap.append(RULA_1_mocap.rula_score())
                    # if sub==4 and task==2 and t==170:
                    #     print("mocap:")
                    #     print(RULA_1_mocap.upper_arm_index)
                    #     print(RULA_1_mocap.lower_arm_index)
                    #     print(RULA_1_mocap.wrist_index)
                    #     print(RULA_1_mocap.wrist_twist_index)
                    #     print(RULA_1_mocap.joint_angles)

                mc_max_RULA = max(RULA_ScoreSet_mocap)
                mc_min_RULA = min(RULA_ScoreSet_mocap)
                mc_mean_RULA = np.mean(RULA_ScoreSet_mocap)
                mc_std_RULA = np.std(RULA_ScoreSet_mocap)

                row = [sub,task,mode,max_RULA,min_RULA,mean_RULA,std_RULA,mc_max_RULA,mc_min_RULA,mc_mean_RULA,mc_std_RULA]

                writer.writerow(row)
                # print("estimate")
                # print(RULA_ScoreSet)
                # print("mocap")
                # print(RULA_ScoreSet_mocap)

                rula_scores[sub][mode].append([RULA_ScoreSet, RULA_ScoreSet_mocap])

    plot_rula(rula_scores)
