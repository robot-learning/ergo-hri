#!/usr/bin/env python
# license removed for brevity
import rospy
import numpy as np
import copy
import csv


def read_datapoint():
    with open('/home/amir/working_dataset/CPA/robot_cpa_S03_Link2_Trial1.csv', mode='r') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=' ')
        #next(csv_reader, None)
        data=[]
        for row in csv_reader:

            # data.append([float(row[3]), float(row[4]), float(row[5])])
            data.append([float(row[3]), float(row[4]), float(row[5])])
            # data.append([float(row[3]), 0, float(row[5])])


    return data

def cpa(data_points):
    N=len(data_points)
    c = [sum(data_points[i][0] for i in range(N))/N, sum(data_points[i][1] for i in range(N))/N, sum(data_points[i][2] for i in range(N))/N]
    #print c
    X=[[data_points[i][j] - c[j] for j in range(3)] for i in range(N)]
    H=sum(np.dot(X[i],X[i])*np.ese(3)+ np.matmul([[X[i][0]], [X[i][1]], [X[i][2]]],[[X[i][0], X[i][1], X[i][2]]]) for i in range(N))
    H=H.tolist()
    w,v=np.linalg.eig(H)
    w=w.tolist()
    #x_index=w.index(min(w))
    #z_index=w.index(max(w))
    x_index=0
    z_index=2

    R=[[0,0,0],[0,0,0],[0,0,0]]  #rotation matrix
    for k in range(3):
        if k==x_index:
            for i in range(3):
                R[i][0]=v[i][k]
        elif k==z_index:
            for i in range(3):
                R[i][2]=v[i][k]
        else:
            for i in range(3):
                R[i][1]=v[i][k]
    # rotating data points to frame j-1
    rotated_X=[np.dot(np.transpose(R), X[i]) for i in range(N)]
    #print rotated_data_points
    a1 =  2*sum(rotated_X[i][0]**2 for i in range(N))-float(2)/N*(sum(rotated_X[i][0] for i in range(N))**2)
    a2 =  2*sum(rotated_X[i][0]*rotated_X[i][1] for i in range(N))-float(2)/N*(sum(rotated_X[i][0] for i in range(N))*sum(rotated_X[i][1] for i in range(N)))
    b1 =  2*sum(rotated_X[i][0]*rotated_X[i][1] for i in range(N))-float(2)/N*(sum(rotated_X[i][0] for i in range(N))*sum(rotated_X[i][1] for i in range(N)))
    b2 =  2*sum(rotated_X[i][1]**2 for i in range(N))-float(2)/N*(sum(rotated_X[i][1] for i in range(N))**2)
    c1 =  (sum(rotated_X[i][0] for i in range(N))*sum(rotated_X[i][0]**2+rotated_X[i][1]**2 for i in range(N)))/N- sum(rotated_X[i][0]**3 for i in range(N))-sum(rotated_X[i][0]*(rotated_X[i][1]**2) for i in range(N))
    c2 =  (sum(rotated_X[i][1] for i in range(N))*sum(rotated_X[i][0]**2+rotated_X[i][1]**2 for i in range(N)))/N- sum(rotated_X[i][1]**3 for i in range(N))-sum((rotated_X[i][0]**2)*rotated_X[i][1] for i in range(N))
    A = [[a1, b1], [a2, b2]]
    B = [-c1, -c2]
    center = np.dot(np.linalg.inv(A),B)
    # print center
    final_center = np.dot(R,[center[0], center[1], 0])+c
    radius = np.sqrt(float(1)/N*(sum(rotated_X[i][0]**2 +rotated_X[i][1]**2 for i in range(N))-2*center[0]*sum(rotated_X[i][0] for i in range(N))-2*center[1]*sum(rotated_X[i][1] for i in range(N)))+center[0]**2+center[1]**2)
    return final_center, radius


data_points=read_datapoint()
center, radius = cpa(data_points[500:3500])
print ("center:")
print center
print ("radius:")
print radius
