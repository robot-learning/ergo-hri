#!/usr/bin/env python


import rospy
import message_filters
from time import time
import rosbag
from std_msgs.msg import Int32, String
from sensor_msgs.msg import CompressedImage, Image, CameraInfo




def callback(CameraInfo, image):


    bag.write('/usb_cam/camera_info', CameraInfo)
    bag.write('usb_cam/image_raw/compressed', image)


if __name__ == '__main__':
    global bag
    trial = rospy.get_param("trial")
    bag = rosbag.Bag("/home/amir/dataset_sep19/video/video_{}.bag".format(trial), 'w')

    try:
        # *** read feedbacks here ***

        rospy.init_node('dataCollection_webcam_video', anonymous=True)
        CameraInfo=message_filters.Subscriber("/usb_cam/camera_info", CameraInfo)
        image=message_filters.Subscriber("/usb_cam/image_raw/compressed", CompressedImage)
        
        ts=message_filters.TimeSynchronizer([CameraInfo, image], 10)
        ts.registerCallback(callback)
        
        rate = rospy.Rate(1)
        while not rospy.is_shutdown(): # and i <= 10000:
            rate.sleep()
	bag.close()
        rospy.spin()
    except rospy.ROSInterruptException:

        pass
