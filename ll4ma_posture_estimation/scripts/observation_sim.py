#!/usr/bin/env python
# license removed for brevity
import rospy
import csv
import numpy as np
import math
import rosbag
from ll4ma_posture_estimation.msg import observation, state
from geometry_msgs.msg import Pose, PoseWithCovarianceStamped, Point, Quaternion, Twist, PoseStamped, PointStamped
from nav_msgs.msg import Path
from visualization_msgs.msg import Marker
from tf.transformations import quaternion_from_euler, euler_from_quaternion, quaternion_matrix,  euler_from_matrix
import pandas as pd
from human_model_17_2_10dof import human_model
import copy

def Set_Arrow_Marker(point):
        P = Marker()
        P.header.frame_id = "frame0"
        P.header.stamp    = rospy.get_rostime()
        P.ns = "robot"
        P.id = 0
        P.type = 3 # arrow
        P.action = 0
        P.pose.position.x = point.x
        P.pose.position.y = point.y
        P.pose.position.z = point.z
        # rospy.loginfo(P.pose.position.z)
        quaternion = quaternion_from_euler(point.theta, point.phi ,point.psi)
        P.pose.orientation.x = quaternion[0]
        P.pose.orientation.y = quaternion[1]
        P.pose.orientation.z = quaternion[2]
        P.pose.orientation.w = quaternion[3]
        P.scale.x = 0.02
        P.scale.y = 0.02
        P.scale.z = 0.2

        P.color.r = 0
        P.color.g = 0
        P.color.b = 1
        P.color.a = 1.0

        P.lifetime =  rospy.Duration(0)
        return P

def ee_pose_generator(pose):
    location = state()
    x =  pose[0]
    y =  pose[1]
    z =  pose[2]
    location.x = x
    location.y = y
    location.z = z
    location.theta = pose[3]
    location.phi = pose[4]
    location.psi = pose[5]
    posestamped = PoseStamped()   #we generate two types of data for stylus pose
    point = Pose()
    point.position.x = x
    point.position.y = y
    point.position.z = z
    posestamped.pose = point
    stylus1_marker=Set_Arrow_Marker(location)

    stylus1_marker_pub.publish(stylus1_marker)

    return posestamped, location

def calculate_angle_from_mocap_data(name,segment_quat):
    _R0_=np.array([[-1,0,0,0],[0,-1,0,0],[0,0,1,0],[0,0,0,1]])  # transform coordinate of a bone in urdf system into coordiante of the bone in mocap system
    _R1_=np.array([[0,1,0,0],[-1,0,0,0],[0,0,1,0],[0,0,0,1]])  # transform coordinate of a bone in urdf system into coordiante of the bone in mocap system
    _R2_=np.array([[0,-1,0,0],[1,0,0,0],[0,0,1,0],[0,0,0,1]])  # transform actual(not zero angle) coordinate of a bone into zero-angle coordinate of its child bone (check onenote!)

    if name=="hip":
        # u0_R_m0=np.array([[0,-1,0,0],[1,0,0,0],[0,0,1,0],[0,0,0,1]])
        foo=np.dot(_R0_,quaternion_matrix(segment_quat))
        u0_R_u0=np.dot(foo,_R1_)
        return euler_from_matrix(u0_R_u0, 'ryxz')   #ryxz because we want it to be realet to the previous one
        # and 1st joing is around y, 2nd joint is around x and the 3rd joint is around z

    if name=="ab":
        foo=np.dot(_R2_,quaternion_matrix(segment_quat))
        u0_R_u0=np.dot(foo,_R1_)
        return euler_from_matrix(u0_R_u0, 'ryxz')   #ryxz because we want it to be realet to the previous one
        # and 1st joing is around y, 2nd joint is around x and the 3rd joint is around z

    if name=="chest":
        foo=np.dot(_R2_,quaternion_matrix(segment_quat))
        u0_R_u0=np.dot(foo,_R1_)
        return euler_from_matrix(u0_R_u0, 'ryxz')   #ryxz because we want it to be realet to the previous one
        # and 1st joing is around y, 2nd joint is around x and the 3rd joint is around z
    if  name=="shoulder":
        foo=np.dot(_R2_,quaternion_matrix(segment_quat))
        u0_R_u0=np.dot(foo,_R1_)
        return euler_from_matrix(u0_R_u0, 'ryxz')   #ryxz because we want it to be realet to the previous one
        # and 1st joing is around y, 2nd joint is around x and the 3rd joint is around z
    if  name=="uarm":
        foo=np.dot(_R2_,quaternion_matrix(segment_quat))
        u0_R_u0=np.dot(foo,_R1_)
        return euler_from_matrix(u0_R_u0, 'rxyz')   #ryxz because we want it to be realet to the previous one
        # and 1st joing is around x, 2nd joint is around y and the 3rd joint is around z
    if  name=="farm":
        foo=np.dot(_R2_,quaternion_matrix(segment_quat))
        u0_R_u0=np.dot(foo,_R1_)
        return euler_from_matrix(u0_R_u0, 'rzyx')   #ryxz because we want it to be realet to the previous one
        # and 1st joing is around z, 2nd joint is around y and the 3rd joint is around x

    if  name=="hand":
        foo=np.dot(_R2_,quaternion_matrix(segment_quat))
        u0_R_u0=np.dot(foo,_R1_)
        return euler_from_matrix(u0_R_u0, 'rzxy')   #ryxz because we want it to be realet to the previous one
        # and 1st joing is around z, 2nd joint is around x and the 3rd joint is around y

def fkp(human,posture):
    T = human.FK(posture)
    x = [T[0,3], T[1,3], T[2,3]]
    return x

if __name__ == '__main__':
    try:
        rospy.init_node('ee_pose_generator', anonymous=True)
        observ_pub = rospy.Publisher('observation', observation, queue_size=100)
        # stylus1_marker_pub = rospy.Publisher('stylus1_marker', Marker, queue_size=100)
        # stylus1_path_pub = rospy.Publisher("stylus1_path", Path, queue_size=1)
        trial = rospy.get_param("trial")
        # observed_data = pd.read_csv("/home/amir/working_dataset/robot/robot_posture_{}.csv".format(trial))
        # mocap_data = pd.read_csv("/home/amir/working_dataset/mocap/synch/mocap_posture_{}.csv".format(trial))


        stylus1_path = Path()
        stylus1_path.header.frame_id = 'frame0'
        global observ_freq

        observ_freq=rospy.get_param("observation_frequency")
        rate = rospy.Rate(observ_freq)

        # M=50
        while not rospy.is_shutdown():
            Prev_observation=np.zeros(12)
            Prev_mocap=np.zeros(10)
            Prev_mocap_rviz=np.zeros(18)
            mocap_vel=np.zeros(10)
            mocap_vel_rviz=np.zeros(18)
            Prev_Robot_data_frame=np.zeros(12)
            robot_time_stamp=[]
            for j in range(mocap_data['hip_x'].count()/10): #reducing frequency to 10 Hz fpr mocap
                i=j*10
                second = (abs(int(mocap_data.loc[i,'Timestamp'])) % 1000 +0)# find the second from truth data
                tenth_second = abs(int(mocap_data.loc[i,'Timestamp']*10)) % 10  #find the tenth of second from truth data
                hund_second = (abs(int(mocap_data.loc[i,'Timestamp']*100)) % 100) #find the hundred of second from truth data
                sec_flag= observed_data.loc[observed_data['second'].isin([second])]
                hund_sec_flag= sec_flag.loc[sec_flag['hund_second'].isin([hund_second])]
                # rospy.loginfo("sec_flag")
                # rospy.loginfo(sec_flag)
                # rospy.loginfo(hund_sec_flag)
                if len(sec_flag)!=0 and len(hund_sec_flag)!=0:
                    # rospy.loginfo(second)
                    # # # rospy.loginfo(i)
                    # rospy.loginfo(tenth_second)
                    # rospy.loginfo(hund_second)

                    eul_hip=calculate_angle_from_mocap_data("hip", [
                    mocap_data.loc[i,'hip_x'],
                    mocap_data.loc[i,'hip_y'],
                    mocap_data.loc[i,'hip_z'],
                    mocap_data.loc[i,'hip_w']])
                    # rospy.loginfo("eul_hip")
                    # rospy.loginfo(eul_hip)

                    mat_hip=quaternion_matrix([
                    mocap_data.loc[i,'hip_x'],
                    mocap_data.loc[i,'hip_y'],
                    mocap_data.loc[i,'hip_z'],
                    mocap_data.loc[i,'hip_w']])


                    eul_abb=calculate_angle_from_mocap_data("ab",[
                    mocap_data.loc[i,'abb_x'],
                    mocap_data.loc[i,'abb_y'],
                    mocap_data.loc[i,'abb_z'],
                    mocap_data.loc[i,'abb_w']])
                    # rospy.loginfo("eul_abb")
                    # rospy.loginfo(eul_abb)

                    mat_abb=quaternion_matrix([
                    mocap_data.loc[i,'abb_x'],
                    mocap_data.loc[i,'abb_y'],
                    mocap_data.loc[i,'abb_z'],
                    mocap_data.loc[i,'abb_w']])

                    eul_chest=calculate_angle_from_mocap_data("chest",[
                    mocap_data.loc[i,'chest_x'],
                    mocap_data.loc[i,'chest_y'],
                    mocap_data.loc[i,'chest_z'],
                    mocap_data.loc[i,'chest_w']])
                    # rospy.loginfo("eul_chest")
                    # rospy.loginfo(eul_chest)

                    mat_chest=quaternion_matrix([
                    mocap_data.loc[i,'chest_x'],
                    mocap_data.loc[i,'chest_y'],
                    mocap_data.loc[i,'chest_z'],
                    mocap_data.loc[i,'chest_w']])


                    # rospy.loginfo(eul_chest)
                    eul_Rshoulder=calculate_angle_from_mocap_data("shoulder",[
                    mocap_data.loc[i,'Rshoulder_x'],
                    mocap_data.loc[i,'Rshoulder_y'],
                    mocap_data.loc[i,'Rshoulder_z'],
                    mocap_data.loc[i,'Rshoulder_w']])
                    # rospy.loginfo("eul_Rshoulder")
                    # rospy.loginfo(eul_Rshoulder)

                    mat_Rshoulder=quaternion_matrix([
                    mocap_data.loc[i,'Rshoulder_x'],
                    mocap_data.loc[i,'Rshoulder_y'],
                    mocap_data.loc[i,'Rshoulder_z'],
                    mocap_data.loc[i,'Rshoulder_w']])

                    eul_RUarm=calculate_angle_from_mocap_data("uarm",[
                    mocap_data.loc[i,'RUarm_x'],
                    mocap_data.loc[i,'RUarm_y'],
                    mocap_data.loc[i,'RUarm_z'],
                    mocap_data.loc[i,'RUarm_w']])
                    # rospy.loginfo("eul_RUarm")
                    # rospy.loginfo(eul_RUarm)

                    mat_RUarm=quaternion_matrix([
                    mocap_data.loc[i,'RUarm_x'],
                    mocap_data.loc[i,'RUarm_y'],
                    mocap_data.loc[i,'RUarm_z'],
                    mocap_data.loc[i,'RUarm_w']])

                    eul_RFarm=calculate_angle_from_mocap_data("farm",[
                    mocap_data.loc[i,'RFarm_x'],
                    mocap_data.loc[i,'RFarm_y'],
                    mocap_data.loc[i,'RFarm_z'],
                    mocap_data.loc[i,'RFarm_w']])

                    mat_RFarm=quaternion_matrix([
                    mocap_data.loc[i,'RFarm_x'],
                    mocap_data.loc[i,'RFarm_y'],
                    mocap_data.loc[i,'RFarm_z'],
                    mocap_data.loc[i,'RFarm_w']])
                    # rospy.loginfo("eul_RFarm")
                    # rospy.loginfo(eul_RFarm)

                    eul_Rhand=calculate_angle_from_mocap_data("hand",[
                    mocap_data.loc[i,'Rhand_x'],
                    mocap_data.loc[i,'Rhand_y'],
                    mocap_data.loc[i,'Rhand_z'],
                    mocap_data.loc[i,'Rhand_w']])

                    mat_RFarm=quaternion_matrix([
                    mocap_data.loc[i,'Rhand_x'],
                    mocap_data.loc[i,'Rhand_y'],
                    mocap_data.loc[i,'Rhand_z'],
                    mocap_data.loc[i,'Rhand_w']])
                    # rospy.loginfo("eul_RFarm")
                    # rospy.loginfo(eul_RFarm)



                    _R0_=np.array([[-1,0,0,0],[0,-1,0,0],[0,0,1,0],[0,0,0,1]])  # transform coordinate of a bone in urdf system into coordiante of the bone in mocap system
                    _R1_=np.array([[0,1,0,0],[-1,0,0,0],[0,0,1,0],[0,0,0,1]])  # transform coordinate of a bone in urdf system into coordiante of the bone in mocap system

                    hip_R_shoulder= np.dot(np.dot(mat_chest, mat_abb),mat_hip)
                    foo=np.dot(_R0_,hip_R_shoulder)
                    u0_R_u0=np.dot(foo,_R1_)
                    eul_torso=euler_from_matrix(u0_R_u0, 'ryxz')
                    hand_transform = [-0.6, -0.2 , 0.5] #subj 1
                    # hand_transform = [-0.4, -0.2 , 0.9] #subj 2

                    # hand_transform = [-0.1, 0.1 , 0.50] #subj 4
                    # hand_transform = [-0.4, -0.0 , 0.5] #subj 5 task 1
                    # hand_transform = [-0.4, -0.0 , 0.7] #subj 5 task 2
                    # hand_transform = [-0.1, -0.2 , 0.7] #subj 9 task 1
                    # hand_transform = [-0.1, -0.0 , 0.7] #subj 9 task 2&3&4
                    # hand_transform = [-0.8, -0.4 , 0.7] #subj 7 task 2
                    # hand_transform = [-0.8, -0.3 , 0.7] #subj 7 task 3
                    # hand_transform = [-0.4, 0.2 , 0.9] #subj 15 task 1,2,3,4
                    # hand_transform = [-0.4, 0.1 , 0.5] #subj 11 task 1


                    mocap_df_rviz_base = [
                        eul_hip[0],
                        eul_hip[1],
                        eul_hip[2],
                        eul_abb[0],
                        eul_abb[1],
                        eul_abb[2],
                        eul_chest[0],
                        eul_chest[1],
                        eul_chest[2],
                        0,
                        eul_Rshoulder[2],
                        eul_RUarm[0],
                        eul_RUarm[1]+eul_RFarm[2],
                        eul_RUarm[2],
                        eul_RFarm[0],
                        eul_RFarm[1]+eul_Rhand[2],
                        eul_Rhand[0],
                        eul_Rhand[1]]

                    mocap_df_rviz = copy.deepcopy(mocap_df_rviz_base)
                    mocap_df_rviz[15] += hand_transform[0]
                    mocap_df_rviz[16] += hand_transform[1]
                    mocap_df_rviz[17] += hand_transform[2]

                    urdf_file="/home/amir/catkin_ws/src/ergo-hri/ll4ma_posture_estimation/urdf/rviz_gt_v2.urdf.xacro"
                    human_chest=human_model(urdf_file, 'ground_truth_waist', 'ground_truth_chest_shoulder_sphere1')   # generate a kdl model from waist to chest
                    human_shoulder=human_model(urdf_file, 'ground_truth_waist', 'ground_truth_right_shoulder_abduction')  # generate a kdl model from waist to shoulder

                    neck_point = fkp(human_chest,[eul_hip[0], eul_hip[1], eul_hip[2], eul_abb[0], eul_abb[1], eul_abb[2], eul_chest[0], eul_chest[1], eul_chest[2], 0])
                    shoulder_point = fkp(human_shoulder,[eul_hip[0], eul_hip[1], eul_hip[2], eul_abb[0], eul_abb[1], eul_abb[2], eul_chest[0], eul_chest[1], eul_chest[2], eul_Rshoulder[2], 0])

                    neck_point_projected_sagital = [neck_point[0], neck_point[2]]
                    neck_point_projected_frontal = [neck_point[1], neck_point[2]]
                    shoulder_point_projected_top = [shoulder_point[0]- neck_point[0], shoulder_point[1]-neck_point[1]]

                    theta_1_10DOF = np.arctan2(neck_point_projected_sagital[0], neck_point_projected_sagital[1])
                    theta_2_10DOF = np.arctan2(neck_point_projected_frontal[1], neck_point_projected_frontal[0])-np.pi/2
                    theta_3_10DOF = np.arctan2(shoulder_point_projected_top[1], shoulder_point_projected_top[0])+np.pi/2

                    mocap_df_base = [
                        theta_1_10DOF,
                        theta_2_10DOF,
                        theta_3_10DOF,
                        eul_RUarm[0],
                        eul_RUarm[1]+eul_RFarm[2],
                        eul_RUarm[2],
                        eul_RFarm[0],
                        eul_RFarm[1]+eul_Rhand[2],
                        eul_Rhand[0],
                        eul_Rhand[1]] #negative because of Sarvenaz task 4

                    mocap_df = copy.deepcopy(mocap_df_base)
                    mocap_df[7] += hand_transform[0]
                    mocap_df[8] += hand_transform[1]
                    mocap_df[9] += hand_transform[2]

                    for k in range(len(mocap_df)):
                        if mocap_df[k]>2*np.pi:
                            mocap_df[k] -= 2*np.pi
                        if mocap_df[k]<-2*np.pi:
                            mocap_df[k] += 2*np.pi
                    for k in range(len(mocap_df_rviz)):
                        if mocap_df_rviz[k]>2*np.pi:
                            mocap_df_rviz[k] -= 2*np.pi
                        if mocap_df_rviz[k]<-2*np.pi:
                            mocap_df_rviz[k] += 2*np.pi

                    # rospy.loginfo("mocap_df")
                    # rospy.loginfo(mocap_df)

                    data_frame_robot = observed_data.loc[observed_data['second'].isin([second])] #find the frames from observe_data that have same second as the selected data_frame in mocap_data
                    # data_frame_kinect = kinect_data.loc[kinect_data['second'].isin([second])] #find the frames from observe_data that have same second as the selected data_frame in mocap_data
                    # rospy.loginfo("data_frame")
                    # rospy.loginfo(data_frame_kinect)
                    new_data_frame_robot = data_frame_robot.loc[data_frame_robot['hund_second'].isin([hund_second])] #find the frame from observe_data that have same mili_second as the selected data_frame in mocap_data
                    # new_data_frame_kinect = data_frame_kinect.loc[data_frame_kinect['hund_second'].isin([tenth_second])] #find the frame from observe_data that have same mili_second as the selected data_frame in mocap_data
                    # rospy.loginfo(new_data_frame_robot)
                    # rospy.loginfo(new_data_frame_kinect)
                    #
                    # # rospy.loginfo("new_data_frame")
                    # rospy.loginfo(new_data_frame_robot)
                    np_df_tot_robot = new_data_frame_robot.as_matrix() #stylus data convert to matrix
                    np_df_robot=np_df_tot_robot[0]     #taking the first dataframe of the matching dataframes (it can be substituted with the average values)
                    # np_df_tot_kinect = new_data_frame_kinect.as_matrix() #stylus data convert to matrix
                    # np_df_kinect=np_df_tot_kinect[0]
                    # if not np_df_tot.any():
                    # rospy.loginfo("np_df_robot")
                    # rospy.loginfo(np_df_robot)
                    # rospy.loginfo(np_df_kinect)
                    # rospy.loginfo(observ_data[i*M])
                    # kinect_df = [np_df_kinect[4], np_df_kinect[5], np_df_kinect[6], np_df_kinect[7], np_df_kinect[8], np_df_kinect[9], np_df_kinect[10], np_df_kinect[11], np_df_kinect[12], np_df_kinect[13]]

                    # eul=[np_df_robot[8], np_df_robot[9], np_df_robot[10]]  #convert quaternion to euler for stylus orientation
                    # eul=euler_from_quaternion([0,0,0,1])  #convert quaternion to euler for stylus orientation
                    #
                    # stylus1_theta=eul[0]
                    # stylus1_phi=eul[1]
                    # stylus1_psi=eul[2]
                    # if (eul[2]+np.pi>=np.pi):   #limit phi angle to be in (-pi,+pi)
                    #     stylus1_psi= eul[2] - np.pi
                    # elif (eul[2]+np.pi<-np.pi):
                    #     stylus1_psi=eul[2]+3*np.pi
                    # else:
                    #     stylus1_psi = eul[2]+np.pi  # psi angles needs to be added by a pi to match with reality



                    # hip_joint_angles = euler_from_quaternion([mocap_data.loc[i,'hip_x'], mocap_data.loc[i,'hip_y'], mocap_data.loc[i,'hip_z'], mocap_data.loc[i,'hip_w']], axes='sxyz')
                    # rospy.loginfo(hip_joint_angles)
                    # PP=[rospy.get_param("frame0_T_robot0_x")+0.04, rospy.get_param("frame0_T_robot0_y"), rospy.get_param("frame0_T_robot0_z")+0.02] # To compensate the grasping point on stylus
                    PP=[rospy.get_param("frame0_T_robot0_x")+0.04, rospy.get_param("frame0_T_robot0_y")+0.02, rospy.get_param("frame0_T_robot0_z")+0.02] # To compensate the grasping point on stylus   #Subj 1&4 task1
                    # PP=[rospy.get_param("frame0_T_robot0_x")+0.05, rospy.get_param("frame0_T_robot0_y")-0.02, rospy.get_param("frame0_T_robot0_z")+0.02] # To compensate the grasping point on stylus   #Subj 2 task1&2
                    # PP=[rospy.get_param("frame0_T_robot0_x")+0.06, rospy.get_param("frame0_T_robot0_y")-0.02, rospy.get_param("frame0_T_robot0_z")+0.02] # To compensate the grasping point on stylus   #Subj 2 task3
                    # PP=[rospy.get_param("frame0_T_robot0_x")+0.07, rospy.get_param("frame0_T_robot0_y")+0.0, rospy.get_param("frame0_T_robot0_z")+0.05] # To compensate the grasping point on stylus   #Subj 5 task1
                    # PP=[rospy.get_param("frame0_T_robot0_x")+0.07, rospy.get_param("frame0_T_robot0_y")+0.02, rospy.get_param("frame0_T_robot0_z")+0.08] # To compensate the grasping point on stylus   #Subj 5 task2&3
                    # PP=[rospy.get_param("frame0_T_robot0_x")+0.05, rospy.get_param("frame0_T_robot0_y")+0.15, rospy.get_param("frame0_T_robot0_z")+0.02] # To compensate the grasping point on stylus   #Subj 9 task1
                    # PP=[rospy.get_param("frame0_T_robot0_x")+0.05, rospy.get_param("frame0_T_robot0_y")+0.10, rospy.get_param("frame0_T_robot0_z")+0.02] # To compensate the grasping point on stylus   #Subj 9 task2&3&4
                    # PP=[rospy.get_param("frame0_T_robot0_x")+0.01, rospy.get_param("frame0_T_robot0_y")+0.02, rospy.get_param("frame0_T_robot0_z")+0.03] # To compensate the grasping point on stylus   #Subj 7 task2
                    # PP=[rospy.get_param("frame0_T_robot0_x")+0.01, rospy.get_param("frame0_T_robot0_y")+0.02, rospy.get_param("frame0_T_robot0_z")+0.01] # To compensate the grasping point on stylus   #Subj 7 task3,4
                    # PP=[rospy.get_param("frame0_T_robot0_x")+0.01, rospy.get_param("frame0_T_robot0_y")+0.02, rospy.get_param("frame0_T_robot0_z")+0.04] # To compensate the grasping point on stylus   #Subj 15 task1
                    # PP=[rospy.get_param("frame0_T_robot0_x")+0.01, rospy.get_param("frame0_T_robot0_y")-0.02, rospy.get_param("frame0_T_robot0_z")+0.05] # To compensate the grasping point on stylus   #Subj 15 task2
                    # PP=[rospy.get_param("frame0_T_robot0_x")+0.02, rospy.get_param("frame0_T_robot0_y")-0.01, rospy.get_param("frame0_T_robot0_z")+0.05] # To compensate the grasping point on stylus   #Subj 15 task3
                    # PP=[rospy.get_param("frame0_T_robot0_x")+0.02, rospy.get_param("frame0_T_robot0_y")+0.1, rospy.get_param("frame0_T_robot0_z")+0.06] # To compensate the grasping point on stylus   #Subj 11 task1
                    # PP=[rospy.get_param("frame0_T_robot0_x")+0.03, rospy.get_param("frame0_T_robot0_y")+0.09, rospy.get_param("frame0_T_robot0_z")+0.05] # To compensate the grasping point on stylus   #Subj 11 task2
                    # PP=[rospy.get_param("frame0_T_robot0_x")+0.03, rospy.get_param("frame0_T_robot0_y")+0.09, rospy.get_param("frame0_T_robot0_z")+0.04] # To compensate the grasping point on stylus   #Subj 11 task3





                    # PP=[0.6379, -0.1513, 1.1119]
                    # rospy.loginfo(PP)


                    # stylus1_pose_array= [-np_df_robot[6]+0.786+0.08, np_df_robot[5]-0.147-0.05, np_df_robot[7]+1, stylus1_theta, stylus1_phi, stylus1_psi]  #converting to frame_0 from old study
                    stylus1_pose_array= [-np_df_robot[6]+PP[0], np_df_robot[5]+PP[1], np_df_robot[7]+PP[2], -np_df_robot[9], np_df_robot[8], np_df_robot[10]]  #converting to frame_0
                    stylus1_vel_array= [-np_df_robot[12], np_df_robot[11], np_df_robot[13]+PP[2], -np_df_robot[15], np_df_robot[14], np_df_robot[16]]  #converting to frame_0
                    # rospy.loginfo(np_df_robot[7])
                    # rospy.loginfo(PP[2])
                    # rospy.loginfo('stylus1_pose_array')
                    # rospy.loginfo(stylus1_pose_array)
                    # rospy.loginfo(Prev_Robot_data_frame[1])
                    robot_time_stamp= [np_df_robot[2],np_df_robot[4]]
                    if np_df_robot[1] == Prev_Robot_data_frame[1]:

                        dtime = (np_df_robot[3]-Prev_Robot_data_frame[3])/100 #in sec
                    else:
                        dtime = (100+np_df_robot[3]-Prev_Robot_data_frame[3])/100 # in sec
                    # rospy.loginfo("dtime:")
                    # rospy.loginfo(dtime)
                    if dtime != 0:
                        mocap_vel=[(mocap_df[k]-Prev_mocap[k])/(dtime) for k in range(10)]
                        mocap_vel_rviz=[(mocap_df_rviz[k]-Prev_mocap_rviz[k])/(dtime) for k in range(18)]
                        # stylus_vel_array = [(stylus1_pose_array[k]-Prev_observation[k])/(dtime) for k in range(6)]
                        # rospy.loginfo("stylus_vel_array:")
                        # rospy.loginfo(stylus_vel_array)
                        # rospy.loginfo("stylus_vel_array")
                        # rospy.loginfo(stylus_vel_array)
                        # posture= [mocap_data.loc[i,'hip_x'], mocap_data.loc[i,'hip_x'], mocap_data.loc[i,'hip_x'], mocap_data.loc[i,'hip_x'], mocap_data.loc[i,'hip_x'], mocap_data.loc[i,'hip_x']]
                        # rospy.loginfo(robot1_ee_initial)
                        # robot1_ee_initial= [0.446, -0.2275, 1.106, 0, 0, 0]
                        stylus1_pos_vel = stylus1_pose_array + stylus1_vel_array

                        stylus1_pose_posestamped, stylus1_pose_state = ee_pose_generator(stylus1_pose_array)
                        # rospy.loginfo("stylus1_pose_state")
                        # rospy.loginfo(stylus1_pose_state)


                        observation_data = observation()

                        stylus1_path.poses.append(stylus1_pose_posestamped)
                        if len(stylus1_path.poses) > 10:  #only take the last 10 poses of stylus
                                stylus1_path.poses.pop(0)

                        observation_data.stylus1_pose=stylus1_pose_state
                        observation_data.stylus1_vel.x=stylus1_vel_array[0]
                        observation_data.stylus1_vel.y=stylus1_vel_array[1]
                        observation_data.stylus1_vel.z=stylus1_vel_array[2]
                        observation_data.stylus1_vel.theta=stylus1_vel_array[3]
                        observation_data.stylus1_vel.phi=stylus1_vel_array[4]
                        observation_data.stylus1_vel.psi=stylus1_vel_array[5]
                        # rospy.loginfo(mocap_df)
                        observation_data.mocap_posture_10dof=mocap_df
                        observation_data.mocap_vel_10dof=mocap_vel
                        observation_data.mocap_posture_17dof=mocap_df_rviz
                        observation_data.mocap_vel_17dof=mocap_vel_rviz
                        observation_data.header.stamp.secs=robot_time_stamp[0]
                        observation_data.header.stamp.nsecs=robot_time_stamp[1]

                        # observation_data.kinect=kinect_df

                        observ_pub.publish(observation_data)
                        stylus1_path_pub.publish(stylus1_path)


                        Prev_Robot_data_frame=np.copy(np_df_robot)
                        Prev_observation=np.copy(stylus1_pose_array) #need to be corrected

                        Prev_mocap=np.copy(mocap_df)
                        Prev_mocap_rviz=np.copy(mocap_df_rviz)

                    rate.sleep()

    except rospy.ROSInterruptException:
        pass
