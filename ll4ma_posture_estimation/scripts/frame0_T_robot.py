#!/usr/bin/env python
# license removed for brevity
import rospy
import numpy as np


seat=[0.40410, -0.12877, 0.74013]
robot=[-0.43325, 0.53569, 0.38686]
seat_type="high"

# seat=[seat[i]/1000 for i in range(3)]
# robot=[robot[i]/1000 for i in range(3)]



if seat_type=="low":
    h_seat=[0, 0.445, 0]
else:
    h_seat=[0, 0.609, 0]
frame0_R_M=[[-1,0,0], [0,0,1], [0,1,0]]
frame0_r=[-0.0965, 0.12, -0.185]
M_d=[robot[i]-seat[i]+h_seat[i] for i in range(3)]

frame0_d=np.dot(frame0_R_M,M_d)
frame0_T_robot=frame0_d+frame0_r

print "M_d:"
print M_d

print "frame0_d:"
print frame0_d

print "frame0_T_robot:"
print frame0_T_robot
