#!/usr/bin/env python
# license removed for brevity
import rospy
import numpy as np
from sensor_msgs.msg import JointState
from ll4ma_posture_estimation.msg import results, sensory_data
import message_filters

def posture_generator(particle_num, data):
    pub = rospy.Publisher('/particle{}/joint_states'.format(particle_num), JointState, queue_size=100)
    pose=JointState()
    pose.header.stamp = rospy.Time.now()
    pose.header.frame_id = 'frame_0'
    pose.name =  [
    "particle{}_waist_flextion".format(particle_num),
    "particle{}_waist_lateral_bending".format(particle_num),
    "particle{}_waist_rotation".format(particle_num),
    "particle{}_right_shoulder_abduction".format(particle_num),
    "particle{}_right_shoulder_ver_flextion".format(particle_num),
    "particle{}_right_shoulder_hor_flextion".format(particle_num),
    "particle{}_right_elbow_flextion".format(particle_num),
    "particle{}_right_elbow_supination".format(particle_num),
    "particle{}_right_hand_rad_deviation".format(particle_num),
    "particle{}_right_hand_flextion".format(particle_num)]

    pose.position = len(pose.name) * [0.0]

    pose.position[0]=data.theta[0]
    pose.position[1]=data.theta[1]
    pose.position[2]=data.theta[2]
    pose.position[3]=data.theta[3]
    pose.position[4]=data.theta[4]
    pose.position[5]=data.theta[5]
    pose.position[6]=data.theta[6]
    pose.position[7]=data.theta[7]
    pose.position[8]=data.theta[8]
    pose.position[9]=data.theta[9]

    pub.publish(pose)

def posture_generator_mocap_reduced(data):
    pub = rospy.Publisher('/mocap_reduced/joint_states', JointState, queue_size=100)
    pose=JointState()
    pose.header.stamp = rospy.Time.now()
    pose.header.frame_id = 'frame_0'
    pose.name =  [
    "mocap_reduced_hip_flextion",
    "mocap_reduced_hip_lateral_bending",
    "mocap_reduced_hip_rotation",
    "mocap_reduced_right_shoulder_abduction",
    "mocap_reduced_right_shoulder_ver_flextion",
    "mocap_reduced_right_shoulder_hor_flextion",
    "mocap_reduced_right_elbow_flextion",
    "mocap_reduced_right_elbow_supination",
    "mocap_reduced_right_hand_rad_deviation",
    "mocap_reduced_right_hand_flextion"]

    pose.position = len(pose.name) * [0.0]

    pose.position[0]=data[0]
    pose.position[1]=data[1]
    pose.position[2]=data[2]
    pose.position[3]=data[3]
    pose.position[4]=data[4]
    pose.position[5]=data[5]
    pose.position[6]=data[6]
    pose.position[7]=data[7]
    pose.position[8]=data[8]
    pose.position[9]=data[9]

    pub.publish(pose)



def posture_generator_mocap_full(data):
    pub = rospy.Publisher('/mocap_full/joint_states', JointState, queue_size=100)
    pose=JointState()
    pose.header.stamp = rospy.Time.now()
    pose.header.frame_id = 'torso'
    pose.name =  [
    "mocap_full_hip_flextion",
    "mocap_full_hip_lateral_bending",
    "mocap_full_hip_rotation",
    "mocap_full_abb_flextion",
    "mocap_full_abb_lateral_bending",
    "mocap_full_abb_rotation",
    "mocap_full_chest_flextion",
    "mocap_full_chest_lateral_bending",
    "mocap_full_chest_rotation",
    "mocap_full_chest_shoulder_sphere",
    "mocap_full_chest_to_shoulder",
    "mocap_full_right_shoulder_abduction",
    "mocap_full_right_shoulder_ver_flextion",
    "mocap_full_right_shoulder_hor_flextion",
    "mocap_full_right_elbow_flextion",
    "mocap_full_right_elbow_supination",
    "mocap_full_right_hand_rad_deviation",
    "mocap_full_right_hand_flextion"]
    pose.position = len(pose.name) * [0.0]

    pose.position[0]=data[0]
    pose.position[1]=data[1]
    pose.position[2]=data[2]
    pose.position[3]=data[3]
    pose.position[4]=data[4]
    pose.position[5]=data[5]
    pose.position[6]=data[6]
    pose.position[7]=data[7]
    pose.position[8]=data[8]
    pose.position[9]=data[9]
    pose.position[10]=data[10]
    pose.position[11]=data[11]
    pose.position[12]=data[12]
    pose.position[13]=data[13]
    pose.position[14]=data[14]
    pose.position[15]=data[15]
    pose.position[16]=data[16]
    pose.position[17]=data[17]

    pub.publish(pose)

def posture_generator_LS(data):
    '''

    '''
    pub = rospy.Publisher('/ik_solution/joint_states', JointState, queue_size=100)
    pose = JointState()
    pose.header.stamp = rospy.Time.now()
    pose.header.frame_id = 'frame_0'
    pose.name =  [
    "ik_solution_waist_flextion",
    "ik_solution_waist_lateral_bending",
    "ik_solution_waist_rotation",
    "ik_solution_right_shoulder_abduction",
    "ik_solution_right_shoulder_ver_flextion",
    "ik_solution_right_shoulder_hor_flextion",
    "ik_solution_right_elbow_flextion",
    "ik_solution_right_elbow_supination",
    "ik_solution_right_hand_rad_deviation",
    "ik_solution_right_hand_flextion"]

    pose.position = len(pose.name) * [0.0]

    pose.position[0]=data[0]
    pose.position[1]=data[1]
    pose.position[2]=data[2]
    pose.position[3]=data[3]
    pose.position[4]=data[4]
    pose.position[5]=data[5]
    pose.position[6]=data[6]
    pose.position[7]=data[7]
    pose.position[8]=data[8]
    pose.position[9]=data[9]

    pub.publish(pose)


def callback(sensory_data, results):
    global spawn_only_most_probable, solver

    num_of_particles = 12
    rand = np.random.randint(0,12,num_of_particles)

    if solver == "PF" or solver == "PF+LS":
        if spawn_only_most_probable:
            posture_generator(1,results.most_probable_particle)
        else:
            for particle_counter in range(0, num_of_particles):
                # posture_generator(particle_counter+1,data.particles[particle_counter])
                posture_generator(particle_counter+1, results.particles[rand[particle_counter]])
    # setting motion capture data into particles 13 and 14
    posture_generator_mocap_full(sensory_data.mocap.full_posture)
    posture_generator_mocap_reduced(sensory_data.mocap.reduced_posture)

    if solver == "PF+LS" or solver == "LS":
            posture_generator_LS(results.ik_solution.theta)





if __name__ == '__main__':
    try:
        global solver
        solver = rospy.get_param("solver")
        if solver == "PF" or solver == "PF+LS":
            global spawn_only_most_probable
            spawn_only_most_probable = rospy.get_param("spawn_only_most_probable")

        rospy.init_node('rviz_vis', anonymous=True)

        # using message_filters to subscribe into two topics and run the callback
        sensory_data_sub = message_filters.Subscriber("sensory_data", sensory_data)
        results_sub = message_filters.Subscriber("results", results)
        ts = message_filters.TimeSynchronizer([sensory_data_sub, results_sub], 10)
        ts.registerCallback(callback)
        rospy.spin()

    except rospy.ROSInterruptException:
        pass
