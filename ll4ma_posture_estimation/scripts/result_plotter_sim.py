#!/usr/bin/env python
# license removed for brevity
import rospy
import rosbag
import numpy as np
from particle_filtering import ParticleFiltering, fkp
from sensor_msgs.msg import JointState
from ll4ma_posture_estimation.msg import observation, state, particle, weights_before_normalize
import matplotlib.pyplot as plt
from human_model import human_model


def plot_results(error_gt,estimates,ground_truth,estimates_vel,ground_truth_vel,stylus_curr_obs,stylus_pose_x, stylus_vel_xdot):

    T = []

    # m = min([len(estimates[i][0]) for i in range(len(estimates))])
    m = 300

    for i in range(m):
        T.append(i*0.02)


    plt.rcParams.update({'font.size':26})
    plt.rcParams['text.usetex'] = True
    plt.rcParams['text.latex.preamble'] = [r'\usepackage{amsmath}']
    rospy.loginfo(len(T[0:m]))
    rospy.loginfo(len(np.zeros(m)))

    mode= 0
    trial = 1
    plt.figure(1, figsize=(25,15))
    plt.grid(True)
    ax1 = plt.subplot(5,2,1)
    plt.plot(T[0:m], error_gt[mode][trial][0][0][0:m], 'r')
    # plt.plot(T[0:m], error_gt[mode][trial][1][0][0:m], 'g')
    plt.plot(T[0:m], error_gt[mode][trial][2][0][0:m], 'b')
    plt.plot(T[0:m], error_gt[mode][0][3][0][0:m], 'g')
    ax1.set_ylim([-1, 1])
    ax1.set_ylabel(r'$E_{\theta_1} (rad)$', fontsize=28)
    plt.grid(True)
    ax2 = plt.subplot(5,2,2)
    plt.plot(T[0:m], error_gt[mode][trial][0][1][0:m], 'r')
    # plt.plot(T[0:m], error_gt[mode][trial][1][1][0:m], 'g')
    plt.plot(T[0:m], error_gt[mode][trial][2][1][0:m], 'b')
    plt.plot(T[0:m], error_gt[mode][0][3][1][0:m], 'g')
    ax2.set_ylim([-1, 1])
    ax2.set_ylabel(r'$E_{\theta_2} (rad)$', fontsize=28)
    plt.grid(True)
    ax3 = plt.subplot(5,2,3)
    plt.plot(T[0:m], error_gt[mode][trial][0][2][0:m], 'r')
    # plt.plot(T[0:m], error_gt[mode][trial][1][2][0:m], 'g')
    plt.plot(T[0:m], error_gt[mode][trial][2][2][0:m], 'b')
    plt.plot(T[0:m], error_gt[mode][0][3][2][0:m], 'g')
    ax3.set_ylim([-1, 1])
    ax3.set_ylabel(r'$E_{\theta_3} (rad)$', fontsize=28)
    plt.grid(True)
    ax4 = plt.subplot(5,2,4)
    plt.plot(T[0:m], error_gt[mode][trial][0][3][0:m], 'r')
    # plt.plot(T[0:m], error_gt[mode][trial][1][3][0:m], 'g')
    plt.plot(T[0:m], error_gt[mode][trial][2][3][0:m], 'b')
    plt.plot(T[0:m], error_gt[mode][0][3][3][0:m], 'g')
    ax4.set_ylim([-1, 1])
    ax4.set_ylabel(r'$E_{\theta_4} (rad)$', fontsize=28)
    plt.grid(True)
    ax5 = plt.subplot(5,2,5)
    plt.plot(T[0:m], error_gt[mode][trial][0][4][0:m], 'r')
    # plt.plot(T[0:m], error_gt[mode][trial][1][4][0:m], 'g')
    plt.plot(T[0:m], error_gt[mode][trial][2][4][0:m], 'b')
    plt.plot(T[0:m], error_gt[mode][0][3][4][0:m], 'g')
    ax5.set_ylim([-1, 1])
    ax5.set_ylabel(r'$E_{\theta_5} (rad)$', fontsize=28)
    plt.grid(True)
    ax6 = plt.subplot(5,2,6)
    plt.plot(T[0:m], error_gt[mode][trial][0][5][0:m], 'r')
    # plt.plot(T[0:m], error_gt[mode][trial][1][5][0:m], 'g')
    plt.plot(T[0:m], error_gt[mode][trial][2][5][0:m], 'b')
    plt.plot(T[0:m], error_gt[mode][0][3][5][0:m], 'g')
    ax6.set_ylim([-1, 1])
    ax6.set_ylabel(r'$E_{\theta_6} (rad)$', fontsize=28)
    plt.grid(True)
    ax7 = plt.subplot(5,2,7)
    plt.plot(T[0:m], error_gt[mode][trial][0][6][0:m], 'r')
    # plt.plot(T[0:m], error_gt[mode][trial][1][6][0:m], 'g')
    plt.plot(T[0:m], error_gt[mode][trial][2][6][0:m], 'b')
    plt.plot(T[0:m], error_gt[mode][0][3][6][0:m], 'g')
    ax7.set_ylim([-1, 1])
    ax7.set_ylabel(r'$E_{\theta_7} (rad)$', fontsize=28)
    plt.grid(True)
    ax8 = plt.subplot(5,2,8)
    plt.plot(T[0:m], error_gt[mode][trial][0][7][0:m], 'r')
    # plt.plot(T[0:m], error_gt[mode][trial][1][7][0:m], 'g')
    plt.plot(T[0:m], error_gt[mode][trial][2][7][0:m], 'b')
    plt.plot(T[0:m], error_gt[mode][0][3][7][0:m], 'g')
    ax8.set_ylim([-1, 1])
    ax8.set_ylabel(r'$E_{\theta_8}(rad)$', fontsize=28)
    plt.grid(True)
    ax9 = plt.subplot(5,2,9)
    plt.plot(T[0:m], error_gt[mode][trial][0][8][0:m], 'r')
    # plt.plot(T[0:m], error_gt[mode][trial][1][8][0:m], 'g')
    plt.plot(T[0:m], error_gt[mode][trial][2][8][0:m], 'b')
    plt.plot(T[0:m], error_gt[mode][0][3][8][0:m], 'g')
    ax9.set_ylim([-1, 1])
    ax9.set_ylabel(r'$E_{\theta_9}(rad)$', fontsize=28)
    ax9.set_xlabel('Time(s)', fontsize=28)
    plt.grid(True)
    ax10 = plt.subplot(5,2,10)
    plt.plot(T[0:m], error_gt[mode][trial][0][9][0:m], 'r', label="Task 1")
    # plt.plot(T[0:m], error_gt[mode][trial][1][9][0:m], 'g')
    plt.plot(T[0:m], error_gt[mode][trial][2][9][0:m], 'b', label="Task 2")
    plt.plot(T[0:m], error_gt[mode][0][3][9][0:m], 'g', label="Task 3")
    ax10.set_ylim([-1, 1])
    ax10.set_ylabel(r'$E_{\theta_{10}} (rad)$', fontsize=28)
    ax10.set_xlabel('Time(s)', fontsize=28)
    plt.grid(True)
    plt.legend(loc='upper center',ncol=3, bbox_to_anchor=(-0.15, 6.5))
    # \text{(rad)}

    plt.savefig("/home/amir/Fig_1.pdf", dpi =300)


    rmse = [[[[],[],[],[]],[[],[],[],[]],[[],[],[],[]]],[[[],[],[],[]],[[],[],[],[]],[[],[],[],[]]],[[[],[],[],[]],[[],[],[],[]],[[],[],[],[]]]]
    for mode in range(len(error_gt)):
        for trial in range(len(error_gt[mode])):
            for task in range(len(error_gt[mode][trial])):
                for joint in range(10):
                    rmse[mode][trial][task].append(np.sqrt(float(sum([error_gt[mode][trial][task][joint][t]**2 for t in range(m)]))/m))


    plt.figure(2, figsize=(15,10))
    N = 3
    ind = np.arange(N)
    width = 0.15
    mean_rsme_full = []
    mean_rsme_height = []
    mean_rsme_no = []
    std_rsme_full = []
    std_rsme_height = []
    std_rsme_no = []

    if len(error_gt) == 2:
        for i in [0,2,3]:
            mean_rsme_height.append(np.mean([rmse[1][trial][i][0] for trial in range(len(error_gt[0]))]))
            std_rsme_height.append(np.std([rmse[1][trial][i][0] for trial in range(len(error_gt[0]))]))
            mean_rsme_no.append(np.mean([rmse[2][trial][i][0] for trial in range(len(error_gt[0]))]))
            std_rsme_no.append(np.std([rmse[2][trial][i][0] for trial in range(len(error_gt[0]))]))
        plt.bar(ind + width+0.02, mean_rsme_height, width, yerr=std_rsme_height, align='center', alpha=0.5, ecolor='black', capsize=10, color=(0.89, 0.51, 0.09 , 1), label='Height Measurement', edgecolor = "none")
        plt.bar(ind + 2*width+0.04, mean_rsme_no, width, yerr=std_rsme_no, align='center', alpha=0.5, ecolor='black', capsize=10, color=(0.2, 0.6, 0.8, 1), label='No Measurement', edgecolor = "none")

        plt.ylabel('RMSE', fontsize=32)

        plt.xticks(ind + 1.7*width, ('Task1', 'Task2', 'Task3'), fontsize=32)
    else:
        for i in [0,2,3]:
            mean_rsme_full.append(np.mean([rmse[0][trial][i][0] for trial in range(len(error_gt[0]))]))
            std_rsme_full.append(np.std([rmse[0][trial][i][0] for trial in range(len(error_gt[0]))]))
            mean_rsme_height.append(np.mean([rmse[1][trial][i][0] for trial in range(len(error_gt[0]))]))
            std_rsme_height.append(np.std([rmse[1][trial][i][0] for trial in range(len(error_gt[0]))]))
            mean_rsme_no.append(np.mean([rmse[2][trial][i][0] for trial in range(len(error_gt[0]))]))
            std_rsme_no.append(np.std([rmse[2][trial][i][0] for trial in range(len(error_gt[0]))]))
        plt.bar(ind + 2*width, mean_rsme_full, width, yerr=std_rsme_full, align='center', alpha=0.8, ecolor='black', capsize=10, color=(0.16, 0.68, 0.48, 1), label='Full Measurement', edgecolor = "none")
        plt.bar(ind + 3*width+0.02, mean_rsme_height, width, yerr=std_rsme_height, align='center', alpha=0.6, ecolor='black', capsize=10, color=(0.89, 0.51, 0.09 , 1), label='Height Measurement', edgecolor = "none")
        plt.bar(ind + 4*width+0.04, mean_rsme_no, width, yerr=std_rsme_no, align='center', alpha=0.9, ecolor='black', capsize=10, color=(0.2, 0.6, 0.8, 1), label='No Measurement', edgecolor = "none")

        plt.ylabel('RMSE', fontsize=32)

        plt.xticks(ind + 3*width, ('Task1', 'Task2', 'Task3'), fontsize=32)
    plt.legend(loc='best')
    plt.ylim((0,0.25))

    plt.savefig("/home/amir/Fig_2.pdf", dpi =300)


    mode= 0
    trial = 1
    task = 2

    plt.figure(3, figsize=(25,15))
    ax1 = plt.subplot(5,2,1)
    plt.plot(T[0:m], estimates[mode][trial][task][0][0:m], 'b')
    plt.plot(T[0:m], estimates[mode+1][trial][task][0][0:m], 'k')
    plt.plot(T[0:m], estimates[mode+2][trial][task][0][0:m], 'g')
    ax1.set_ylim([-1, 1])
    ax1.set_ylabel(r'$\theta_1 (rad)$', fontsize=28)
    plt.plot(T[0:m], ground_truth[mode][trial][task][0][0:m], 'r--', linewidth=3.0)
    ax2 = plt.subplot(5,2,2)
    plt.plot(T[0:m], estimates[mode][trial][task][1][0:m], 'b', label="Estimated posture - Full Measurement")
    plt.plot(T[0:m], estimates[mode+1][trial][task][1][0:m], 'k', label="Estimated posture - Height Measurement")
    plt.plot(T[0:m], estimates[mode+2][trial][task][1][0:m], 'g',  label="Estimated posture - No Measurement")
    ax2.set_ylim([-1, 1])
    ax2.set_ylabel(r'$\theta_2 (rad)$', fontsize=28)
    plt.plot(T[0:m], ground_truth[mode][trial][task][1][0:m], 'r--', label="Real posture", linewidth=3.0)
    plt.legend(loc='upper center',ncol=2, fontsize=24, bbox_to_anchor=(-0.1, 1.75))
    ax3 = plt.subplot(5,2,3)
    plt.plot(T[0:m], estimates[mode][trial][task][2][0:m], 'b')
    plt.plot(T[0:m], estimates[mode+1][trial][task][2][0:m], 'k')
    plt.plot(T[0:m], estimates[mode+2][trial][task][2][0:m], 'g')
    ax3.set_ylim([-1, 1])
    ax3.set_ylabel(r'$\theta_3 (rad)$', fontsize=28)
    plt.plot(T[0:m], ground_truth[mode][trial][task][2][0:m], 'r--', linewidth=3.0)
    ax4 = plt.subplot(5,2,4)
    plt.plot(T[0:m], estimates[mode][trial][task][3][0:m], 'b')
    plt.plot(T[0:m], estimates[mode+1][trial][task][3][0:m], 'k')
    plt.plot(T[0:m], estimates[mode+2][trial][task][3][0:m], 'g')
    ax4.set_ylim([-1, 1])
    ax4.set_ylabel(r'$\theta_4 (rad)$', fontsize=28)
    plt.plot(T[0:m], ground_truth[mode][trial][task][3][0:m], 'r--', linewidth=3.0)
    ax5 = plt.subplot(5,2,5)
    plt.plot(T[0:m], estimates[mode][trial][task][4][0:m], 'b')
    plt.plot(T[0:m], estimates[mode+1][trial][task][4][0:m], 'k')
    plt.plot(T[0:m], estimates[mode+2][trial][task][4][0:m], 'g')
    ax5.set_ylim([-1, 1])
    ax5.set_ylabel(r'$\theta_5 (rad)$', fontsize=28)
    plt.plot(T[0:m], ground_truth[mode][trial][task][4][0:m], 'r--', linewidth=3.0)
    ax6 = plt.subplot(5,2,6)
    plt.plot(T[0:m], estimates[mode][trial][task][5][0:m], 'b')
    plt.plot(T[0:m], estimates[mode+1][trial][task][5][0:m], 'k')
    plt.plot(T[0:m], estimates[mode+2][trial][task][5][0:m], 'g')
    ax6.set_ylim([-1, 1])
    ax6.set_ylabel(r'$\theta_6 (rad)$', fontsize=28)
    plt.plot(T[0:m], ground_truth[mode][trial][task][5][0:m], 'r--', linewidth=3.0)
    ax7 = plt.subplot(5,2,7)
    plt.plot(T[0:m], estimates[mode][trial][task][6][0:m], 'b')
    plt.plot(T[0:m], estimates[mode+1][trial][task][6][0:m], 'k')
    plt.plot(T[0:m], estimates[mode+2][trial][task][6][0:m], 'g')
    ax7.set_ylim([-1, 1])
    ax7.set_ylabel(r'$\theta_7 (rad)$', fontsize=28)
    plt.plot(T[0:m], ground_truth[mode][trial][task][6][0:m], 'r--', linewidth=3.0)
    ax8 = plt.subplot(5,2,8)
    plt.plot(T[0:m], estimates[mode][trial][task][7][0:m], 'b')
    plt.plot(T[0:m], estimates[mode+1][trial][task][7][0:m], 'k')
    plt.plot(T[0:m], estimates[mode+2][trial][task][7][0:m], 'g')
    ax8.set_ylim([-1, 1])
    ax8.set_ylabel(r'$\theta_8 (rad)$', fontsize=28)
    plt.plot(T[0:m], ground_truth[mode][trial][task][7][0:m], 'r--', linewidth=3.0)
    ax9 = plt.subplot(5,2,9)
    plt.plot(T[0:m], estimates[mode][trial][task][8][0:m], 'b')
    plt.plot(T[0:m], estimates[mode+1][trial][task][8][0:m], 'k')
    plt.plot(T[0:m], estimates[mode+2][trial][task][8][0:m], 'g')
    ax9.set_ylim([-1, 1])
    ax9.set_ylabel(r'$\theta_9 (rad)$', fontsize=28)
    ax9.set_xlabel('Time(s)', fontsize=28)
    plt.plot(T[0:m], ground_truth[mode][trial][task][8][0:m], 'r--', linewidth=3.0)
    ax10 = plt.subplot(5,2,10)
    plt.plot(T[0:m], estimates[mode][trial][task][9][0:m], 'b')
    plt.plot(T[0:m], estimates[mode+1][trial][task][9][0:m], 'k')
    plt.plot(T[0:m], estimates[mode+2][trial][task][9][0:m], 'g')
    ax10.set_ylim([-1, 1])
    ax10.set_ylabel(r'$\theta_{10} (rad)$', fontsize=28)
    ax10.set_xlabel('Time(s)', fontsize=28)
    plt.plot(T[0:m], ground_truth[mode][trial][task][9][0:m], 'r--', linewidth=3.0)

    plt.savefig("/home/amir/Fig_3.pdf", dpi =300)

    #
    # plt.figure(4, figsize=(25,15))
    # ax1 = plt.subplot(5,2,1)
    # plt.plot(T[0:m], estimates_vel[0][0][0:m], 'k')
    # ax1.set_ylim([-3.14, 3.14])
    # ax1.set_ylabel(r'$\boldsymbol{\theta_1} \text{(rad)}$', fontsize=22)
    # plt.plot(T[0:m], ground_truth_vel[0][0][0:m], 'r--')
    # plt.grid(True)
    # ax2 = plt.subplot(5,2,2)
    # plt.plot(T[0:m], estimates_vel[0][1][0:m], 'k', label="Estimated velocity")
    # ax2.set_ylim([-3.14, 3.14])
    # ax2.set_ylabel(r'$\boldsymbol{\theta_2} \text{(rad)}$', fontsize=22)
    # plt.plot(T[0:m], ground_truth_vel[0][1][0:m], 'r--', label="Real velocity")
    # plt.legend(ncol=2)
    # plt.grid(True)
    # ax3 = plt.subplot(5,2,3)
    # plt.plot(T[0:m], estimates_vel[0][2][0:m], 'k')
    # ax3.set_ylim([-3.14, 3.14])
    # ax3.set_ylabel(r'$\boldsymbol{\theta_3} \text{(rad)}$', fontsize=22)
    # plt.plot(T[0:m], ground_truth_vel[0][2][0:m], 'r--')
    # plt.grid(True)
    # ax4 = plt.subplot(5,2,4)
    # plt.plot(T[0:m], estimates_vel[0][3][0:m], 'k')
    # ax4.set_ylim([-3.14, 3.14])
    # ax4.set_ylabel(r'$\boldsymbol{\theta_4} \text{(rad)}$', fontsize=22)
    # plt.plot(T[0:m], ground_truth_vel[0][3][0:m], 'r--')
    # plt.grid(True)
    # ax5 = plt.subplot(5,2,5)
    # ax5.set_ylim([-3.14, 3.14])
    # plt.plot(T[0:m], estimates_vel[0][4][0:m], 'k')
    # ax5.set_ylabel(r'$\boldsymbol{\theta_5} \text{(rad)}$', fontsize=22)
    # plt.plot(T[0:m], ground_truth_vel[0][4][0:m], 'r--')
    # plt.grid(True)
    # ax6 = plt.subplot(5,2,6)
    # plt.plot(T[0:m], estimates_vel[0][5][0:m], 'k')
    # ax6.set_ylim([-3.14, 3.14])
    # ax6.set_ylabel(r'$\boldsymbol{\theta_6} \text{(rad)}$', fontsize=22)
    # plt.plot(T[0:m], ground_truth_vel[0][5][0:m], 'r--')
    # plt.grid(True)
    # ax7 = plt.subplot(5,2,7)
    # plt.plot(T[0:m], estimates_vel[0][6][0:m], 'k')
    # ax7.set_ylim([-3.14, 3.14])
    # ax7.set_ylabel(r'$\boldsymbol{\theta_7} \text{(rad)}$', fontsize=22)
    # plt.plot(T[0:m], ground_truth_vel[0][6][0:m], 'r--')
    # plt.grid(True)
    # ax8 = plt.subplot(5,2,8)
    # plt.plot(T[0:m], estimates_vel[0][7][0:m], 'k')
    # ax8.set_ylim([-3.14, 3.14])
    # ax8.set_ylabel(r'$\boldsymbol{\theta_8} \text{(rad)}$', fontsize=22)
    # plt.plot(T[0:m], ground_truth_vel[0][7][0:m], 'r--')
    # plt.grid(True)
    # ax9 = plt.subplot(5,2,9)
    # plt.plot(T[0:m], estimates_vel[0][8][0:m], 'k')
    # ax9.set_ylim([-3.14, 3.14])
    # ax9.set_ylabel(r'$\boldsymbol{\theta_9} \text{(rad)}$', fontsize=22)
    # ax9.set_xlabel('Time(ms)')
    # plt.plot(T[0:m], ground_truth_vel[0][8][0:m], 'r--')
    # plt.grid(True)
    # ax10 = plt.subplot(5,2,10)
    # plt.plot(T[0:m], estimates_vel[0][9][0:m], 'k')
    # ax10.set_ylim([-3.14, 3.14])
    # ax10.set_ylabel(r'$\boldsymbol{\theta_{10}} \text{(rad)}$', fontsize=22)
    # ax10.set_xlabel('Time(ms)')
    # plt.plot(T[0:m], ground_truth_vel[0][9][0:m], 'r--')
    # plt.grid(True)
    #
    # plt.savefig("/home/amir/Fig_3.pdf", dpi =300)
    # #
    # plt.figure(5, figsize=(25,15))
    # ax1 = plt.subplot(4,3,1)
    # plt.plot(T[0:m], stylus_curr_obs[0][0][0:m], 'r--')
    # ax1.set_ylim([-1, 1.5])
    # ax1.set_ylabel(r'$\boldsymbol{x} \text{(m)}$', fontsize=22)
    # plt.plot(T[0:m], stylus_pose_x[0][0][0:m], 'k')
    # ax2 = plt.subplot(4,3,2)
    # plt.plot(T[0:m], stylus_curr_obs[0][1][0:m], 'r--')
    # ax2.set_ylim([-0.3, 0.3])
    # ax2.set_ylabel(r'$\boldsymbol{y} \text{(m)}$', fontsize=22)
    # plt.plot(T[0:m], stylus_pose_x[0][1][0:m], 'k')
    # ax3 = plt.subplot(4,3,3)
    # plt.plot(T[0:m], stylus_curr_obs[0][2][0:m], 'r--', label="Observed EE")
    # ax3.set_ylim([0.5, 1.5])
    # ax3.set_ylabel(r'$\boldsymbol{z} \text{(m)}$', fontsize=22)
    # plt.plot(T[0:m], stylus_pose_x[0][2][0:m], 'k', label="Estimated EE")
    # plt.legend(ncol=2)
    # ax4 = plt.subplot(4,3,4)
    # plt.plot(T[0:m], stylus_curr_obs[0][3][0:m], 'r--')
    # ax4.set_ylim([-3.14, 3.14])
    # ax4.set_ylabel(r'$\boldsymbol{\theta} \text{(rad)}$', fontsize=22)
    # plt.plot(T[0:m], stylus_pose_x[0][3][0:m], 'k')
    # ax5 = plt.subplot(4,3,5)
    # plt.plot(T[0:m], stylus_curr_obs[0][4][0:m], 'r--')
    # ax5.set_ylim([-3.14, 3.14])
    # ax5.set_ylabel(r'$\boldsymbol{\phi} \text{(rad)}$', fontsize=22)
    # plt.plot(T[0:m], stylus_pose_x[0][4][0:m], 'k')
    # ax6 = plt.subplot(4,3,6)
    # plt.plot(T[0:m], stylus_curr_obs[0][5][0:m], 'r--')
    # ax6.set_ylim([-3.14, 3.14])
    # ax6.set_ylabel(r'$\boldsymbol{\psi} \text{(rad)}$', fontsize=22)
    # plt.plot(T[0:m], stylus_pose_x[0][5][0:m], 'k')
    # ax7 = plt.subplot(4,3,7)
    # plt.plot(T[0:m], stylus_curr_obs[0][6][0:m], 'r--')
    # ax7.set_ylim([-3.14, 3.14])
    # ax7.set_ylabel(r'$\boldsymbol{\dot{x}} \text{(m)}$', fontsize=22)
    # plt.plot(T[0:m], stylus_vel_xdot[0][0][0:m], 'k')
    # ax8 = plt.subplot(4,3,8)
    # plt.plot(T[0:m], stylus_curr_obs[0][7][0:m], 'r--')
    # ax8.set_ylim([-3.14, 3.14])
    # ax8.set_ylabel(r'$\boldsymbol{\dot{y}} \text{(m)}$', fontsize=22)
    # plt.plot(T[0:m], stylus_vel_xdot[0][1][0:m], 'k')
    # ax9 = plt.subplot(4,3,9)
    # plt.plot(T[0:m], stylus_curr_obs[0][8][0:m], 'r--')
    # ax9.set_ylim([-3.14, 3.14])
    # ax9.set_ylabel(r'$\boldsymbol{\dot{z}} \text{(m)}$', fontsize=22)
    # plt.plot(T[0:m], stylus_vel_xdot[0][2][0:m], 'k')
    # ax10 = plt.subplot(4,3,10)
    # plt.plot(T[0:m], stylus_curr_obs[0][9][0:m], 'r--')
    # ax10.set_ylim([-3.14, 3.14])
    # ax10.set_ylabel(r'$\boldsymbol{\dot{\theta}} \text{(rad)}$', fontsize=22)
    # ax10.set_xlabel('Time(ms)')
    # plt.plot(T[0:m], stylus_vel_xdot[0][3][0:m], 'k')
    # ax11 = plt.subplot(4,3,11)
    # plt.plot(T[0:m], stylus_curr_obs[0][10][0:m], 'r--')
    # ax11.set_ylim([-3.14, 3.14])
    # ax11.set_ylabel(r'$\boldsymbol{\dot{\phi}} \text{(rad)}$', fontsize=22)
    # ax11.set_xlabel('Time(ms)')
    # plt.plot(T[0:m], stylus_vel_xdot[0][4][0:m], 'k')
    # ax12 = plt.subplot(4,3,12)
    # plt.plot(T[0:m], stylus_curr_obs[0][11][0:m], 'r--')
    # ax12.set_ylim([-3.14, 3.14])
    # ax12.set_ylabel(r'$\boldsymbol{\dot{\psi}} \text{(rad)}$', fontsize=22)
    # ax12.set_xlabel('Time(ms)')
    # plt.plot(T[0:m], stylus_vel_xdot[0][5][0:m], 'k')
    #
    # plt.savefig("/home/amir/Fig_4.pdf", dpi =300)

    plt.show()

if __name__ == '__main__':
    urdf_file="/home/amir/catkin_ws/src/ll4ma_posture_estimation/ll4ma_posture_estimation/urdf/one_arm/upperbody/sim/kdl.urdf.xacro"
    human=human_model(urdf_file)
    most_prob = particle()
    error_gt = []
    estimates = []
    ground_truth = []
    estimates_vel = []
    ground_truth_vel = []
    stylus_curr_obs = []
    stylus_pose_x = []
    stylus_vel_xdot = []

# *******************************Simulation******************************************************************************************************
    num_tasks = 4
    num_trials = 3

    for mode in ['full', 'height', 'no']:
        for j in range(1, num_trials+1):
            for i in range(1, num_tasks+1):
                globals()["estimates_{0}_{1}_trial_{2}".format(mode, i, j)] = [[],[],[],[],[],[],[],[],[],[]]
                globals()["stylus_pose_x_{0}_{1}_trial_{2}".format(mode, i, j)] = [[],[],[],[],[],[],[],[],[],[]]
                globals()["estimates_vel_{0}_{1}_trial_{2}".format(mode, i, j)] = [[],[],[],[],[],[],[],[],[],[]]
                globals()["stylus_vel_xdot_{0}_{1}_trial_{2}".format(mode, i, j)] = [[],[],[],[],[],[],[],[],[],[]]
                globals()["ground_truth_{0}_{1}_trial_{2}".format(mode, i, j)] = [[],[],[],[],[],[],[],[],[],[]]
                globals()["ground_truth_vel_{0}_{1}_trial_{2}".format(mode, i, j)] = [[],[],[],[],[],[],[],[],[],[]]
                globals()["stylus_curr_obs_{0}_{1}_trial_{2}".format(mode, i, j)] = [[],[],[],[],[],[],[],[],[],[],[],[]]
                globals()["error_gt_{0}_{1}_trial_{2}".format(mode, i, j)] = [[],[],[],[],[],[],[],[],[],[]]

                globals()["{0}_bag_{1}_trial{2}".format(mode, i, j)] = rosbag.Bag("/home/amir/catkin_ws/src/ll4ma_posture_estimation/ll4ma_posture_estimation/data/sim/{0}_measure_sim_{1}_trial_{2}.bag".format(mode, i, j))

# *******************************Real******************************************************************************************************
    # num_tasks = 4
    # num_subs = 3 #num of subjects
    #
    # for mode in ['height', 'no']:
    #     for j in range(1, num_trials+1):
    #         for i in range(1, num_tasks+1):
    #             globals()["estimates_{0}_{1}_trial_{2}".format(mode, i, j)] = [[],[],[],[],[],[],[],[],[],[]]
    #             globals()["stylus_pose_x_{0}_{1}_trial_{2}".format(mode, i, j)] = [[],[],[],[],[],[],[],[],[],[]]
    #             globals()["estimates_vel_{0}_{1}_trial_{2}".format(mode, i, j)] = [[],[],[],[],[],[],[],[],[],[]]
    #             globals()["stylus_vel_xdot_{0}_{1}_trial_{2}".format(mode, i, j)] = [[],[],[],[],[],[],[],[],[],[]]
    #             globals()["ground_truth_{0}_{1}_trial_{2}".format(mode, i, j)] = [[],[],[],[],[],[],[],[],[],[]]
    #             globals()["ground_truth_vel_{0}_{1}_trial_{2}".format(mode, i, j)] = [[],[],[],[],[],[],[],[],[],[]]
    #             globals()["stylus_curr_obs_{0}_{1}_trial_{2}".format(mode, i, j)] = [[],[],[],[],[],[],[],[],[],[],[],[]]
    #             globals()["error_gt_{0}_{1}_trial_{2}".format(mode, i, j)] = [[],[],[],[],[],[],[],[],[],[]]
    #
    #             globals()["{0}_bag_{1}_trial{2}".format(mode, i, j)] = rosbag.Bag("/home/amir/catkin_ws/src/ll4ma_posture_estimation/ll4ma_posture_estimation/data/experiments/{0}_measure_{1}_trial_{2}.bag".format(mode, i, j))

                for topic, msg, t in globals()["{0}_bag_{1}_trial{2}".format(mode, i, j)].read_messages(topics=['estimates']):
                    for k in range(10):
                        globals()["estimates_{0}_{1}_trial_{2}".format(mode, i, j)][k].append(msg.theta[k])
                        globals()["estimates_vel_{0}_{1}_trial_{2}".format(mode, i, j)][k].append(msg.theta_dot[k])
                    most_prob.theta = list(msg.theta)
                    most_prob.theta_dot = list(msg.theta_dot)
                    globals()["stylus_{0}_{1}_trial_{2}".format(mode, i, j)]=fkp(human,most_prob)
                    for k in range(6):
                        globals()["stylus_pose_x_{0}_{1}_trial_{2}".format(mode, i, j)][k].append(globals()["stylus_{0}_{1}_trial_{2}".format(mode, i, j)][0][k])
                        globals()["stylus_vel_xdot_{0}_{1}_trial_{2}".format(mode, i, j)][k].append(globals()["stylus_{0}_{1}_trial_{2}".format(mode, i, j)][1][k])


                for topic, msg, t in globals()["{0}_bag_{1}_trial{2}".format(mode, i, j)].read_messages(topics=['observation']):
                        current_observation=[msg.stylus1_pose.x, msg.stylus1_pose.y,
                        msg.stylus1_pose.z,msg.stylus1_pose.theta, msg.stylus1_pose.phi,
                         msg.stylus1_pose.psi,msg.stylus1_vel.x, msg.stylus1_vel.y,
                         msg.stylus1_vel.z,msg.stylus1_vel.theta, msg.stylus1_vel.phi,
                          msg.stylus1_vel.psi]
                        for k in range(10):
                            globals()["ground_truth_{0}_{1}_trial_{2}".format(mode, i, j)][k].append(msg.stylus1_pose_gt[k])
                            globals()["ground_truth_vel_{0}_{1}_trial_{2}".format(mode, i, j)][k].append(msg.stylus1_vel_gt[k])
                        for k in range(12):
                            globals()["stylus_curr_obs_{0}_{1}_trial_{2}".format(mode, i, j)][k].append(current_observation[k])

                for k in range(10):
                    globals()["error_gt_{0}_{1}_trial_{2}".format(mode, i, j)][k] = [abs(globals()["estimates_{0}_{1}_trial_{2}".format(mode, i, j)][k][l]-globals()["ground_truth_{0}_{1}_trial_{2}".format(mode, i, j)][k][l]) for l in range(len(globals()["estimates_{0}_{1}_trial_{2}".format(mode, i, j)][0]))]

                globals()["{0}_bag_{1}_trial{2}".format(mode, i, j)].close()
        estimates.append([[globals()["estimates_{0}_{1}_trial_{2}".format(mode, i, j)] for i in range(1,5)] for j in range(1,4)])
        ground_truth.append([[globals()["ground_truth_{0}_{1}_trial_{2}".format(mode, i, j)] for i in range(1,5)] for j in range(1,4)])
        estimates_vel.append([[globals()["estimates_vel_{0}_{1}_trial_{2}".format(mode, i, j)] for i in range(1,5)] for j in range(1,4)])
        ground_truth_vel.append([[globals()["ground_truth_vel_{0}_{1}_trial_{2}".format(mode, i, j)] for i in range(1,5)] for j in range(1,4)])
        stylus_curr_obs.append([[globals()["stylus_curr_obs_{0}_{1}_trial_{2}".format(mode, i, j)] for i in range(1,5)] for j in range(1,4)])
        stylus_pose_x.append([[globals()["stylus_pose_x_{0}_{1}_trial_{2}".format(mode, i, j)] for i in range(1,5)] for j in range(1,4)])
        stylus_vel_xdot.append([[globals()["stylus_vel_xdot_{0}_{1}_trial_{2}".format(mode, i, j)] for i in range(1,5)] for j in range(1,4)])
        error_gt.append([[globals()["error_gt_{0}_{1}_trial_{2}".format(mode, i, j)] for i in range(1,5)] for j in range(1,4)])


    plot_results(error_gt,estimates,ground_truth,estimates_vel,ground_truth_vel, stylus_curr_obs, stylus_pose_x, stylus_vel_xdot)
