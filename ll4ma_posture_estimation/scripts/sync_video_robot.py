#!/usr/bin/env python
# license removed for brevity
import rospy
import numpy as np
from sensor_msgs.msg import JointState, Image, CameraInfo, CompressedImage
import message_filters
from time import time
from ll4ma_posture_estimation.msg import particles, particle, observation
from rviz_vis import posture_generator, posture_generator_ground_truth

# def posture_generator(particle_num,data):
#
#     pose=JointState()
#     pose.header.stamp = rospy.Time.now()
#     pose.header.frame_id = 'frame_0'
#     pose.name =  [
#     "particle{}_waist_flextion".format(particle_num),
#     "particle{}_waist_lateral_bending".format(particle_num),
#     "particle{}_waist_rotation".format(particle_num),
#     "particle{}_right_shoulder_abduction".format(particle_num),
#     "particle{}_right_shoulder_ver_flextion".format(particle_num),
#     "particle{}_right_shoulder_hor_flextion".format(particle_num),
#     "particle{}_right_elbow_flextion".format(particle_num),
#     "particle{}_right_elbow_supination".format(particle_num),
#     "particle{}_right_hand_rad_deviation".format(particle_num),
#     "particle{}_right_hand_flextion".format(particle_num)]
#
#     pose.position = len(pose.name) * [0.0]
#
#     pose.position[0]=data.theta[0]
#     pose.position[1]=np.pi/2+data.theta[1]
#     pose.position[2]=data.theta[2]
#     pose.position[3]=np.pi/2+data.theta[3]
#     pose.position[4]=3*np.pi/2+data.theta[4]
#     pose.position[5]=data.theta[5]
#     pose.position[6]=np.pi/2+data.theta[6]
#     pose.position[7]=np.pi/2+data.theta[7]
#     pose.position[8]=np.pi/2+data.theta[8]
#     pose.position[9]=data.theta[9]
#
#     pub.publish(pose)
#
# def posture_generator_ground_truth(particle_num,data):
#
#     pose=JointState()
#     pose.header.stamp = rospy.Time.now()
#     pose.header.frame_id = 'torso'
#     pose.name =  [
#     "ground_truth_hip_flextion".format(particle_num),
#     "ground_truth_hip_lateral_bending".format(particle_num),
#     "ground_truth_hip_rotation".format(particle_num),
#     "ground_truth_abb_flextion".format(particle_num),
#     "ground_truth_abb_lateral_bending".format(particle_num),
#     "ground_truth_abb_rotation".format(particle_num),
#     "ground_truth_chest_flextion".format(particle_num),
#     "ground_truth_chest_lateral_bending".format(particle_num),
#     "ground_truth_chest_rotation".format(particle_num),
#     "ground_truth_chest_to_shoulder".format(particle_num),
#     "ground_truth_right_shoulder_abduction".format(particle_num),
#     "ground_truth_right_shoulder_ver_flextion".format(particle_num),
#     "ground_truth_right_shoulder_hor_flextion".format(particle_num),
#     "ground_truth_right_elbow_flextion".format(particle_num),
#     "ground_truth_right_elbow_supination".format(particle_num),
#     "ground_truth_right_hand_rad_deviation".format(particle_num),
#     "ground_truth_right_hand_flextion".format(particle_num)]
#     pose.position = len(pose.name) * [0.0]
#
#     pose.position[0]=data.theta[0]
#     pose.position[1]=np.pi/2+data.theta[1]
#     pose.position[2]=data.theta[2]
#     pose.position[3]=data.theta[3]
#     pose.position[4]=np.pi/2+data.theta[4]
#     pose.position[5]=data.theta[5]
#     pose.position[6]=data.theta[6]
#     pose.position[7]=np.pi/2+data.theta[7]
#     pose.position[8]=data.theta[8]
#     pose.position[9]=data.theta[9]
#     pose.position[10]=np.pi/2+data.theta[10]
#     pose.position[11]=3*np.pi/2+data.theta[11]
#     pose.position[12]=data.theta[12]
#     pose.position[13]=np.pi/2+data.theta[13]
#     pose.position[14]=np.pi/2+data.theta[14]
#     pose.position[15]=np.pi/2+data.theta[15]
#     pose.position[16]=data.theta[16]
#
#     pub.publish(pose)

def republish_synched_data(estimate_data, mocap_data, video,camera_info):


    pose=JointState()
    pose.header.stamp = rospy.Time.now()
    pose.header.frame_id = 'frame_0'
    pose.name =  [
    "particle1_waist_flextion",
    "particle1_waist_lateral_bending",
    "particle1_waist_rotation",
    "particle1_right_shoulder_abduction",
    "particle1_right_shoulder_ver_flextion",
    "particle1_right_shoulder_hor_flextion",
    "particle1_right_elbow_flextion",
    "particle1_right_elbow_supination",
    "particle1_right_hand_rad_deviation",
    "particle1_right_hand_flextion"]

    pose.position = len(pose.name) * [0.0]

    pose.position[0]=estimate_data.theta[0]
    pose.position[1]=estimate_data.theta[1]
    pose.position[2]=estimate_data.theta[2]
    pose.position[3]=estimate_data.theta[3]
    pose.position[4]=estimate_data.theta[4]
    pose.position[5]=estimate_data.theta[5]
    pose.position[6]=estimate_data.theta[6]
    pose.position[7]=estimate_data.theta[7]
    pose.position[8]=estimate_data.theta[8]
    pose.position[9]=estimate_data.theta[9]

    pub.publish(pose)


    pose_mocap=JointState()
    pose_mocap.header.stamp = rospy.Time.now()
    pose_mocap.header.frame_id = 'torso'
    pose_mocap.name =  [
    "ground_truth_hip_flextion",
    "ground_truth_hip_lateral_bending",
    "ground_truth_hip_rotation",
    "ground_truth_abb_flextion",
    "ground_truth_abb_lateral_bending",
    "ground_truth_abb_rotation",
    "ground_truth_chest_flextion",
    "ground_truth_chest_lateral_bending",
    "ground_truth_chest_rotation",
    "ground_truth_chest_shoulder_sphere",
    "ground_truth_chest_to_shoulder",
    "ground_truth_right_shoulder_abduction",
    "ground_truth_right_shoulder_ver_flextion",
    "ground_truth_right_shoulder_hor_flextion",
    "ground_truth_right_elbow_flextion",
    "ground_truth_right_elbow_supination",
    "ground_truth_right_hand_rad_deviation",
    "ground_truth_right_hand_flextion"]
    pose_mocap.position = 18 * [0.0]

    pose_mocap.position[0]=mocap_data.mocap_posture_17dof[0]
    pose_mocap.position[1]=mocap_data.mocap_posture_17dof[1]
    pose_mocap.position[2]=mocap_data.mocap_posture_17dof[2]
    pose_mocap.position[3]=mocap_data.mocap_posture_17dof[3]
    pose_mocap.position[4]=mocap_data.mocap_posture_17dof[4]
    pose_mocap.position[5]=mocap_data.mocap_posture_17dof[5]
    pose_mocap.position[6]=mocap_data.mocap_posture_17dof[6]
    pose_mocap.position[7]=mocap_data.mocap_posture_17dof[7]
    pose_mocap.position[8]=mocap_data.mocap_posture_17dof[8]
    pose_mocap.position[9]=mocap_data.mocap_posture_17dof[9]
    pose_mocap.position[10]=mocap_data.mocap_posture_17dof[10]
    pose_mocap.position[11]=mocap_data.mocap_posture_17dof[11]
    pose_mocap.position[12]=mocap_data.mocap_posture_17dof[12]
    pose_mocap.position[13]=mocap_data.mocap_posture_17dof[13]
    pose_mocap.position[14]=mocap_data.mocap_posture_17dof[14]
    pose_mocap.position[15]=mocap_data.mocap_posture_17dof[15]
    pose_mocap.position[16]=mocap_data.mocap_posture_17dof[16]
    pose_mocap.position[17]=mocap_data.mocap_posture_17dof[17]

    pub_mocap.publish(pose_mocap)

    video_pub.publish(video)
    camera_info_pub.publish(camera_info)
    # rospy.loginfo("published")


if __name__ == '__main__':
    pub = rospy.Publisher('/particle1/joint_states', JointState, queue_size=100)
    pub_mocap = rospy.Publisher('/ground_truth/joint_states', JointState, queue_size=100)

    try:

        rospy.init_node('video_synchronizer', anonymous=True)
        posture = message_filters.Subscriber('/estimates',particle)
        mocap = message_filters.Subscriber('/observation',observation)
        video = message_filters.Subscriber('/usb_cam/image_raw/compressed', CompressedImage)
        camera_info = message_filters.Subscriber('/usb_cam/camera_info', CameraInfo)

        video_pub = rospy.Publisher('/camera_sync/video_sync/compressed', CompressedImage, queue_size=1)
        camera_info_pub = rospy.Publisher('/camera_sync/camera_info', CameraInfo, queue_size=1)

        ts = message_filters.ApproximateTimeSynchronizer([posture, mocap, video,camera_info], 10000, 0.5)
        ts.registerCallback(republish_synched_data)
        rate = rospy.Rate(1)

        while not rospy.is_shutdown():
            rate.sleep()

        rospy.spin()

    except rospy.ROSInterruptException:
        pass
