#!/usr/bin/env python


import rospy
import message_filters
import csv
# from pam.msg import feedback, force_reading
from hd2_ros_relay.msg import HD2State
from geometry_msgs.msg import PoseStamped
from tf.transformations import euler_from_quaternion
from time import time
import rosbag
from std_msgs.msg import Int32, String




def call_save_data(hd2):
    # r = rospy.Rate(10)
    # rospy.loginfo(hd2.full_pose.position.x)
    global trial
    with open("/home/amir/dataset_sep19/robot/robot_{}.csv".format(trial), 'a') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=' ',
                                quotechar='|', quoting=csv.QUOTE_MINIMAL)
        spamwriter.writerow([hd2.header.seq, hd2.header.stamp.secs, hd2.header.stamp.nsecs, hd2.full_pose.position.x, hd2.full_pose.position.y, hd2.full_pose.position.z, hd2.full_pose.orientation.x, hd2.full_pose.orientation.y, hd2.full_pose.orientation.z, hd2.velocity.linear.x, hd2.velocity.linear.y, hd2.velocity.linear.z, hd2.velocity.angular.x, hd2.velocity.angular.y, hd2.velocity.angular.z]) #mocap.pose.position.x, mocap.pose.position.y, mocap.pose.position.z])


if __name__ == '__main__':
    # bag = rosbag.Bag('test.bag', 'w')
    # with open('/home/amir/catkin_ws/src/hd2_ros_relay/data/amir/take5_robot.csv', 'wb') as csvfile:
    #     spamwriter = csv.writer(csvfile, delimiter=' ',
    #                             quotechar='|', quoting=csv.QUOTE_MINIMAL)
    trial = rospy.get_param("trial")

    try:
        # *** read feedbacks here ***

        rospy.init_node('dataCollection_robot', anonymous=True)
        # pub = rospy.Publisher('feedback', feedback, queue_size=1)
        hd2_pose = message_filters.Subscriber('/hd2/robot_state',HD2State)
        # mocap_pose = message_filters.Subscriber('/vrpn_client_node/RigidBody03/pose', PoseStamped)

        ts = message_filters.ApproximateTimeSynchronizer([hd2_pose], 2, 0.05)
        ts.registerCallback(call_save_data)

        rospy.spin()
    except rospy.ROSInterruptException:
        pass
