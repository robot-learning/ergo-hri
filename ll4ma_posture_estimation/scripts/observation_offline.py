#!/usr/bin/env python
# license removed for brevity
"""
This node provides the following observed data from the dataset recorded in
a human subject study that contains a human sitting on a stool working with a
Quanser HD2 haptic robot.


@author: Amir Yazdani
email: amir.yazdani@utah.edu
https://amir-yazdani.github.io/
"""
import rospy
import csv
import numpy as np
import math
from utils.visualization_utils import *
from utils.data_prepration_utils import *
from ll4ma_posture_estimation.msg import sensory_data, sensory_data_traj
from utils.subject_specific_utils import *
from nav_msgs.msg import Path
import pandas as pd
import copy


if __name__ == '__main__':
    try:
        #  initializing the node that generates the observation data
        rospy.init_node('observation_feeder', anonymous = True)

        #  initializing the publishers to the topics
        sensory_data_pub = rospy.Publisher('sensory_data_traj', sensory_data_traj, queue_size = 100)

        # getting trial info from ROS paramters
        trial = rospy.get_param("trial")

        # Reading the stylus pose observation data from the dataset
        observed_data = pd.read_csv("/home/amir/working_dataset/robot/robot_posture_{}.csv".format(trial))
        # Reading the motion capture data from the dataset
        mocap_data = pd.read_csv("/home/amir/working_dataset/mocap/mocap_posture_{}.csv".format(trial))

        ## TODO: check these lines, seems irrelevant
        # setting the stylus path
        right_stylus_path = Path()
        right_stylus_path.header.frame_id = 'frame0'

        # setting the rospy.rate to the observation frequency from the ROS parameters
        global observ_freq
        observ_freq = rospy.get_param("observation_frequency")
        rate = rospy.Rate(observ_freq)

        trajectory_sampling = rospy.get_param("trajectory_sampling")


        # initializing the variables
        Prev_observation = np.zeros(12)  # stored stylus pose from the previous step to calculate the velocity (pose and velocity)
        prev_mocap_reduced = np.zeros(10)  # stored mocap data (reduced joints to match the human model) from the previous step to calculate the velocity
        prev_mocap_full = np.zeros(18)  # stored mocap data (full joints) from the previous step to calculate the velocity
        mocap_vel_reduced = np.zeros(10) # reduced joint velocity of mocap
        mocap_vel_full = np.zeros(18) # full joint velocity of mocap
        Prev_Robot_data_frame = np.zeros(12) # previous dataframe from the stulys pose dataset (pose and velocity)
        robot_time_stamp = [] # time stamp for robot stylus data

        sensory_traj_array = []
        data_traj = sensory_data_traj()


        # generating arrays of data for each time step
        for j in range(mocap_data['hip_x'].count()/trajectory_sampling): # reducing frequency to 10 Hz fpr mocap
            i = j*trajectory_sampling

            # getting required info for synching stylus data and mocap data
            # since they have different frequency and
            # transforming the mocap angles into euler angles
            second = (abs(int(mocap_data.loc[i,'Timestamp'])) % 1000 +0)# find the second from truth data
            tenth_second = abs(int(mocap_data.loc[i,'Timestamp']*10)) % 10  #find the tenth of second from truth data
            hund_second = (abs(int(mocap_data.loc[i,'Timestamp']*100)) % 100) #find the hundred of second from truth data
            sec_flag = observed_data.loc[observed_data['second'].isin([second])]
            hund_sec_flag = sec_flag.loc[sec_flag['hund_second'].isin([hund_second])]

            if len(sec_flag)!= 0 and len(hund_sec_flag)!= 0:
                # converting quaternions to Euler angles for mocap data
                mocap_df_full, eul_hip, eul_ab, eul_chest, eul_Rshoulder = calculate_euler_angles_from_mocap_data(mocap_data,i)

                # making an equivalent torso segment for mocap data to be
                # similar to the human model
                mocap_df_reduced = equivalent_torso_for_mocap(mocap_df_full, eul_hip, eul_ab, eul_chest, eul_Rshoulder)

                #Prepare the stylus data (observation)
                right_stylus_pose_array, right_stylus_vel_array, robot_time_stamp, dtime, right_stylus_pose_posestamped, right_stylus_pose_state, np_df_robot  = prepare_stylus_data(observed_data, second, hund_second, Prev_Robot_data_frame)

                if dtime != 0:
                    #initializing parameters
                    sensor_data = sensory_data()

                    ## Calculating mocap velocity
                    mocap_vel_reduced = [(mocap_df_reduced[k]-prev_mocap_reduced[k])/(dtime) for k in range(10)] # 10-dof mocap skeleton
                    mocap_vel_full = [(mocap_df_full[k]-prev_mocap_full[k])/(dtime) for k in range(18)] # 18-dof mocap skeleton

                    # generating stylus path
                    right_stylus_path.poses.append(right_stylus_pose_posestamped)
                    if len(right_stylus_path.poses) > 10:  #only take the last 10 poses of stylus
                            right_stylus_path.poses.pop(0)

                    # generating the final observation data
                    ## stylus pose part of the observation
                    sensor_data.right_stylus.pose =         right_stylus_pose_state
                    sensor_data.right_stylus.vel.x =        right_stylus_vel_array[0]
                    sensor_data.right_stylus.vel.y =        right_stylus_vel_array[1]
                    sensor_data.right_stylus.vel.z =        right_stylus_vel_array[2]
                    sensor_data.right_stylus.vel.theta =    right_stylus_vel_array[3]
                    sensor_data.right_stylus.vel.phi =      right_stylus_vel_array[4]
                    sensor_data.right_stylus.vel.psi =      right_stylus_vel_array[5]
                    ## motion capture part of the observation
                    sensor_data.mocap.reduced_posture =  mocap_df_reduced
                    sensor_data.mocap.reduced_vel =      mocap_vel_reduced
                    sensor_data.mocap.full_posture =  mocap_df_full
                    sensor_data.mocap.full_vel =      mocap_vel_full
                    ## timestamps for observation
                    sensor_data.header.stamp.secs =    robot_time_stamp[0]
                    sensor_data.header.stamp.nsecs =   robot_time_stamp[1]

                    #publishing the observation data and stylus path
                    sensory_traj_array.append(sensor_data)

                    # storing the data from previous timestep for finite differencing
                    Prev_Robot_data_frame = np.copy(np_df_robot)
                    Prev_observation = np.copy(right_stylus_pose_array) #need to be corrected
                    prev_mocap_reduced = np.copy(mocap_df_reduced)
                    prev_mocap_full = np.copy(mocap_df_full)

        data_traj.traj = sensory_traj_array
        sensory_data_pub.publish(data_traj)
        rospy.loginfo("Observation sent!")


    except rospy.ROSInterruptException:
        pass
