#!/usr/bin/env python


import rospy
import message_filters
from time import time
import rosbag
from std_msgs.msg import Int32, String
from sensor_msgs.msg import Image, CameraInfo, CompressedImage




def callback_kinect(qhd_CameraInfo, hd_CameraInfo, sd_CameraInfo, qhd_image, qhd_image_rect, qhd_image_depth, hd_image, hd_image_rect, hd_image_depth, sd_image_color, sd_image_depth, sd_image_ir):

	bag.write('/kinect2/qhd/CameraInfo',qhd_CameraInfo)
	bag.write('/kinect2/hd/CameraInfo',hd_CameraInfo)
	bag.write('/kinect2/sd/CameraInfo',sd_CameraInfo)
	bag.write('/kinect2/qhd/image/compressed',qhd_image)
	bag.write('/kinect2/qhd/image_rect/compressed',qhd_image_rect)
	bag.write('/kinect2/qhd/image_depth/compressed',qhd_image_depth)
	bag.write('/kinect2/hd/image/compressed',hd_image)
	bag.write('/kinect2/hd/image_rect/compressed',hd_image_rect)
	bag.write('/kinect2/hd/image_depth/compressed',hd_image_depth)
	bag.write('/kinect2/sd/image_color/compressed',sd_image_color)
	bag.write('/kinect2/sd/image_depth/compressed', sd_image_depth)
	bag.write('/kinect2/sd/image_ir/compressed', sd_image_ir)

if __name__ == '__main__':
    global bag
    trial = rospy.get_param("trial")
    bag = rosbag.Bag("/home/amir/dataset_sep19/kinect/kinect_{}.bag".format(trial), 'w')

    try:
        # *** read feedbacks here ***

        rospy.init_node('dataCollection_kinect', anonymous=True)
        qhd_CameraInfo=message_filters.Subscriber("/kinect2/qhd/camera_info", CameraInfo)
        hd_CameraInfo=message_filters.Subscriber("/kinect2/hd/camera_info", CameraInfo)
        sd_CameraInfo=message_filters.Subscriber("/kinect2/sd/camera_info", CameraInfo)

        qhd_image=message_filters.Subscriber("/kinect2/qhd/image_color/compressed", CompressedImage)
        qhd_image_rect=message_filters.Subscriber("/kinect2/qhd/image_color_rect/compressed", CompressedImage)
        qhd_image_depth=message_filters.Subscriber("/kinect2/qhd/image_depth_rect/compressed", CompressedImage)

        hd_image=message_filters.Subscriber("/kinect2/hd/image_color/compressed", CompressedImage)
        hd_image_rect=message_filters.Subscriber("/kinect2/hd/image_color_rect/compressed", CompressedImage)
        hd_image_depth=message_filters.Subscriber("/kinect2/hd/image_depth_rect/compressed", CompressedImage)

        sd_image_color=message_filters.Subscriber("/kinect2/sd/image_color_rect/compressed", CompressedImage)
        sd_image_depth=message_filters.Subscriber("/kinect2/sd/image_depth/compressed", CompressedImage)
        sd_image_ir=message_filters.Subscriber("/kinect2/sd/image_ir/compressed", CompressedImage)
        ts=message_filters.TimeSynchronizer([qhd_CameraInfo, hd_CameraInfo, sd_CameraInfo, qhd_image, qhd_image_rect, qhd_image_depth, hd_image, hd_image_rect, hd_image_depth, sd_image_color, sd_image_depth, sd_image_ir], 10)
        ts.registerCallback(callback_kinect)
        rate = rospy.Rate(1)
        while not rospy.is_shutdown(): # and i <= 10000:
            rate.sleep()
	bag.close()
        rospy.spin()
    except rospy.ROSInterruptException:

        pass
