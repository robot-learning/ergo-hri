#!/usr/bin/env python
# license removed for brevity
import rospy
import copy
import math
import numpy as np
from ll4ma_posture_estimation.msg import posture
from human_model import human_model
from utils.visualization_utils import *
from utils.data_prepration_utils import *
from utils.subject_specific_utils import *



class ParticleFiltering(object):
    """

    """

    def __init__(self, n_particles, subject_num, task_num, trial_num, posture_feasiblity_model, n_joint=10):
        '''

        '''
        #initialize variables
        urdf_file="/home/amir/catkin_ws/src/ergo-hri/ll4ma_posture_estimation/urdf/kdl.urdf.xacro"
        self.posture_feasiblity_model = posture_feasiblity_model
        self.human=human_model(urdf_file)
        self.n_joints= n_joint
        self.n_particles = n_particles
        self.weights=np.ones(self.n_particles)
        self.weights_before_normalize=np.ones(self.n_particles)
        self.particles=[]
        self.T=0.02 #time step for process update (50Hz)
        self.prev_particles = []
        self.sigma = float(np.pi*2)/180
        self.sigma_velocity = float(np.pi*2)/180 #deg/s  #noise due to velocity estimation # TODO: should convert into deg/iteration
        self.sigma_kinematic =[                                 # covariance matrix for position and orintation
                    [0.0001, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0.0001, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0.0001, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0.005, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0.005, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0.005, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0.01, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0.01, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0.01, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0.1, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.1, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.1]]

        self.sigma_kinematic_pos_only =[        # covariance matrix for position and orintation
                    [0.001, 0, 0, 0, 0, 0],
                    [0, 0.001, 0, 0, 0, 0],
                    [0, 0, 0.001, 0, 0, 0],
                    [0, 0, 0, 0.005, 0, 0],
                    [0, 0, 0, 0, 0.005, 0],
                    [0, 0, 0, 0, 0, 0.005]]

        self.initiate_at_neutral_posture = rospy.get_param("initiate_at_neutral_posture")
        self.posture_validation = rospy.get_param("posture_validation")
        self.pose_only_weight_for_particles = rospy.get_param("pose_only_weight_for_particles")
        # setting the natural posture
        self.neutral_posture_deg=[0, 0, 0, 90, 0, 0, 90, 0, 0, 0]
        self.neutral_posture=[self.neutral_posture_deg[i]*np.pi/180 for i in range(10)]

        #calculating joint limits
        self.joint_limits , self.joint_limits_len = human_joint_limit()

        # initiate particles
        for p in range(self.n_particles):
            init_particle = posture()
            if self.posture_validation == True:
                feasible = 0
                while not feasible:
                    for i in range(self.n_joints):
                        if self.initiate_at_neutral_posture:
                            # init_particle.theta[i]=np.random.normal(self.neutral_posture[i], 0.05*self.joint_limits_len[i])
                            init_particle.theta[i]=np.random.normal(self.neutral_posture[i], 0.05)
                        else:
                            init_particle.theta[i]=np.random.uniform(self.joint_limits[i][0], self.joint_limits[i][1])

                        init_particle.theta_dot[i]=0
                    # set the list of joint angles for checking the feasiblity of posture
                    arm_feasiblity_config = np.array([[-(init_particle.theta[3]+np.pi/2), init_particle.theta[4], -init_particle.theta[5], -init_particle.theta[6]]])
                    # check the feasiblity
                    feasible = self.posture_feasiblity_model.predict_classes(arm_feasiblity_config)[0][0]
                    if feasible == 0:
                        rospy.loginfo("infeasibe")
            else:
                for i in range(self.n_joints):
                    if self.initiate_at_neutral_posture:
                        # init_particle.theta[i]=np.random.normal(self.neutral_posture[i], 0.05*self.joint_limits_len[i])
                        init_particle.theta[i]=np.random.normal(self.neutral_posture[i], 0.05)
                    else:
                        init_particle.theta[i]=np.random.uniform(self.joint_limits[i][0], self.joint_limits[i][1])

                    init_particle.theta_dot[i]=0

            self.particles.append(init_particle)
            self.prev_particles.append(init_particle)

    def process_update(self):
        '''
        updating the particle based on the dynamics of ll4ma_posture_estimation
        '''
        # initiating required variables
        new_particles = []
        self.prev_particles = copy.copy(self.particles)

        for i in range(self.n_particles):
            p = posture()

            for joint in range(self.n_joints):
                sampled = 0
                # updating the angular velocity
                new_theta_dot = np.random.normal(self.prev_particles[i].theta_dot[joint], self.sigma_velocity)
                p.theta_dot[joint] = new_theta_dot

                # updating the joint angles
                new_theta = np.random.normal(self.prev_particles[i].theta[joint]+(new_theta_dot*self.T),self.sigma)

                # checking joint angles being in joint limits and add to the particle
                if (new_theta < self.joint_limits[joint][0]):
                    p.theta[joint]=self.joint_limits[joint][0]
                elif (new_theta > self.joint_limits[joint][1]):
                    p.theta[joint]=self.joint_limits[joint][1]
                else:
                    p.theta[joint] = new_theta

                # checking if joint angles are more than 2pi
                if (p.theta[joint]>=np.pi):
                    p.theta[joint]-=2*np.pi
                elif (p.theta[joint]<-np.pi):
                    p.theta[joint]+=2*np.pi

            new_particles.append(p)

        self.particles = copy.copy(new_particles)

    def observation_update(self, observation):
        '''
        updating particles based on seen an observation
        '''
        for i in range(self.n_particles):
            # calculating pose and velocity of at the EE of the particle
            [x,x_dot]=fkp(self.human,self.particles[i])

            # calculating the error at the EE of the particle (EE pose/vel - observed pose/vel)
            x_diff=[observation[j]-x[j] for j in range(6)]
            x_dot_diff=[observation[j+6]-x_dot[j] for j in range(6)]
            diff=x_diff+x_dot_diff  # including both pose and velocity
            diff2=x_diff            # including only pose

            # calculating the weight for each particle
            if self.pose_only_weight_for_particles:
                self.weights[i]=float(np.power(np.linalg.det(np.multiply(2*3.14,self.sigma_kinematic_pos_only)),-0.5)*np.exp(-0.5*np.dot(np.dot(diff2,np.linalg.inv(self.sigma_kinematic_pos_only)),np.transpose(diff2))))
            else:
                self.weights[i]=float(np.power(np.linalg.det(np.multiply(2*3.14,self.sigma_kinematic)),-0.5)*np.exp(-0.5*np.dot(np.dot(diff,np.linalg.inv(self.sigma_kinematic)),np.transpose(diff))))
            if self.posture_validation==True:
                arm_feasiblity_config = np.array([[-(self.particles[i].theta[3]+np.pi/2), self.particles[i].theta[4], -self.particles[i].theta[5], -self.particles[i].theta[6]]])
                # check the feasiblity
                feasible = self.posture_feasiblity_model.predict_classes(arm_feasiblity_config)[0][0]
                if feasible == 0:
                    rospy.loginfo("infeasible")
                    self.weights[i] = self.weights[i] * feasible
        self.weights_before_normalize=copy.copy(self.weights)

        self.normalize()

        self.re_sampling()


    def re_sampling(self):
        '''
        resampling the particles based on the weighted particles

        '''
        new_particles=[]
        for i in range(self.n_particles):
            eps=np.random.uniform(0,1)
            lb=0
            ub=0
            for p in range(self.n_particles):
                ub+=self.weights[p]
                if (lb<=eps and eps<ub):
                    new_particles.append(self.particles[p])
                lb=ub
        self.particles=copy.copy(new_particles)

    def normalize(self):
        '''
        nomalizing the weight of the particles
        '''
        summation=sum(self.weights)
        w_normalized=[float(self.weights[i])/summation for i in range(len(self.weights))]
        self.weights=copy.copy(w_normalized)

    def find_most_probable(self):
        '''
        fining the particle with the hights weights
        '''
        weights_find = copy.deepcopy(self.weights)
        particles_find = copy.deepcopy(self.particles)
        most_probable = particles_find[np.argmax(weights_find)]

        return most_probable
