#!/usr/bin/env python
# license removed for brevity
import rospy
import rosbag
import numpy as np
from particle_filtering import ParticleFiltering, fkp
from sensor_msgs.msg import JointState
from ll4ma_posture_estimation.msg import sensory_data, results, sensory_data_traj
import matplotlib.pyplot as plt
from ls_ik import LS_IK
from offline_smoothing import Offline_Smoothing
from keras.models import load_model


def LS_callback(sensory_data):   #calls when we have observation
    '''

    '''
    global pose_estimate, iteration
    results_msg = results()

    iteration += 1
    rospy.loginfo(iteration)

    # setting the proper observation data
    observation = np.array([sensory_data.right_stylus.pose.x, sensory_data.right_stylus.pose.y,
    sensory_data.right_stylus.pose.z, sensory_data.right_stylus.pose.theta, sensory_data.right_stylus.pose.phi,
     sensory_data.right_stylus.pose.psi])
    # Optimizing the leat square problem
    posture = pose_estimate.optimize(observation)

    # turning the results into a ros message to be saved in bag file
    ## header data
    results_msg.header.stamp.secs =      sensory_data.header.stamp.secs
    results_msg.header.stamp.nsecs =     sensory_data.header.stamp.nsecs

    ## ik solution
    results_msg.ik_solution.theta =      posture

    # Publishing the results
    results_pub.publish(results_msg)

    # writing info in the bag file
    data_bag.write('results', results_msg)
    data_bag.write('sensory_data', sensory_data)



def PF_callback(sensory_data):   #calls when we have observation

    global data_bag, pose_estimate, iteration
    results_msg = results()
    iteration += 1
    rospy.loginfo(iteration)

    # collecting the proper obsersation data from the data
    current_observation=[sensory_data.right_stylus.pose.x, sensory_data.right_stylus.pose.y,
    sensory_data.right_stylus.pose.z,sensory_data.right_stylus.pose.theta, sensory_data.right_stylus.pose.phi,
     sensory_data.right_stylus.pose.psi,sensory_data.right_stylus.vel.x, sensory_data.right_stylus.vel.y,
     sensory_data.right_stylus.vel.z,sensory_data.right_stylus.vel.theta, sensory_data.right_stylus.vel.phi,
      sensory_data.right_stylus.vel.psi]

    # Process update
    pose_estimate.process_update()

    # Observation update
    pose_estimate.observation_update(current_observation)

    # Getting the most probable particle data
    most_probable_particle =                        pose_estimate.find_most_probable()

    # turning the particles, mocap, most probable particles into a ros message to be saved in bag file
    ## particles
    results_msg.header.stamp.secs =      sensory_data.header.stamp.secs
    results_msg.header.stamp.nsecs =     sensory_data.header.stamp.nsecs
    results_msg.particles =                         pose_estimate.particles
    ## particle weights
    results_msg.weights_before_normalize =          pose_estimate.weights_before_normalize
    ## most probable particles
    results_msg.most_probable_particle =            most_probable_particle

    # Publishing the results
    results_pub.publish(results_msg)

    # writing info in the bag file
    data_bag.write('results', results_msg)
    data_bag.write('PF_solution', most_probable_particle)
    data_bag.write('sensory_data', sensory_data)


def PF_LS_callback(sensory_data):   #calls when we have observation

    global data_bag, pose_estimate_LS, pose_estimate_PF, iteration
    results_msg = results()
    iteration += 1
    rospy.loginfo(iteration)

    # collecting the proper obsersation data from the data
    PF_observation = [sensory_data.right_stylus.pose.x, sensory_data.right_stylus.pose.y,
    sensory_data.right_stylus.pose.z,sensory_data.right_stylus.pose.theta, sensory_data.right_stylus.pose.phi,
    sensory_data.right_stylus.pose.psi,sensory_data.right_stylus.vel.x, sensory_data.right_stylus.vel.y,
    sensory_data.right_stylus.vel.z,sensory_data.right_stylus.vel.theta, sensory_data.right_stylus.vel.phi,
    sensory_data.right_stylus.vel.psi]

    LS_observation = np.array([sensory_data.right_stylus.pose.x, sensory_data.right_stylus.pose.y,
    sensory_data.right_stylus.pose.z, sensory_data.right_stylus.pose.theta, sensory_data.right_stylus.pose.phi,
    sensory_data.right_stylus.pose.psi])

    # Solver (1): Particle filter
    ## Process update
    pose_estimate_PF.process_update()

    ## Observation update
    pose_estimate_PF.observation_update(PF_observation)

    ## Getting the most probable particle data
    most_probable_particle =                        pose_estimate_PF.find_most_probable()

    # Solver (2): Least Square Gauss-Newton
    ## Optimizing the leat square problem
    posture = pose_estimate_LS.optimize(LS_observation)

    # turning the particles, mocap, most probable particles into a ros message to be saved in bag file
    ## particles
    results_msg.header.stamp.secs =                 sensory_data.header.stamp.secs
    results_msg.header.stamp.nsecs =                sensory_data.header.stamp.nsecs
    results_msg.particles =                         pose_estimate_PF.particles
    ## particle weights
    results_msg.weights_before_normalize =          pose_estimate_PF.weights_before_normalize
    ## most probable particles
    results_msg.most_probable_particle =            most_probable_particle
    ## ik solution
    results_msg.ik_solution.theta =                 posture

    # Publishing the results
    results_pub.publish(results_msg)

    # writing info in the bag file
    data_bag.write('results', results_msg)
    data_bag.write('PF_solution', most_probable_particle)
    data_bag.write('sensory_data', sensory_data)

def offline_smoothing_callback(sensory_data_traj):   #calls when we have observation
    '''

    '''
    global pose_estimate, iteration, subject_num, task_num, trial_num, seg_length_mode, joint_limits_type, trajectory_sampling
    results_msg = results()

    # check the flag initialization parameters
    OfflineSmoothing_initialization = rospy.get_param("OS_init_type")

    # Optimizing the leat square problem
    if OfflineSmoothing_initialization == "neutral_posture":
        postures = pose_estimate.optimize(sensory_data_traj)
    elif OfflineSmoothing_initialization == "ik_solution":
        IK_LS_solutions = []
        IK_bag = rosbag.Bag("/home/amir/working_dataset/final_posture_estimation_results/subject{0}_task{1}_{2}_PF+LS_{3}_trajsample{4}.bag".format(subject_num, task_num, seg_length_mode, joint_limits_type, trajectory_sampling))

        # Reading the IK solution
        for topic, msg, t in IK_bag.read_messages(topics = ['results']):
            IK_LS_solutions.append(msg.ik_solution.theta)    #
        IK_LS_solutions = np.array(IK_LS_solutions).flatten()

        postures = pose_estimate.optimize(sensory_data_traj, IK_LS_solutions)
    rospy.loginfo("Solved!")

    observ_freq = rospy.get_param("observation_frequency")
    rate = rospy.Rate(observ_freq)

    for i in range(len(postures)):
        iteration += 1
        rospy.loginfo(iteration)

        posture = postures[i]
        sensory_data = sensory_data_traj.traj[i]
        # setting the proper observation data
        observation = np.array([sensory_data.right_stylus.pose.x, sensory_data.right_stylus.pose.y,
        sensory_data.right_stylus.pose.z, sensory_data.right_stylus.pose.theta, sensory_data.right_stylus.pose.phi,
        sensory_data.right_stylus.pose.psi])
        # turning the results into a ros message to be saved in bag file
        ## header data
        results_msg.header.stamp.secs =      sensory_data.header.stamp.secs
        results_msg.header.stamp.nsecs =     sensory_data.header.stamp.nsecs

        ## ik solution
        results_msg.ik_solution.theta =      posture

        # Publishing the results
        results_pub.publish(results_msg)
        sensory_data_pub.publish(sensory_data)


        # writing info in the bag file
        data_bag.write('results', results_msg)
        data_bag.write('sensory_data', sensory_data)
        rate.sleep()

if __name__ == '__main__':
    '''
    handler for the solver and other stuff
    '''
    global subject_num, task_num, trial_num, seg_length_mode, joint_limits_type, trajectory_sampling
    # loading the parameters from the server
    solver = rospy.get_param("solver")
    trial_subj_task = rospy.get_param("trial_subj_task")
    subject_num = rospy.get_param("subject_num")
    task_num = rospy.get_param("task_num")
    trial_num = rospy.get_param("trial")
    n_particles =rospy.get_param("number_of_particles")
    process_freq=rospy.get_param("filtering_frequency") #Hz
    observ_freq=rospy.get_param("observation_frequency") #Hz
    seg_length_mode = rospy.get_param("seg_length_mode")
    joint_limits_type = rospy.get_param("joint_limit")
    trajectory_sampling = rospy.get_param("trajectory_sampling")

    # dealing with the issue of using _f and _m
    if seg_length_mode== "height_measure_f" or seg_length_mode=="height_measure_m":
        seg_length_mode= "height_measure"

    # loading the posture feasiblity models
    posture_feasiblity_model = load_model('/home/amir/catkin_ws/src/ergo-hri/ll4ma_posture_estimation/models/realistic_arm_limits_model.h5')

    try:
        #initializing the parameters, nodes, publishers and Subscribers
        if solver == "PF+LS":
            global data_bag, pose_estimate_LS, pose_estimate_PF, iteration
        else:
            global data_bag, pose_estimate, iteration

        iteration = 0
        rospy.init_node('main', anonymous=True)
        # setting the rospy rate
        rate = rospy.Rate(process_freq)
        ## defining the bag name for the resultsn_particles
        data_bag = rosbag.Bag('/home/amir/working_dataset/final_posture_estimation_results/{0}_{1}_{2}_{3}_trajsample{4}.bag'.format(trial_subj_task, seg_length_mode, solver, joint_limits_type, trajectory_sampling), 'w')

        ## publishing the results
        results_pub = rospy.Publisher('results', results, queue_size=1)
        sensory_data_pub = rospy.Publisher('sensory_data', sensory_data, queue_size=1)

        if solver == "PF":
            # Setting the class for the solver
            pose_estimate = ParticleFiltering(n_particles, subject_num, task_num, trial_num, posture_feasiblity_model) #set pose_estimate global variable as ParticleFiltering class with initialization of n_particles
            ## subscribing to observation as run the call back when it recieve the data
            rospy.Subscriber("sensory_data", sensory_data, PF_callback)
        elif solver == "LS":
            pose_estimate = LS_IK() #set pose_estimate global variable as ik_ls class
            rospy.Subscriber("sensory_data", sensory_data, LS_callback)

        elif solver == "PF+LS":
            pose_estimate_LS = LS_IK() #set pose_estimate global variable as ik_ls class
            pose_estimate_PF = ParticleFiltering(n_particles, subject_num, task_num, trial_num, posture_feasiblity_model) #set pose_estimate global variable as ParticleFiltering class with initialization of n_particles
            ## subscribing to observation as run the call back when it recieve the data
            rospy.Subscriber("sensory_data", sensory_data, PF_LS_callback)

        elif solver == "offline_smoothing":
            pose_estimate = Offline_Smoothing(trajectory_sampling) #set pose_estimate global variable as ik_ls class
            rospy.Subscriber("sensory_data_traj", sensory_data_traj, offline_smoothing_callback)
        else:
            rospy.loginfo("unknown solver")

        while not rospy.is_shutdown():
            rate.sleep()
        data_bag.close()
        rospy.spin()

    except rospy.ROSInterruptException:
        pass
