#!/usr/bin/env python
# license removed for brevity

import numpy as np
import matplotlib.pyplot as plt
from pylab import plot, show, savefig, xlim, figure, \
                hold, ylim, legend, boxplot, setp, axes

import csv

def setBoxColors(bp, edge_color, fill_color, mid_color):
		for element in ['boxes', 'whiskers', 'fliers', 'caps']:
			plt.setp(bp[element], color=edge_color)
		plt.setp(bp['medians'], color=mid_color)
		for patch in bp['boxes']:
			patch.set(facecolor=fill_color)

if __name__ == '__main__':

    filename = "Measurements.csv"
    rows = []

    # reading csv file
    with open(filename, 'r') as csvfile:
        # creating a csv reader object
        csvreader = csv.reader(csvfile)

        # extracting each data row one by one
        for row in csvreader:
            rows.append(row)



    for mode in range(3): # modes : 'cpa', 'full_measure','height_measure'
        globals()["error_mode{0}".format(mode)] = []
        for segment in range(5): #segments: 'torso', 'shoulder', 'uarm', 'farm', 'hand'
            globals()["error_mode{0}_segment{1}".format(mode, segment)] = []
            for sub in range(8):
                globals()["error_mode{0}_segment{1}".format(mode, segment)].append(abs(float(rows[sub+2][5*mode+segment+3]) - float(rows[sub+2][segment+18]))/float(rows[sub+2][segment+18])*100)
                globals()["error_mode{0}".format(mode)].append(abs(float(rows[sub+2][5*mode+segment+3]) - float(rows[sub+2][segment+18]))/float(rows[sub+2][segment+18])*100)

    for mode in range(3): # modes : 'cpa', 'mocap','height_measure'
        globals()["error_vs_full_measure_mode{0}".format(mode)] = []
        for segment in range(5): #segments: 'torso', 'shoulder', 'uarm', 'farm', 'hand'
            globals()["error_vs_full_measure_mode{0}_segment{1}".format(mode, segment)] = []
            for sub in range(8):
                globals()["error_vs_full_measure_mode{0}_segment{1}".format(mode, segment)].append(abs(float(rows[sub+16][5*mode+segment+3]) - float(rows[sub+16][segment+18]))/float(rows[sub+16][segment+18])*100)
                globals()["error_vs_full_measure_mode{0}".format(mode)].append(abs(float(rows[sub+16][5*mode+segment+3]) - float(rows[sub+16][segment+18]))/float(rows[sub+16][segment+18])*100)

            # print(globals()["error_mode{0}_segment{1}".format(mode, segment)])


    # plot
    blue_circle = dict(markerfacecolor='deepskyblue', marker='o')
    peru_circle = dict(markerfacecolor='olivedrab', marker='o')
    red_circle = dict(markerfacecolor='red', marker='o')
    N = 5
    ind = np.arange(N)

    # fig,(ax1,ax2) = plt.subplots(2,1, figsize=(13,13))
    fig, ax1 = plt.subplots(1,1, figsize=(13,6))
    # fig,(ax1,ax2) = plt.subplots(2,1, figsize=(13,13))

    width = 0.2

    # bp0 = [[],[],[],[],[],[]]
    # bp1 = [[],[],[],[],[],[]]
    # bp2 = [[],[],[],[],[],[]]
    #
    plt.rcParams.update({'font.size':26})
    plt.rcParams['text.usetex'] = True
    plt.rcParams['text.latex.preamble'] = [r'\usepackage{amsmath}']
    #
    #
    # for i in range(5):
    #     bp0[i] = ax1.boxplot(globals()["error_mode0_segment{0}".format(i)], positions = [i+1.1-0.24], widths = width, patch_artist=True, flierprops=blue_circle, showfliers=False)
    #     setBoxColors(bp0[i], 'black', '#7bf2da', 'black')
    #
    #     bp1[i] = ax1.boxplot(globals()["error_mode1_segment{0}".format(i)], positions = [i+1.1], widths = width, patch_artist=True, flierprops=peru_circle, showfliers=False)
    #     setBoxColors(bp1[i], 'black', '#cb9d06', 'black')
    #
    #     bp2[i] = ax1.boxplot(globals()["error_mode2_segment{0}".format(i)], positions = [i+1.1+0.24], widths = width, patch_artist=True, flierprops=red_circle, showfliers=False)
    #     setBoxColors(bp2[i], 'black', '#bb3f3f', 'black')
    #
    # bp0[5] = ax1.boxplot(globals()["error_mode0"], positions = [5+1.1-0.24], widths = width, patch_artist=True, flierprops=blue_circle, showfliers=False)
    # setBoxColors(bp0[5], 'black', '#7bf2da', 'black')
    #
    # bp1[5] = ax1.boxplot(globals()["error_mode1"], positions = [5+1.1], widths = width, patch_artist=True, flierprops=blue_circle, showfliers=False)
    # setBoxColors(bp1[5], 'black', '#cb9d06', 'black')
    #
    # bp2[5] = ax1.boxplot(globals()["error_mode2"], positions = [5+1.1+0.24], widths = width, patch_artist=True, flierprops=blue_circle, showfliers=False)
    # setBoxColors(bp2[5], 'black', '#bb3f3f', 'black')
    #
    #
    #
    # ax1.set_ylim((0, 50))
    # ax1.set_xlim((0.5,6.5))
    #
    # ax1.set_xticklabels([r'$\mathrm{Torso}$', r'$\mathrm{Shoulder}$', r'$\mathrm{Upper~Arm}$', r'$\mathrm{Fore~Arm}$', r'$\mathrm{Hand}$', r'$\mathrm{All}$'], fontsize=22)
    #
    # ax1.set_xticks([1, 2, 3, 4, 5, 6])
    # ax1.set_yticks([0,10, 20, 30, 40, 50])
    # ax1.set_yticklabels([r'$\mathrm{0}$', r'$\mathrm{10}$', r'$\mathrm{20}$', r'$\mathrm{30}$', r'$\mathrm{40}$', r'$\mathrm{50}$'], fontsize=22)
    # ax1.set_ylabel(r'$\mathrm{Deviation~Percentage~from}$'+ '\n' + r'$\mathrm{Motion~Capture~(\%)}$', fontsize=22)
    #
    # ax1.legend([bp0[0]["boxes"][0], bp1[0]["boxes"][0], bp2[0]["boxes"][0]], [r'$\mathrm{CPA}$', r'$\mathrm{Full~Measurement}$', r'$\mathrm{Height~Measurement}$'], loc='upper right', ncol=1, fontsize=22)
    #
    # ax1.yaxis.grid(True)

    # fig2,ax2 = plt.subplots(2,1,2, figsize=(15,8))
    # width = 0.2
    #

    fig, ax2 = plt.subplots(1,1, figsize=(13,6))

    bp3 = [[],[],[],[],[],[]]
    bp4 = [[],[],[],[],[],[]]
    bp5 = [[],[],[],[],[],[]]



    for i in range(5):
        bp3[i] = ax2.boxplot(globals()["error_vs_full_measure_mode0_segment{0}".format(i)], positions = [i+1.1-0.24], widths = width, patch_artist=True, flierprops=blue_circle, showfliers=False)
        setBoxColors(bp3[i], 'black', '#7bf2da', 'black')

        bp4[i] = ax2.boxplot(globals()["error_vs_full_measure_mode1_segment{0}".format(i)], positions = [i+1.1], widths = width, patch_artist=True, flierprops=peru_circle, showfliers=False)
        setBoxColors(bp4[i], 'black', '#cb9d06', 'black')

        bp5[i] = ax2.boxplot(globals()["error_vs_full_measure_mode2_segment{0}".format(i)], positions = [i+1.1+0.24], widths = width, patch_artist=True, flierprops=red_circle, showfliers=False)
        setBoxColors(bp5[i], 'black', '#bb3f3f', 'black')

    bp3[5] = ax2.boxplot(globals()["error_vs_full_measure_mode0"], positions = [5+1.1-0.24], widths = width, patch_artist=True, flierprops=blue_circle, showfliers=False)
    setBoxColors(bp3[5], 'black', '#7bf2da', 'black')

    bp4[5] = ax2.boxplot(globals()["error_vs_full_measure_mode1"], positions = [5+1.1], widths = width, patch_artist=True, flierprops=blue_circle, showfliers=False)
    setBoxColors(bp4[5], 'black', '#cb9d06', 'black')

    bp5[5] = ax2.boxplot(globals()["error_vs_full_measure_mode2"], positions = [5+1.1+0.24], widths = width, patch_artist=True, flierprops=blue_circle, showfliers=False)
    setBoxColors(bp5[5], 'black', '#bb3f3f', 'black')



    ax2.set_ylim((0, 50))
    ax2.set_xlim((0.5,6.5))

    ax2.set_xticklabels([r'$\mathrm{Torso}$', r'$\mathrm{Shoulder}$', r'$\mathrm{Upper~Arm}$', r'$\mathrm{Fore~Arm}$', r'$\mathrm{Hand}$', r'$\mathrm{All}$'], fontsize=22)

    ax2.set_xticks([1, 2, 3, 4, 5, 6])
    ax2.set_yticks([0,10, 20, 30, 40, 50])
    ax2.set_yticklabels([r'$\mathrm{0}$', r'$\mathrm{10}$', r'$\mathrm{20}$', r'$\mathrm{30}$', r'$\mathrm{40}$', r'$\mathrm{50}$'], fontsize=22)
    ax2.set_ylabel(r'$\mathrm{Deviation~Percentage~from}$'+ '\n' + r'$\mathrm{Full~Measurement~(\%)}$', fontsize=22)

    ax2.legend([bp3[0]["boxes"][0], bp4[0]["boxes"][0], bp5[0]["boxes"][0]], [r'$\mathrm{CPA}$', r'$\mathrm{Motion~Capture}$', r'$\mathrm{Height~Measurement}$'], loc='upper right', ncol=1, fontsize=22)

    ax2.yaxis.grid(True)
    # ax2.text(-80, -6, r'$\mathrm{(a)}$', ha="center", va="center",  fontsize=24)



    # plt.savefig("/home/amir/iros_result_files/seg_len_deviation_percent_mocap.pdf", dpi =150, bbox_inches='tight')
    # plt.savefig("/home/amir/iros_result_files/seg_len_deviation_percent_mocap.png", dpi =150, bbox_inches='tight')

    plt.savefig("/home/amir/iros_result_files/seg_len_deviation_percent_full.pdf", dpi =150, bbox_inches='tight')
    plt.savefig("/home/amir/iros_result_files/seg_len_deviation_percent_full.png", dpi =150, bbox_inches='tight')

    plt.show()
