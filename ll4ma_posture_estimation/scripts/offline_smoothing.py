#!/usr/bin/env python
# license removed for brevity
import rospy
import copy
import math
import numpy as np
from human_model import human_model
from utils.visualization_utils import *
from utils.data_prepration_utils import *
from utils.subject_specific_utils import *
from scipy.optimize import least_squares



class Offline_Smoothing(object):
    """

    """


    def __init__(self, trajectory_sampling, n_joint=10):
        '''

        '''
        #initialize variables
        self.n_points = 4000/trajectory_sampling
        rospy.loginfo(self.n_points)
        self.joint_limits_type = rospy.get_param("joint_limit")
        urdf_file="/home/amir/catkin_ws/src/ergo-hri/ll4ma_posture_estimation/urdf/kdl.urdf.xacro"
        self.human=human_model(urdf_file, rob_desc="robot_description")
        self.n_joints= n_joint

        # setting the neutral posture
        self.neutral_posture_deg=np.array([0, 0, 0, 90, 0, 0, 90, 0, 0, 0])
        self.neutral_posture=np.array([self.neutral_posture_deg[i]*np.pi/180 for i in range(10)])

        #calculating joint limits
        self.joint_limits , self.joint_limits_len = human_joint_limit()

        self.joint_limits_lbs = np.array([self.joint_limits[i][0] for i in range(len(self.joint_limits))])
        self.joint_limits_ubs = np.array([self.joint_limits[i][1] for i in range(len(self.joint_limits))])

        # fixing the problem that x0=netral_posture is not in the joint limit
        if self.joint_limits_type == "modified_torso" or self.joint_limits_type == "subj_specific":
            for i in range(4):
                self.neutral_posture[i] = float(self.joint_limits[i][0] + self.joint_limits[i][1])/2
            # self.prev_solution[3] = self.joint_limits_ubs[3]

        self.initial_solution = []
        for i in range(self.n_points):
            self.initial_solution.append(self.neutral_posture)

        self.initial_solution = np.array(self.initial_solution).flatten()

        self.joint_limits_lbs_traj = []
        self.joint_limits_ubs_traj = []
        for i in range(self.n_points):
            self.joint_limits_lbs_traj.append(self.joint_limits_lbs)
            self.joint_limits_ubs_traj.append(self.joint_limits_ubs)

        self.joint_limits_lbs_traj = np.array(self.joint_limits_lbs_traj).flatten()
        self.joint_limits_ubs_traj= np.array(self.joint_limits_ubs_traj).flatten()


    def optimize(self, observations, _init_solution = []):
        '''

        '''
        def get_residual(postures):
            '''
            '''
            alpha = [50, 50, 50, 10, 10, 10]
            r1 = []
            for j in range(self.n_points):
                x=fk_ls(self.human, postures[j:j+self.n_joints])

                observation = np.array([observations.traj[j].right_stylus.pose.x, observations.traj[j].right_stylus.pose.y,
                observations.traj[j].right_stylus.pose.z, observations.traj[j].right_stylus.pose.theta, observations.traj[j].right_stylus.pose.phi,
                 observations.traj[j].right_stylus.pose.psi])
                for i in range(len(x)):
                    r1.append(alpha[i]*(x[i]- observation[i]))
            for j in range(1,self.n_points):
                for i in range(self.n_joints):
                    r1.append(postures[j+i] - postures[(j-1)+i])
            residual = np.array(r1)

            return residual

        rospy.loginfo("SOLVING ... ")
        if len(_init_solution):
            _initial_solution = _init_solution[0:self.n_points*self.n_joints]

            rospy.loginfo("OK")
            rospy.loginfo(_initial_solution.shape)
        else:
            _initial_solution = self.initial_solution

        rospy.loginfo(self.joint_limits_lbs_traj.shape)
        solution = least_squares(get_residual, _initial_solution, bounds=(self.joint_limits_lbs_traj, self.joint_limits_ubs_traj), method='dogbox')
        postures_result = copy.deepcopy(solution.x).reshape((self.n_points,self.n_joints))

        return postures_result
