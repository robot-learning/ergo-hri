#!/usr/bin/env python
# license removed for brevity
import rospy
import rosbag
import numpy as np
from particle_filtering import ParticleFiltering, fkp
from sensor_msgs.msg import JointState
from ll4ma_posture_estimation.msg import observation, state, particle
import matplotlib.pyplot as plt
from human_model import human_model
from pylab import plot, show, savefig, xlim, figure, \
                hold, ylim, legend, boxplot, setp, axes
from rula import Task, Rula

def setBoxColors(bp, edge_color, fill_color, mid_color):
	for element in ['boxes', 'whiskers', 'fliers', 'caps']:
		plt.setp(bp[element], color=edge_color)
	plt.setp(bp['medians'], color=mid_color)
	for patch in bp['boxes']:
		patch.set(facecolor=fill_color)


def normalize(weights):
    '''
    nomalizing the weight of the particles
    '''
    summation=sum(weights)
    w_normalized=[float(weights[i])/summation for i in range(len(weights))]
    return w_normalized


# def plot_results(error_PFvsMocap_cases, plot_type):
def plot_results(PF_estimates, IK_LS_solutions , mocap , error_PFvsMocap , error_IK_LSvsMocap, PF_estimates_vel , mocap_vel , all_particles, all_weights_before_normalize, plot_type):
# def plot_results(PF_estimates, IK_LS_solutions , offline_smoothing_solutions, mocap , error_PFvsMocap , error_IK_LSvsMocap, error_offline_smoothingvsMocap, PF_estimates_vel , mocap_vel , plot_type):
    '''
    '''
    global seg_len_modes, joint_limit_type
    T = [] # time
    m = 300 # number of points from the results to plot
    # setting the plotting font and size
    plt.rcParams.update({'font.size':26})
    plt.rcParams['text.usetex'] = True
    plt.rcParams['text.latex.preamble'] = [r'\usepackage{amsmath}']

    if plot_type == 0:
        # calculating the error for each joint
        PFvsMocap_errors_vs_joints = [[[],[],[]],[[],[],[]],[[],[],[]],[[],[],[]],[[],[],[]],
        [[],[],[]],[[],[],[]],[[],[],[]],[[],[],[]],[[],[],[]]]    # 10(#joints) x 3(# seg_len_modes) x [#subject x #task]
        IK_LSvsMocap_errors_vs_joints = [[[],[],[]],[[],[],[]],[[],[],[]],[[],[],[]],[[],[],[]],
        [[],[],[]],[[],[],[]],[[],[],[]],[[],[],[]],[[],[],[]]]    # 10(#joints) x 3(# seg_len_modes) x [#subject x #task]
        offline_smoothingvsMocap_errors_vs_joints = [[[],[],[]],[[],[],[]],[[],[],[]],[[],[],[]],[[],[],[]],
        [[],[],[]],[[],[],[]],[[],[],[]],[[],[],[]],[[],[],[]]]    # 10(#joints) x 3(# seg_len_modes) x [#subject x #task]
        for joint in range(10): #joints
            for mode in range(len(seg_len_modes)): # seg_len_modes
                for subj in range(len(error_PFvsMocap[mode])):
                    for task in range(len(error_PFvsMocap[mode][subj])):
                        for t in range(m):
                            # print joint,mode,subj,task,t
                            PFvsMocap_errors_vs_joints[joint][mode].append(error_PFvsMocap[mode][subj][task][joint][t])
                            IK_LSvsMocap_errors_vs_joints[joint][mode].append(error_IK_LSvsMocap[mode][subj][task][joint][t])
                            offline_smoothingvsMocap_errors_vs_joints[joint][mode].append(error_offline_smoothingvsMocap[mode][subj][task][joint][t])

        blue_circle = dict(markerfacecolor='deepskyblue', marker='o') ## TODO: move to above initialization part of the function
        peru_circle = dict(markerfacecolor='olivedrab', marker='o')
        red_circle = dict(markerfacecolor='red', marker='o')

        N = 10 # number of joints
        ind = np.arange(N) # index number of joints as an array

        #setting up the plot ax and fig
        fig,ax = plt.subplots(1, figsize=(25,7))
        width = 0.25


        # initilizing the box plot data
        bp0 = [[],[],[],[],[],[],[],[],[],[]]
        bp1 = [[],[],[],[],[],[],[],[],[],[]]
        bp2 = [[],[],[],[],[],[],[],[],[],[]]

        # adding points to the box plot
        for i in range(10):
            bp0[i] = ax.boxplot(PFvsMocap_errors_vs_joints[i][0], positions = [i+1-0.3], widths = width, patch_artist=True, flierprops=blue_circle, showfliers=False)
            setBoxColors(bp0[i], 'black', 'blue', 'black')

            bp1[i] = ax.boxplot(IK_LSvsMocap_errors_vs_joints[i][0], positions = [i+1], widths = width, patch_artist=True, flierprops=peru_circle, showfliers=False)
            setBoxColors(bp1[i], 'black', 'crimson', 'black')

            bp2[i] = ax.boxplot(offline_smoothingvsMocap_errors_vs_joints[i][0], positions = [i+1+0.3], widths = width, patch_artist=True, flierprops=red_circle, showfliers=False)
            setBoxColors(bp2[i], 'black', 'orange', 'black')

        # setting the axes names, lables and numbers
        ax.set_xticklabels([r'$q_1$', r'$q_2$', r'$q_3$', r'$q_4$', r'$q_5$', r'$q_6$', r'$q_7$',r'$q_8$', r'$q_9$', r'$q_{10}$'], fontsize=32)
        ax.set_yticklabels([r'$\mathrm{0.0}$', r'$\mathrm{0.2}$', r'$\mathrm{0.4}$', r'$\mathrm{0.6}$', r'$\mathrm{0.8}$', r'$\mathrm{1.0}$', r'$\mathrm{1.2}$', r'$\mathrm{1.4}$', r'$\mathrm{1.6}$', r'$\mathrm{1.8}$'], fontsize=32)
        ax.set_xticks([1, 2, 3, 4, 5, 6, 7, 8, 9 ,10])
        ax.set_yticks([0, 0.2, 0.4, 0.6, 0.8, 1, 1.2, 1.4, 1.6, 1.8])
        ax.set_ylabel(r'$\mathrm{Deviation~from}$'+ '\n' + r'$\mathrm{Motion~Capture~(rad)}$', fontsize=32)
        # setting the legend
        # ax.legend([bp0[0]["boxes"][0], bp1[0]["boxes"][0], bp2[0]["boxes"][0]], [r'$\mathrm{CPA}$', r'$\mathrm{Full~Measurement}$', r'$\mathrm{Height~Measurement}$'], loc='upper center', ncol=3, fontsize=28)
        ax.legend([bp0[0]["boxes"][0], bp1[0]["boxes"][0], bp2[0]["boxes"][0]], [r'$\mathrm{Our~approach}$', r'$\mathrm{Online-IK}$', r'$\mathrm{Offline-TrajIK}$'], loc='upper center', ncol=3, fontsize=28)
        # grid on
        ax.yaxis.grid(True)
        ax.set_ylim((0,1.9))
        ax.set_xlim((0.5,10.5))
        # save the plot
        plt.savefig("/home/amir/PF_paper_figures/PF_IKvsMocap_error_all_trials_vs_joints-boxplot_{}.pdf".format(joint_limit_type), dpi =300, bbox_inches='tight')


    if plot_type == 11:
        T=[]
        task1 = Task()
        RULAs = []
        w_expect = []
        w_sigma = []
        print len(all_weights_before_normalize)
        for t in range(400):

            norm_weights = normalize(all_weights_before_normalize[t])
            RULAs.append([])
            for p in range(500):
                Rula1 = Rula(all_particles[t][p],task1)
                RULAs[t].append(Rula1.rula_score())
            w_expect.append(sum([RULAs[t][p] * norm_weights[p] for p in range(500)]))
            w_sigma.append(sum([(RULAs[t][p]-w_expect[t])**2 * norm_weights[p] for p in range(500)]))








        subject = 0 # subj 1
        task = 2   #task3
        m = 400 #number of data points to show the particles for
        # setting the time
        for j in range(m):
            T.append(j*0.1)

        # N = 50
        # x = np.random.rand(N)
        # y = np.random.rand(N)
        # colors = np.random.rand(N)
        # # area = (30 * np.random.rand(N))**2  # 0 to 15 point radii

        plt.figure(1, figsize=(18,6))
        xlim = [0, 40]
        ylim = [2.95, 4]
        y_ticks = [2.8, 3, 3.2, 3.4, 3.6, 3.8, 4.0]

        # left column: no initialization at the neutral posture & full joint joint_limits
        # right column: initialization at the neutral posture & modified joint joint_limits

        ax1 = plt.subplot(1,1,1)
        # calculating the mean and std for the initial particles
        # mean = [np.mean(np.random.uniform(joint_limits[3][0], joint_limits[3][1],500))]  # TODO: get the number of particles from the yaml file
        # std = [np.std(np.random.uniform(joint_limits[3][0], joint_limits[3][1],500))]    # TODO: get the number of particles from the yaml file

        # calculating the mean and std for the rest of the particles
        # for ii in range(m-1):
        #     mean.append(np.mean([all_particles[1][ii][p].theta[3] for p in range(500)]))
        #     std.append(np.std([all_particles[1][ii][p].theta[3] for p in range(500)]))

        # calculating the lower (mean-std) and upper (mean + std) bound for the shaded area
        w_expect_high = [w_expect[i]+w_sigma[i] for i in range(len(w_expect))]
        w_expect_low = [w_expect[i]-w_sigma[i] for i in range(len(w_expect))]

        # plotting the scatter points for particles
        # for p in range(500): # TODO: get the number of particles from the yaml file
        #     plt.scatter(0,np.random.uniform(joint_limits[3][0], joint_limits[3][1]), c = 'blue', s = 10, marker = 'o', edgecolors = 'none', alpha = 0.15)
        #     plt.scatter([T[i]+1 for i in range(m)], [all_particles[1][ii][p].theta[3] for ii in range(m)], c = 'blue', s = 10, marker = 'o', edgecolors = 'none', alpha = 0.15)

        # adding the mean line
        print "T", len(T)
        print "w_expect", len(w_expect)
        plt.plot(T, w_expect, c = 'blue')

        # adding the shaded area between (mean-std) and (mean+std)
        plt.fill_between(T,w_expect_low,w_expect_high, alpha = 0.3, edgecolor = '#CC4F1B', facecolor = '#089FFF')

        # setting the lables, numbers, etc
        ax1.set_ylabel(r'$\mathrm{Expected~RULA~Score}$', fontsize = 28)
        ax1.set_xlabel(r'$\mathrm{Time~(s)}$', fontsize = 28)
        ax1.tick_params(axis = 'both', which = 'major', labelsize = 24)
        ax1.set_yticks(y_ticks)
        ax1.set_ylim(ylim)
        ax1.set_xlim(xlim)
        ax1.grid(axis='y')


        #
        # # Saving the figure
        # plt.savefig("/home/amir/PF_paper_figures/particle_converge_subj_{0}_task_{1}_shade.png".format(1, 3), dpi  = 150,bbox_inches = 'tight')
        plt.savefig("/home/amir/PF_paper_figures/expected_rula.pdf", dpi  = 150,bbox_inches = 'tight')
        plt.show()



    if plot_type == 10:
        # calculating the error for each joint
        PFvsMocap_errors_vs_tasks = [[[],[],[]],[[],[],[]],[[],[],[]],[[],[],[]]]    # 10(#joints) x 3(# seg_len_modes) x [#subject x #task]
        IK_LSvsMocap_errors_vs_tasks = [[[],[],[]],[[],[],[]],[[],[],[]],[[],[],[]]]    # 10(#joints) x 3(# seg_len_modes) x [#subject x #task]
        offline_smoothingvsMocap_errors_vs_tasks = [[[],[],[]],[[],[],[]],[[],[],[]],[[],[],[]]]    # 10(#joints) x 3(# seg_len_modes) x [#subject x #task]
        for task in range(4):
            for mode in range(len(seg_len_modes)): # seg_len_modes
                for subj in range(len(error_PFvsMocap[mode])):
                    for joint in range(10): #joints
                        for t in range(m):
                            PFvsMocap_errors_vs_tasks[task][mode].append(error_PFvsMocap[mode][subj][task][joint][t])
                            IK_LSvsMocap_errors_vs_tasks[task][mode].append(error_IK_LSvsMocap[mode][subj][task][joint][t])
                            offline_smoothingvsMocap_errors_vs_tasks[task][mode].append(error_offline_smoothingvsMocap[mode][subj][task][joint][t])

        blue_circle = dict(markerfacecolor='deepskyblue', marker='o') ## TODO: move to above initialization part of the function
        peru_circle = dict(markerfacecolor='olivedrab', marker='o')
        red_circle = dict(markerfacecolor='red', marker='o')

        N = 4 # number of joints
        ind = np.arange(N) # index number of joints as an array

        #setting up the plot ax and fig
        fig,ax = plt.subplots(1, figsize=(15,4))
        width = 0.4


        # initilizing the box plot data
        bp0 = [[],[],[],[],[],[],[],[],[],[]]
        bp1 = [[],[],[],[],[],[],[],[],[],[]]
        bp2 = [[],[],[],[],[],[],[],[],[],[]]

        # adding points to the box plot
        for i in range(4):
            bp0[i] = ax.boxplot(PFvsMocap_errors_vs_tasks[i][0], positions = [2.5*i+1.2], widths = width, patch_artist=True, flierprops=blue_circle, showfliers=False)
            setBoxColors(bp0[i], 'black', 'blue', 'black')

            bp1[i] = ax.boxplot(IK_LSvsMocap_errors_vs_tasks[i][0], positions = [2.5*i+1.7], widths = width, patch_artist=True, flierprops=peru_circle, showfliers=False)
            setBoxColors(bp1[i], 'black', 'crimson', 'black')

            bp2[i] = ax.boxplot(offline_smoothingvsMocap_errors_vs_tasks[i][0], positions = [2.5*i+2.2], widths = width, patch_artist=True, flierprops=red_circle, showfliers=False)
            setBoxColors(bp2[i], 'black', 'orange', 'black')

        # setting the axes names, lables and numbers
        ax.set_xticklabels([r'$\mathrm{Task~1}$', r'$\mathrm{Task~2}$', r'$\mathrm{Task~3}$', r'$\mathrm{Task~4}$'], fontsize=28)
        ax.set_yticklabels([r'$\mathrm{0.0}$', r'$\mathrm{0.2}$', r'$\mathrm{0.4}$', r'$\mathrm{0.6}$', r'$\mathrm{0.8}$', r'$\mathrm{1.0}$', r'$\mathrm{1.2}$', r'$\mathrm{1.4}$', r'$\mathrm{1.6}$', r'$\mathrm{1.8}$'], fontsize=32)
        ax.set_xticks([1.7, 4.2, 6.7, 9.2])
        ax.set_yticks([0, 0.2, 0.4, 0.6, 0.8, 1, 1.2, 1.4, 1.6, 1.8])
        ax.set_ylabel(r'$\mathrm{Deviation~from}$'+ '\n' + r'$\mathrm{Motion~Capture~(rad)}$', fontsize=28)
        # setting the legend
        # ax.legend([bp0[0]["boxes"][0], bp1[0]["boxes"][0], bp2[0]["boxes"][0]], [r'$\mathrm{CPA}$', r'$\mathrm{Full~Measurement}$', r'$\mathrm{Height~Measurement}$'], loc='upper center', ncol=3, fontsize=28)
        ax.legend([bp0[0]["boxes"][0], bp1[0]["boxes"][0], bp2[0]["boxes"][0]], [r'$\mathrm{Our~approach}$', r'$\mathrm{Online-IK}$', r'$\mathrm{Offline-TrajIK'], loc='upper center', ncol=3, fontsize=24)
        # grid on
        ax.yaxis.grid(True)
        ax.set_ylim((0,0.7))
        ax.set_xlim((0.5,10.5))
        # save the plot
        plt.savefig("/home/amir/PF_paper_figures/PF_IKvsMocap_error_all_trials_vs_tasks-boxplot_{}.pdf".format(joint_limit_type), dpi =300, bbox_inches='tight')


    #*********************************** PFvsMocap error for all trials vs joints - box plot ********************************************
    elif  plot_type == 1:
        # calculating the error for each joint
        PFvsMocap_errors_vs_joints = [[[],[],[]],[[],[],[]],[[],[],[]],[[],[],[]],[[],[],[]],
        [[],[],[]],[[],[],[]],[[],[],[]],[[],[],[]],[[],[],[]]]    # 10(#joints) x 3(# seg_len_modes) x [#subject x #task]
        IK_LSvsMocap_errors_vs_joints = [[[],[],[]],[[],[],[]],[[],[],[]],[[],[],[]],[[],[],[]],
        [[],[],[]],[[],[],[]],[[],[],[]],[[],[],[]],[[],[],[]]]    # 10(#joints) x 3(# seg_len_modes) x [#subject x #task]
        for joint in range(10): #joints
            for mode in range(len(seg_len_modes)): # seg_len_modes
                for subj in range(len(error_PFvsMocap[mode])):
                    for task in range(len(error_PFvsMocap[mode][subj])):
                        for t in range(m):
                            PFvsMocap_errors_vs_joints[joint][mode].append(error_PFvsMocap[mode][subj][task][joint][t])
                            IK_LSvsMocap_errors_vs_joints[joint][mode].append(error_IK_LSvsMocap[mode][subj][task][joint][t])

        blue_circle = dict(markerfacecolor='deepskyblue', marker='o') ## TODO: move to above initialization part of the function
        peru_circle = dict(markerfacecolor='olivedrab', marker='o')
        red_circle = dict(markerfacecolor='red', marker='o')

        N = 10 # number of joints
        ind = np.arange(N) # index number of joints as an array

        #setting up the plot ax and fig
        fig,ax = plt.subplots(1, figsize=(25,7))
        width = 0.25


        # initilizing the box plot data
        bp0 = [[],[],[],[],[],[],[],[],[],[]]
        bp1 = [[],[],[],[],[],[],[],[],[],[]]
        # bp2 = [[],[],[],[],[],[],[],[],[],[]]

        # adding points to the box plot
        for i in range(10):
            bp0[i] = ax.boxplot(PFvsMocap_errors_vs_joints[i][0], positions = [i+1-0.15], widths = width, patch_artist=True, flierprops=blue_circle, showfliers=False)
            setBoxColors(bp0[i], 'black', 'blue', 'black')

            bp1[i] = ax.boxplot(IK_LSvsMocap_errors_vs_joints[i][0], positions = [i+1+0.15], widths = width, patch_artist=True, flierprops=peru_circle, showfliers=False)
            setBoxColors(bp1[i], 'black', 'crimson', 'black')

            # bp2[i] = ax.boxplot(PFvsMocap_errors_vs_joints[i][2], positions = [i+1+0.24], widths = width, patch_artist=True, flierprops=red_circle, showfliers=False)
            # setBoxColors(bp2[i], 'black', 'orange', 'black')

        # setting the axes names, lables and numbers
        ax.set_xticklabels([r'$q_1$', r'$q_2$', r'$q_3$', r'$q_4$', r'$q_5$', r'$q_6$', r'$q_7$',r'$q_8$', r'$q_9$', r'$q_{10}$'], fontsize=32)
        ax.set_yticklabels([r'$\mathrm{0.0}$', r'$\mathrm{0.2}$', r'$\mathrm{0.4}$', r'$\mathrm{0.6}$', r'$\mathrm{0.8}$', r'$\mathrm{1.0}$', r'$\mathrm{1.2}$', r'$\mathrm{1.4}$', r'$\mathrm{1.6}$', r'$\mathrm{1.8}$'], fontsize=32)
        ax.set_xticks([1, 2, 3, 4, 5, 6, 7, 8, 9 ,10])
        ax.set_yticks([0, 0.2, 0.4, 0.6, 0.8, 1, 1.2, 1.4, 1.6, 1.8])
        ax.set_ylabel(r'$\mathrm{Deviation~from~Motion~Capture~(rad)}$', fontsize=32)
        # setting the legend
        # ax.legend([bp0[0]["boxes"][0], bp1[0]["boxes"][0], bp2[0]["boxes"][0]], [r'$\mathrm{CPA}$', r'$\mathrm{Full~Measurement}$', r'$\mathrm{Height~Measurement}$'], loc='upper center', ncol=3, fontsize=28)
        ax.legend([bp0[0]["boxes"][0], bp1[0]["boxes"][0]], [r'$\mathrm{PF}$', r'$\mathrm{IK}$'], loc='upper center', ncol=2, fontsize=28)
        # grid on
        ax.yaxis.grid(True)
        ax.set_ylim((0,1.9))
        ax.set_xlim((0.5,10.5))
        # save the plot
        plt.savefig("/home/amir/PF_paper_figures/PF_IKvsMocap_error_all_trials_vs_joints-boxplot_{}.pdf".format(joint_limit_type), dpi =300, bbox_inches='tight')

# #*********************************** PFvsMocap error for all trials vs tasks - box plot ********************************************
    elif plot_type == 2:
        # calculating the error for each task
        PFvsMocap_errors_vs_tasks = [[[],[],[]],[[],[],[]],[[],[],[]],[[],[],[]]]
        IK_LSvsMocap_errors_vs_joints = [[[],[],[]],[[],[],[]],[[],[],[]],[[],[],[]]]

        for joint in range(10):
            for mode in range(len(seg_len_modes)):
                for subj in range(len(error_PFvsMocap[mode])):
                    for task in range(len(error_PFvsMocap[mode][subj])):
                        for t in range(m):
                            PFvsMocap_errors_vs_tasks[task][mode].append(error_PFvsMocap[mode][subj][task][joint][t])
                            IK_LSvsMocap_errors_vs_joints[task][mode].append(error_IK_LSvsMocap[mode][subj][task][joint][t])

        # TODO: do the above for IK_error

        blue_circle = dict(markerfacecolor='deepskyblue', marker='o') ## TODO: move to above initialization part of the function
        peru_circle = dict(markerfacecolor='olivedrab', marker='o')
        red_circle = dict(markerfacecolor='red', marker='o')

        N = 10 # number of joints
        ind = np.arange(N) # index number of joints as an array

        #setting up the plot ax and fig
        fig,ax = plt.subplots(1, figsize=(13,4))
        width = 0.25


        # initilizing the box plot data
        bp3 = [[],[],[]]
        bp4 = [[],[],[]]
        # bp5 = [[],[],[],[]]

        # adding points to the box plot
        for i in range(3):
            bp3[i] = ax.boxplot(PFvsMocap_errors_vs_tasks[i][0], positions = [1*i+1-0.15], widths = width, patch_artist=True, flierprops=blue_circle, showfliers=False)
            setBoxColors(bp3[i], 'black', 'blue', 'black')

            bp4[i] = ax.boxplot(IK_LSvsMocap_errors_vs_joints[i][0], positions = [1*i+1.15], widths = width, patch_artist=True, flierprops=peru_circle, showfliers=False)
            setBoxColors(bp4[i], 'black', 'crimson', 'black')

            # bp5[i] = ax.boxplot(PFvsMocap_errors_vs_tasks[i][2], positions = [1.5*i+1+0.24], widths = width, patch_artist=True, flierprops=red_circle, showfliers=False)
            # setBoxColors(bp5[i], 'black', 'orange', 'black')

        # setting the axes names, lables and numbers
        ax.set_xticklabels([r'$\mathrm{Task~1}$', r'$\mathrm{Task~2}$', r'$\mathrm{Task~3}$', r'$\mathrm{Task~4}$'], fontsize=26)
        ax.set_yticklabels([r'$\mathrm{0.0}$', r'$\mathrm{0.2}$', r'$\mathrm{0.4}$', r'$\mathrm{0.6}$', r'$\mathrm{0.8}$', r'$\mathrm{1}$'], fontsize=26)
        ax.set_xticks([1, 2, 3])
        ax.set_yticks([0, 0.2, 0.4, 0.6, 0.8, 1])
        ax.set_ylabel(r'$\mathrm{Deviation~from}$'+ '\n' + r'$\mathrm{Motion~Capture~(rad)}$', fontsize=28)
        ax.set_ylim((0,1))
        ax.set_xlim((0.5,3.5))
        # setting the legend
        # ax.legend([bp3[0]["boxes"][0], bp4[0]["boxes"][0], bp5[0]["boxes"][0]], [r'$\mathrm{CPA}$', r'$\mathrm{Full~Measurement}$', r'$\mathrm{Height~Measurement}$'], loc='upper center', ncol=3, fontsize=22)
        ax.legend([bp3[0]["boxes"][0], bp4[0]["boxes"][0]], [r'$\mathrm{PF}$', r'$\mathrm{IK}$'], loc='upper center', ncol=2, fontsize=22)

        # grid on
        ax.yaxis.grid(True)
        # saving the plot figure
        plt.savefig("/home/amir/PF_paper_figures/PF_IKvsMocap_error_all_trials_vs_tasks-boxplot_{}.pdf".format(joint_limit_type), dpi =300, bbox_inches='tight')
    # ------------------------------------- particles convergence vs time - scatter plot ----------------------------
    elif plot_type == 3:

        # set subject number and task to the ones that we used for showing particle convergence
        subject=0 # subj 1
        task=2   #task3
        m = 80 #number of data points to show the particles for
        # setting the time
        for j in range(m):
            T.append(j*1)

        # setting the joint limits and neutral posture for plotting the scatter of initial particlesself
        # because the initial particles is not in the results  (writing to the result bag file happens after initialzing, process and observation update)
        ## TODO fix the above issue
        neutral_posture_deg=[0, 0, 0, 90, 0, 0, 90, 0, 0, 0]
        neutral_posture=[neutral_posture_deg[i]*np.pi/180 for i in range(10)]
        joint_limits_deg = [[5, 10], [-2, 2], [-5,5], [-90, 135], [-90, 90], [-45, 135], [0, 150], [-180, 90], [-30, 20], [-70, 80]]
        joint_limits = [[joint_limits_deg[i][0]*np.pi/180, joint_limits_deg[i][1]*np.pi/180] for i in range(len(joint_limits_deg))]
        joint_limits_len=[joint_limits[i][1]-joint_limits[i][0] for i in range(10)]

        N = 50
        x = np.random.rand(N)
        y = np.random.rand(N)
        colors = np.random.rand(N)
        area = (30 * np.random.rand(N))**2  # 0 to 15 point radii

        plt.figure(1, figsize=(18,12))
        y_lim = [-3.14, 3.14]
        y_tick = [-3,0,3]
        x_tick = []
        # left column: no initialization at the neutral posture & full joint joint_limits
        # right column: initialization at the neutral posture & modified joint joint_limits
        ax1 = plt.subplot(7,2,1)
        for p in range(500): # number of particles TODO: get it from the yaml file
            # initial particles
            plt.scatter(0,np.random.uniform(joint_limits[3][0], joint_limits[3][1]), c='blue', s=10, marker='o', edgecolors='none', alpha=0.15)
            # rest of the particles
            plt.scatter([T[i]+1 for i in range(m)], [all_particles[1][ii][p].theta[3] for ii in range(m)], c='blue', s=10, marker='o', edgecolors='none', alpha=0.15)
        # setting the plot limits and lables
        ax1.set_ylim(y_lim)
        ax1.set_ylabel(r'$\theta_4 (rad)$', fontsize=24)
        ax1.tick_params(axis='both', which='major', labelsize=20)
        ax1.set_yticks(y_tick)
        ax1.set_xticks(x_tick)

        ax2 = plt.subplot(7,2,2)
        for p in range(500):
            plt.scatter(0, np.random.normal(neutral_posture[3], 0.2*joint_limits_len[3]), c='red', s=10, marker='o', edgecolors='none', alpha=0.15)
            plt.scatter([T[i]+1 for i in range(m)], [all_particles[0][ii][p].theta[3] for ii in range(m)], c='red', s=10, marker='o', edgecolors='none', alpha=0.15)
        # setting the plot limits and lables
        ax2.set_ylim(y_lim)
        ax2.set_ylabel(r'$\theta_4 (rad)$', fontsize=24)
        ax2.tick_params(axis='both', which='major', labelsize=20)
        ax2.set_yticks(y_tick)
        ax2.set_xticks(x_tick)


        ax3 = plt.subplot(7,2,3)
        for p in range(500):
            plt.scatter(0,np.random.uniform(joint_limits[4][0], joint_limits[4][1]), s=10, marker='o', edgecolors='none', alpha=0.15)
            plt.scatter([T[i]+1 for i in range(m)], [all_particles[1][ii][p].theta[4] for ii in range(m)], c='blue', s=10, marker='o', edgecolors='none', alpha=0.15)
        # setting the plot limits and lables
        ax3.set_ylim(y_lim)
        ax3.set_ylabel(r'$\theta_5 (rad)$', fontsize=24)
        ax3.tick_params(axis='both', which='major', labelsize=20)
        ax3.set_yticks(y_tick)
        ax3.set_xticks(x_tick)

        ax4 = plt.subplot(7,2,4)
        for p in range(500):
            plt.scatter(0, np.random.normal(neutral_posture[4], 0.2*joint_limits_len[4]), c='red', s=10, marker='o', edgecolors='none', alpha=0.15)
            plt.scatter([T[i]+1 for i in range(m)], [all_particles[0][ii][p].theta[4] for ii in range(m)], c='red', s=10, marker='o', edgecolors='none', alpha=0.15)
        # setting the plot limits and lables
        ax4.set_ylim(y_lim)
        ax4.set_ylabel(r'$\theta_5 (rad)$', fontsize=24)
        ax4.tick_params(axis='both', which='major', labelsize=20)
        ax4.set_yticks(y_tick)
        ax4.set_xticks(x_tick)

        ax5 = plt.subplot(7,2,5)
        for p in range(500):
            plt.scatter(0,np.random.uniform(joint_limits[5][0], joint_limits[5][1]), c='blue', s=10, marker='o', edgecolors='none', alpha=0.15)
            plt.scatter([T[i]+1 for i in range(m)], [all_particles[1][ii][p].theta[5] for ii in range(m)], c='blue', s=10, marker='o', edgecolors='none', alpha=0.15)
        # setting the plot limits and lables
        ax5.set_ylim(y_lim)
        ax5.set_ylabel(r'$\theta_6 (rad)$', fontsize=24)
        ax5.tick_params(axis='both', which='major', labelsize=20)
        ax5.set_yticks(y_tick)
        ax5.set_xticks(x_tick)

        ax6 = plt.subplot(7,2,6)
        for p in range(500):
            plt.scatter(0, np.random.normal(neutral_posture[5], 0.2*joint_limits_len[5]), c='red', s=10, marker='o', edgecolors='none', alpha=0.15)
            plt.scatter([T[i]+1 for i in range(m)], [all_particles[0][ii][p].theta[5] for ii in range(m)], c='red', s=10, marker='o', edgecolors='none', alpha=0.15)
        # setting the plot limits and lables
        ax6.set_ylim(y_lim)
        ax6.set_ylabel(r'$\theta_6 (rad)$', fontsize=24)
        ax6.tick_params(axis='both', which='major', labelsize=20)
        ax6.set_yticks(y_tick)
        ax6.set_xticks(x_tick)

        ax7 = plt.subplot(7,2,7)
        for p in range(500):
            plt.scatter(0,np.random.uniform(joint_limits[6][0], joint_limits[6][1]), c='blue', s=10, marker='o', edgecolors='none', alpha=0.15)
            plt.scatter([T[i]+1 for i in range(m)], [all_particles[1][ii][p].theta[6] for ii in range(m)], c='blue', s=10, marker='o', edgecolors='none', alpha=0.15)
        # setting the plot limits and lables
        ax7.set_ylim(y_lim)
        ax7.set_ylabel(r'$\theta_7 (rad)$', fontsize=24)
        ax7.tick_params(axis='both', which='major', labelsize=20)
        ax7.set_yticks(y_tick)
        ax7.set_xticks(x_tick)


        ax8 = plt.subplot(7,2,8)
        for p in range(500):
            plt.scatter(0, np.random.normal(neutral_posture[6], 0.2*joint_limits_len[6]), c='red', s=10, marker='o', edgecolors='none', alpha=0.15)
            plt.scatter([T[i]+1 for i in range(m)], [all_particles[0][ii][p].theta[6] for ii in range(m)], c='red', s=10, marker='o', edgecolors='none', alpha=0.15)
        # setting the plot limits and lables
        ax8.set_ylim(y_lim)
        ax8.set_ylabel(r'$\theta_7 (rad)$', fontsize=24)
        ax8.tick_params(axis='both', which='major', labelsize=20)
        ax8.set_yticks(y_tick)
        ax8.set_xticks(x_tick)

        ax9 = plt.subplot(7,2,9)
        for p in range(500):
            plt.scatter(0,np.random.uniform(joint_limits[7][0], joint_limits[7][1]), c='blue', s=10, marker='o', edgecolors='none', alpha=0.15)
            plt.scatter([T[i]+1 for i in range(m)], [all_particles[1][ii][p].theta[7] for ii in range(m)], c='blue', s=10, marker='o', edgecolors='none', alpha=0.15)
        # setting the plot limits and lables
        ax9.set_ylim(y_lim)
        ax9.set_ylabel(r'$\theta_8 (rad)$', fontsize=24)
        ax9.tick_params(axis='both', which='major', labelsize=20)
        ax9.set_yticks(y_tick)
        ax9.set_xticks(x_tick)

        ax10 = plt.subplot(7,2,10)
        for p in range(500):
            plt.scatter(0, np.random.normal(neutral_posture[7], 0.2*joint_limits_len[7]), c='red', s=10, marker='o', edgecolors='none', alpha=0.15)
            plt.scatter([T[i]+1 for i in range(m)], [all_particles[0][ii][p].theta[7] for ii in range(m)], c='red', s=10, marker='o', edgecolors='none', alpha=0.15)
        # setting the plot limits and lables
        ax10.set_ylim(y_lim)
        ax10.set_ylabel(r'$\theta_8 (rad)$', fontsize=24)
        ax10.tick_params(axis='both', which='major', labelsize=20)
        ax10.set_yticks(y_tick)
        ax10.set_xticks(x_tick)

        ax11 = plt.subplot(7,2,11)
        for p in range(500):
            plt.scatter(0,np.random.uniform(joint_limits[8][0], joint_limits[8][1]), c='blue', s=10, marker='o', edgecolors='none', alpha=0.15)
            plt.scatter([T[i]+1 for i in range(m)], [all_particles[1][ii][p].theta[8] for ii in range(m)], c='blue', s=10, marker='o', edgecolors='none', alpha=0.15)
        # setting the plot limits and lables
        ax11.set_ylim(y_lim)
        ax11.set_ylabel(r'$\theta_9 (rad)$', fontsize=24)
        ax11.tick_params(axis='both', which='major', labelsize=20)
        ax11.set_yticks(y_tick)
        ax11.set_xticks(x_tick)

        ax12 = plt.subplot(7,2,12)
        for p in range(500):
            plt.scatter(0, np.random.normal(neutral_posture[8], 0.2*joint_limits_len[8]), c='red', s=10, marker='o', edgecolors='none', alpha=0.15)
            plt.scatter([T[i]+1 for i in range(m)], [all_particles[0][ii][p].theta[8] for ii in range(m)], c='red', s=10, marker='o', edgecolors='none', alpha=0.15)
        # setting the plot limits and lables
        ax12.set_ylim(y_lim)
        ax12.set_ylabel(r'$\theta_9 (rad)$', fontsize=24)
        ax12.tick_params(axis='both', which='major', labelsize=20)
        ax12.set_yticks(y_tick)
        ax12.set_xticks(x_tick)

        ax13 = plt.subplot(7,2,13)
        for p in range(500):
            plt.scatter(0,np.random.uniform(joint_limits[9][0], joint_limits[9][1]), c='blue', s=10, marker='o', edgecolors='none', alpha=0.15)
            plt.scatter([T[i]+1 for i in range(m)], [all_particles[1][ii][p].theta[3] for ii in range(m)], c='blue',s=10, marker='o', edgecolors='none', alpha=0.15)
        # setting the plot limits and lables
        ax13.set_ylim(y_lim)
        ax13.set_ylabel(r'$\theta_{10} (rad)$', fontsize=24)
        ax13.tick_params(axis='both', which='major', labelsize=20)
        ax13.set_xlabel(r'$\mathrm{Time~Steps}$', fontsize=24)
        ax13.set_yticks(y_tick)
        # ax13.set_xticks(x_tick)

        ax14 = plt.subplot(7,2,14)
        for p in range(500):
            plt.scatter(0, np.random.normal(neutral_posture[9], 0.2*joint_limits_len[9]), c='red', s=10, marker='o', edgecolors='none', alpha=0.15)
            plt.scatter([T[i]+1 for i in range(m)], [all_particles[0][ii][p].theta[3] for ii in range(m)], c='red',s=10, marker='o', edgecolors='none', alpha=0.15)
        # setting the plot limits and lables
        ax14.set_ylim(y_lim)
        ax14.tick_params(axis='both', which='major', labelsize=20)
        ax14.set_ylabel(r'$\theta_{10} (rad)$', fontsize=24)
        ax14.set_xlabel(r'$\mathrm{Time~Steps}$', fontsize=24)
        ax14.set_yticks(y_tick)
        # ax14.set_xticks(x_tick)

        # add the caption for columns
        plt.text(-80, 50, r'$\mathrm{(a)}$', ha="center", va="center",  fontsize=24)
        plt.text(40, 50, r'$\mathrm{(b)}$', ha="center", va="center",  fontsize=24)

        # Saving the plot
        plt.savefig("/home/amir/PF_paper_figures/particle_converge_subj_{0}_task_{1}.png".format(1, 3), dpi =150,bbox_inches='tight')
        plt.savefig("/home/amir/PF_paper_figures/particle_converge_subj_{0}_task_{1}.pdf".format(1, 3), dpi =150,bbox_inches='tight')

    # ------------------------------------- particles convergence vs time - shaded scatter plot ----------------------------
    elif plot_type == 4:
        # set subject number and task to the ones that we used for showing particle convergence
        subject = 0 # subj 1
        task = 2   #task3
        m = 70 #number of data points to show the particles for
        # setting the time
        for j in range(m):
            T.append(j*1)

        # setting the joint limits and neutral posture for plotting the scatter of initial particlesself
        # because the initial particles is not in the results  (writing to the result bag file happens after initialzing, process and observation update)
        ## TODO fix the above issue


        neutral_posture_deg = [0, 0, 0, 90, 0, 0, 90, 0, 0, 0]
        neutral_posture = [neutral_posture_deg[i]*np.pi/180 for i in range(10)]
        joint_limits_deg = [[5, 10], [-2, 2], [-5,5], [-90, 135], [-90, 90], [-45, 135], [0, 150], [-180, 90], [-30, 20], [-70, 80]]
        joint_limits = [[joint_limits_deg[i][0]*np.pi/180, joint_limits_deg[i][1]*np.pi/180] for i in range(len(joint_limits_deg))]
        joint_limits_len = [joint_limits[i][1]-joint_limits[i][0] for i in range(10)]

        N = 50
        x = np.random.rand(N)
        y = np.random.rand(N)
        colors = np.random.rand(N)
        area = (30 * np.random.rand(N))**2  # 0 to 15 point radii

        plt.figure(1, figsize=(9,6))
        xlim = [-2, 72]
        ylim = [-2, 3.14]
        y_ticks = [-2, -1, 0, 1, 2, 3]

        # left column: no initialization at the neutral posture & full joint joint_limits
        # right column: initialization at the neutral posture & modified joint joint_limits

        ax1 = plt.subplot(2,1,1)
        # calculating the mean and std for the initial particles
        mean = [np.mean(np.random.uniform(joint_limits[3][0], joint_limits[3][1],500))]  # TODO: get the number of particles from the yaml file
        std = [np.std(np.random.uniform(joint_limits[3][0], joint_limits[3][1],500))]    # TODO: get the number of particles from the yaml file

        # calculating the mean and std for the rest of the particles
        for ii in range(m-1):
            mean.append(np.mean([all_particles[1][ii][p].theta[3] for p in range(500)]))
            std.append(np.std([all_particles[1][ii][p].theta[3] for p in range(500)]))

        # calculating the lower (mean-std) and upper (mean + std) bound for the shaded area
        mean_high = [mean[i]+std[i] for i in range(len(mean))]
        mean_low = [mean[i]-std[i] for i in range(len(mean))]

        # plotting the scatter points for particles
        for p in range(500): # TODO: get the number of particles from the yaml file
            plt.scatter(0,np.random.uniform(joint_limits[3][0], joint_limits[3][1]), c = 'blue', s = 10, marker = 'o', edgecolors = 'none', alpha = 0.15)
            plt.scatter([T[i]+1 for i in range(m)], [all_particles[1][ii][p].theta[3] for ii in range(m)], c = 'blue', s = 10, marker = 'o', edgecolors = 'none', alpha = 0.15)

        # adding the mean line
        plt.plot(T, mean, c = 'blue')

        # adding the shaded area between (mean-std) and (mean+std)
        plt.fill_between(T,mean_low,mean_high, alpha = 0.3, edgecolor = '#CC4F1B', facecolor = '#089FFF')

        # setting the lables, numbers, etc
        ax1.set_ylabel(r'$q_4~(rad)$', fontsize = 24)
        ax1.tick_params(axis = 'both', which = 'major', labelsize = 20)
        ax1.set_yticks(y_ticks)
        ax1.set_ylim(ylim)
        ax1.set_xlim(xlim)


        ax2 = plt.subplot(2,1,2)
        # calculating the mean and std for the initial particles
        mean = [np.mean(np.random.uniform(joint_limits[3][0], joint_limits[3][1],500))] # TODO: get the number of particles from the yaml file
        std = [np.std(np.random.uniform(joint_limits[3][0], joint_limits[3][1],500))] # TODO: get the number of particles from the yaml file

        # calculating the mean and std for the rest of the particles
        for ii in range(m-1):
            mean.append(np.mean([all_particles[0][ii][p].theta[3] for p in range(500)])) # TODO: get the number of particles from the yaml file
            std.append(np.std([all_particles[0][ii][p].theta[3] for p in range(500)])) # TODO: get the number of particles from the yaml file

        # calculating the lower (mean-std) and upper (mean + std) bound for the shaded area
        mean_high = [mean[i]+std[i] for i in range(len(mean))]
        mean_low = [mean[i]-std[i] for i in range(len(mean))]

        # plotting the scatter points for particles
        for p in range(500):
            plt.scatter(0, np.random.normal(neutral_posture[3], 0.2*joint_limits_len[3]), c = 'red', s = 10, marker = 'o', edgecolors = 'none', alpha = 0.15)
            plt.scatter([T[i]+1 for i in range(m)], [all_particles[0][ii][p].theta[3] for ii in range(m)], c = 'red', s = 10, marker = 'o', edgecolors = 'none', alpha = 0.15)

        # adding the mean line
        plt.plot(T, mean, c = 'red')

        # adding the shaded area between (mean-std) and (mean+std)
        plt.fill_between(T,mean_low,mean_high, alpha = 0.3, edgecolor = '#CC4F1B', facecolor = '#fc5a50')

        # setting the lables, numbers, etc
        ax2.set_ylim(ylim)
        ax2.set_ylabel(r'$q_4~(rad)$', fontsize = 24)
        ax2.tick_params(axis = 'both', which = 'major', labelsize = 20)
        ax2.set_yticks(y_ticks)
        ax2.set_xlim(xlim)
        ax2.set_xlabel(r'$\mathrm{Time~Steps}$', fontsize = 24)

        # Saving the figure
        plt.savefig("/home/amir/PF_paper_figures/particle_converge_subj_{0}_task_{1}_shade.png".format(1, 3), dpi  = 150,bbox_inches = 'tight')
        plt.savefig("/home/amir/PF_paper_figures/particle_converge_subj_{0}_task_{1}_shade.pdf".format(1, 3), dpi  = 150,bbox_inches = 'tight')

    # ----------------------------- PFvsMocap error for each subject each task each joint vs time - line plot ----------------------------
    elif plot_type == 5:

        i = 1    # number of first figure

        for subj in range(num_subjects):
            for task in range(num_correct_tasks[subj]):
                i += 1

                # setting the time
                for j in range(m):
                    T.append(j*0.1)

                plt.figure(i, figsize = (25,15))

                #-----------------------------------------#
                ax1 = plt.subplot(5,2,1)

                # plotting the data for each subplot (each joint)
                plt.plot(T[0:m], error_PFvsMocap[0][subj][task][0][0:m], 'r', linewidth = 2.0)
                plt.plot(T[0:m], error_PFvsMocap[1][subj][task][0][0:m], 'g', linewidth = 2.0)
                plt.plot(T[0:m], error_PFvsMocap[2][subj][task][0][0:m], 'b', linewidth = 2.0)

                #setting the plot limits and lables
                ax1.set_ylim([-1, 1])
                ax1.set_ylabel(r'$\theta_1 (rad)$', fontsize = 28)
                #-----------------------------------------#

                ax2 = plt.subplot(5,2,2)

                # plotting the data for each subplot (each joint)
                plt.plot(T[0:m], error_PFvsMocap[0][subj][task][1][0:m], 'r', linewidth = 2.0, label = r'$\mathrm{CPA}$')
                plt.plot(T[0:m], error_PFvsMocap[1][subj][task][1][0:m], 'g', linewidth = 2.0, label = r'$\mathrm{Full~Measurement}$')
                plt.plot(T[0:m], error_PFvsMocap[2][subj][task][1][0:m], 'b', linewidth = 2.0, label = r'$\mathrm{Height~Measurement}$')

                #setting the plot limits and lables
                ax2.set_ylim([-1, 1])
                ax2.set_ylabel(r'$\theta_2 (rad)$', fontsize = 28)
                #-----------------------------------------#

                plt.legend(loc = 'upper center',ncol = 3, bbox_to_anchor = (-0.1, 1.6), prop = {'size': 28})
                #-----------------------------------------#

                ax3 = plt.subplot(5,2,3)

                # plotting the data for each subplot (each joint)
                plt.plot(T[0:m], error_PFvsMocap[0][subj][task][2][0:m], 'r', linewidth = 2.0)
                plt.plot(T[0:m], error_PFvsMocap[1][subj][task][2][0:m], 'g', linewidth = 2.0)
                plt.plot(T[0:m], error_PFvsMocap[2][subj][task][2][0:m], 'b', linewidth = 2.0)

                #setting the plot limits and lables
                ax3.set_ylim([-1, 1])
                ax3.set_ylabel(r'$\theta_3 (rad)$', fontsize = 28)
                #-----------------------------------------#

                ax4 = plt.subplot(5,2,4)

                # plotting the data for each subplot (each joint)
                plt.plot(T[0:m], error_PFvsMocap[0][subj][task][3][0:m], 'r', linewidth = 2.0)
                plt.plot(T[0:m], error_PFvsMocap[1][subj][task][3][0:m], 'g', linewidth = 2.0)
                plt.plot(T[0:m], error_PFvsMocap[2][subj][task][3][0:m], 'b', linewidth = 2.0)

                #setting the plot limits and lables
                ax4.set_ylim([-1, 1])
                ax4.set_ylabel(r'$\theta_4 (rad)$', fontsize = 28)
                #-----------------------------------------#

                ax5 = plt.subplot(5,2,5)

                # plotting the data for each subplot (each joint)
                plt.plot(T[0:m], error_PFvsMocap[0][subj][task][4][0:m], 'r', linewidth = 2.0)
                plt.plot(T[0:m], error_PFvsMocap[1][subj][task][4][0:m], 'g', linewidth = 2.0)
                plt.plot(T[0:m], error_PFvsMocap[2][subj][task][4][0:m], 'b', linewidth = 2.0)

                #setting the plot limits and lables
                ax5.set_ylim([-1, 1])
                ax5.set_ylabel(r'$\theta_5 (rad)$', fontsize = 28)
                #-----------------------------------------#

                ax6 = plt.subplot(5,2,6)

                # plotting the data for each subplot (each joint)
                plt.plot(T[0:m], error_PFvsMocap[0][subj][task][5][0:m], 'r', linewidth = 2.0)
                plt.plot(T[0:m], error_PFvsMocap[1][subj][task][5][0:m], 'g', linewidth = 2.0)
                plt.plot(T[0:m], error_PFvsMocap[2][subj][task][5][0:m], 'b', linewidth = 2.0)

                #setting the plot limits and lables
                ax6.set_ylim([-1, 1])
                ax6.set_ylabel(r'$\theta_6 (rad)$', fontsize = 28)
                #-----------------------------------------#

                ax7 = plt.subplot(5,2,7)

                # plotting the data for each subplot (each joint)
                plt.plot(T[0:m], error_PFvsMocap[0][subj][task][6][0:m], 'r', linewidth = 2.0)
                plt.plot(T[0:m], error_PFvsMocap[1][subj][task][6][0:m], 'g', linewidth = 2.0)
                plt.plot(T[0:m], error_PFvsMocap[2][subj][task][6][0:m], 'b', linewidth = 2.0)

                #setting the plot limits and lables
                ax7.set_ylim([-1, 1])
                ax7.set_ylabel(r'$\theta_7 (rad)$', fontsize = 28)
                #-----------------------------------------#

                ax8 = plt.subplot(5,2,8)

                # plotting the data for each subplot (each joint)
                plt.plot(T[0:m], error_PFvsMocap[0][subj][task][7][0:m], 'r', linewidth = 2.0)
                plt.plot(T[0:m], error_PFvsMocap[1][subj][task][7][0:m], 'g', linewidth = 2.0)
                plt.plot(T[0:m], error_PFvsMocap[2][subj][task][7][0:m], 'b', linewidth = 2.0)

                #setting the plot limits and lables
                ax8.set_ylim([-1, 1])
                ax8.set_ylabel(r'$\theta_8 (rad)$', fontsize = 28)
                #-----------------------------------------#

                ax9 = plt.subplot(5,2,9)

                # plotting the data for each subplot (each joint)
                plt.plot(T[0:m], error_PFvsMocap[0][subj][task][8][0:m], 'r', linewidth = 2.0)
                plt.plot(T[0:m], error_PFvsMocap[1][subj][task][8][0:m], 'g', linewidth = 2.0)
                plt.plot(T[0:m], error_PFvsMocap[2][subj][task][8][0:m], 'b', linewidth = 2.0)
                # plt.plot(T[0:m], kinect[0][subj][task][8][0:m], 'k')
                ax9.set_ylim([-1, 1])
                ax9.set_ylabel(r'$\theta_9 (rad)$', fontsize = 28)
                ax9.set_xlabel(r'$\mathrm{Time\; (s)}$', fontsize = 28)
                #-----------------------------------------#

                ax10 = plt.subplot(5,2,10)

                # plotting the data for each subplot (each joint)
                plt.plot(T[0:m], error_PFvsMocap[0][subj][task][9][0:m], 'r', linewidth = 2.0)
                plt.plot(T[0:m], error_PFvsMocap[1][subj][task][9][0:m], 'g', linewidth = 2.0)
                plt.plot(T[0:m], error_PFvsMocap[2][subj][task][9][0:m], 'b', linewidth = 2.0)

                # setting the plot limits and lables
                ax10.set_ylim([-1, 1])
                ax10.set_ylabel(r'$\theta_{10} (rad)$', fontsize = 28)
                ax10.set_xlabel(r'$\mathrm{Time\;(s)}$', fontsize = 28)
                #-----------------------------------------#

                # saving the figures
                plt.savefig("/home/amir/PF_paper_figures/error_sub_{0}_task_{1}.pdf".format(subj+1, task+1), dpi  = 300)

    # ------------------------------------ PFvsMocap error for one subject, one joint vs time - line plot  ----------------------------
    elif plot_type == 6:

        i=1    # number of first figure
        for subj in [0]: #subject number = 1
            for task in [2]: # task number = 3
                i += 1

                # Setting the time
                for j in range(m):
                    T.append(j*0.1)

                plt.figure(i, figsize = (16,5))
            #     ax1 = plt.subplot(5,2,1)
            # #plt.plot(T[0:m], PF_estimates[mode][subj][task][theta][0:m]
            #     plt.plot(T[0:m], error_PFvsMocap[0][subj][task][0][0:m], 'r', linewidth = 2.0)
            #     plt.plot(T[0:m], error_PFvsMocap[1][subj][task][0][0:m], 'g', linewidth = 2.0)
            #     plt.plot(T[0:m], error_PFvsMocap[2][subj][task][0][0:m], 'b', linewidth = 2.0)
            #     # plt.plot(T[0:m], kinect[0][subj][task][0][0:m], 'k')
            #     ax1.set_ylim([-1, 1])
            #     ax1.set_ylabel(r'$\theta_1 (rad)$', fontsize = 28)
            #
            #     ax2 = plt.subplot(5,2,2)
            #     plt.plot(T[0:m], error_PFvsMocap[0][subj][task][1][0:m], 'r', linewidth = 2.0, label = r'$\mathrm{CPA}$')
            #     plt.plot(T[0:m], error_PFvsMocap[1][subj][task][1][0:m], 'g', linewidth = 2.0, label = r'$\mathrm{Full~Measurement}$')
            #     plt.plot(T[0:m], error_PFvsMocap[2][subj][task][1][0:m], 'b', linewidth = 2.0, label = r'$\mathrm{Height~Measurement}$')
            #     # plt.plot(T[0:m], kinect[0][subj][task][1][0:m], 'k', label = "Kinect posture")
            #     ax2.set_ylim([-1, 1])
            #     ax2.set_ylabel(r'$\theta_2 (rad)$', fontsize = 28)
            #
            #     plt.legend(loc = 'upper center',ncol = 3, bbox_to_anchor = (-0.1, 1.6), prop = {'size': 28})
            #     ax3  =  plt.subplot(5,2,3)
            #     plt.plot(T[0:m], error_PFvsMocap[0][subj][task][2][0:m], 'r', linewidth = 2.0)
            #     plt.plot(T[0:m], error_PFvsMocap[1][subj][task][2][0:m], 'g', linewidth = 2.0)
            #     plt.plot(T[0:m], error_PFvsMocap[2][subj][task][2][0:m], 'b', linewidth = 2.0)
            #     # plt.plot(T[0:m], kinect[0][subj][task][2][0:m], 'k')
            #     ax3.set_ylim([-1, 1])
            #     ax3.set_ylabel(r'$\theta_3 (rad)$', fontsize = 28)
            #
            #     ax4 = plt.subplot(5,2,4)
            #     plt.plot(T[0:m], error_PFvsMocap[0][subj][task][3][0:m], 'r', linewidth = 2.0)
            #     plt.plot(T[0:m], error_PFvsMocap[1][subj][task][3][0:m], 'g', linewidth = 2.0)
            #     plt.plot(T[0:m], error_PFvsMocap[2][subj][task][3][0:m], 'b', linewidth = 2.0)
            #     # plt.plot(T[0:m], kinect[0][subj][task][3][0:m], 'k')
            #     ax4.set_ylim([-1, 1])
            #     ax4.set_ylabel(r'$\theta_4 (rad)$', fontsize = 28)
            #
            #     ax5 = plt.subplot(5,2,5)
            #     plt.plot(T[0:m], error_PFvsMocap[0][subj][task][4][0:m], 'r', linewidth = 2.0)
            #     plt.plot(T[0:m], error_PFvsMocap[1][subj][task][4][0:m], 'g', linewidth = 2.0)
            #     plt.plot(T[0:m], error_PFvsMocap[2][subj][task][4][0:m], 'b', linewidth = 2.0)
            #     # plt.plot(T[0:m], kinect[0][subj][task][4][0:m], 'k')
            #     ax5.set_ylim([-1, 1])
            #     ax5.set_ylabel(r'$\theta_5 (rad)$', fontsize = 28)

                plt.rcParams.update({'font.size': 30})

                ax6 = plt.subplot(1,1,1)
                plt.plot(T[0:m], error_PFvsMocap[0][subj][task][5][0:m], 'blue', linewidth = 4.0, label = r'$\mathrm{CPA}$')
                plt.plot(T[0:m], error_PFvsMocap[1][subj][task][5][0:m], 'crimson', linewidth = 4.0, label = r'$\mathrm{Full~Measurement}$')
                plt.plot(T[0:m], error_PFvsMocap[2][subj][task][5][0:m], 'orange', linewidth = 4.0, label = r'$\mathrm{Height~Measurement}$')
                # plt.plot(T[0:m], kinect[0][subj][task][5][0:m], 'k')
                ax6.set_ylim([-0.1, 0.4])
                ax6.set_ylabel(r'$\mathrm{Deviation~from}$'+ '\n' + r'$\mathrm{Motion~Capture~(rad)}$', fontsize = 32)
                ax6.set_xlabel(r'$\mathrm{Time\;(s)}$', fontsize = 32)
                ax6.legend(loc = 'upper right',ncol = 3, prop = {'size': 28})
            #
            #     ax7 = plt.subplot(5,2,7)
            #     plt.plot(T[0:m], error_PFvsMocap[0][subj][task][6][0:m], 'r', linewidth = 2.0)
            #     plt.plot(T[0:m], error_PFvsMocap[1][subj][task][6][0:m], 'g', linewidth = 2.0)
            #     plt.plot(T[0:m], error_PFvsMocap[2][subj][task][6][0:m], 'b', linewidth = 2.0)
            #     # plt.plot(T[0:m], kinect[0][subj][task][6][0:m], 'k')
            #     ax7.set_ylim([-1, 1])
            #     ax7.set_ylabel(r'$\theta_7 (rad)$', fontsize = 28)
            #
            #     ax8 = plt.subplot(5,2,8)
            #     plt.plot(T[0:m], error_PFvsMocap[0][subj][task][7][0:m], 'r', linewidth = 2.0)
            #     plt.plot(T[0:m], error_PFvsMocap[1][subj][task][7][0:m], 'g', linewidth = 2.0)
            #     plt.plot(T[0:m], error_PFvsMocap[2][subj][task][7][0:m], 'b', linewidth = 2.0)
            #     # plt.plot(T[0:m], kinect[0][subj][task][7][0:m], 'k')
            #     ax8.set_ylim([-1, 1])
            #     ax8.set_ylabel(r'$\theta_8 (rad)$', fontsize = 28)
            #
                # ax9 = plt.subplot(1,1,1)
                # plt.plot(T[0:m], error_PFvsMocap[0][subj][task][8][0:m], 'r', linewidth = 2.0)
                # plt.plot(T[0:m], error_PFvsMocap[1][subj][task][8][0:m], 'g', linewidth = 2.0)
                # plt.plot(T[0:m], error_PFvsMocap[2][subj][task][8][0:m], 'b', linewidth = 2.0)
                # # plt.plot(T[0:m], kinect[0][subj][task][8][0:m], 'k')
                # ax9.set_ylim([-1, 1])
                # ax9.set_ylabel(r'$\theta_9 (rad)$', fontsize = 28)
                # ax9.set_xlabel(r'$\mathrm{Time\; (s)}$', fontsize = 28)


                # ax10 = plt.subplot(5,2,10)
                # plt.plot(T[0:m], error_PFvsMocap[0][subj][task][9][0:m], 'r', linewidth = 2.0)
                # plt.plot(T[0:m], error_PFvsMocap[1][subj][task][9][0:m], 'g', linewidth = 2.0)
                # plt.plot(T[0:m], error_PFvsMocap[2][subj][task][9][0:m], 'b', linewidth = 2.0)
                # # plt.plot(T[0:m], kinect[0][subj][task][9][0:m], 'k')
                # ax10.set_ylim([-1, 1])
                # ax10.set_ylabel(r'$\theta_{10} (rad)$', fontsize = 28)
                # ax10.set_xlabel(r'$\mathrm{Time\;(s)}$', fontsize = 28)

                plt.savefig("/home/amir/PF_paper_figures/error_sub_{0}_task_{1}.pdf".format(subj+1, task+1), dpi  = 300, bbox_inches = 'tight')

# ------------------------ raw PF_estiamte and mocap for each subject each task each joint vs time - line plot -----
    elif plot_type == 7:
        for subj in range(2):
            for task in range(4):
                i += 1
                # Setting the time
                for j in range(m):
                    T.append(j*0.1)

                plt.figure(i, figsize = (25,15))


                ax1 = plt.subplot(5,2,1)
                plt.plot(T[0:m], PF_estimates[0][subj][task][0][0:m], 'coral', linewidth = 2.0)
                plt.plot(T[0:m], PF_estimates[1][subj][task][0][0:m], 'g', linewidth = 2.0)
                plt.plot(T[0:m], PF_estimates[2][subj][task][0][0:m], 'b', linewidth = 2.0)
                ax1.set_ylim([-1.57, 1.57])
                ax1.set_ylabel(r'$\theta_1 (rad)$', fontsize = 28)
                plt.plot(T[0:m], ground_truth[0][subj][task][0][0:m], 'r', linewidth = 3.0)

                ax2 = plt.subplot(5,2,2)
                plt.plot(T[0:m], PF_estimates[0][subj][task][1][0:m], 'coral', linewidth = 2.0, label = "$\mathrm{PF-CPA}$")
                plt.plot(T[0:m], PF_estimates[1][subj][task][1][0:m], 'g', linewidth = 2.0, label = "$\mathrm{PF-full\; measure}$")
                plt.plot(T[0:m], PF_estimates[2][subj][task][1][0:m], 'b', linewidth = 2.0, label = "$\mathrm{PF-height\; measure}$")
                ax2.set_ylim([-1.57, 1.57])
                ax2.set_ylabel(r'$\theta_2 (rad)$', fontsize = 28)
                plt.plot(T[0:m], ground_truth[0][subj][task][1][0:m], 'r', label = "$\mathrm{Motion\; capture}$", linewidth = 3.0)
                plt.legend(loc = 'upper center',ncol = 4, bbox_to_anchor = (-0.1, 1.6), prop = {'size': 28})

                ax3  =  plt.subplot(5,2,3)
                plt.plot(T[0:m], PF_estimates[0][subj][task][2][0:m], 'coral', linewidth = 2.0)
                plt.plot(T[0:m], PF_estimates[1][subj][task][2][0:m], 'g', linewidth = 2.0)
                plt.plot(T[0:m], PF_estimates[2][subj][task][2][0:m], 'b', linewidth = 2.0)
                ax3.set_ylim([-1.57, 1.57])
                ax3.set_ylabel(r'$\theta_3 (rad)$', fontsize = 28)
                plt.plot(T[0:m], ground_truth[0][subj][task][2][0:m], 'r', linewidth = 3.0)

                ax4  =  plt.subplot(5,2,4)
                plt.plot(T[0:m], PF_estimates[0][subj][task][3][0:m], 'coral', linewidth = 2.0)
                plt.plot(T[0:m], PF_estimates[1][subj][task][3][0:m], 'g', linewidth = 2.0)
                plt.plot(T[0:m], PF_estimates[2][subj][task][3][0:m], 'b', linewidth = 2.0)
                ax4.set_ylim([-1.57, 1.57])
                ax4.set_ylabel(r'$\theta_4 (rad)$', fontsize = 28)
                plt.plot(T[0:m], ground_truth[0][subj][task][3][0:m], 'r', linewidth = 3.0)

                ax5  =  plt.subplot(5,2,5)
                plt.plot(T[0:m], PF_estimates[0][subj][task][4][0:m], 'coral', linewidth = 2.0)
                plt.plot(T[0:m], PF_estimates[1][subj][task][4][0:m], 'g', linewidth = 2.0)
                plt.plot(T[0:m], PF_estimates[2][subj][task][4][0:m], 'b', linewidth = 2.0)
                ax5.set_ylim([-1.57, 1.57])
                ax5.set_ylabel(r'$\theta_5 (rad)$', fontsize = 28)
                plt.plot(T[0:m], ground_truth[0][subj][task][4][0:m], 'r', linewidth = 3.0)

                ax6  =  plt.subplot(5,2,6)
                plt.plot(T[0:m], PF_estimates[0][subj][task][5][0:m], 'coral', linewidth = 2.0)
                plt.plot(T[0:m], PF_estimates[1][subj][task][5][0:m], 'g', linewidth = 2.0)
                plt.plot(T[0:m], PF_estimates[2][subj][task][5][0:m], 'b', linewidth = 2.0)
                ax6.set_ylim([-1.57, 1.57])
                ax6.set_ylabel(r'$\theta_6 (rad)$', fontsize = 28)
                plt.plot(T[0:m], ground_truth[0][subj][task][5][0:m], 'r', linewidth = 3.0)

                ax7  =  plt.subplot(5,2,7)
                plt.plot(T[0:m], PF_estimates[0][subj][task][6][0:m], 'coral', linewidth = 2.0)
                plt.plot(T[0:m], PF_estimates[1][subj][task][6][0:m], 'g', linewidth = 2.0)
                plt.plot(T[0:m], PF_estimates[2][subj][task][6][0:m], 'b', linewidth = 2.0)
                ax7.set_ylim([-1.57, 1.57])
                ax7.set_ylabel(r'$\theta_7 (rad)$', fontsize = 28)
                plt.plot(T[0:m], ground_truth[0][subj][task][6][0:m], 'r', linewidth = 3.0)
                ax8  =  plt.subplot(5,2,8)
                plt.plot(T[0:m], PF_estimates[0][subj][task][7][0:m], 'coral', linewidth = 2.0)
                plt.plot(T[0:m], PF_estimates[1][subj][task][7][0:m], 'g', linewidth = 2.0)
                plt.plot(T[0:m], PF_estimates[2][subj][task][7][0:m], 'b', linewidth = 2.0)

                ax8.set_ylim([-1.57, 1.57])
                ax8.set_ylabel(r'$\theta_8 (rad)$', fontsize = 28)
                plt.plot(T[0:m], ground_truth[0][subj][task][7][0:m], 'r', linewidth = 3.0)
                ax9  =  plt.subplot(5,2,9)
                plt.plot(T[0:m], PF_estimates[0][subj][task][8][0:m], 'coral', linewidth = 2.0)
                plt.plot(T[0:m], PF_estimates[1][subj][task][8][0:m], 'g', linewidth = 2.0)
                plt.plot(T[0:m], PF_estimates[2][subj][task][8][0:m], 'b', linewidth = 2.0)

                ax9.set_ylim([-1.57, 1.57])
                ax9.set_ylabel(r'$\theta_9 (rad)$', fontsize = 28)
                ax9.set_xlabel(r'$\mathrm{Time\; (s)}$', fontsize = 28)
                plt.plot(T[0:m], ground_truth[0][subj][task][8][0:m], 'r', linewidth = 3.0)
                ax10  =  plt.subplot(5,2,10)
                plt.plot(T[0:m], PF_estimates[0][subj][task][9][0:m], 'coral', linewidth = 2.0)
                plt.plot(T[0:m], PF_estimates[1][subj][task][9][0:m], 'g', linewidth = 2.0)
                plt.plot(T[0:m], PF_estimates[2][subj][task][9][0:m], 'b', linewidth = 2.0)

                ax10.set_ylim([-1.57, 1.57])
                ax10.set_ylabel(r'$\theta_{10} (rad)$', fontsize = 28)
                ax10.set_xlabel(r'$\mathrm{Time\;(s)}$', fontsize = 28)
                plt.plot(T[0:m], ground_truth[0][subj][task][9][0:m], 'r', linewidth = 3.0)

                plt.savefig("/home/amir/raw_PF_estimate_and_mocap_eachsubj_eachtask_eachjoint_vs_time_sub_{0}_task_{1}.pdf".format(subj, task), dpi  = 300)

# #*********************************** PFvsMocap error for all trials vs tasks - box plot ********************************************
    elif plot_type == 8:
        # calculating the error for each task
        print len(error_PFvsMocap_cases[0])



        blue_circle = dict(markerfacecolor='deepskyblue', marker='o') ## TODO: move to above initialization part of the function
        peru_circle = dict(markerfacecolor='olivedrab', marker='o')
        red_circle = dict(markerfacecolor='red', marker='o')

        N = 4 # number of cases
        ind = np.arange(N) # index number of joints as an array

        #setting up the plot ax and fig
        fig,ax = plt.subplots(1, figsize=(13,4))
        width = 0.25


        # initilizing the box plot data


        # adding points to the box plot
        d=0.75
        bp8 = ax.boxplot(error_PFvsMocap_cases[0], positions = [1*d], widths = width, patch_artist=True, flierprops=blue_circle, showfliers=False)
        setBoxColors(bp8, 'black', 'deepskyblue', 'black')
        bp9 = ax.boxplot(error_PFvsMocap_cases[1], positions = [2*d], widths = width, patch_artist=True, flierprops=blue_circle, showfliers=False)
        setBoxColors(bp9, 'black', 'deepskyblue', 'black')
        bp10 = ax.boxplot(error_PFvsMocap_cases[2], positions = [3*d], widths = width, patch_artist=True, flierprops=blue_circle, showfliers=False)
        setBoxColors(bp10, 'black', 'deepskyblue', 'black')
        bp11 = ax.boxplot(error_PFvsMocap_cases[3], positions = [4*d], widths = width, patch_artist=True, flierprops=blue_circle, showfliers=False)
        setBoxColors(bp11, 'black', 'deepskyblue', 'black')

        # setting the axes names, lables and numbers
        ax.set_xticklabels([r'$\mathrm{Validation: ON}$'+ '\n' +r'$\mathrm{Initialization: ON}$' , r'$\mathrm{Validation: OFF}$'+ '\n' +r'$\mathrm{Initialization: ON}$', r'$\mathrm{Validation: ON}$'+ '\n' +r'$\mathrm{Initialization: OFF}$', r'$\mathrm{Validation.: OFF}$'+ '\n' +r'$\mathrm{Initialization: OFF}$'], fontsize=22)
        ax.set_yticklabels([r'$\mathrm{0.0}$', r'$\mathrm{0.2}$', r'$\mathrm{0.4}$', r'$\mathrm{0.6}$', r'$\mathrm{0.8}$', r'$\mathrm{1}$'], fontsize=26)
        ax.set_xticks([1*d, 2*d, 3*d, 4*d])
        ax.set_yticks([0, 0.2, 0.4, 0.6, 0.8, 1])
        ax.set_ylabel(r'$\mathrm{Deviation~from}$'+ '\n' + r'$\mathrm{Motion~Capture~(rad)}$', fontsize=28)
        ax.set_ylim((0,1))
        ax.set_xlim((0.5,3.5))
        # setting the legend
        # ax.legend([bp3[0]["boxes"][0], bp4[0]["boxes"][0], bp5[0]["boxes"][0]], [r'$\mathrm{CPA}$', r'$\mathrm{Full~Measurement}$', r'$\mathrm{Height~Measurement}$'], loc='upper center', ncol=3, fontsize=22)
        # ax.legend([bp3[0]["boxes"][0], bp4[0]["boxes"][0]], [r'$\mathrm{PF}$', r'$\mathrm{IK}$'], loc='upper center', ncol=2, fontsize=22)

        # grid on
        ax.yaxis.grid(True)
        # saving the plot figure
        plt.savefig("/home/amir/PF_paper_figures/validity.pdf", dpi =150, bbox_inches='tight')

    # plt.show()
if __name__ == '__main__':

    ## TODO: urdf and human model are seem to be no needed anymore, delet if correct
    # urdf_file = "/home/amir/catkin_ws/src/ergo-hri/ll4ma_posture_estimation/urdf/kdl.urdf.xacro"
    # human = human_model(urdf_file)

    # initialing the gevariables
    error_PFvsMocap = []   # |PF-mocap|
    error_IK_LSvsMocap = []   # |IK_LS-mocap|
    error_offline_smoothingvsMocap = []   # |Offline_Smoothing - mocap|
    PF_estimates = []
    PF_estimates_vel = []
    IK_LS_solutions = []
    offline_smoothing_solutions = []
    mocap = []
    mocap_vel = []
    kinect = []
    stylus_curr_obs = []   ## TODO: no needed anymore?
    stylus_pose_x = []     ## TODO: no needed anymore?
    stylus_vel_xdot = []   ## TODO: no needed anymore?
    all_particles=[]
    # num_correct_tasks = [4,3,4,3,4,4,4,4]  #number of correctly done tasks for each subject
    num_correct_tasks = [1]  #number of correctly done tasks for each subject for the result at Aug 23
    # num_correct_tasks = [1]  #number of correctly done tasks for each subject for the result at Aug 23
    num_subjects = len(num_correct_tasks) #num of subjects
    seg_len_modes = ['cpa']#, 'full_measure', 'height_measure']
    # joint_limit_type = 'full'
    joint_limit_type = 'modified_torso'
    trajectory_sampling = 10

    # solver = "PF+LS"
    solver = "PF+LS"

    T = 1  # start from task T
    S = 1  # start from subject S
    # making paramters global
    global num_correct_tasks , num_subjects , seg_len_modes , joint_limit_type


# # ********************************** posture validity **********************************
#     error_PFvsMocap_cases = []
#     for case in ["v1n1","v0n1","v1n0","v0n0"]:
#         PF_estimates = [[], [], [], [], [], [], [], [], [], []]
#         mocap = [[], [], [], [], [], [], [], [], [], []]
#         error = []
#         globals()["bag_s1t1_{0}".format(case)] = rosbag.Bag("/home/amir/working_dataset/final_posture_estimate_results/validityFig_s1t1_{0}.bag".format(case))
#         globals()["bag_s1t3_{0}".format(case)] = rosbag.Bag("/home/amir/working_dataset/final_posture_estimate_results/validityFig_s1t3_{0}.bag".format(case))
#         globals()["bag_s5t3_{0}".format(case)] = rosbag.Bag("/home/amir/working_dataset/final_posture_estimate_results/validityFig_s5t3_{0}.bag".format(case))
#         # Reading the PF solution
#         for topic, msg, t in globals()["bag_s1t1_{0}".format(case)].read_messages(topics = ['PF_solution']):
#             for k in range(10):  # number of DOF
#                 PF_estimates[k].append(msg.theta[k])    # adding the angles of each joint into a list
#         for topic, msg, t in globals()["bag_s1t3_{0}".format(case)].read_messages(topics = ['PF_solution']):
#             for k in range(10):  # number of DOF
#                 PF_estimates[k].append(msg.theta[k])    # adding the angles of each joint into a list
#         for topic, msg, t in globals()["bag_s5t3_{0}".format(case)].read_messages(topics = ['PF_solution']):
#             for k in range(10):  # number of DOF
#                 PF_estimates[k].append(msg.theta[k])    # adding the angles of each joint into a list
#
#         # Reading the Sensory_data/mocap (reduced version)
#         for topic, msg, t in globals()["bag_s1t1_{0}".format(case)].read_messages(topics = ['sensory_data']):
#             for k in range(3,10):
#                 mocap[k].append(msg.mocap.reduced_posture[k])
#         for topic, msg, t in globals()["bag_s1t3_{0}".format(case)].read_messages(topics = ['sensory_data']):
#             for k in range(3,10):
#                 mocap[k].append(msg.mocap.reduced_posture[k])
#         for topic, msg, t in globals()["bag_s5t3_{0}".format(case)].read_messages(topics = ['sensory_data']):
#             for k in range(3,10):
#                 mocap[k].append(msg.mocap.reduced_posture[k])
#
#
#         len_estimate = len(PF_estimates[0])
#         for k in range(3,10):
#             for l in range(len_estimate):
#                 error.append(abs(PF_estimates[k][l]-mocap[k][l]))
#
#         error_PFvsMocap_cases.append(error)
#
#     for cases in ["v1n1","v0n1","v1n0","v0n0"]:
#         globals()["bag_s1t1_{0}".format(case)].close()
#         globals()["bag_s1t3_{0}".format(case)].close()
#         globals()["bag_s5t3_{0}".format(case)].close()
#
#     plot_type = 8
#     plot_results(error_PFvsMocap_cases, plot_type)



# *******************************Real******************************************************************************************************
    for length_mode in seg_len_modes: #, 'full_measure', 'height_measure']: # for all the segment length modes
        for s in range(S, num_subjects+1): # for all the subjects
            for task in range(T, num_correct_tasks[s-1]+1): # for all the correct tasks

                # initialize global variables for each trial
                globals()["PF_estimates_subject{0}_task{1}_{2}".format(s, task, length_mode)] = [[], [], [], [], [], [], [], [], [], []] # array of size 10, each contain a joint angle during the whole time of motion
                if solver == "PF+LS+OS" or solver == "PF+LS":
                    all_particles = []
                    globals()["IK_LS_solutions_subject{0}_task{1}_{2}".format(s, task, length_mode)] = [[], [], [], [], [], [], [], [], [], []] # array of size 10, each contain a joint angle during the whole time of motion
                if solver == "PF+LS+OS":
                    globals()["offline_smoothing_solutions_subject{0}_task{1}_{2}".format(s, task, length_mode)] = [[], [], [], [], [], [], [], [], [], []] # array of size 10, each contain a joint angle during the whole time of motion

                # if length_mode=='cpa' and s==1 and task==3: # for the case of comparing particles behaviour with/without initialization and modified joint limits
                #     globals()["particles_subject{0}_task{1}_{2}_fulljointlimit".format(s, task, length_mode)] = []
                #     globals()["particles_subject{0}_task{1}_{2}_modjointlimit".format(s, task, length_mode)] = []

                globals()["PF_estimates_vel_subject{0}_task{1}_{2}".format(s, task, length_mode)] = [[], [], [], [], [], [], [], [], [], []]
                globals()["mocap_subject{0}_task{1}_{2}".format(s, task, length_mode)] = [[], [], [], [], [], [], [], [], [], []]  # reduced version of mocap
                globals()["mocap_vel_subject{0}_task{1}_{2}".format(s, task, length_mode)] = [[], [], [], [], [], [], [], [], [], []]
                globals()["error_PFvsMocap_subject{0}_task{1}_{2}".format(s, task, length_mode)] = [[], [], [], [], [], [], [], [], [], []]
                if solver == "PF+LS+OS" or solver == "PF+LS":
                    globals()["error_IK_LSvsMocap_subject{0}_task{1}_{2}".format(s, task, length_mode)] = [[], [], [], [], [], [], [], [], [], []]
                if solver == "PF+LS+OS":
                    globals()["error_offline_smoothingvsMocap_subject{0}_task{1}_{2}".format(s, task, length_mode)] = [[], [], [], [], [], [], [], [], [], []]

                # setting the bag file name for each trial and read and store the data (allparticles, PF_solution, mocap_reduced)
                if solver == "PF":
                    globals()["bag_subject{0}_task{1}_{2}_PF".format(s, task, length_mode)] = rosbag.Bag("/home/amir/working_dataset/final_posture_estimation_results/smoothing_plot/subject{0}_task{1}_{2}_PF_{3}_trajsample{4}.bag".format(s, task, length_mode, joint_limit_type, trajectory_sampling))
                elif solver == "PF+LS":
                    globals()["bag_subject{0}_task{1}_{2}_PF+LS".format(s, task, length_mode)] = rosbag.Bag("/home/amir/working_dataset/final_posture_estimation_results/smoothing_plot/subject{0}_task{1}_{2}_PF+LS_{3}_trajsample{4}.bag".format(s, task, length_mode, joint_limit_type, trajectory_sampling))

                elif solver == "PF+LS+OS":
                    globals()["bag_subject{0}_task{1}_{2}_PF+LS".format(s, task, length_mode)] = rosbag.Bag("/home/amir/working_dataset/final_posture_estimation_results/smoothing_plot/subject{0}_task{1}_{2}_PF+LS_{3}_trajsample{4}.bag".format(s, task, length_mode, joint_limit_type, trajectory_sampling))
                    globals()["bag_subject{0}_task{1}_{2}_offline_smoothing".format(s, task, length_mode)] = rosbag.Bag("/home/amir/working_dataset/final_posture_estimation_results/smoothing_plot/subject{0}_task{1}_{2}_offline_smoothing_{3}_trajsample{4}.bag".format(s, task, length_mode, joint_limit_type, trajectory_sampling))

                # read the bag files and store into gevariables
                ## handling the case for particle behaviour comparision
                ## ! HINT ! : the normal bag file is the modified joint limit version!
                # if length_mode=='cpa' and s==1 and task==3:
                #     ### read the all particles full-joint limit version
                #     globals()["bag_subject{0}_task{1}_{2}_full_limit".format(s, task, length_mode)] = rosbag.Bag("/home/amir/working_dataset/final_posture_estimation_results_rename/subject{0}_task{1}_{2}_full_limit.bag".format(s, task, length_mode))
                #     ### read the corresponding particles for modofied joint limit and append into the particles list
                #     for topic, msg, t in globals()["bag_subject{0}_task{1}_{2}".format(s, task, length_mode)].read_messages(topics = ['results']):
                #         globals()["particles_subject{0}_task{1}_{2}_modjointlimit".format(s, task, length_mode)].append(msg.particles)
                #     ### read the corresponding particles for full joint limit and append into the particles list
                #     for topic, msg, t in globals()["bag_subject{0}_task{1}_{2}_full_limit".format(s, task, length_mode)].read_messages(topics = ['results']):
                #         globals()["particles_subject{0}_task{1}_{2}_fulljointlimit".format(s, task, length_mode)].append(msg.particles)

                if solver == "PF":
                    # Reading the PF solution
                    for topic, msg, t in globals()["bag_subject{0}_task{1}_{2}_PF".format(s, task, length_mode)].read_messages(topics = ['PF_solution']):
                        for k in range(10):  # number of DOF
                            globals()["PF_estimates_subject{0}_task{1}_{2}".format(s, task, length_mode)][k].append(msg.theta[k])    # adding the angles of each joint into a list
                            globals()["PF_estimates_vel_subject{0}_task{1}_{2}".format(s, task, length_mode)][k].append(msg.theta_dot[k]) # adding the vel of each joint into a list

                    # Reading the Sensory_data/mocap (reduced version)
                    for topic, msg, t in globals()["bag_subject{0}_task{1}_{2}_PF".format(s, task, length_mode)].read_messages(topics = ['sensory_data']):
                            for k in range(10):
                                globals()["mocap_subject{0}_task{1}_{2}".format(s, task, length_mode)][k].append(msg.mocap.reduced_posture[k])
                                globals()["mocap_vel_subject{0}_task{1}_{2}".format(s, task, length_mode)][k].append(msg.mocap.reduced_vel[k])

                elif solver == "PF+LS":
                    # Reading the PF solution
                    for topic, msg, t in globals()["bag_subject{0}_task{1}_{2}_PF+LS".format(s, task, length_mode)].read_messages(topics = ['PF_solution']):
                        for k in range(10):  # number of DOF
                            globals()["PF_estimates_subject{0}_task{1}_{2}".format(s, task, length_mode)][k].append(msg.theta[k])    # adding the angles of each joint into a list
                            globals()["PF_estimates_vel_subject{0}_task{1}_{2}".format(s, task, length_mode)][k].append(msg.theta_dot[k]) # adding the vel of each joint into a list

                    # Reading the IK solution
                    all_weights_before_normalize=[]
                    for topic, msg, t in globals()["bag_subject{0}_task{1}_{2}_PF+LS".format(s, task, length_mode)].read_messages(topics = ['results']):
                        for k in range(10):  # number of DOF
                            globals()["IK_LS_solutions_subject{0}_task{1}_{2}".format(s, task, length_mode)][k].append(msg.ik_solution.theta[k])    # adding the angles of each joint into a list
                        particles = []

                        for i in range(500):
                            particles.append(msg.particles[i].theta)
                        all_weights_before_normalize.append(msg.weights_before_normalize)
                        all_particles.append(particles)

                    # Reading the Sensory_data/mocap (reduced version)
                    for topic, msg, t in globals()["bag_subject{0}_task{1}_{2}_PF+LS".format(s, task, length_mode)].read_messages(topics = ['sensory_data']):
                            for k in range(10):
                                globals()["mocap_subject{0}_task{1}_{2}".format(s, task, length_mode)][k].append(msg.mocap.reduced_posture[k])
                                globals()["mocap_vel_subject{0}_task{1}_{2}".format(s, task, length_mode)][k].append(msg.mocap.reduced_vel[k])

                elif solver == "PF+LS+OS":
                    # Reading the PF solution
                    for topic, msg, t in globals()["bag_subject{0}_task{1}_{2}_PF+LS".format(s, task, length_mode)].read_messages(topics = ['PF_solution']):
                        for k in range(10):  # number of DOF
                            globals()["PF_estimates_subject{0}_task{1}_{2}".format(s, task, length_mode)][k].append(msg.theta[k])    # adding the angles of each joint into a list
                            globals()["PF_estimates_vel_subject{0}_task{1}_{2}".format(s, task, length_mode)][k].append(msg.theta_dot[k]) # adding the vel of each joint into a list

                    # Reading the IK solution
                    for topic, msg, t in globals()["bag_subject{0}_task{1}_{2}_PF+LS".format(s, task, length_mode)].read_messages(topics = ['results']):
                        for k in range(10):  # number of DOF
                            globals()["IK_LS_solutions_subject{0}_task{1}_{2}".format(s, task, length_mode)][k].append(msg.ik_solution.theta[k])    # adding the angles of each joint into a list
                    # Reading the OS solution
                    for topic, msg, t in globals()["bag_subject{0}_task{1}_{2}_offline_smoothing".format(s, task, length_mode)].read_messages(topics = ['results']):
                        for k in range(10):  # number of DOF
                            globals()["offline_smoothing_solutions_subject{0}_task{1}_{2}".format(s, task, length_mode)][k].append(msg.ik_solution.theta[k])    # adding the angles of each joint into a list

                    # Reading the Sensory_data/mocap (reduced version)
                    for topic, msg, t in globals()["bag_subject{0}_task{1}_{2}_PF+LS".format(s, task, length_mode)].read_messages(topics = ['sensory_data']):
                            for k in range(10):
                                globals()["mocap_subject{0}_task{1}_{2}".format(s, task, length_mode)][k].append(msg.mocap.reduced_posture[k])
                                globals()["mocap_vel_subject{0}_task{1}_{2}".format(s, task, length_mode)][k].append(msg.mocap.reduced_vel[k])

                # calculating the error PFvsMocap and IK_LSvsMocap
                for k in range(10):
                    len_estimate = len(globals()["PF_estimates_subject{0}_task{1}_{2}".format(s, task, length_mode)][0])
                    globals()["error_PFvsMocap_subject{0}_task{1}_{2}".format(s, task, length_mode)][k] = [abs(globals()["PF_estimates_subject{0}_task{1}_{2}".format(s, task, length_mode)][k][l]-globals()["mocap_subject{0}_task{1}_{2}".format(s, task, length_mode)][k][l]) for l in range(len_estimate)]
                    globals()["error_IK_LSvsMocap_subject{0}_task{1}_{2}".format(s, task, length_mode)][k] = [abs(globals()["IK_LS_solutions_subject{0}_task{1}_{2}".format(s, task, length_mode)][k][l]-globals()["mocap_subject{0}_task{1}_{2}".format(s, task, length_mode)][k][l]) for l in range(len_estimate)]
                    # len_estimate = len(globals()["offline_smoothing_solutions_subject{0}_task{1}_{2}".format(s, task, length_mode)][0])
                    # globals()["error_offline_smoothingvsMocap_subject{0}_task{1}_{2}".format(s, task, length_mode)][k] = [abs(globals()["offline_smoothing_solutions_subject{0}_task{1}_{2}".format(s, task, length_mode)][k][l]-globals()["mocap_subject{0}_task{1}_{2}".format(s, task, length_mode)][k][l]) for l in range(len_estimate)]

                # closing the bag file
                if solver == "PF":
                    globals()["bag_subject{0}_task{1}_{2}_PF+LS".format(s, task, length_mode)].close()
                elif solver == "PF+LS":
                    globals()["bag_subject{0}_task{1}_{2}_PF+LS".format(s, task, length_mode)].close()
                elif solver == "PF+LS+OS":
                    globals()["bag_subject{0}_task{1}_{2}_PF+LS".format(s, task, length_mode)].close()
                    globals()["bag_subject{0}_task{1}_{2}_offline_smoothing".format(s, task, length_mode)].close()


        #---------------------- convert data into a nested list: length_mode>Subj>Task ----#
        PF_estimates.append([[globals()["PF_estimates_subject{0}_task{1}_{2}".format(s, task, length_mode)] for task in range(T,num_correct_tasks[s-1]+1)] for s in range(S, num_subjects+1)])
        if solver == "PF+LS":
            IK_LS_solutions.append([[globals()["IK_LS_solutions_subject{0}_task{1}_{2}".format(s, task, length_mode)] for task in range(T,num_correct_tasks[s-1]+1)] for s in range(S, num_subjects+1)])
        elif solver == "PF+LS+OS":
            offline_smoothing_solutions.append([[globals()["offline_smoothing_solutions_subject{0}_task{1}_{2}".format(s, task, length_mode)] for task in range(T,num_correct_tasks[s-1]+1)] for s in range(S, num_subjects+1)])
        mocap.append([[globals()["mocap_subject{0}_task{1}_{2}".format(s, task, length_mode)] for task in range(T,num_correct_tasks[s-1]+1)] for s in range(S, num_subjects+1)])
        PF_estimates_vel.append([[globals()["PF_estimates_vel_subject{0}_task{1}_{2}".format(s, task, length_mode)] for task in range(T,num_correct_tasks[s-1]+1)] for s in range(S, num_subjects+1)])
        mocap_vel.append([[globals()["mocap_vel_subject{0}_task{1}_{2}".format(s, task, length_mode)] for task in range(T,num_correct_tasks[s-1]+1)] for s in range(S, num_subjects+1)])
        error_PFvsMocap.append([[globals()["error_PFvsMocap_subject{0}_task{1}_{2}".format(s, task, length_mode)] for task in range(T,num_correct_tasks[s-1]+1)] for s in range(S, num_subjects+1)]) # size : size(seg_len_modes)
        if solver == "PF+LS":
            error_IK_LSvsMocap.append([[globals()["error_IK_LSvsMocap_subject{0}_task{1}_{2}".format(s, task, length_mode)] for task in range(T,num_correct_tasks[s-1]+1)] for s in range(S, num_subjects+1)])
        elif solver == "PF+LS+OS":
            error_offline_smoothingvsMocap.append([[globals()["error_offline_smoothingvsMocap_subject{0}_task{1}_{2}".format(s, task, length_mode)] for task in range(T,num_correct_tasks[s-1]+1)] for s in range(S, num_subjects+1)])
            error_IK_LSvsMocap.append([[globals()["error_IK_LSvsMocap_subject{0}_task{1}_{2}".format(s, task, length_mode)] for task in range(T,num_correct_tasks[s-1]+1)] for s in range(S, num_subjects+1)])


    # Plottingt the results
    '''
    plot_type:
    (0)             PF_IK_OSvsMocap error for all trials vs joints - box plot
    (1)             PFvsMocap error for all trials vs joints - box plot
    (2)             PFvsMocap error for all trials vs tasks - box plot
    (3)             particles convergence vs time - scatter plot
    (4)             particles convergence vs time - shaded scatter plot
    (5)             PFvsMocap error for each subject each task each joint vs time - line plot
    (6)             PFvsMocap error for one subject, one joint vs time - line plot
    (7)             PFvsMocap raw PF_estiamte and mocap for each subject each task each joint vs time - line plot
    (8)             effect of validity of the posture for 1 task, 1 subj, arm joints only - box plot


    '''
    # Setting the plot parameters:
    plot_type = 11

    if solver == "PF+LS+OS":
        plot_results(PF_estimates, IK_LS_solutions , offline_smoothing_solutions , mocap , error_PFvsMocap , error_IK_LSvsMocap, error_offline_smoothingvsMocap, PF_estimates_vel , mocap_vel, plot_type)
    elif solver == "PF+LS":
        plot_results(PF_estimates, IK_LS_solutions , mocap , error_PFvsMocap , error_IK_LSvsMocap, PF_estimates_vel , mocap_vel , all_particles, all_weights_before_normalize, plot_type)
    elif solver == "PF":
        plot_results(PF_estimates, mocap , error_PFvsMocap , PF_estimates_vel , mocap_vel , all_particles, plot_type)
