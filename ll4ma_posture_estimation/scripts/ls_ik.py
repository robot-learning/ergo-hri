#!/usr/bin/env python
# license removed for brevity
import rospy
import copy
import math
import numpy as np
from human_model import human_model
from utils.visualization_utils import *
from utils.data_prepration_utils import *
from utils.subject_specific_utils import *
from scipy.optimize import least_squares



class LS_IK(object):
    """

    """


    def __init__(self, subject_num=1, task_num=1, trial_num=2, n_joint=10):
        '''

        '''
        #initialize variables
        self.joint_limits_type = rospy.get_param("joint_limit")
        urdf_file="/home/amir/catkin_ws/src/ergo-hri/ll4ma_posture_estimation/urdf/kdl.urdf.xacro"
        self.human=human_model(urdf_file, rob_desc="robot_description")
        self.n_joints= n_joint

        # setting the natural posture
        self.neutral_posture_deg=np.array([0, 0, 0, 90, 0, 0, 90, 0, 0, 0])
        self.neutral_posture=np.array([self.neutral_posture_deg[i]*np.pi/180 for i in range(10)])

        #calculating joint limits
        self.joint_limits , self.joint_limits_len = human_joint_limit()

        self.joint_limits_lbs = np.array([self.joint_limits[i][0] for i in range(len(self.joint_limits))])
        self.joint_limits_ubs = np.array([self.joint_limits[i][1] for i in range(len(self.joint_limits))])

        self.prev_solution = self.neutral_posture

        self.joint_limits_lbs = np.array([self.joint_limits[i][0] for i in range(len(self.joint_limits))])
        self.joint_limits_ubs = np.array([self.joint_limits[i][1] for i in range(len(self.joint_limits))])

        if self.joint_limits_type == "modified_torso" or self.joint_limits_type == "subj_specific":
            for i in range(4):
                self.prev_solution[i] = float(self.joint_limits[i][0] + self.joint_limits[i][1])/2
            # self.prev_solution[3] = self.joint_limits_ubs[3]


    def optimize(self, observation):
        '''

        '''
        def get_residual(posture):
            '''
            '''
            alpha = [50, 50, 50, 10, 10, 10]
            x=fk_ls(self.human, posture)

            r1=[alpha[i]*(x[i]- observation[i]) for i in range(len(x))]
            for i in range(len(posture)):
                r1.append(posture[i] - self.prev_solution[i])
            residual = np.array(r1)

            return residual


        solution = least_squares(get_residual, self.prev_solution, bounds=(self.joint_limits_lbs, self.joint_limits_ubs))
        self.prev_solution = copy.deepcopy(solution.x)

        return solution.x
