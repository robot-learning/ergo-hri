#!/usr/bin/env python

import numpy as np
from rula import *
from posture_optimization_problem import Posture_Optimization_GradFree, Posture_Optimization_GradBased
from posture_optimization_solvers import CEMSolver
from simulation import *
from motion_generation import *
from std_msgs.msg import Char
from sensor_msgs.msg import JointState
from utils.util import teleoperator_to_leader_transform
import pickle
import torch
from torch.autograd import Variable

def rad_to_deg(x):
    x = [ x[i]/3.14*180  for i in range(len(x))]
    return x

if __name__ == '__main__':

    rospy.init_node('teleoperation', anonymous=True)

    teleoperator = sim_teleoperator()
    leader = sim_leader_robot()
    follower = sim_follower_robot()
    task = Task()
    RULA = RULA_assessment()

    # loading learned RULA model
    device = torch.device('cpu')
    RULA_PATH = "/home/amir/catkin_ws/src/ergo-hri/ll4ma_posture_optimization/src/rula/RULA_model_regression_1200000_1000E_python2_torch140.pt"
    model = torch.load(RULA_PATH, map_location=device)
    model.eval()

    posture_opt_method = rospy.get_param("posture_opt_method")
    if posture_opt_method == "grad_free":
        # loading the problem
        posture_opt_problem = Posture_Optimization_GradFree(teleoperator, RULA)
        # defining params for CEM solver
        max_iterations = 100
        kl_epsilon = 3.0
        n_elite = 10
        n_samples = 10000
        n_samples_initial = 100000
        # loading the solver
        solver_cem = CEMSolver(max_iterations, kl_epsilon, n_elite, n_samples)


    elif posture_opt_method == "grad_based":
        # loading the problem
        posture_opt_problem = Posture_Optimization_GradBased(teleoperator, RULA)
        #TODO : add params for the solver here

    else:
        rospy.logerr("!!!!!! Wrong Posture Optimization Method !!!!!")

    experiment = rospy.get_param("experiment")
    postural_correction = rospy.get_param("postural_correction")


    if experiment == 1:
        task.type = "goal-constrained"
        task.goal = [2.5293795834515223, 0.20396423160957124, 1.3320823754722357, 3.014715823558981, 0.003613837427830148, 0.13630637329505096]# local follower frame
        teleoperator_posture_deg = [20, 7, 0,
                        25, 0, 20,
                        130, -50,
                        -20, -40,
                        -90, 0, 0, 0, 0, 0, 0]
        teleoperator_posture_rad = [teleoperator_posture_deg[i]*np.pi/180 for i in range(10)]
        teleoperator.current_posture = teleoperator_posture_rad
        teleoperator.previous_posture = teleoperator.current_posture

        leader.current_config = [0.022712994388582004, 0.635018237551346, 0.7335849357344068, -1.538368084452975, -0.34428363196734, 1.060201923272224, -2.477942379882445]
        follower.current_config = leader.current_config

    if experiment == "2":
        task.type = "traj-constrained"



    # loading the teleoperator simulator motion generation
    simulator_motion = Motion_Generation(task, teleoperator, leader, follower)

    try:
        r = rospy.Rate(20)

        # preparing to publish the agents' config for gazebo
        human_left_arm_publisher = rospy.Publisher("/left_human_arm/joint_cmd", JointState, queue_size=1)
        human_right_arm_publisher = rospy.Publisher("/right_human_arm/joint_cmd", JointState, queue_size=1)
        human_trunk_publisher = rospy.Publisher("/human_trunk/joint_cmd", JointState, queue_size=1)

        optimal_human_left_arm_publisher = rospy.Publisher("/left_optimal_human_arm/joint_cmd", JointState, queue_size=1)
        optimal_human_right_arm_publisher = rospy.Publisher("/right_optimal_human_arm/joint_cmd", JointState, queue_size=1)
        optimal_human_trunk_publisher = rospy.Publisher("/optimal_human_trunk/joint_cmd", JointState, queue_size=1)

        leader_arm_name = rospy.get_param("leader_arm_name", "lbr4")
        follower_arm_name = rospy.get_param("follower_arm_name", "lbr4")
        leader_arm_type = rospy.get_param("/{}/robot_type".format(leader_arm_name))
        follower_arm_type = rospy.get_param("/{}/robot_type".format(follower_arm_name))

        leader_hand_name = rospy.get_param("leader_hand_name", "")
        follower_hand_name = rospy.get_param("follower_hand_name", "")

        leader_arm_cmd_topic = rospy.get_param("/{}/joint_command_topic".format(leader_arm_name))
        follower_arm_cmd_topic = rospy.get_param("/{}/joint_command_topic".format(follower_arm_name))

        leader_arm_cmd_pub = rospy.Publisher(leader_arm_cmd_topic, JointState, queue_size=1)
        follower_arm_cmd_pub = rospy.Publisher(follower_arm_cmd_topic, JointState, queue_size=1)

        left_human_command = JointState()
        left_human_command.name = ['left_human_arm_j{}'.format(i) for i in range(7)]
        left_human_command.position = [0.0] * len(left_human_command.name)
        left_human_command.velocity = [0.0] * len(left_human_command.name)
        left_human_command.effort = [0.0] * len(left_human_command.name)
        right_human_command = JointState()
        right_human_command.name = ['right_human_arm_j{}'.format(i) for i in range(7)]
        right_human_command.position = [0.0] * len(right_human_command.name)
        right_human_command.velocity = [0.0] * len(right_human_command.name)
        right_human_command.effort = [0.0] * len(right_human_command.name)
        human_trunk_command = JointState()
        human_trunk_command.name = ['human_trunk_j{}'.format(i) for i in range(3)]
        human_trunk_command.position = [0.0] * len(human_trunk_command.name)
        human_trunk_command.velocity = [0.0] * len(human_trunk_command.name)
        human_trunk_command.effort = [0.0] * len(human_trunk_command.name)

        left_optimal_human_command = JointState()
        left_optimal_human_command.name = ['left_optimal_human_arm_j{}'.format(i) for i in range(7)]
        left_optimal_human_command.position = [0.0] * len(left_optimal_human_command.name)
        left_optimal_human_command.velocity = [0.0] * len(left_optimal_human_command.name)
        left_optimal_human_command.effort = [0.0] * len(left_optimal_human_command.name)
        right_optimal_human_command = JointState()
        right_optimal_human_command.name = ['right_optimal_human_arm_j{}'.format(i) for i in range(7)]
        right_optimal_human_command.position = [0.0] * len(right_optimal_human_command.name)
        right_optimal_human_command.velocity = [0.0] * len(right_optimal_human_command.name)
        right_optimal_human_command.effort = [0.0] * len(right_optimal_human_command.name)
        optimal_human_trunk_command = JointState()
        optimal_human_trunk_command.name = ['optimal_human_trunk_j{}'.format(i) for i in range(3)]
        optimal_human_trunk_command.position = [0.0] * len(optimal_human_trunk_command.name)
        optimal_human_trunk_command.velocity = [0.0] * len(optimal_human_trunk_command.name)
        optimal_human_trunk_command.effort = [0.0] * len(optimal_human_trunk_command.name)

        leader_command = JointState()
        leader_command.name = ['leader_lbr4_j{}'.format(i) for i in range(7)]
        leader_command.position = [0.0] * len(leader_command.name)
        leader_command.velocity = [0.0] * len(leader_command.name)
        leader_command.effort = [0.0] * len(leader_command.name)

        follower_command = JointState()
        follower_command.name = ['follower_lbr4_j{}'.format(i) for i in range(7)]
        follower_command.position = [0.0] * len(follower_command.name)
        follower_command.velocity = [0.0] * len(follower_command.name)
        follower_command.effort = [0.0] * len(follower_command.name)


        traj_follower_ee = [follower.current_ee_pose] # global
        traj_follower_joint = [follower.current_config]
        traj_leader_ee = [leader.current_ee_pose] # global
        traj_leader_joint = [leader.current_config]
        traj_teleoperator_ee = [teleoperator.current_ee_pose] # global
        traj_teleoperator_joint = [teleoperator.current_posture]
        traj_RULA_teleoperator = [RULA.rula_score(teleoperator.current_posture)]
        traj_optimal_posture = []
        traj_RULA_optimal = []

        # setting the rate for the loop
        rate=rospy.Rate(100)

        counter = 0
        # main loop
        while not task.check_complete(follower.current_ee_pose):
            # reducing alpha value through time
            simulator_motion.alpha = simulator_motion.alpha * 0.6
            # print "alpha: ", simulator_motion.alpha

            #initialize cost and converged params
            cost = float('inf')
            converged = False

            # postural optimization
            if posture_opt_method == "grad_free":
                optimal_posture, cost, converged = solver_cem.optimize(posture_opt_problem.cost_online_2, teleoperator.current_posture) # We use cost_online_2 since having rula as cost works better that as constraint
            elif posture_opt_method == "grad_based":
                optimal_posture, cost, converged = posture_opt_problem.optimize_online(teleoperator.current_posture) # same as above comment about cost_online_2

            if converged and cost!= float('inf'):
                # collecting the results for optimal posture
                traj_optimal_posture.append(optimal_posture)
                traj_RULA_optimal.append(RULA.rula_score(optimal_posture))
                print " Optimal Posture: " , rad_to_deg(optimal_posture)
                print "RULA score for the q_opt (network): ", model(torch.tensor(optimal_posture).reshape(1,10).float()).detach().numpy()+1
                print "RULA score for the q_opt : ", traj_RULA_optimal[-1]
                # updating the command for optimal human model
                optimal_human_trunk_command.position = optimal_posture[0:3]
                right_optimal_human_command.position = optimal_posture[3:10]
                # # publishing the commands for optimal human model
                # for i in range(300):
                #     optimal_human_right_arm_publisher.publish(right_optimal_human_command)
                #     optimal_human_trunk_publisher.publish(optimal_human_trunk_command)
                #     rate.sleep()
                ####################################
                # motion planing for teleoperator
                ####################################
                ## updating the initial solution for boosting the posture_optimization_problem
                motion_opt_init_solution = []
                for i in range(simulator_motion.T):
                    for j in range(teleoperator.n_h):
                        motion_opt_init_solution.append(teleoperator.current_posture[j])
                for i in range(simulator_motion.T):
                    for j in range(leader.n_l):
                        motion_opt_init_solution.append(leader.current_config[j])
                for i in range(simulator_motion.T):
                    for j in range(follower.n_f):
                        motion_opt_init_solution.append(follower.current_config[j])

                ## motion planning optimization boosted by cuurrent config as the initial solution

                plan_teleoperator, plan_leader, plan_follower, cost = simulator_motion.optimize(postural_correction, motion_opt_init_solution, optimal_posture)
            else:
                # use the previous results of optimal posture
                traj_optimal_posture.append(traj_optimal_posture[-1])
                traj_RULA_optimal.append(traj_RULA_optimal[-1])

                # motion planing for teleoperator
                plan_teleoperator, plan_leader, plan_follower, cost = simulator_motion.optimize(postural_correction, motion_opt_init_solution)


            counter +=1

            teleoperator.update(plan_teleoperator) # joint space plan
            leader.update(plan_leader) # task space plan
            follower.update(plan_follower) # task space plan


            # print "follower.current_config: ", follower.current_config
            # print "follower.current_ee_pose: ", follower.current_ee_pose
            print "Teleoperator Current Posture: ", rad_to_deg(teleoperator.current_posture)
            print "RULA_score: ", RULA.rula_score(teleoperator.current_posture)
            print " number os steps: ", counter
            print "********************************************************************"
            #
            # collecting the results for teleoperator, leader, follower
            traj_follower_ee.append(follower.current_ee_pose) # global
            traj_follower_joint.append(follower.current_config)
            traj_leader_ee.append(leader.current_ee_pose) # global
            traj_leader_joint.append(leader.current_config)
            traj_teleoperator_ee.append(teleoperator.current_ee_pose) # global
            traj_teleoperator_joint.append(teleoperator.current_posture)
            traj_RULA_teleoperator.append(RULA.rula_score(teleoperator.current_posture))



            # update the data for publishing to gazebo
            human_trunk_command.position = teleoperator.current_posture[0:3]
            right_human_command.position = teleoperator.current_posture[3:10]

            optimal_human_trunk_command.position = optimal_posture[0:3]
            right_optimal_human_command.position = optimal_posture[3:10]

            leader_command.position = leader.current_config
            follower_command.position = follower.current_config
            # publish agents to gazebo
            for i in range(300):
                # publishing the commands for optimal human model
                optimal_human_right_arm_publisher.publish(right_optimal_human_command)
                optimal_human_trunk_publisher.publish(optimal_human_trunk_command)

                human_right_arm_publisher.publish(right_human_command)
                human_trunk_publisher.publish(human_trunk_command)
                leader_arm_cmd_pub.publish(leader_command)
                follower_arm_cmd_pub.publish(follower_command)
                rate.sleep()

            r.sleep()
        print "finished"

        # Saving the result trajectories as pickles
        pickle_res = open("/home/amir/posture_optimization_results/exp_{0}_correction_{1}_alpha{2}.pickle".format(experiment, postural_correction, simulator_motion.alpha),"wb")
        # pickle_res = open("/home/amir/posture_optimization_results/exp_test_correction.pickle","wb")
        pickle.dump([traj_teleoperator_ee,
                     traj_teleoperator_joint,
                     traj_RULA_teleoperator,
                     traj_optimal_posture,
                     traj_RULA_optimal,
                     traj_leader_ee,
                     traj_leader_joint,
                     traj_follower_ee,
                     traj_follower_joint], pickle_res)
        pickle_res.close()

        rospy.spin()

    except rospy.ROSInterruptException:
        pass
