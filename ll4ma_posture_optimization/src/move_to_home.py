#!/usr/bin/env python
import rospy
from sensor_msgs.msg import JointState
import numpy as np
from simulation import sim_teleoperator,sim_follower_robot, sim_leader_robot
from utils.teleop_transformations import teleoperator_to_leader_transform
from tf.transformations import euler_matrix, euler_from_matrix
from std_msgs.msg import Char


class RobotsJointTester:

    def __init__(self):
        self.leader_arm_name = rospy.get_param("leader_arm_name", "lbr4")
        self.follower_arm_name = rospy.get_param("follower_arm_name", "lbr4")
        # print ("leader_arm_name", self.leader_arm_name)
        # print ("follower_arm_name", self.follower_arm_name)

        self.leader_arm_type = rospy.get_param("/{}/robot_type".format(self.leader_arm_name))
        self.follower_arm_type = rospy.get_param("/{}/robot_type".format(self.follower_arm_name))
        # print ("leader_arm_type", self.leader_arm_type)
        # print ("follower_arm_type", self.follower_arm_type)

        self.leader_hand_name = rospy.get_param("leader_hand_name", "")
        self.follower_hand_name = rospy.get_param("follower_hand_name", "")

        leader_arm_cmd_topic = rospy.get_param("/{}/joint_command_topic".format(self.leader_arm_name))
        follower_arm_cmd_topic = rospy.get_param("/{}/joint_command_topic".format(self.follower_arm_name))
        # print ("leader_arm_cmd_topic", leader_arm_cmd_topic)
        # print ("follower_arm_cmd_topic", follower_arm_cmd_topic)

        self.leader_arm_cmd_pub = rospy.Publisher(leader_arm_cmd_topic, JointState, queue_size=1)
        self.follower_arm_cmd_pub = rospy.Publisher(follower_arm_cmd_topic, JointState, queue_size=1)

        if self.leader_hand_name!='none':
            self.leader_hand_type = rospy.get_param("/{}/robot_type".format(self.leader_hand_name))
            leader_hand_cmd_topic = rospy.get_param("/{}/joint_command_topic".format(self.leader_hand_name), "")
            self.leader_hand_cmd_pub = rospy.Publisher(leader_hand_cmd_topic, JointState, queue_size=1)
        self.rate=rospy.Rate(100)

        if self.follower_hand_name!='none':
            self.follower_hand_type = rospy.get_param("/{}/robot_type".format(self.follower_hand_name))
            follower_hand_cmd_topic = rospy.get_param("/{}/joint_command_topic".format(self.follower_hand_name), "")
            self.follower_hand_cmd_pub = rospy.Publisher(follower_hand_cmd_topic, JointState, queue_size=1)
        self.rate=rospy.Rate(100)

    def run_lbr4_test(self):
        follower_command = JointState()
        follower_command.name = ['follower_lbr4_j{}'.format(i) for i in range(7)]
        follower_command.position = [0.0] * len(follower_command.name)
        follower_command.velocity = [0.0] * len(follower_command.name)
        follower_command.effort = [0.0] * len(follower_command.name)

        leader_command = JointState()
        leader_command.name = ['leader_lbr4_j{}'.format(i) for i in range(7)]
        leader_command.position = [0.0] * len(leader_command.name)
        leader_command.velocity = [0.0] * len(leader_command.name)
        leader_command.effort = [0.0] * len(leader_command.name)

        for i in range(300):
            self.follower_arm_cmd_pub.publish(follower_command)
            for j in range(len(follower_command.name)):
                follower_command.position[j]+=0.005
            self.rate.sleep()

        rospy.sleep(2.0)

        for i in range(300):
            self.follower_arm_cmd_pub.publish(follower_command)
            for j in range(len(follower_command.name)):
                follower_command.position[j]-=0.005
            self.rate.sleep()


        for i in range(300):
            self.leader_arm_cmd_pub.publish(leader_command)
            for j in range(len(leader_command.name)):
                leader_command.position[j]+=0.005
            self.rate.sleep()

        rospy.sleep(2.0)

        for i in range(300):
            self.leader_arm_cmd_pub.publish(leader_command)
            for j in range(len(leader_command.name)):
                leader_command.position[j]-=0.005
            self.rate.sleep()


    def run_leader_lbr4_motion_test(self):

        leader_command = JointState()
        leader_command.name = ['leader_lbr4_j{}'.format(i) for i in range(7)]
        leader_command.position = [0.0] * len(leader_command.name)
        leader_command.velocity = [0.0] * len(leader_command.name)
        leader_command.effort = [0.0] * len(leader_command.name)

        for i in range(300):
            self.leader_arm_cmd_pub.publish(leader_command)
            for j in range(len(leader_command.name)):
                leader_command.position[j]+=0.005
            self.rate.sleep()

        rospy.sleep(2.0)

        for i in range(300):
            self.leader_arm_cmd_pub.publish(leader_command)
            for j in range(len(leader_command.name)):
                leader_command.position[j]-=0.005
            self.rate.sleep()

    def run_leader_lbr4_joint_test(self):

        leader_command = JointState()
        leader_command.name = ['leader_lbr4_j{}'.format(i) for i in range(7)]
        leader_command.position = [0.0] * len(leader_command.name)
        leader_command.velocity = [0.0] * len(leader_command.name)
        leader_command.effort = [0.0] * len(leader_command.name)

        leader_command.position = rospy.get_param("/experiment_{0}/leader_init_config".format(experiment))

        for i in range(300):
            self.leader_arm_cmd_pub.publish(leader_command)

            self.rate.sleep()

        # rospy.sleep(2.0)


    def run_follower_lbr4_motion_test(self):
        follower_command = JointState()
        follower_command.name = ['follower_lbr4_j{}'.format(i) for i in range(7)]
        follower_command.position = [0.0] * len(follower_command.name)
        follower_command.velocity = [0.0] * len(follower_command.name)
        follower_command.effort = [0.0] * len(follower_command.name)

        for i in range(300):
            self.follower_arm_cmd_pub.publish(follower_command)
            for j in range(len(follower_command.name)):
                follower_command.position[j]+=0.005
            self.rate.sleep()

        rospy.sleep(2.0)

        for i in range(300):
            self.follower_arm_cmd_pub.publish(follower_command)
            for j in range(len(follower_command.name)):
                follower_command.position[j]-=0.005
            self.rate.sleep()

    def run_follower_lbr4_joint_test(self):
        follower_command = JointState()
        follower_command.name = ['follower_lbr4_j{}'.format(i) for i in range(7)]
        follower_command.position = [0.0] * len(follower_command.name)
        follower_command.velocity = [0.0] * len(follower_command.name)
        follower_command.effort = [0.0] * len(follower_command.name)


        follower_command.position = rospy.get_param("/experiment_{0}/follower_init_config".format(experiment))

        for i in range(300):
            self.follower_arm_cmd_pub.publish(follower_command)
            self.rate.sleep()
        # rospy.sleep(2.0)

    # def run_allegro_test(self):
    #     command = JointState()
    #     command.name = []
    #     command.name += ['index_joint_{}'.format(i) for i in range(4)]
    #     command.name += ['middle_joint_{}'.format(i) for i in range(4)]
    #     command.name += ['ring_joint_{}'.format(i) for i in range(4)]
    #     command.name += ['thumb_joint_{}'.format(i) for i in range(4)]
    #     command.position = [0.0] * len(command.name)
    #     command.velocity = [0.0] * len(command.name)
    #     command.effort = [0.0] * len(command.name)
    #
    #     delta_position = 0.015
    #
    #     for i in range(100):
    #         self.hand_cmd_pub.publish(command)
    #         for j in range(len(command.name)):
    #             command.position[j] += delta_position
    #         self.rate.sleep()
    #
    #     for i in range(100):
    #         self.hand_cmd_pub.publish(command)
    #         for j in range(len(command.name)):
    #             command.position[j] -= delta_position
    #         self.rate.sleep()
    #
    # def run_reflex_test(self):
    #     command = JointState()
    #     command.name = [f'{self.hand_name}_preshape_joint_{i}' for i in [1,2]]
    #     command.name += [f'{self.hand_name}_proximal_joint_{i}' for i in [1,2,3]]
    #     command.position = [0.0] * len(command.name)
    #
    #     delta_position = 0.01
    #
    #     for i in range(120):
    #         self.hand_cmd_pub.publish(command)
    #         for j in range(len(command.name)):
    #             if j > 1:
    #                 command.position[j] += delta_position
    #         self.rate.sleep()
    #
    #     for i in range(120):
    #         self.hand_cmd_pub.publish(command)
    #         for j in range(len(command.name)):
    #             if j <= 1:
    #                 command.position[j] += delta_position
    #         self.rate.sleep()
    #
    #     rospy.sleep(1.0)
    #
    #     for i in range(120):
    #         self.hand_cmd_pub.publish(command)
    #         for j in range(len(command.name)):
    #             if j <= 1:
    #                 command.position[j] -= delta_position
    #         self.rate.sleep()
    #
    #     for i in range(120):
    #         self.hand_cmd_pub.publish(command)
    #         for j in range(len(command.name)):
    #             if j > 1:
    #                 command.position[j] -= delta_position
    #         self.rate.sleep()

class CommandLeaderRobot():

    def __init__(self):
        self.experiment = rospy.get_param("experiment")
        self.arm_name = rospy.get_param("leader_arm_name", "lbr4")
        self.arm_type = rospy.get_param("/{}/robot_type".format(self.arm_name))
        self.hand_name = rospy.get_param("leader_hand_name", "")

        self.arm_cmd_topic = rospy.get_param("/{}/joint_command_topic".format(self.arm_name))

        self.arm_cmd_pub = rospy.Publisher(self.arm_cmd_topic, JointState, queue_size=1)


        if self.hand_name!='none':
            self.hand_type = rospy.get_param("/{}/robot_type".format(self.hand_name))
            self.cmd_topic = rospy.get_param("/{}/joint_command_topic".format(self.hand_name), "")
            self.hand_cmd_pub = rospy.Publisher(self.hand_cmd_topic, JointState, queue_size=1)

        self.command = JointState()
        self.command.name = ['leader_lbr4_j{}'.format(i) for i in range(7)]
        self.command.position = [0.0] * len(self.command.name)
        self.command.velocity = [0.0] * len(self.command.name)
        self.command.effort = [0.0] * len(self.command.name)

        self.initial_config = rospy.get_param("/experiment_{0}/leader_init_config".format(self.experiment))

    def update_config(self,config):
        '''

        '''
        self.command.position = config

        # self.rate=rospy.Rate(100)

class CommandFollowerRobot():

    def __init__(self):
        self.experiment = rospy.get_param("experiment")
        self.arm_name = rospy.get_param("follower_arm_name", "lbr4")
        self.arm_type = rospy.get_param("/{}/robot_type".format(self.arm_name))
        self.hand_name = rospy.get_param("follower_hand_name", "")

        self.arm_cmd_topic = rospy.get_param("/{}/joint_command_topic".format(self.arm_name))

        self.arm_cmd_pub = rospy.Publisher(self.arm_cmd_topic, JointState, queue_size=1)


        if self.hand_name!='none':
            self.hand_type = rospy.get_param("/{}/robot_type".format(self.hand_name))
            self.cmd_topic = rospy.get_param("/{}/joint_command_topic".format(self.hand_name), "")
            self.hand_cmd_pub = rospy.Publisher(self.hand_cmd_topic, JointState, queue_size=1)

        self.command = JointState()
        self.command.name = ['follower_lbr4_j{}'.format(i) for i in range(7)]
        self.command.position = [0.0] * len(self.command.name)
        self.command.velocity = [0.0] * len(self.command.name)
        self.command.effort = [0.0] * len(self.command.name)

        self.initial_config = rospy.get_param("/experiment_{0}/follower_init_config".format(self.experiment))

    def update_config(self,config):
        '''

        '''
        self.command.position = config


class CommandTeleoperator():

    def __init__(self):

        self.experiment = rospy.get_param("experiment")

        # setting the publishers for teleoperator
        self.left_publisher = rospy.Publisher("/left_human_arm/joint_cmd", JointState, queue_size=1)
        self.right_publisher = rospy.Publisher("/right_human_arm/joint_cmd", JointState, queue_size=1)
        self.trunk_publisher = rospy.Publisher("/human_trunk/joint_cmd", JointState, queue_size=1)

        # Setting the commands for teleoperator
        self.left_command = JointState()
        self.left_command.name = ['left_human_arm_j{}'.format(i) for i in range(7)]
        self.left_command.position = [0.0] * len(self.left_command.name)
        self.left_command.velocity = [0.0] * len(self.left_command.name)
        self.left_command.effort = [0.0] * len(self.left_command.name)
        self.right_command = JointState()
        self.right_command.name = ['right_human_arm_j{}'.format(i) for i in range(7)]
        self.right_command.position = [0.0] * len(self.right_command.name)
        self.right_command.velocity = [0.0] * len(self.right_command.name)
        self.right_command.effort = [0.0] * len(self.right_command.name)
        self.trunk_command = JointState()
        self.trunk_command.name = ['human_trunk_j{}'.format(i) for i in range(3)]
        self.trunk_command.position = [0.0] * len(self.trunk_command.name)
        self.trunk_command.velocity = [0.0] * len(self.trunk_command.name)
        self.trunk_command.effort = [0.0] * len(self.trunk_command.name)

        self.initial_posture = rospy.get_param("/experiment_{0}/teleoperator_initial_config".format(self.experiment))
        self.initial_posture_rad = [self.initial_posture[i]*np.pi/180 for i in range(17)]

    def update_posture(self, posture):

        self.trunk_command.position[0] = posture[0]
        self.trunk_command.position[1] = posture[1]
        self.trunk_command.position[2] = posture[2]

        self.right_command.position[0]= posture[3]
        self.right_command.position[1]= posture[4]
        self.right_command.position[2]= posture[5]
        self.right_command.position[3]= posture[6]
        self.right_command.position[4]= posture[7]
        self.right_command.position[5]= posture[8]
        self.right_command.position[6]= posture[9]

        self.left_command.position[0]= posture[10]
        self.left_command.position[1]= posture[11]
        self.left_command.position[2]= posture[12]
        self.left_command.position[3]= posture[13]
        self.left_command.position[4]= posture[14]
        self.left_command.position[5]= posture[15]
        self.left_command.position[6]= posture[16]



class CommandOptimalHuman():

    def __init__(self):
        self.experiment = rospy.get_param("experiment")
        # setting the publishers for optimal_human
        self.left_arm_publisher = rospy.Publisher("/left_optimal_human_arm/joint_cmd", JointState, queue_size=1)
        self.right_arm_publisher = rospy.Publisher("/right_optimal_human_arm/joint_cmd", JointState, queue_size=1)
        self.trunk_publisher = rospy.Publisher("/optimal_human_trunk/joint_cmd", JointState, queue_size=1)

        # Setting the commands for optimal_human
        self.left_command = JointState()
        self.left_command.name = ['left_optimal_human_arm_j{}'.format(i) for i in range(7)]
        self.left_command.position = [0.0] * len(self.left_command.name)
        self.left_command.velocity = [0.0] * len(self.left_command.name)
        self.left_command.effort = [0.0] * len(self.left_command.name)
        self.right_command = JointState()
        self.right_command.name = ['right_optimal_human_arm_j{}'.format(i) for i in range(7)]
        self.right_command.position = [0.0] * len(self.right_command.name)
        self.right_command.velocity = [0.0] * len(self.right_command.name)
        self.right_command.effort = [0.0] * len(self.right_command.name)
        self.trunk_command = JointState()
        self.trunk_command.name = ['optimal_human_trunk_j{}'.format(i) for i in range(3)]
        self.trunk_command.position = [0.0] * len(self.trunk_command.name)
        self.trunk_command.velocity = [0.0] * len(self.trunk_command.name)
        self.trunk_command.effort = [0.0] * len(self.trunk_command.name)

        self.initial_posture = rospy.get_param("/experiment_{0}/teleoperator_initial_config".format(self.experiment))
        self.initial_posture_rad = [self.initial_posture[i]*np.pi/180 for i in range(17)]

    def update_posture(self, posture):

        self.trunk_command.position[0] = posture[0]
        self.trunk_command.position[1] = posture[1]
        self.trunk_command.position[2] = posture[2]

        self.right_command.position[0]= posture[3]
        self.right_command.position[1]= posture[4]
        self.right_command.position[2]= posture[5]
        self.right_command.position[3]= posture[6]
        self.right_command.position[4]= posture[7]
        self.right_command.position[5]= posture[8]
        self.right_command.position[6]= posture[9]

        self.left_command.position[0]= posture[10]
        self.left_command.position[1]= posture[11]
        self.left_command.position[2]= posture[12]
        self.left_command.position[3]= posture[13]
        self.left_command.position[4]= posture[14]
        self.left_command.position[5]= posture[15]
        self.left_command.position[6]= posture[16]


def agents_move_to_home(teleoperator, optimal_human, leader, follower):
    '''

    '''
    rate = rospy.Rate(100)
    teleoperator.update_posture(teleoperator.initial_posture_rad)
    optimal_human.update_posture(optimal_human.initial_posture_rad)
    leader.update_config(leader.initial_config)
    follower.update_config(follower.initial_config)

    print "####### Agents! Let's Go Home! ########"

    # if joint_tester.follower_arm_type == "lbr4":
    #     # joint_tester.run_lbr4_test()
    #     joint_tester.run_leader_lbr4_joint_test()
    #     joint_tester.run_follower_lbr4_joint_test()

    for i in range (200):
        # publish optimal human joints
        optimal_human.left_arm_publisher.publish(optimal_human.left_command)
        optimal_human.right_arm_publisher.publish(optimal_human.right_command)
        optimal_human.trunk_publisher.publish(optimal_human.trunk_command)
        # publish teleoperator joints
        teleoperator.left_publisher.publish(teleoperator.left_command)
        teleoperator.right_publisher.publish(teleoperator.right_command)
        teleoperator.trunk_publisher.publish(teleoperator.trunk_command)

        leader.arm_cmd_pub.publish(leader.command)

        follower.arm_cmd_pub.publish(follower.command)


        rate.sleep()





if __name__=='__main__':
    rospy.init_node("joint_tester")

    teleoperator = CommandTeleoperator()
    optimal_human = CommandOptimalHuman()
    leader = CommandLeaderRobot()
    follower = CommandFollowerRobot()

    agents_move_to_home(teleoperator, optimal_human, leader, follower)
