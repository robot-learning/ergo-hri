#!/usr/bin/env python


import rospy
import rosbag
import numpy as np
from tf.transformations import euler_from_quaternion
from time import time
from std_msgs.msg import Int32, String
from rula import Task, Rula
import csv

if __name__ == '__main__':
    writer = csv.writer(open("/home/amir/working_dataset/RULA_scores_subject_study", 'w'))
    # writer = csv.writer(open("/home/amir/working_dataset/RULA_scores_sim", 'w'))

    row = ["subject","task","max","min","mean","std","mc_max","mc_min","mc_mean","mc_std"]
    writer.writerow(row)
    # for sub in range(2,7):
    #     for task in range(1,5):
    for task in range(1,5):
        for trial in range(1,4):
            # data_bag = rosbag.Bag('/home/amir/datasets/data/final_experiments/sub_{0}_height_measure_{1}_trial_1.bag'.format(sub, task))
            # data_bag = rosbag.Bag('/home/amir/working_dataset/posture_estimate_results/height_measure_sim_{0}_trial_{1}.bag'.format(task,trial))
            data_bag = rosbag.Bag('/home/amir/working_dataset/posture_estimate_results/sub_2_height_measure_2_trial_1.bag')

            task1=Task()
            RULA_ScoreSet=[]
            for topic, msg, t in data_bag.read_messages(topics='estimates'):
                # print msg
                posture=list(msg.theta)
                posture[6]+=np.pi/2

                RULA_1= Rula(posture,task1)
                RULA_ScoreSet.append(RULA_1.rula_score())
            # data_bag.close()
            # print RULA_ScoreSet
            max_RULA = max(RULA_ScoreSet)
            min_RULA = min(RULA_ScoreSet)
            # print "number of max= " , RULA_ScoreSet.count(max(RULA_ScoreSet))
            # print "out of ", len(RULA_ScoreSet)
            mean_RULA = np.mean(RULA_ScoreSet)
            std_RULA = np.std(RULA_ScoreSet)

            RULA_ScoreSet = []
            for topic, msg, t in data_bag.read_messages(topics='observation'):
                # print msg
                posture=list(msg.stylus1_pose_gt)
                posture[6]+=np.pi/2

                RULA_1= Rula(posture,task1)
                RULA_ScoreSet.append(RULA_1.rula_score())
            data_bag.close()
            # print RULA_ScoreSet
            mc_max_RULA = max(RULA_ScoreSet)
            mc_min_RULA = min(RULA_ScoreSet)
            # print "mc_number of max= " , RULA_ScoreSet.count(max(RULA_ScoreSet))
            # print "mc_out of ", len(RULA_ScoreSet)
            mc_mean_RULA = np.mean(RULA_ScoreSet)
            mc_std_RULA = np.std(RULA_ScoreSet)

            # row = [sub-1,task,max_RULA,min_RULA,mean_RULA,std_RULA,mc_max_RULA,mc_min_RULA,mc_mean_RULA,mc_std_RULA]
            row = [task,trial,max_RULA,min_RULA,mean_RULA,std_RULA,mc_max_RULA,mc_min_RULA,mc_mean_RULA,mc_std_RULA]

            writer.writerow(row)
    # try:
    #
    #     # posture=[0, 0, 0, 0, 1, 0, 0, 3, 0, 0]
    #     # task1=Task()
    #     # # task1.raised_shoulder = False
    #     # # task1.supported_arm = False
    #     # # task1.lean_on_arm = False
    #     # # task1.arm_motion = 'static'  #or 'dynamic'
    #     # # task1.arm_motion_freq = 3 #frequency (occurance/min)
    #     # # task1.arm_load = 1 # max load on arm in (kg)
    #     # # task1.arm_load_type = 'intermittent'  # or 'repeated' or 'shock'
    #     # # task1.neck_angle = [0,0,0]  #we do not have neck angle as a part of posture in our model so we define it in the task
    #     # # task1.supported_leg_feet = False
    #     # # task1.body_motion = 'static'  #or 'dynamic'
    #     # # task1.body_motion_freq = 3 #frequency (occurance/min)
    #     # # task1.body_load = 1 # max load on body in (kg)
    #     # # task1.body_load_type = 'intermittent'  # or 'repeated' or 'shock'
    #     #
    #     # RULA_1= Rula(posture,task1)
    #     # RULA_1.rula_score()
    #
    #     # rospy.spin()
    # except rospy.ROSInterruptException:
    #     pass
