#!/usr/bin/env python

import numpy as np
from rula import *
import torch
import pickle
import pycuda.driver as cuda
from torch.utils.data import Dataset, random_split, DataLoader
import matplotlib.pyplot as plt
cuda.init()

print (torch.cuda.is_available())

## Get Id of default device
print (torch.cuda.current_device())
print (cuda.Device(0).name())



# if torch.cuda.is_available():
#   dev = "cuda:0"
# else:
#   dev = "cpu"
# device = torch.device(dev)
class RULADataset(Dataset):

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __getitem__(self,index):
        sample = {
        'feature': torch.tensor([self.x[index]], dtype=torch.float32).cuda(),
        'label': torch.tensor([self.y[index]], dtype=torch.float32).cuda()}
        return sample

    def __len__(self):
        return len(self.x)



def check_accuracy(test_loader, model, dev):
    num_correct = 0
    total = 0
    model.eval()

    with torch.no_grad():
        for data in test_loader:
            # print (data)
            inputs,labels = data['feature'].reshape(1,10), data['label'].reshape(1)
            inputs = inputs.to(device=dev)
            labels = labels.to(device=dev)

            predictions = model(inputs)
            # print (torch.round(predictions[0]), labels[0])
            if torch.round(predictions[0]) == labels[0]:
                num_correct += 1
            total += 1

        print(f"Test Accuracy of the model: {float(num_correct)/float(total)*100:.2f}")

RULA = RULA_assessment()
# generate random posture and get rula scores
# N is batch size; D_in is input dimension;
# H is hidden dimension; D_out is output dimension.
N, D_in, H1, H2, H3, H4, D_out = 1200000, 10, 124, 124, 124, 7,  1
n_epochs = 1000
learning_rate = 1e-3

# load the dataset
pickle_RULA = open("/home/amir/catkin_ws/src/ergo-hri/ll4ma_posture_optimization/src/rula/data/RULA_data_{}sample.pickle".format(N),"rb")
x, y = pickle.load(pickle_RULA)  # x: input data, y: labels
pickle_RULA.close()

dataset = RULADataset(x, y)
# print('length: ', len(dataset))
for i in range(len(dataset)):
    #pp.pprint(dataset[i])
    pass
# Splitting the dataset
train, test = random_split(dataset, [int(0.8*len(dataset)), int(0.2*len(dataset))])

train_loader = DataLoader(train, batch_size=10000, shuffle=True, sampler=None,
           batch_sampler=None, num_workers=0, collate_fn=None,
           pin_memory=False, drop_last=False, timeout=0,
           worker_init_fn=None)

test_loader = DataLoader(test, batch_size=1, shuffle=True, sampler=None,
           batch_sampler=None, num_workers=0, collate_fn=None,
           pin_memory=False, drop_last=False, timeout=0,
           worker_init_fn=None)


# Use the nn package to define our model and loss function.
model = torch.nn.Sequential(
    torch.nn.Linear(D_in, H1),
    torch.nn.ReLU(),
    torch.nn.Linear(H1, H2),
    torch.nn.ReLU(),
    torch.nn.Linear(H2, H3),
    torch.nn.ReLU(),
    torch.nn.Linear(H3, H4),
    torch.nn.ReLU(),
    torch.nn.Linear(H4, D_out),
)
model.cuda()

loss_fn = torch.nn.MSELoss(reduction='sum')

optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)


loss_list = []
for t in range (n_epochs):  #Number of epochs
    print (t)
    c = 0
    for data in train_loader:

        inputs,labels = data['feature'].reshape(10000,D_in), data['label'].reshape(10000)

        # model.train()

        # Forward pass: compute predicted y by passing x to the model.
        outputs = model(inputs)

        # Compute and print loss.
        loss = loss_fn(outputs.reshape(10000), labels)

        if c % 1000 == 0:
            print (loss)
            loss_list.append(loss)

        # Before the backward pass, use the optimizer object to zero all of the
        #     # gradients for the variables it will update (which are the learnable
        #     # weights of the model). This is because by default, gradients are
        #     # accumulated in buffers( i.e, not overwritten) whenever .backward()
        #     # is called. Checkout docs of torch.autograd.backward for more details.
        optimizer.zero_grad()

        # Backward pass: compute gradient of the loss with respect to model parameters
        loss.backward()

        # Calling the step function on an Optimizer makes an update to its parameters
        optimizer.step()

        c+=1


PATH = "RULA_model_regression_1200000_1000E.pt"
torch.save(model, PATH)
print('Finished Training')

check_accuracy(test_loader, model, "cuda:0")

plt.plot(loss_list)
plt.show()

#
# # Use the optim package to define an Optimizer that will update the weights of
# # the model for us. Here we will use Adam; the optim package contains many other
# # optimization algorithms. The first argument to the Adam constructor tells the
# # optimizer which Tensors it should update.

#
# for t in range(len(train)):
#     # Forward pass: compute predicted y by passing x to the model.
#     y_pred = model(x)
#     # print(list(y_pred.size()))
#     #
#     # print(list(y.size()))
#
#     # Compute and print loss.
#     loss = loss_fn(y_pred, y)
#     if t % 100 == 99:
#         print(t, loss.item())  # print loss every 100 steps
#
#     # Before the backward pass, use the optimizer object to zero all of the
#     # gradients for the variables it will update (which are the learnable
#     # weights of the model). This is because by default, gradients are
#     # accumulated in buffers( i.e, not overwritten) whenever .backward()
#     # is called. Checkout docs of torch.autograd.backward for more details.
#     optimizer.zero_grad()
#
#     # Backward pass: compute gradient of the loss with respect to model
#     # parameters
#     loss.backward()
#
#     # Calling the step function on an Optimizer makes an update to its
#     # parameters
#     optimizer.step()
#
