#!/usr/bin/env python


import rospy
import numpy as np
from time import time
from std_msgs.msg import Int32, String


class REBA_assessment(object):
    """docstring for REBA."""
    def __init__(self):
        # super(REBA_assessment, self).__init__()

        # print self.joint_angles
        self.task = Task()
        self.posture = CurrentPosture()

        #table A
        #key = "Trunk score, neck score, legs score"
        self.table_A={
        "111":1, "112":2, "113":3, "114":4,     "121":1, "122":2, "123":3, "124":4,     "131":3, "132":3, "133":5, "134":6,
        "211":2, "212":3, "213":4, "214":5,     "221":3, "222":4, "223":5, "224":5,     "231":4, "232":5, "233":6, "234":7,
        "311":2, "312":4, "313":5, "314":6,     "321":4, "322":5, "323":6, "324":7,     "331":5, "332":6, "333":7, "334":8,
        "411":3, "412":5, "413":6, "414":7,     "421":5, "422":6, "423":7, "424":8,     "431":6, "432":7, "433":8, "434":9,
        "511":4, "512":6, "513":7, "514":8,     "521":6, "522":7, "523":8, "524":9,     "531":7, "532":8, "531":9, "534":9,
        }

        #table B
        #key = "upper arm score, Lower arm score, wrist score"
        self.table_B={
        "111":1, "112":2, "113":2,     "121":1, "122":2, "123":3,
        "211":1, "212":2, "213":3,     "221":2, "222":3, "223":4,
        "311":3, "312":4, "313":5,     "321":4, "322":5, "323":4,
        "411":4, "412":5, "413":5,     "421":5, "422":6, "423":7,
        "511":6, "512":7, "513":8,     "521":7, "522":8, "523":8,
        "611":7, "612":8, "613":8,     "621":8, "622":9, "623":9,
        }

        #table C
        #key = "table A score, table B score"

        self.table_C={
        "1,1":1,    "1,2":1,    "1,3":1,    "1,4":2,    "1,5":3,    "1,6":3,    "1,7":4,    "1,8":5,    "1,9":6,    "1,10":7,   "1,11":7,   "1,12":7,
        "2,1":1,    "2,2":2,    "2,3":2,    "2,4":3,    "2,5":4,    "2,6":4,    "2,7":5,    "2,8":6,    "2,9":6,    "2,10":7,   "2,11":7,   "2,12":8,
        "3,1":2,    "3,2":3,    "3,3":3,    "3,4":3,    "3,5":4,    "3,6":5,    "3,7":6,    "3,8":7,    "3,9":7,    "3,10":8,   "3,11":8,   "3,12":8,
        "4,1":3,    "4,2":4,    "4,3":4,    "4,4":4,    "4,5":5,    "4,6":6,    "4,7":7,    "4,8":8,    "4,9":8,    "4,10":9,   "4,11":9,   "4,12":9,
        "5,1":4,    "5,2":4,    "5,3":4,    "5,4":5,    "5,5":6,    "5,6":7,    "5,7":8,    "5,8":8,    "5,9":9,    "5,10":9,   "5,11":9,   "5,12":9,
        "6,1":6,    "6,2":6,    "6,3":6,    "6,4":7,    "6,5":8,    "6,6":8,    "6,7":9,    "6,8":9,    "6,9":10,   "6,10":10,  "6,11":10,  "6,12":10,
        "7,1":7,    "7,2":7,    "7,3":7,    "7,4":8,    "7,5":9,    "7,6":9,    "7,7":9,    "7,8":10,   "7,9":10,   "7,10":11,  "7,11":11,  "7,12":11,
        "8,1":8,    "8,2":8,    "8,3":8,    "8,4":9,    "8,5":10,   "8,6":10,   "8,7":10,   "8,8":10,   "8,9":10,   "8,10":11,  "8,11":11,  "8,12":11,
        "9,1":9,    "9,2":9,    "9,3":9,    "9,4":10,   "9,5":10,   "9,6":10,   "9,7":11,   "9,8":11,   "9,9":11,   "9,10":12,  "9,11":12,  "9,12":12,
        "10,1":10,  "10,2":10,  "10,3":10,  "10,4":11,  "10,5":11,  "10,6":11,  "10,7":11,  "10,8":12,  "10,9":12,  "10,10":12, "10,11":12, "10,12":12,
        "11,1":11,  "11,2":11,  "11,3":11,  "11,4":11,  "11,5":12,  "11,6":12,  "11,7":12,  "11,8":12,  "11,9":12,  "11,10":12, "11,11":12, "11,12":12,
        "12,1":12,  "12,2":12,  "12,3":12,  "12,4":12,  "12,5":12,  "12,6":12,  "12,7":12,  "12,8":12,  "12,9":12,  "12,10":12, "12,11":12, "12,12":12,

        }


    def REBA_score(self, posture_array):

        self.upper_arm_score = 0
        self.lower_arm_score = 0
        self.wrist_score = 0
        self.wrist_twist_score = 0
        self.arm_muscle_use_score = 0
        self.arm_load_score = 0
        self.neck_score = 0
        self.trunk_score = 0
        self.leg_score = 0
        self.body_muscle_use_score = 0
        self.body_load_score = 0

        self.posture_score = 0
        self.neck_trunk_leg_score = 0
        self.score = 0

        self.prepare_posture(posture_array)
        # Neck Trunk Leg analysis#################
        self.neck_analysis()
        self.trunk_analysis()
        self.leg_analysis()
        # Find the posture score from table A
        table_A_key=str(self.trunk_score * 100 + self.neck_score * 10
         + self.leg_score)
        self.neck_trunk_leg_score = self.table_A[table_A_key]
        # other adjustments
        self.body_load_analysis()
        self.neck_trunk_leg_score += self.body_load_score

        # Upper Arm analysis#################
        self.upper_arm_analysis()
        self.lower_arm_analysis()
        self.wrist_analysis()
        #Find the posture score from table A
        table_B_key=str(self.upper_arm_score * 100 + self.lower_arm_score * 10
        + self.wrist_score)
        self.arm_wrist_score = self.table_B[table_B_key]
        # other adjustments
        self.arm_wrist_score += self.task.coupling_score

        # Find the score frim table C
        table_C_key = "{0},{1}".format(self.neck_trunk_leg_score, self.arm_wrist_score)
        self.score = self.table_C[table_C_key]
        self.score += self.task.activity_score
        # self.analyze_REBA()

        # print "REBA SCORE = "
        # print self.score
        # print self.REBA_interpretion
        return self.score

    def prepare_posture(self, posture_array):
        self.posture.torso = posture_array[0:3]
        self.posture.shoulder = posture_array[3:6]
        self.posture.elbow = posture_array[6:8]
        self.posture.wrist = posture_array[8:10]
        self.posture.ankle = posture_array[10]

    def neck_analysis(self):

        if self.posture.neck[0] < -5: #neck in extention backward
            self.neck_score += 2
        elif self.posture.neck[0] >= -5 and self.posture.neck[0] < 20:
            self.neck_score += 1
        elif self.posture.neck[0] >= 20:
            self.neck_score += 2
        else:
            pass
        # Adjustment
        if self.posture.neck[2] > 5 or self.posture.neck[2] < -5:  #if neck is twisted
            self.neck_score += 1
        if self.posture.neck[1] > 5 or self.posture.neck[1] < -5:  #if neck is side_bended
            self.neck_score += 1

    def trunk_analysis(self):

        if self.posture.torso[0] >= -5 and self.posture.torso[0] <= 5:
            self.trunk_score += 1
        elif self.posture.torso[0] < -5:
                self.trunk_score += 2
        elif self.posture.torso[0] > 5 and self.posture.torso[0] <= 20:
                self.trunk_score += 2
        elif self.posture.torso[0] > 20 and self.posture.torso[0] <= 60:
                self.trunk_score += 3
        elif self.posture.torso[0] > 60:
                self.trunk_score += 4
        else:
             pass
        # Adjustment
        if self.posture.torso[1] > 5 or self.posture.torso[1] < -5:
                self.trunk_score += 1
        if self.posture.torso[2] > 5 or self.posture.torso[2] < -5:
                self.trunk_score += 1

    def leg_analysis(self):

        if self.task.bilateral_weight_bearing or self.task.seating or self.task.walking:
            self.leg_score += 1
        else:
            self.leg_score += 2

        #Adjustment
        if not self.task.seating:
            if self.posture.ankle >= 30 and self.posture.ankle < 60:
                self.leg_score += 1
            elif  self.posture.ankle > 60:
                self.leg_score += 2
            else:
                pass


    def upper_arm_analysis(self):

        if self.posture.shoulder[1] >= -20 and self.posture.shoulder[1] <= 20:
            self.upper_arm_score += 1
        elif self.posture.shoulder[1] < -20:
            self.upper_arm_score += 2
        elif self.posture.shoulder[1] > 20 and self.posture.shoulder[1] <= 45:
            self.upper_arm_score += 2
        elif self.posture.shoulder[1] > 45 and self.posture.shoulder[1] <= 90:
            self.upper_arm_score += 3
        elif self.posture.shoulder[1] > 90 :
            self.upper_arm_score += 4
        else:
            pass
        # Adjustment
        if self.posture.raised_shoulder :
            self.upper_arm_score +=1
        if self.posture.shoulder[0] > 5 : #upperarm is abducted  --- different rerference point than RULA
            self.upper_arm_score +=1
        if self.task.supported_arm or self.task.lean_on_arm:   #supported arm or learning
            self.upper_arm_score +=-1

    def lower_arm_analysis(self):

        if self.posture.elbow[0] >= 60 and self.posture.elbow[0] <= 100:
            self.lower_arm_score +=1
        else:
            self.lower_arm_score +=2


    def wrist_analysis(self):
        if self.posture.wrist[0] >= -15 and self.posture.wrist[0] <= 15:
            self.wrist_score +=1
        else:
            self.wrist_score +=2
        # Adjustment
        if self.posture.wrist[1] >= 5 or self.posture.wrist[1] <= -5 or self.posture.elbow[1] < -20 or self.posture.elbow[1] > 20:
                self.wrist_score +=1



    # def arm_muscle_use_anslysis(self):
    #     if self.task.arm_motion == 'static' or self.task.arm_motion_freq >= 4: #held>10min
    #         self.arm_muscle_use_score +=1
    #
    # def arm_load_analysis(self):
    #     if self.task.arm_load < 2 and self.task.arm_load_type == 'intermittent': # (Kg)
    #         self.arm_load_score +=0
    #     elif self.task.arm_load >=2 and self.task.arm_load <10 and self.task.arm_load_type == 'intermittent':
    #         self.arm_lead_score +=1
    #     elif self.task.arm_load >=2 and self.task.arm_load <10 and self.task.arm_load_type != 'intermittent' and self.task.arm_load_type !='shock':
    #         self.arm_load_score +=2
    #     elif self.task.arm_load > 10 or self.task.arm_load_type=='repeated' or self.task.arm_load_type =='shock':
    #         self.arm_load_score +=3
    #     else:
    #         pass





    # def body_muscle_use_analysis(self):
    #     if self.task.body_motion == 'dynamic' or self.task.body_motion_freq >= 4: #held>10min
    #         self.body_muscle_use_score +=1

    def body_load_analysis(self):
        if self.task.body_load < 11 : # (lbs)
            self.body_load_score += 0
        elif self.task.body_load >= 11 and self.task.body_load < 22:
            self.body_load_score += 1
        elif self.task.body_load >= 22:
            self.body_load_score += 2
        else:
            pass

        if self.task.body_load_type == 'rapid_buildup' or self.task.body_load_type =='shock':
            self.body_load_score += 1


    def analyze_REBA(self):
        if self.score == 1 or self.score == 1:
            self.REBA_interpretion = 'Acceptable posture'
        elif self.score == 3  or self.score == 4:
            self.REBA_interpretion = 'Further investigation, change may be needed'
        elif self.score == 5 or self.score == 6:
            self.REBA_interpretion = 'Further investigation, change soon'
        elif self.score == 7:
            self.REBA_interpretion = 'Investigate and implement change'
        else:
            self.REBA_interpretion = 'Wrong REBA score'





class Task():

    def __init__(self):
        # super(Task, self).__init__()
        self.raised_shoulder = False
        self.supported_arm = False
        self.lean_on_arm = False
        self.arm_motion = 'dynamic'  #or 'dynamic'
        self.arm_motion_freq = 3 #frequency (occurance/min)
        self.arm_load = 0.1 # max load on arm in (kg)
        self.arm_load_type = 'intermittent'  # or 'repeated' or 'shock'
        self.supported_leg_feet = True
        self.body_motion = 'static'  #or 'dynamic'
        self.body_motion_freq = 3 #frequency (occurance/min)
        self.body_load = 0 # max load on body in (Lbs)
        self.body_load_type = 'intermittent'  # or 'repeated' or 'shock'
        self.seating = True
        self.walking = False
        self.bilateral_weight_bearing = False
        self.coupling_score = 0   # 0: good, 1: fair, 2:poor, 3: unacceptable
        self.activity_score = 0


class CurrentPosture(object):
    '''
    zero is based on RULA-REBA worksheet, not the neutral posture or T-pose
    '''
    def __init__(self):
        # super(CurrentPosture, self).__init__()
        self.torso = [0, 0, 0]
        self.neck = [0, 0, 0]
        self.shoulder = [0, 0, 0] #abduction, forward-backward, rotation
        self.elbow = [0, 0] # flextion, rotation
        self.wrist = [0, 0] #flextion, lateral bending
        self.ankle = 0
        self.raised_shoulder = False
if __name__ == '__main__':
    posture = CurrentPosture()

    assessment = REBA_assessment()
    print assessment.REBA_score(posture)
