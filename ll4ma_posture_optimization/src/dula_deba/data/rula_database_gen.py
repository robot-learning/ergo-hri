#!/usr/bin/env python

import numpy as np
from rula import *
import torch
import pickle


RULA = RULA_assessment()
# generate random posture and get rula scores
# N is batch size; D_in is input dimension;
# H is hidden dimension; D_out is output dimension.
N = 3200000

# Create random Tensors to hold inputs and outputs
joints_ll = [-5.0,  -15.0,  -15.0,  -45.0,  -90.0,  -45.0,  0.0,  -70.0,  -20.0,  -45.0]
joints_ul = [20.0, 15.0, 15.0, 120.0, 90.0, 135.0, 150.0, 70.0, 20.0, 45.0]
a = [float(joints_ll[i])*np.pi/180 for i in range(len(joints_ll))]
b = [float(joints_ul[i])*np.pi/180 for i in range(len(joints_ul))]

neutral_posture = [0. , 0. , 0. , 1.5708 , 0. , 0. , 1.5708 , 0. , 0. , 0. ]
netrual_std = [0.08 , 0.08 , 0.08 , 0.08 , 0.08 , 0.08 , 0.08 , 0.08 , 0.08, 0.08 ]

score_counter = np.zeros(7) #number of RULA scores to be included in the dataset, Currently from 0 to 6
x = []
y = []



# Uncomment the bellow if you want to generate the data######
#############################################################
###############################################################

'''
The dataset gets labels from 0 t0 6!!

'''

# while (not found_all) and (sum(score_counter)<N):
while min(score_counter[1:-1]) < 300000:
    # generating score=1 data
    while score_counter[0] < 300000:
        #checking if the data is in joint limit
        in_limit = True
        rand_x = np.random.normal(neutral_posture, netrual_std)
        for i in range(10):
            if rand_x[i] < a[i] or rand_x[i]>b[i]:
                in_limit = False
        if in_limit:
            rand_y = RULA.rula_score(rand_x.tolist())
            if rand_y == 1:
                x.append(rand_x.tolist())

                y.append(rand_y-1)
                score_counter[rand_y-1] += 1
                print score_counter

    # Generating data for other scores
    rand_x = np.random.uniform(a, b)
    rand_y = RULA.rula_score(rand_x.tolist())

    if score_counter[-1] < 300000 and rand_y != 1:
        x.append(rand_x.tolist())
        y.append(rand_y-1)
        score_counter[rand_y-1] += 1
    elif score_counter [1] < 300000 and rand_y == 2:
        x.append(rand_x.tolist())
        y.append(rand_y-1)
        score_counter[rand_y-1] += 1
    else:
        pass
    print score_counter

pickle_RULA = open("/home/amir/catkin_ws/src/ergo-hri/ll4ma_posture_optimization/src/rula/data/RULA_data_{}sample_unbalance.pickle".format(len(y)),"wb")

'''
Pickle uses different protocols to convert your data to a binary stream.

In python 2 there are 3 different protocols (0, 1, 2) and the default is 0.

In python 3 there are 5 different protocols (0, 1, 2, 3, 4) and the default is 3.

You must specify in python 3 a protocol lower than 3 in order to be able to load the data in python 2. You can specify the protocol parameter when invoking pickle.dump.
'''

pickle.dump([x,y], pickle_RULA, protocol=2) #
pickle_RULA.close()
