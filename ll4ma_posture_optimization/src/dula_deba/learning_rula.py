#!/usr/bin/env python

import numpy as np
from rula import *
import torch


RULA = RULA_assessment()
# generate random posture and get rula scores
# N is batch size; D_in is input dimension;
# H is hidden dimension; D_out is output dimension.
N, D_in, H1, H2, H3, D_out = 10000, 10, 50, 30, 20, 1

# Create random Tensors to hold inputs and outputs
joints_ll = [-10.0,  -10.0,  -10.0,  -45.0,  -90.0,  -45.0,  0.0,  -70.0,  -30.0,  -45.0]
joints_ul = [10.0, 10.0, 10.0, 120.0, 90.0, 135.0, 150.0, 70.0, 20.0, 45.0]

a = torch.tensor([float(joints_ll[i])*np.pi/180 for i in range(len(joints_ll))])
b = torch.tensor([float(joints_ul[i])*np.pi/180 for i in range(len(joints_ul))])

x = torch.distributions.Uniform(a, b).sample((N,1))
x = torch.reshape(x, (N, 10))
print(x)
# y = torch.randn(N, D_out)

y = torch.stack([torch.tensor(float(RULA.rula_score(x_i.tolist()))) for x_i in torch.unbind(x, dim=0)], dim=0)
y = torch.reshape(y, (N, 1))

# Use the nn package to define our model and loss function.
model = torch.nn.Sequential(
    torch.nn.Linear(D_in, H1),
    # torch.nn.Linear(H1, H2),
    # torch.nn.Linear(H2, H3),
    torch.nn.ReLU(),
    torch.nn.Linear(H1, D_out),
)
loss_fn = torch.nn.MSELoss(reduction='sum')


# Use the optim package to define an Optimizer that will update the weights of
# the model for us. Here we will use Adam; the optim package contains many other
# optimization algorithms. The first argument to the Adam constructor tells the
# optimizer which Tensors it should update.
learning_rate = 1e-1
optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)
for t in range(5000):
    # Forward pass: compute predicted y by passing x to the model.
    y_pred = model(x)
    # print(list(y_pred.size()))
    #
    # print(list(y.size()))

    # Compute and print loss.
    loss = loss_fn(y_pred, y)
    # print(loss)
    if t % 100 == 99:
        print(t, loss.item())

    # Before the backward pass, use the optimizer object to zero all of the
    # gradients for the variables it will update (which are the learnable
    # weights of the model). This is because by default, gradients are
    # accumulated in buffers( i.e, not overwritten) whenever .backward()
    # is called. Checkout docs of torch.autograd.backward for more details.
    optimizer.zero_grad()

    # Backward pass: compute gradient of the loss with respect to model
    # parameters
    loss.backward()

    # Calling the step function on an Optimizer makes an update to its
    # parameters
    optimizer.step()

joints_1 = [-2.0,  0,  -2.0,  30.0,  -45.0,  -20.0,  30.0,  -60.0,  10.0,  25.0]
j_1 =[float(joints_1[i])*np.pi/180 for i in range(len(joints_1))]
joints_2 = [5.0, 1.0, 5.0, 60.0, 45.0, 35.0, 80.0, 50.0, -10.0, 15.0]
j_2 =[float(joints_2[i])*np.pi/180 for i in range(len(joints_2))]
joints_3 = [0.0, 0.0, 0.0, 90.0, 0.0, 0.0, 90.0, 0.0, 0.0, 0.0]
j_3 =[float(joints_3[i])*np.pi/180 for i in range(len(joints_3))]
print(j_3)

print RULA.rula_score(j_1)
print model(torch.tensor([j_1]))
print RULA.rula_score(j_2)
print model(torch.tensor([j_2]))
print RULA.rula_score(j_3)
print model(torch.tensor([j_3]))
