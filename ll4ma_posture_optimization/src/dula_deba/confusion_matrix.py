import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from rula import *

import pickle
import torch
from torch.autograd import Variable
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
import skorch
from skorch import NeuralNetRegressor, NeuralNetClassifier
from torch.utils.data import Dataset, random_split, DataLoader
from sklearn.model_selection import RandomizedSearchCV, GridSearchCV

from sklearn.metrics import mean_squared_error as MSE
from sklearn.metrics import r2_score, accuracy_score, confusion_matrix


import pycuda.driver as cuda


class RULADataset(Dataset):

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __getitem__(self,index):
        sample = {
        'feature': torch.tensor([self.x[index]], dtype=torch.float32).cuda(),
        'label': torch.tensor([self.y[index]], dtype=torch.float32).cuda()}
        return sample

    def __len__(self):
        return len(self.x)

def check_accuracy(test_loader, model, dev):
    num_correct = 0
    total = 0
    model.eval()
    predictions_list = []
    labels_list = []

    with torch.no_grad():
        for data in test_loader:
            # print (data)
            inputs,labels = data['feature'].reshape(1,10), data['label'].reshape(1)
            inputs = inputs.to(device=dev)
            labels = labels.to(device=dev)

            predictions = model(inputs)
            # print (torch.round(predictions).detach().numpy()[0], labels.detach().numpy()[0])
            predictions_list.append(torch.round(predictions[0]).detach().numpy())
            labels_list.append(labels.detach().numpy()[0])

            if torch.round(predictions[0]) == labels[0]:
                num_correct += 1
            total += 1

        print(f"Test Accuracy of the model: {float(num_correct)/float(total)*100:.2f}")
    cm = confusion_matrix(labels_list,predictions_list)
    print ("confusion matrix: \n" , cm)
    plot_confusion_matrix(cm,
                      normalize    = True,
                      target_names = ['1', '2', '3', '4', '5', '6', '7'],
                      title        = "Confusion Matrix for DULA")


def plot_confusion_matrix(cm,
                          target_names,
                          title='Confusion matrix',
                          cmap=None,
                          normalize=True):
    """
    given a sklearn confusion matrix (cm), make a nice plot

    Arguments
    ---------
    cm:           confusion matrix from sklearn.metrics.confusion_matrix

    target_names: given classification classes such as [0, 1, 2]
                  the class names, for example: ['high', 'medium', 'low']

    title:        the text to display at the top of the matrix

    cmap:         the gradient of the values displayed from matplotlib.pyplot.cm
                  see http://matplotlib.org/examples/color/colormaps_reference.html
                  plt.get_cmap('jet') or plt.cm.Blues

    normalize:    If False, plot the raw numbers
                  If True, plot the proportions

    Usage
    -----
    plot_confusion_matrix(cm           = cm,                  # confusion matrix created by
                                                              # sklearn.metrics.confusion_matrix
                          normalize    = True,                # show proportions
                          target_names = y_labels_vals,       # list of names of the classes
                          title        = best_estimator_name) # title of graph

    Citiation
    ---------
    http://scikit-learn.org/stable/auto_examples/model_selection/plot_confusion_matrix.html

    """
    import matplotlib.pyplot as plt
    import numpy as np
    import itertools
    font_size = 12
    accuracy = np.trace(cm) / float(np.sum(cm))
    misclass = 1 - accuracy


    if cmap is None:
        cmap = plt.get_cmap('Blues')

    plt.figure(figsize=(8, 7))
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title, fontsize=font_size)
    # plt.colorbar()

    if target_names is not None:
        tick_marks = np.arange(len(target_names))
        plt.xticks(tick_marks, target_names, rotation=45, fontsize=font_size)
        plt.yticks(tick_marks, target_names, fontsize=font_size)

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]*100


    thresh = cm.max() / 1.5 if normalize else cm.max() / 2
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        if normalize:
            plt.text(j, i, "{:0.2f}".format(cm[i, j]),
                     horizontalalignment="center", fontsize=font_size,
                     color="white" if cm[i, j] > thresh else "black")
        else:
            plt.text(j, i, "{:,}".format(cm[i, j]),
                     horizontalalignment="center",
                     color="white" if cm[i, j] > thresh else "black")


    plt.tight_layout()
    plt.ylabel('RULA Score (True Label)', fontsize=font_size)
    # plt.xlabel('Predicted label\n accuracy={:0.2f}; misclass={:0.2f}'.format(accuracy, misclass))
    plt.xlabel('DULA Score (Predicted Label)' , fontsize=font_size)
    plt.show()
    plt.savefig('confusion_matrix_new.pdf')

if __name__=='__main__':

    device = torch.device('cpu')
    N = 100000

    # load the model
    PATH = "/home/amir/catkin_ws/src/ergo-hri/ll4ma_posture_optimization/src/rula/RULA_regression_99-73percent.pt"
    model = torch.load(PATH, map_location=device)
    model.eval()

    # load the dataset
    pickle_RULA = open("/home/amir/catkin_ws/src/ergo-hri/ll4ma_posture_optimization/src/rula/data/RULA_data_{}sample.pickle".format(N),"rb")
    x, y = pickle.load(pickle_RULA)  # x: input data, y: labels
    pickle_RULA.close()

    dataset = RULADataset(x, y)
    # Splitting the dataset
    train, test = random_split(dataset, [int(0.1*len(dataset)), int(0.9*len(dataset))])

    test_loader = DataLoader(test, batch_size=1, shuffle=True, sampler=None,
               batch_sampler=None, num_workers=0, collate_fn=None,
               pin_memory=False, drop_last=False, timeout=0,
               worker_init_fn=None)

    check_accuracy(test_loader, model, device)
