#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from rula import *

import pickle
import torch
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
import skorch
from skorch import NeuralNetRegressor, NeuralNetClassifier

from sklearn.model_selection import RandomizedSearchCV, GridSearchCV

from sklearn.metrics import mean_squared_error as MSE
from sklearn.metrics import r2_score, accuracy_score


class MyModule(torch.nn.Module):
    ###### define the model #############
    def __init__(self,num_units=10,nonlin=torch.nn.functional.relu,drop=.5):
        super(MyModule,self).__init__()
        D_in, H1, H2, H3, D_out = 10, 48, 10, 30, 6 # N is batch size; D_in is input dimension;
        # H is hidden dimension; D_out is output dimension.
        self.module = torch.nn.Sequential(
            torch.nn.Linear(D_in, H1),
            torch.nn.ReLU(),
            torch.nn.Linear(H1, H2),
            torch.nn.ReLU(),
            torch.nn.Linear(H2, H3),
            torch.nn.ReLU(),
            torch.nn.Linear(H3, D_out),
        )
    ###### forward of the model: gets input, returns output #############
    def forward(self,X):
        X = self.module(X)
        return X


###### report the cross-validation results #############
def report(results, n_top=3):
    for i in range(1, n_top + 1):
        candidates = np.flatnonzero(results['rank_test_score'] == i)
        for candidate in candidates:
            print("Model with rank: {0}".format(i))
            print("Mean validation score: {0:.3f} (std: {1:.3f})".format(
                  results['mean_test_score'][candidate],
                  results['std_test_score'][candidate]))
            print("Parameters: {0}".format(results['params'][candidate]))
            print("")



############## Loading the data #####################################
RULA = RULA_assessment() # generate random posture and get rula scores

N = 600000 # number of data samples

pickle_RULA = open("data/RULA_data_600000.pickle","rb")
x, y = pickle.load(pickle_RULA)  # x: input data, y: labels
pickle_RULA.close()
#


#############3 Splitting dataset for test and train #######################
X_train,X_test,y_train,y_test = train_test_split(np.array(x), np.array(y).reshape(len(y),1), test_size=.2, shuffle = True)

######### if classification


net = NeuralNetClassifier(
    MyModule,
    criterion=torch.nn.CrossEntropyLoss,
    max_epochs=10,
    optimizer=torch.optim.SGD,
    optimizer__lr = .005
)



params = {
    'optimizer__lr': [0.1],   # learning rate
    'max_epochs':[300],
    'module__num_units': [320],
    'module__drop' : [0.3]
}
############## hint: number of hidden layers as hyper param did not work with pytorch
############## we do it manually and compare them together

############ searching for the best hyper paramaters through cross validation ####################
gs = GridSearchCV(net,params,refit=True,cv=3,scoring='accuracy')
gs.fit(X_train.astype('f'), y_train.squeeze(1));    # for classification to work, y_train should be a row-like array that
# is done using squeeze function, reshape does not work too.
report(gs.cv_results_,10)  # report the top 10 results

###########3# saving the best estimator model ##############3
PATH = "CV_RULA_model_classifocation.pt"
torch.save(gs.best_estimator_, PATH)

################# printing and plottin the result for cross validation #################################3
# get training and validation loss
epochs = [i for i in range(len(gs.best_estimator_.history))]
train_loss = gs.best_estimator_.history[:,'train_loss']
valid_loss = gs.best_estimator_.history[:,'valid_loss']

plt.plot(epochs,train_loss,'g-')    # plot for train loss and validation loss
plt.plot(epochs,valid_loss,'r-')
plt.title('Training Loss Curves')
plt.xlabel('Epochs')
plt.ylabel('Cross Entropy Loss')
plt.legend(['Train','Validation'])

plt.show()

# predict on test data
y_pred = gs.best_estimator_.predict(X_test.astype(np.float32))

sns.kdeplot(y_pred.squeeze(), label='estimate', shade=True)
sns.kdeplot(y_test.squeeze(), label='true', shade=True)
plt.xlabel('RULA');

plt.show()

sns.distplot(y_test.squeeze()-y_pred.squeeze(),label='error');
plt.xlabel('RULA Error');
plt.show()


############### Calculating the accuracy on test dataset ####################
accuracy = accuracy_score(y_test, y_pred.round(), normalize=True)
print("Accuracy:",(accuracy * 100.0))

print(r2_score(y_test,y_pred))

#################33 plot the data #############
plt.plot(y_pred,y_test,'g*')
plt.xlabel('predicted')
plt.ylabel('actual')
plt.title('$R^{2}$ visual');

plt.show()


# show where the big errors were
# errors = np.where(abs(y_test-y_pred)>.2)
# for tup in zip(y_test[errors],y_pred[errors]):
#     print(tup)
