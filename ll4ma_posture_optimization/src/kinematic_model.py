#!/usr/bin/env python

import rospy
import roslib
from urdf_parser_py.urdf import Robot,URDF
from pykdl_utils.kdl_parser import kdl_tree_from_urdf_model
from pykdl_utils.kdl_kinematics import KDLKinematics
#from sensor_msgs.msg import JointState

import numpy as np
from math import sin, cos, pi
import time

_DISPLAY_RATE = 100



_TEST_TORQUE_CONTROL=False
_TEST_IK_CONTROL=True

class Kinematic_Model:
    def __init__(self,_EE_NAME, _BASE_LINK, rob_desc='robot_description'):
        '''
        Simple model of manipulator kinematics and controls
        Assume following state and action vectors
        urdf_file_name - model file to load
        '''
        # Load KDL tree
        # urdf_file = file(urdf_file_name, 'r')
        self.robot = URDF.from_parameter_server(key=rob_desc)
        # self.robot = Robot.from_xml_string(urdf_file.read())
        # urdf_file.close()
        self.tree = kdl_tree_from_urdf_model(self.robot)

        task_space_ik_weights = np.diag([1.0, 1.0, 1.0, 0.0, 0.0, 0.0]).tolist()

        #self.base_link = self.robot.get_root()
        self.base_link = _BASE_LINK

        self.joint_chains=[]

        self.chain = KDLKinematics(self.robot, self.base_link, _EE_NAME)
        _LINK_NAMES = self.chain.get_link_names()
        # print _LINK_NAMES
        self.chain.joint_limits_upper
        self.chain.joint_limits_lower

        for l_name in _LINK_NAMES:
            jc=KDLKinematics(self.robot,self.base_link,l_name)
            self.joint_chains.append(jc)


    def get_joint_limits(self):

        return self.chain.joint_limits_upper, self.chain.joint_limits_lower


    def FK_joint(self,joint_angles,j_index):
        '''
        Method to return task coordinates between base link and any joint
        joint_angles must contain only 0:j_index joints
        '''
        fi_x=self.joint_chains[j_index].forward(joint_angles)

        return fi_x

    def Jacobian_joint(self,joint_angles,j_index):
        ji_x=self.joint_chains[j_index].jacobian(joint_angles)
        return ji_x

    def FK(self, joint_angles):
        '''
        Method to convert joint positions to task coordinates
        '''
        fi_x = self.chain.forward(joint_angles)
        return fi_x

    def Jacobian(self,joint_angles):
        ji_x = self.chain.jacobian(joint_angles)
        return ji_x


    #TODO: CHECK if IK works
    def IK(self, pose, q_guess):
        joint_angles = self.chain.inverse(pose, q_guess)

        return joint_angles
