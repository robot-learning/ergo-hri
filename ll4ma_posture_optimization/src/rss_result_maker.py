import numpy as np
from rula import *
from posture_optimization_problem import *
from solvers import *
from simulation import *
from motion_generation import *
from std_msgs.msg import Char
from sensor_msgs.msg import JointState
from utils.util import *
import pickle
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d
from pylab import plot, show, savefig, xlim, figure, \
                hold, ylim, legend, boxplot, setp, axes


def setBoxColors(bp, edge_color, fill_color, mid_color):
	for element in ['boxes', 'whiskers', 'fliers', 'caps']:
		plt.setp(bp[element], color=edge_color)
	plt.setp(bp['medians'], color=mid_color)
	# for patch in bp['boxes']:
		# patch.set(facecolor=fill_color)

def smooth_traj(joint_traj, smoothing_resolution):
    '''
    joint traj: 10 DOF human trajectory

    '''
    smoothed_traj = []
    for i in range(len(joint_traj)-1):
            smoothed_traj += np.linspace(np.array(joint_traj[i]), np.array(joint_traj[i+1]),smoothing_resolution+1, endpoint=False).tolist()
    smoothed_traj.append(joint_traj[-1])

    return smoothed_traj

def smooth_traj_optimal_posture(optimal_posture_traj, smoothing_resolution):
    '''
    joint traj: 10 DOF human trajectory

    '''
    smoothed_traj = []
    for i in range(len(optimal_posture_traj)-1):
            for j in range(smoothing_resolution+1):
                smoothed_traj.append(optimal_posture_traj[i])
                smoothed_traj.append(optimal_posture_traj[-1])

    return smoothed_traj

def plot_results(wc_traj_teleoperator_joint_smooth, nc_traj_teleoperator_joint_smooth, wc_traj_optimal_posture_smooth, nc_traj_optimal_posture_smooth,
             wc_traj_teleoperator_ee_smooth, nc_traj_teleoperator_ee_smooth, wc_traj_RULA_teleoperator_smooth, nc_traj_RULA_teleoperator_smooth,
             wc_traj_leader_ee_smooth, nc_traj_leader_ee_smooth, wc_traj_leader_joint_smooth, nc_traj_leader_joint_smooth,
             wc_traj_RULA_optimal_smooth, nc_traj_RULA_optimal_smooth, wc_traj_follower_ee_smooth, nc_traj_follower_ee_smooth,
             wc_traj_follower_joint_smooth, nc_traj_follower_joint_smooth, wc_traj_RULA_teleoperator, nc_traj_RULA_teleoperator):

    plt.rcParams.update({'font.size':14})
    plt.rcParams['text.usetex'] = True
    plt.rcParams['text.latex.preamble'] = [r'\usepackage{amsmath}']

    blue_circle = dict(markerfacecolor='deepskyblue', marker='o') ## TODO: move to above initialization part of the function
    peru_circle = dict(markerfacecolor='olivedrab', marker='o')
    red_circle = dict(markerfacecolor='red', marker='o')
    #############################################################################3
    #Plot (1): RULA of wc, nc before smoothing
    fig, ax = plt.subplots()
    ax.plot(wc_traj_RULA_teleoperator)
    ax.plot(nc_traj_RULA_teleoperator)
    ax.plot(wc_traj_RULA_optimal)

    ax.set_ylabel(r'$\mathrm{RULA~Score}$', fontsize=20)
    ax.set_xlabel(r'$\mathrm{Time~(s)}$', fontsize=20)
    ax.set_yticklabels([r'$\mathrm{1}$', r'$\mathrm{2}$', r'$\mathrm{3}$', r'$\mathrm{4}$', r'$\mathrm{5}$', r'$\mathrm{6}$', r'$\mathrm{7}$'])
    ax.set_yticks([1, 2, 3, 4, 5, 6, 7])
    ax.legend([r'$\mathrm{With~Postural~Correction}$', r'$\mathrm{Without~Postural~Correction}$', r'$\mathrm{Optimal~Posture}$' ], loc='upper right', ncol=1, fontsize=14)
    ax.set_ylim((1,8))


    ############################################################################
    # # Plot (2): RULA of wc, nc after smoothing
    # fig, ax = plt.subplots()
    # ax.plot(wc_traj_RULA_teleoperator_smooth)
    # ax.plot(nc_traj_RULA_teleoperator_smooth)
    #
    # ax.set_ylabel(r'$\mathrm{RULA~Score}$', fontsize=20)
    # ax.set_xlabel(r'$\mathrm{Time~(s)}$', fontsize=20)
    # ax.set_yticklabels([r'$\mathrm{1}$', r'$\mathrm{2}$', r'$\mathrm{3}$', r'$\mathrm{4}$', r'$\mathrm{5}$', r'$\mathrm{6}$', r'$\mathrm{7}$'])
    # ax.set_yticks([1, 2, 3, 4, 5, 6, 7])
    # ax.legend([r'$\mathrm{Withour~Postural~Correction}$', r'$\mathrm{With~Postural~Correction}$' ], loc='upper right', ncol=1, fontsize=14)
    # ax.set_ylim((1,8))

    ###########################################################################
    # Plot (3): traj of wc, nc after smoothing
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    # ax.set_aspect('equal')

    wc_x = np.array([wc_traj_teleoperator_ee[t][0] for t in range(1,len(wc_traj_teleoperator_ee))])
    wc_y = np.array([wc_traj_teleoperator_ee[t][1] for t in range(1,len(wc_traj_teleoperator_ee))])
    wc_z = np.array([wc_traj_teleoperator_ee[t][2] for t in range(1,len(wc_traj_teleoperator_ee))])
    nc_x = np.array([nc_traj_teleoperator_ee[t][0] for t in range(1,len(nc_traj_teleoperator_ee))])
    nc_y = np.array([nc_traj_teleoperator_ee[t][1] for t in range(1,len(nc_traj_teleoperator_ee))])
    nc_z = np.array([nc_traj_teleoperator_ee[t][2] for t in range(1,len(nc_traj_teleoperator_ee))])
    ax.plot(wc_x,wc_y,wc_z, zdir='z')
    ax.plot(nc_x,nc_y,nc_z, zdir='z' )
    ax.set_xlabel(r'$\mathrm{X}$', fontsize=14)
    ax.set_ylabel(r'$\mathrm{Y}$', fontsize=14)
    ax.set_zlabel(r'$\mathrm{Z}$', fontsize=14)

    max_range = np.array([wc_x.max()-wc_x.min(), wc_y.max()-wc_y.min(), wc_z.max()-wc_z.min()]).max() / 2.0

    mid_x = (wc_x.max()+wc_x.min()) * 0.5
    mid_y = (wc_y.max()+wc_y.min()) * 0.5
    mid_z = (wc_z.max()+wc_z.min()) * 0.5
    ax.set_xlim(mid_x - max_range, mid_x + max_range)
    ax.set_ylim(mid_y - max_range, mid_y + max_range)
    ax.set_zlim(mid_z - max_range, mid_z + max_range)
    ax.view_init(30,-160)
    ax.legend([r'$\mathrm{With~Postural~Correction}$', r'$\mathrm{Without~Postural~Correction}$' ], loc='upper right', ncol=1, fontsize=14)

    ###########################################################################
    # Plot (4): bar plot of RULA scores before smoothing

    fig,ax = plt.subplots()
    width = 0.15

    # adding points to the box plot
    print np.mean(wc_traj_RULA_teleoperator), np.std(wc_traj_RULA_teleoperator)

    bp1= ax.boxplot(wc_traj_RULA_teleoperator, positions = [0.1],patch_artist=True, flierprops=blue_circle, showfliers=True)
    setBoxColors(bp1, 'black', 'deepskyblue', 'red')

    bp2= ax.boxplot(nc_traj_RULA_teleoperator, positions = [0.4], patch_artist=True, flierprops=blue_circle, showfliers=True)
    setBoxColors(bp2, 'black', 'coral', 'red')

    # setting the axes names, lables and numbers
    ax.set_xticklabels([r'$\mathrm{With~Postural}$'+ '\n' + r'$\mathrm{~Correction}$', r'$\mathrm{Without~Postural}$'+ '\n' + r'$\mathrm{~Correction}$'], fontsize=14)
    ax.set_xticks([0.1, 0.4])
    ax.set_yticks([2, 3, 4, 5, 6, 7])
    ax.set_xlim(0, 0.8)
    ax.set_ylabel(r'$\mathrm{RULA Score}$')
    # setting the legend
    # ax.legend([r'$\mathrm{With~Postural~Correction}$', r'$\mathrm{Without~Postural~Correction}$' ], loc='upper right', ncol=1, fontsize=14)
    # grid on
    ax.yaxis.grid(True)
    # saving the plot figure
    # plt.savefig("/home/amir/PF_paper_figures/PF_IKvsMocap_error_all_trials_vs_tasks-boxplot_{}.pdf".format(joint_limit_type), dpi =300, bbox_inches='tight')

    plt.show()



def plot_results_trials(wc_traj_RULA_trials, wc_traj_teleoperator_joint_trials, nc_traj_RULA_trials, nc_traj_teleoperator_joint_trials):

    plt.rcParams.update({'font.size':14})
    plt.rcParams['text.usetex'] = True
    plt.rcParams['text.latex.preamble'] = [r'\usepackage{amsmath}']

    blue_circle = dict(markerfacecolor='deepskyblue', marker='o') ## TODO: move to above initialization part of the function
    peru_circle = dict(markerfacecolor='olivedrab', marker='o')
    red_circle = dict(markerfacecolor='red', marker='o')

	# Plot (4): bar plot of RULA scores before smoothing

    fig,ax = plt.subplots()
    width = 0.15


    # adding points to the box plot
    # print np.mean(wc_traj_RULA_teleoperator), np.std(wc_traj_RULA_teleoperator)

    # bp1= ax.boxplot(wc_traj_RULA_trials[0], positions = [0.1],patch_artist=True, flierprops=blue_circle, showfliers=True)
    bp1= ax.hist(wc_traj_RULA_trials[0], orientation="horizontal")
    # setBoxColors(bp1, 'black', 'deepskyblue', 'red')



    # setting the axes names, lables and numbers
    # ax.set_xticklabels([r'$\mathrm{With~Postural}$'+ '\n' + r'$\mathrm{~Correction}$', r'$\mathrm{Without~Postural}$'+ '\n' + r'$\mathrm{~Correction}$'], fontsize=14)
    # ax.set_xticks([0, 20])
    # ax.set_yticks([2, 3, 4, 5, 6, 7])
    # ax.set_xlim(0, 20)
    # ax.set_ylabel(r'$\mathrm{RULA Score}$')
    # # setting the legend
    # # ax.legend([r'$\mathrm{With~Postural~Correction}$', r'$\mathrm{Without~Postural~Correction}$' ], loc='upper right', ncol=1, fontsize=14)
    # # grid on
    # ax.yaxis.grid(True)
    ax.set_title("derivative-based")
    # saving the plot figure
    # plt.savefig("/home/amir/PF_paper_figures/PF_IKvsMocap_error_all_trials_vs_tasks-boxplot_{}.pdf".format(joint_limit_type), dpi =300, bbox_inches='tight')
###############################################
    # plt.figure()
    fig,ax = plt.subplots()
    width = 0.15

    # adding points to the box plot
    # print np.mean(wc_traj_RULA_teleoperator), np.std(wc_traj_RULA_teleoperator)

    bp1= ax.boxplot(wc_traj_RULA_trials[1], positions = [0.1],patch_artist=True, flierprops=blue_circle, showfliers=True)
    setBoxColors(bp1, 'black', 'deepskyblue', 'red')

    bp2= ax.boxplot(nc_traj_RULA_trials[1], positions = [0.4], patch_artist=True, flierprops=blue_circle, showfliers=True)
    setBoxColors(bp2, 'black', 'coral', 'red')

    # setting the axes names, lables and numbers
    ax.set_xticklabels([r'$\mathrm{With~Postural}$'+ '\n' + r'$\mathrm{~Correction}$', r'$\mathrm{Without~Postural}$'+ '\n' + r'$\mathrm{~Correction}$'], fontsize=14)
    ax.set_xticks([0.1, 0.4])
    ax.set_yticks([2, 3, 4, 5, 6, 7])
    ax.set_xlim(0, 0.8)
    ax.set_ylabel(r'$\mathrm{RULA Score}$')
    # setting the legend
    # ax.legend([r'$\mathrm{With~Postural~Correction}$', r'$\mathrm{Without~Postural~Correction}$' ], loc='upper right', ncol=1, fontsize=14)
    # grid on
    ax.yaxis.grid(True)
    ax.set_title("derivative-free")
    # saving the plot figure
    # plt.savefig("/home/amir/PF_paper_figures/PF_IKvsMocap_error_all_trials_vs_tasks-boxplot_{}.pdf".format(joint_limit_type), dpi =300, bbox_inches='tight')


    plt.show()




if __name__ == '__main__':



	plt.rcParams.update({'font.size':12})
	plt.rcParams['text.usetex'] = True
	plt.rcParams['text.latex.preamble'] = [r'\usepackage{amsmath}']

	q_opt_grad_free = [3, 3, 2, 2, 3, 2, 2, 2, 3, 1, 1, 2, 2, 1, 2, 2, 2, 2, 2]
	corrected_grad_free = [3, 3, 6, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5, 4, 4]

	q_opt_grad_base = [3, 3, 3, 4, 3, 3, 3, 3, 3, 2, 2, 2]
	corrected_grad_base = [3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4]

	fig,ax = plt.subplots(1)
	bp = ax.boxplot([q_opt_grad_free, q_opt_grad_base, corrected_grad_free, corrected_grad_base],  whis = 'range', patch_artist=False)
	setBoxColors(bp, 'black', 'red', 'red')
	ax.set_xticklabels([r'$\mathrm{Optimal~Posture}$'+ '\n' + r'$\mathrm{Grad-Free}$', r'$\mathrm{Optimal~Posture}$'+ '\n' + r'$\mathrm{Grad-Based}$', r'$\mathrm{Corrected~Posture}$'+ '\n' + r'$\mathrm{Grad-Free}$', r'$\mathrm{Corrected~Posture}$'+ '\n' + r'$\mathrm{Grad-Based}$'])
	ax.set_xticks([1, 2, 3, 4])
	ax.set_ylabel(r'$\mathrm{RULA~Score}$', fontsize=12)
	plt.show()
