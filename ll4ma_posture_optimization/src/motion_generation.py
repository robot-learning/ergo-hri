#!/usr/bin/env python

import numpy as np
from rula import *
from simulation import *
from scipy.optimize import minimize, Bounds
from copy import deepcopy
from utils.util import teleoperator_to_leader_transform, rotation_diff, position_diff, rot_from_T, pos_from_T, T_from_R_and_D

################################################


class Motion_Generation():

    def __init__(self,task,teleoperator,leader,follower):

        self.teleoperator = teleoperator
        self.leader = leader
        self.follower = follower
        self.task = task

        self.T = 4
        self.sigma_pose =   np.diag([1, 1, 1, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3])     # covariance matrix for position and orintation
        self.sigma_joints =   np.diag([10, 10, 10, 1, 1, 1, 1, 3, 3, 3])     # covariance matrix for position and orintation

        self.alpha = 1 # weight for correcting the posture vs moving towards the goal

        self.initial_sol = []
        for i in range(self.T):
            for j in range(self.teleoperator.n_h):
                self.initial_sol.append(self.teleoperator.current_posture[j])
        for i in range(self.T):
            for j in range(self.leader.n_l):
                self.initial_sol.append(self.leader.current_config[j])
        for i in range(self.T):
            for j in range(self.follower.n_f):
                self.initial_sol.append(self.follower.current_config[j])

        self.initial_solution = np.array(self.initial_sol)

        self.joint_limits_traj = []

        for i in range(self.T):
            for j in range(self.teleoperator.n_h):
                self.joint_limits_traj.append((self.teleoperator.q_ll_rad[j], self.teleoperator.q_ul_rad[j]))   # joint lims for teleoperator

        for i in range(self.T):
            for j in range(self.leader.n_l):
                self.joint_limits_traj.append((self.leader.q_ll[j], self.leader.q_ul[j]))   # joint lims for leader

        for i in range(self.T):
            for j in range(self.follower.n_f):
                self.joint_limits_traj.append((self.follower.q_ll[j], self.follower.q_ul[j]))   # joint lims for follower

        self.z_f_max_v = 0.1

        self.epsilon_lh_pose = 0.01
        self.epsilon_lh_orientation = 0.1
        self.epsilon_lf_orientation = 0.1

        self.ee_vel_max = 0.01

        self.dt = 0.5

        self.w1 = 1
        self.w2 = 0.1

    def optimize(self, postural_correction, motion_opt_init_solution, optimal_posture=[]):
        '''

        '''
        self.optimal_posture = optimal_posture
        self.postural_correction = postural_correction
        self.initial_sol = motion_opt_init_solution
        ############################### Constraints #########################################################

        '''
        Using scipy, it requires the cost and constraint functions to have only x as the input,
        so using self is not possible. To deal with this issue, we put theese functions in
        the optimize function as a part of the optimization class.

        '''
        def cons_eq(x):
            '''
            Equality Constraints for the optimization
            x is the decision variablne including [   ]

            Constraints include:
            * (1) start pose for follower
            * (2) start pose for leader
            * (3) start posture for human
            * (4) motion scaling
            '''


            x_h = x[0:self.T*self.teleoperator.n_h].reshape(self.T,self.teleoperator.n_h)
            x_l = x[self.T*self.teleoperator.n_h : self.T*self.teleoperator.n_h+self.T*self.leader.n_l].reshape(self.T, self.leader.n_l)
            x_f = x[self.T*self.teleoperator.n_h+self.T*self.leader.n_l:len(x)].reshape(self.T,self.follower.n_f)

            # (1) start pose for follower
            const_1 = [x_f[0][i]-self.follower.current_config[i] for i in range(self.follower.n_f)]
            # (2) start pose for leader
            for i in range(self.leader.n_l):
                const_1.append(x_l[0][i]-self.leader.current_config[i])
            # (3) start posture for human
            for i in range(self.teleoperator.n_h):
                const_1.append(x_h[0][i]-self.teleoperator.current_posture[i])

            # leader_ee = [self.leader.to_global(self.leader.FK(x_l[0]))]
            # follower_ee = [self.follower.to_global(self.follower.FK(x_f[0]))]
            # for i in range(1, self.T):
            #     leader_ee.append(self.leader.to_global(self.leader.FK(x_l[i])))
            #     follower_ee.append(self.follower.to_global(self.follower.FK(x_f[i])))
            #     for j in range(3):
            #         const_1.append( (follower_ee[i][j]-follower_ee[i-1][j]) - self.leader.teleop_scale[j]*(leader_ee[i][j]-leader_ee[i-1][j]) )

            # (4) mapping between leader and follower            follower = scale * leader
            leader_ee = [pos_from_T(self.leader.to_global(self.leader.FK(x_l[0], output = "T_matrix")))]
            follower_ee = [pos_from_T(self.follower.to_global(self.follower.FK(x_f[0], output = "T_matrix")))]
            for i in range(1, self.T):
                leader_ee.append(pos_from_T(self.leader.to_global(self.leader.FK(x_l[i], output = "T_matrix"))))
                follower_ee.append(pos_from_T(self.follower.to_global(self.follower.FK(x_f[i], output = "T_matrix"))))
                for j in range(3): # for three postions
                    const_1.append( (follower_ee[i][j]-follower_ee[i-1][j]) - self.leader.teleop_scale[j]*(leader_ee[i][j]-leader_ee[i-1][j]) )

            return const_1

        def cons_ineq(x):
            '''
            InEquality constraints for optimization
            x is the decision variablne including [   ]

            Constraints include:
            # (1) velocity limit on human joints
            # (2) same rotation matrix for leader and desired leader
            # (3) same rotation matrix for leader and follower
            # (4) same position for teleoperator EE and leader EE
            # (5) velocity limit on leader EE pose


            '''
            x_h = x[0:self.T*self.teleoperator.n_h].reshape(self.T,self.teleoperator.n_h)
            x_l = x[self.T*self.teleoperator.n_h : self.T*self.teleoperator.n_h+self.T*self.leader.n_l].reshape(self.T, self.leader.n_l)
            x_f = x[self.T*self.teleoperator.n_h+self.T*self.leader.n_l:len(x)].reshape(self.T,self.follower.n_f)

            const_1 = []

            leader_ee_T_previous = self.leader.to_global(self.leader.FK(x_l[0], output = "T_matrix" ))

            # (1) velocity limit on human joints
            for t in range(1, self.T):
                for i in range(self.teleoperator.n_h):
                    const_1.append(self.teleoperator.max_v-abs(x_h[t][i]-x_h[t-1][i])/self.dt)

            # mapping from Teleoperator EE pose to Leader EE pose

            for t in range(1,self.T):
                desired_leader_ee_T = teleoperator_to_leader_transform(self.teleoperator.to_global(self.teleoperator.FK(x_h[t], output = "T_matrix")))
                leader_ee_T = self.leader.to_global(self.leader.FK(x_l[t], output="T_matrix"))
                follower_ee_T = self.follower.to_global(self.follower.FK(x_f[t], output="T_matrix"))
                # leader_orientation = euler_matrix(leader_ee[3], leader_ee[4], leader_ee[5], 'sxyz')
                # follower_orientation = euler_matrix(follower_ee[3], follower_ee[4], follower_ee[5], 'sxyz')

                # (2) same rotation matrix for leader and desired leader
                diff_rot_lh = rotation_diff(desired_leader_ee_T, leader_ee_T, input="T_matrix", output="axis_angle")
                const_1.append(self.epsilon_lh_orientation - abs(diff_rot_lh))

                # (3) same rotation matrix for leader and follower #TODO: it should be equality
                diff_rot_lf = rotation_diff(follower_ee_T, leader_ee_T, input="T_matrix", output="axis_angle")
                const_1.append(self.epsilon_lf_orientation - abs(diff_rot_lf))


                # for i in range(3):
                #     for j in range(3):
                #         # (2) same rotation matrix for leader and desired leader
                #         const_1.append(self.epsilon_lh_orientation - abs(leader_orientation[i,j]-desired_leader_pose[i,j]))
                #         # (3) same rotation matrix for leader and follower #TODO: it should be equality
                #         const_1.append(self.epsilon_lh_orientation - abs(leader_orientation[i,j]-follower_orientation[i,j]))

                # (4) same position for teleoperator EE and leader EE, we assume z_l = z_h
                for j in range(3):
                    const_1.append(self.epsilon_lh_pose - abs(leader_ee_T[j,3]-deepcopy(desired_leader_ee_T[j,3]))) #

                # (5) velocity limit on leader EE pose
                vel_leader_ee_pos = [(leader_ee_T[i,3]- leader_ee_T_previous[i,3])/self.dt for i in range(3)]
                const_1.append(self.ee_vel_max - np.dot(vel_leader_ee_pos,vel_leader_ee_pos))

                # update leader_ee_T_previous
                leader_ee_T_previous = deepcopy(leader_ee_T)

            return const_1
        ############################ Objective Function #########################################################
        def cost_goal_no_correction(x):
            '''
            (1) move towards goal simga(||z_g -FK(q_f_t)) for both position and orientatation
            '''
            x_h = x[0:self.T*self.teleoperator.n_h].reshape(self.T,self.teleoperator.n_h)
            x_l = x[self.T*self.teleoperator.n_h : self.T*self.teleoperator.n_h+self.T*self.leader.n_l].reshape(self.T, self.leader.n_l)
            x_f = x[self.T*self.teleoperator.n_h+self.T*self.leader.n_l:len(x)].reshape(self.T,self.follower.n_f)
            # setting up goal pose
            goal_orientation_T = euler_matrix(self.task.goal[3],self.task.goal[4],self.task.goal[5], 'sxyz') #TODO: use homog transformation for goal pos and rot
            goal_T = T_from_R_and_D(goal_orientation_T, [self.task.goal[0],self.task.goal[1],self.task.goal[2]])
            cost = 0
            ################# I am here about to fix diff using position_diff from utils
            for t in range(self.T):
                follower_ee_T = self.follower.to_global(self.follower.FK(x_f[t], output="T_matrix"))
                # follower_orientation = euler_matrix(follower_ee[3], follower_ee[4], follower_ee[5], 'sxyz')

                # difference in  position
                diff = [follower_ee_T[i,3] - self.task.goal[i] for i in range(3)]
                cost1 = sum([diff[i]**2 for i in range(3)])
                # difference in orientation
                diff_rot_fg = rotation_diff(follower_ee_T, goal_T, input="T_matrix", output="axis_angle")
                cost2 = abs(diff_rot_fg)
                cost += self.w1*cost1+self.w2*cost2

                # for i in range(3):
                #     for j in range(3):
                #         diff.append(follower_orientation[i,j] - task_orientation[i,j]) # orientation
                # cost1 = np.dot(np.dot(diff,self.sigma_pose),diff)

                # cost += cost1
            print cost
            return cost

        def cost_goal_with_correction(x):
            '''
            (1) move towards goal: simga(||z_g -FK(q_f_t)) for both position and orientatation
            (2) apply postural correction
            '''
            print "cost initialized"

            x_h = x[0:self.T*self.teleoperator.n_h].reshape(self.T,self.teleoperator.n_h)
            x_l = x[self.T*self.teleoperator.n_h : self.T*self.teleoperator.n_h+self.T*self.leader.n_l].reshape(self.T, self.leader.n_l)
            x_f = x[self.T*self.teleoperator.n_h+self.T*self.leader.n_l:len(x)].reshape(self.T,self.follower.n_f)
            # print "x_h = ", x_h

            goal_orientation_T = euler_matrix(self.task.goal[3],self.task.goal[4],self.task.goal[5], 'sxyz') #TODO: use homog transformation for goal pos and rot
            goal_T = T_from_R_and_D(goal_orientation_T, [self.task.goal[0],self.task.goal[1],self.task.goal[2]])
            cost = 0

            for t in range(self.T):
                follower_ee_T = self.follower.to_global(self.follower.FK(x_f[t], output="T_matrix"))
                # print "follower_ee = ", follower_ee
                # follower_orientation = euler_matrix(follower_ee[3], follower_ee[4], follower_ee[5], 'sxyz')
                # print "follower_orientation = ", follower_orientation

                #  (1) move towards goal
                # difference in  position

                # difference in  position
                diff = [follower_ee_T[i,3] - self.task.goal[i] for i in range(3)]
                cost1 = sum([diff[i]**2 for i in range(3)])
                # difference in orientation
                diff_rot_fg = rotation_diff(follower_ee_T, goal_T, input="T_matrix", output="axis_angle")
                cost2 = abs(diff_rot_fg)
                cost += self.w1*cost1+self.w2*cost2

            # print "diff_follower_tp_goal = ", diff
            # print "cost of moving toward goal :", cost


            # (2) apply postural correction
            diff2 = [x_h[1][j]-self.optimal_posture[j] for j in range(10)]
            cost3 = np.dot(np.dot(diff2,self.sigma_joints),diff2)
            # print "postural correction cost: ", cost2

            cost += self.alpha * cost3
            print "motion planner cost: " , cost
            # print " alpha: ", self.alpha
            return cost

        def cost_path_no_correction(x):
            '''

            '''
            x_h = x[0:self.T*self.teleoperator.n_h].reshape(self.T,self.teleoperator.n_h)
            x_l = x[self.T*self.teleoperator.n_h : self.T*self.teleoperator.n_h+self.T*self.leader.n_l].reshape(self.T, self.leader.n_l)
            x_f = x[self.T*self.teleoperator.n_h+self.T*self.leader.n_l:len(x)].reshape(self.T,self.follower.n_f)

            cost = 0
            for t in range(self.T):
                follower_ee = [self.follower.to_global(self.follower.FK(x_f[t]))]

                follower_orientation = euler_matrix(follower_ee[3], follower_ee[4], follower_ee[5], 'sxyz')
                task_orientation = euler_matrix(self.task.goal[t][3],self.task.goal[t][4],self.task.goal[t][5], 'sxyz')

                diff = [follower_ee[i] - self.task.goal[t][i] for i in range(3)] # cost1: simga(||z_g -FK(q_f_t))# 6 for 6 DOF pose
                for i in range(3):
                    for j in range(3):
                        diff.append(follower_orientation[i,j] - task_orientation[i,j])
                cost1 = np.dot(np.dot(diff,self.sigma_pose),diff)
                cost += cost1

            return cost

        def cost_path_with_online_correction(x):
            '''

            '''
            x_h = x[0:self.T*self.teleoperator.n_h].reshape(self.T,self.teleoperator.n_h)
            x_l = x[self.T*self.teleoperator.n_h : self.T*self.teleoperator.n_h+self.T*self.leader.n_l].reshape(self.T, self.leader.n_l)
            x_f = x[self.T*self.teleoperator.n_h+self.T*self.leader.n_l:len(x)].reshape(self.T,self.follower.n_f)

            cost = 0
            for t in range(self.T):
                follower_ee = [self.follower.to_global(self.follower.FK(x_f[t]))]

                follower_orientation = euler_matrix(follower_ee[3], follower_ee[4], follower_ee[5], 'sxyz')
                task_orientation = euler_matrix(self.task.goal[t][3],self.task.goal[t][4],self.task.goal[t][5], 'sxyz')
                diff = [follower_ee[i] - self.task.goal[t][i] for i in range(3)] # cost1: simga(||z_g -FK(q_f_t))# 6 for 6 DOF pose
                for i in range(3):
                    for j in range(3):
                        diff.append(follower_orientation[i,j] - task_orientation[i,j])
                cost1 = np.dot(np.dot(diff,self.sigma_pose),diff)
                cost += cost1
            diff2 = [x_h[1][j]-self.optimal_posture[j] for j in range(10)]
            cost2 = np.dot(np.dot(diff2,np.eye(10)),diff2)  # TODO: np.eye can be replaced by a covariance matrix based on importance of joints

            cost += self.alpha * cost2
            return cost

        def cost_path_with_offline_correction(x):
            '''

            '''
            x_h = x[0:self.T*self.teleoperator.n_h].reshape(self.T,self.teleoperator.n_h)
            x_l = x[self.T*self.teleoperator.n_h : self.T*self.teleoperator.n_h+self.T*self.leader.n_l].reshape(self.T, self.leader.n_l)
            x_f = x[self.T*self.teleoperator.n_h+self.T*self.leader.n_l:len(x)].reshape(self.T,self.follower.n_f)

            cost = 0
            for t in range(self.T):
                follower_ee = [self.follower.to_global(self.follower.FK(x_f[t]))]

                follower_orientation = euler_matrix(follower_ee[3], follower_ee[4], follower_ee[5], 'sxyz')
                task_orientation = euler_matrix(self.task.goal[t][3],self.task.goal[t][4],self.task.goal[t][5], 'sxyz')
                diff = [follower_ee[i] - self.task.goal[t][i] for i in range(3)] # cost1: simga(||z_g -FK(q_f_t))# 6 for 6 DOF pose
                for i in range(3):
                    for j in range(3):
                        diff.append(follower_orientation[i,j] - task_orientation[i,j])
                cost1 = np.dot(np.dot(diff,self.sigma_pose),diff)
                cost += cost1

                diff2 = [x_h[t][j]-self.optimal_posture[t][j] for j in range(10)]
                cost2 = np.dot(np.dot(diff2,np.eye(10)),diff2)  # TODO: np.eye can be replaced by a covariance matrix based on importance of joints
                cost += self.alpha * cost2

            return cost


        # constraints input for SLSQP:
        cons = ({'type': 'eq','fun': cons_eq}, {'type': 'ineq','fun': cons_ineq})




        # ---------------- Optimize  -----------#
        print "*** Human Motion Planning ***"
        if self.task.type == "goal-constrained":
            if (self.postural_correction==False or len(optimal_posture)== 0):
                result = minimize(cost_goal_no_correction, self.initial_solution, method='SLSQP', tol=1e-3, bounds=self.joint_limits_traj, constraints=cons, options={'maxiter': 200})
            else:
                result = minimize(cost_goal_with_correction, self.initial_solution, method='SLSQP', tol=1e-3, bounds=self.joint_limits_traj, constraints=cons, options={'maxiter': 200})
        elif self.task.type == "path-constrained":
            if len(optimal_posture)== 0:
                result = minimize(cost_path_no_correction, self.initial_solution, method='SLSQP', tol=1e-3, bounds=self.joint_limits_traj, constraints=cons, options={'maxiter': 200})
            elif optimal_posture.shape[0] == 1:
                result = minimize(cost_path_with_online_correction, self.initial_solution, method='SLSQP', tol=1e-3, bounds=self.joint_limits_traj, constraints=cons, options={'maxiter': 200})
            else:
                result = minimize(cost_path_with_offline_correction, self.initial_solution, method='SLSQP', tol=1e-3, bounds=self.joint_limits_traj, constraints=cons, options={'maxiter': 200})


        print result.success
        print result.status
        print result.message
        cost = result.fun

        human_plan = result.x[0:self.T*self.teleoperator.n_h].reshape(self.T,self.teleoperator.n_h)
        leader_plan = result.x[self.T*self.teleoperator.n_h : self.T*self.teleoperator.n_h+self.T*self.leader.n_l].reshape(self.T, self.leader.n_l)
        follower_plan = result.x[self.T*self.teleoperator.n_h+self.T*self.leader.n_l : len(result.x)].reshape(self.T,self.follower.n_f)

        return human_plan, leader_plan, follower_plan, cost
