#!/usr/bin/env python

import numpy as np
from tf.transformations import euler_matrix, euler_from_matrix
from scipy.optimize import minimize, Bounds
import torch
from torch.autograd import Variable


class Posture_Optimization_GradFree():

    def __init__(self, teleoperator, RULA, lam=0.05, epsilon_pos=0.0027, epsilon_ori=0.5):
        #TODO: tune epsilon_ori parameter

        self.lam = lam
        self.epsilon_pos = epsilon_pos
        self.epsilon_ori = epsilon_ori
        self.current_posture = []
        self.previous_posture = []
        self.teleoperator = teleoperator
        self.RULA = RULA
        self.RULA_max = 3
        self.sigma_pos =   np.diag([3, 3, 3, 0.0001, 0.0001, 0.0001, 0.0001, 0.0001, 0.0001, 0.0001, 0.0001, 0.0001])
        self.observed_traj = []


    def update_traj(self, traj):
        self.observed_traj = traj

    def cost_online_1(self, posture):   # rula as constraint: this does not work well at all, so we should


        out_of_limits = self.check_joint_limits(posture)
        if out_of_limits:
            return float("inf")
        rulaScore = self.RULA.rula_score(posture)
        if (rulaScore>self.RULA_max):
            return float("inf")
        current_ee = self.teleoperator.FK(self.teleoperator.current_posture)
        sample_ee = self.teleoperator.FK(posture)
        current_orientation = euler_matrix(current_ee[3], current_ee[4], current_ee[5], 'sxyz')
        sample_orientation = euler_matrix(sample_ee[3],sample_ee[4],sample_ee[5], 'sxyz')
        diff = [sample_ee[i] - current_ee[i] for i in range(3)]
        for i in range(3):
            for j in range(3):
                diff.append(sample_orientation[i,j] - current_orientation[i,j])
        cost1 = np.dot(np.dot(diff,self.sigma_pos),diff)
        cost2 = sum([(posture[i]-2*self.teleoperator.current_posture[i]+self.teleoperator.previous_posture[i])**2 for i in range(10)])
        cost = cost1+self.lam*cost2

        return cost

    def cost_online_2(self, posture):   # RULA as cost

        out_of_limits = self.check_joint_limits(posture)
        if out_of_limits:
            # print "out"
            return float("inf")

        current_ee = self.teleoperator.FK(self.teleoperator.current_posture)
        sample_ee = self.teleoperator.FK(posture)
        current_orientation = euler_matrix(current_ee[3], current_ee[4], current_ee[5], 'sxyz')
        sample_orientation = euler_matrix(sample_ee[3],sample_ee[4],sample_ee[5], 'sxyz')
        diff_pos = [sample_ee[i] - current_ee[i] for i in range(3)]
        diff_ori = []
        for i in range(3):
            for j in range(3):
                diff_ori.append(abs(sample_orientation[i,j] - current_orientation[i,j]))
        error_pos = np.dot(diff_pos,diff_pos) #position
        # error2 = np.dot(diff2,diff2) #orientation
        # error2 = sum([(posture[i]-2*self.current_posture[i]+self.previous_posture[i])**2 for i in range(10)])
        # print error1
        if (error_pos>self.epsilon_pos):
            return float("inf")
        for i in range(9):
            if (diff_ori[i] > self.epsilon_ori):
                return float("inf")

        rulaScore = self.RULA.rula_score(posture)
        cost = rulaScore
        # print("cost:", cost)

        return cost

    def cost_offline_1(self, posture):

        traj = self.find_traj(posture)

        for p in traj:
            rulaScore = self.RULA.rula_score(p)

            out_of_limits = self.check_joint_limits(p)
            if out_of_limits or (rulaScore>self.RULA_max):
                return float("inf")

        cost1 = 0
        cost2 = 0
        for k in range(len(traj)):
            diff = [self.teleoperator.FK(list(traj[k]))[i] - self.observed_traj[k][i] for i in range(6)]
            cost1 += np.dot(np.dot(diff,self.sigma_pos),diff)
        for k in range(2, len(traj)):
            cost2 += sum([(traj[k][i]-2*traj[k-1][i]+traj[k-2][i])**2 for i in range(10)])
        cost = cost1+self.lam*cost2

        return cost

    def cost_offline_2(self, posture):

        traj = self.find_traj(posture)

        for k in range(len(traj)):
            out_of_limits = self.check_joint_limits(traj[k])
            if out_of_limits:
                return float("inf")
            diff = [self.teleoperator.FK(traj[k])[i] - self.observed_traj[k][i] for i in range(6)]
            error1 = np.dot(np.dot(diff,self.sigma_pos),diff)
            error2 = sum([(traj[k][i]-2*traj[k-1][i]+traj[k-2][i])**2 for i in range(10)])
            if (error1>self.epsilon1) or (error2>self.epsilon2):
                return float("inf")

        cost = 0
        for p in traj:
            rulaScore = self.RULA.rula_score(p)
            cost += rulaScore

        return cost

    def find_traj(self, initial_posture):

        diff = [initial_posture[i]-self.observed_traj[0][i] for i in range(len(initial_posture))]
        traj = []
        for p in self.observed_traj:
            traj.append([p[k]+diff[k] for k in range(len(initial_posture))])

        return traj

    def check_joint_limits(self, posture):

        for i in range(len(posture)):
            if (posture[i]>self.teleoperator.q_ul_rad[i]) or (posture[i]<self.teleoperator.q_ll_rad[i]):
                return 1
        return 0

class Posture_Optimization_GradBased():
    '''
    Using scipy, it requires the cost and constraint functions to have only x as the input,
    so using self is not possible. To deal with this issue, we put theese functions in
    the optimize function as a part of the optimization class.
    So we cannot have a problem class and a solver class, everything should be in one class.

    '''

    def __init__(self, teleoperator, RULA, lam=0.05, epsilon_pos=0.02, epsilon_ori=0.2):
    # def __init__(self, teleoperator, RULA, lam=0.05, epsilon_pos=1, epsilon_ori=1):
        self.teleoperator = teleoperator
        self.RULA = RULA
        self.sigma_pose =   np.diag([1, 1, 1, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3])     # covariance matrix for position and orintation
        # self.sigma_pos =   np.diag([3, 3, 3, 0.0001, 0.0001, 0.0001, 0.0001, 0.0001, 0.0001, 0.0001, 0.0001, 0.0001])
        self.initial_solution_online = np.array(self.teleoperator.current_posture)
        self.initial_sol = []
        #TODO: The following commented part should be fixd for doing the trajectory motion
        # for i in range(self.T):
        #     for j in range(self.teleoperator.n_h):
        #         self.initial_sol.append(self.teleoperator.current_posture[j])
        # self.initial_solution_offline = np.array(self.initial_sol)

        self.joint_limits = []
        for j in range(self.teleoperator.n_h):
            self.joint_limits.append((self.teleoperator.q_ll_rad[j], self.teleoperator.q_ul_rad[j]))   # joint limits for teleoperator
        self.joint_limits_array = np.array(self.joint_limits)
        #TODO: The following commented part should be fixd for doing the trajectory motion
        # self.joint_limits_traj = []
        # for t in range(self.T):
        #     for j in range(self.teleoperator.n_h):
        #         self.joint_limits_traj.append((self.teleoperator.q_ll_rad[j], self.teleoperator.q_ul_rad[j]))   # joint limitss for teleoperator

        # self.lam = 0.1 no needed for SQP posture opt
        self.RULA_max = 3
        self.epsilon_pos = epsilon_pos
        self.epsilon_ori = epsilon_ori

        # loading learned RULA model
        self.device = torch.device('cpu')
        RULA_PATH = "/home/amir/catkin_ws/src/ergo-hri/ll4ma_posture_optimization/src/rula/RULA_regression_99-73percent.pt"
        self.model = torch.load(RULA_PATH, map_location=self.device)
        self.model.eval()




    def optimize_online(self, teleoperator_current_posture):
        '''
        current teleoperator posture is as an input to make sure we use the updated
        posture of the teleoperator

        '''
        def cost_online_2(x):
            '''
            Using the RULA directly as the cost function results in a wronge jacobian of the
            problem and it cannot solve it correctly,
            Hence, we should use the learned NN of RULA here.
            '''
            # print "x @ cost:  ", x

            predict = self.model(torch.tensor(x).reshape(1,10).float())


            # print "cost @ x:  ", predict

            return float(predict.detach().numpy())/100 # to bring the labels from 1 to 7

        def jac_online_2(x):
            x_tensor = Variable(torch.FloatTensor(x), requires_grad=True)
            cost = self.model(x_tensor)
            grad = torch.autograd.grad(cost, x_tensor, retain_graph=True, allow_unused=True)[0]

            # print grad
            return grad.detach().numpy()/100



        def const_ineq_online_2(x):
            '''

            '''
            const_1 = []

            current_teleoperator_ee = self.teleoperator.FK(teleoperator_current_posture)
            X_ee = self.teleoperator.FK(x) # X_ee is the ee pose of the posture x in optimization
            current_teleoperator_orientation = euler_matrix(current_teleoperator_ee[3], current_teleoperator_ee[4], current_teleoperator_ee[5], 'sxyz')
            X_orientation = euler_matrix(X_ee[3], X_ee[4], X_ee[5], 'sxyz')

            for i in range(3):
                for j in range(3):
                    const_1.append(1000*(self.epsilon_ori - abs(X_orientation[i,j] - current_teleoperator_orientation[i,j])))

            for i in range(3):
                const_1.append(1000*(self.epsilon_pos - abs(X_ee[i] - current_teleoperator_ee[i])))

            return np.array(const_1)



        # constraints input for SLSQP:
        constraints = ({'type': 'ineq','fun': const_ineq_online_2})
        # setting initial solution as the current posture of the teleoperator
        # initial_solution = teleoperator_current_posture
        initial_solution = np.array([0, 0, 0, 1.5708, 0, 0, 1.5708, 0, 0, 0])


        # ---------------- Optimize  -----------#
        # print initial_solution
        #
        # print const_ineq_online_2(initial_solution)

        print "*** Postural Optimizaztion ***"
        result = minimize(cost_online_2, initial_solution, method = 'SLSQP', jac=jac_online_2, tol= 1e-5, bounds= self.joint_limits_array, constraints= constraints, options = {'maxiter': 600})

        print result.success
        print result.status
        print result.message
        # print "******************"
        cost = result.fun
        opt_posture = result.x

        return opt_posture, cost, result.success


    def optimize_offline(self, path):

        '''

        '''
        self.path = path
        def cons_ineq(x):
            '''
            x is the decision variablne including [   ]
            '''
            x_h = x.reshape(-1,self.teleoperator.n_h)
            const_1 = []
            for t in range(len(x_h)):
                const_1.append(self.RULA_max - self.RULA.rula_score(x[t]))

            return const_1

        def cost(x):
            '''

            '''
            x_h = x.reshape(-1,self.teleoperator.n_h)
            cost1 = 0
            z = [self.follower.FK(x_h[0])]

            for t in range(1,len(x_h)):
                z.append(self.follower.FK(x_h[t]))
                recorded_orientation = euler_matrix(self.path[t][3],self.path[t][4],self.path[t][5], 'sxyz')
                z_orientation = euler_matrix(z[t][3], z[t][4], z[t][5], 'sxyz')
                diff = [(z[t][i]-z[t-1][i]) - (self.path[t][i]-self.path[t-1][i]) for i in range(3)] # cost1: simga(||z_g -FK(q_f_t))# 6 for 6 DOF pose
                for i in range(3):
                    for j in range(3):
                        diff.append(z_orientation[i,j] - recorded_orientation[i,j])
                cost1 += np.dot(np.dot(diff,self.sigma_pose),diff)

            cost2 = 0
            for t in range(2, len(x_h)):
                for i in range(len(x)):
                    cost2 += (x_h[t][i]-2*x_h[t-1][i]+x_h[t-2][i])**2

            cost += cost1/1000 + self.lam*cost2

            return cost

        # constraints input for SLSQP:
        cons = ({'type': 'ineq','fun': cons_ineq})

        # ---------------- Optimize  -----------#
        result = minimize(cost, self.initial_solution_offline, method='SLSQP', jac=jac_online_2, tol=1e-6, bounds=self.joint_limits_traj, constraints=cons, options={'maxiter': 100})

        print result.success
        print result.status
        print result.message
        cost = result.fun
        opt_posture = result.x

        return opt_posture, cost
