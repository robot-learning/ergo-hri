#!/usr/bin/env python

import numpy as np
import random
import rospy
from copy import deepcopy
from kinematic_model import Kinematic_Model
import math
from tf.transformations import euler_matrix, euler_from_matrix, rotation_from_matrix


def teleoperator_to_leader_transform(pose):
    '''
    pose is the global orientation of teleoperator in euler
    returns desired global orientation of leader robot so that the teleoperator
    holds the leader EE
    pose_dot_R is in 4x4 hemogenous transformation matrix format
    '''
    R = np.matrix('0, 0, -1, 0 ; -1, 0, 0, 0 ; 0, 1, 0, 0 ; 0, 0, 0, 1')
    # print "R_h: ", euler_matrix(pose[3],pose[4],pose[5], 'sxyz')
    if len(pose) == 6:
        pose_dot_R = np.dot(euler_matrix(pose[3],pose[4],pose[5], 'sxyz'),R)
        pose_dot_R[0][3] = pose[0]
        pose_dot_R[1][3] = pose[1]
        pose_dot_R[2][3] = pose[2]
        return pose_dot_R
    elif len(pose) == 4:
        return np.dot(pose,R)


def rotation_diff(desired, actual, input = "T_matrix", output="R_matrix"):
    '''
    input types:
        - T_matrix
        - R_matrix
    desired_rot: rot matrix
    actual_rot: rot matrix
    both rotations are global

    output types:
        - T_matrix
        - R_matrix
        - R_hom
        - frobenius norm
    '''
    if input == "T_matrix":
        rot_a = rot_from_T(actual)
        rot_d = rot_from_T(desired)
    elif input == "R_matrix":
        rot_a = actual
        rot_d = desired
    else:
        print "Error: wrong input type!"

    rotation_diff = np.matmul(np.transpose(rot_a), rot_d)

    if output == "R_matrix":
        return rotation_diff
    elif output == "R_hom":
        return T_from_R_and_D(rotation_diff)
    elif output == "T_matrix" and input=="T_matrix":
        return T_from_R_and_D(rotation_diff, actual[0:3,3])
    elif output == "axis_angle":
        angle,axis,point = rotation_from_matrix(T_from_R_and_D(rotation_diff))
        return angle
    elif output == "Frobenius":
        return np.linalg.norm(rotation_diff)
    else:
        print "Error: wrong output/input type"


def position_diff(desired, actual, input = "T_matrix", output="array"):
    '''
    both rotations are global

    output types:
    - array
    - sum_squared

    '''
    if input == "T_matrix":
        pos_a = pos_from_T(actual)
        pos_d = pos_from_T(desired)
    elif input == "pos":
        pos_a = actual
        pos_d = desired

    diff = [pos_desired[i] - pos_actual[i] for i in range(len(pos_desired))]
    if output == "sum_squared":
        return numpy.sum(np.array(diff)**2)

    elif output == "array":
        return diff

def rot_from_T(T):
    return T[0:3, 0:3]


def pos_from_T(T):
    return T[0:3, 3]

def T_from_R_and_D(R, D=[0,0,0]):
    if len(R) == 3:
        T=np.eye(4)
        T[0:3,0:3] = R
    elif len(R) == 4:
        T = R
    else:
        print "Error!: wrong size of rotation matrix"
    T[0:3,3]=D

    return T
