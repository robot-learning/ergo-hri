#!/usr/bin/env python

import numpy as np
import sys
import torch
import copy
from torch.distributions.multivariate_normal import MultivariateNormal
from torch.distributions.uniform import Uniform
from torch.distributions.kl import kl_divergence


class CEMSolver():
    """
    Cross-Entropy Method (CEM) solver initially developed for rare-event simulation [1] and
    widely used in robotics, e.g. motion planning [2].

    Implements the basic steps of:
        1. Generate N samples.
        2. Evaluate the cost of each sample.
        3. Take the subset of top-K samples with the lowest cost (a.k.a. the elite set).
        4. Re-fit the distribution with the top-K elite samples.
        5. Repeat until distribution converges on low-cost region.

    [1] Rubinstein, Reuven Y., and Dirk P. Kroese. The cross-entropy method: a unified
        approach to combinatorial optimization, Monte-Carlo simulation and machine learning.
        Springer Science & Business Media, 2013.

    [2] Kobilarov, Marin. "Cross-entropy motion planning." The International Journal of
        Robotics Research 31.7 (2012): 855-871.

    TODO: Add options for other distribution types (e.g. GMM), right now only Gaussian is supported.
    """

    def __init__(self, max_iterations=100, kl_epsilon=1, n_elite=10, n_samples=100000, n_samples_initial=100000):
        """
        Args:
            max_iterations (int): Maximum number of iterations to run solver.
            kl_epsilon (flt): Threshold on KL divergence between distributions at consecutive
                              iterations, solver converges when value goes below threshold.
            n_samples (int): Number of samples to generate at each iteration.
            n_elite (int): Number of elite samples to re-fit distribution with at each iteration.
        """
        self.max_iterations = max_iterations
        self.kl_epsilon = kl_epsilon   # TODO: tune to get convergance and cost is not infinity
        self.n_elite = n_elite      #TODO: tune n_elite and n_samples
        self.n_samples = n_samples
        self.n_samples_initial = n_samples_initial
        self.q_ul_degree = [ 30,  10,  10, 135, 90, 135, 150, 70, 20, 80]
        self.q_ll_degree = [-10, -10, -10, -90, -90, -45, 0, -70, -30, -70]
        self.q_ul_rad = [np.pi*self.q_ul_degree[i]/180 for i in range(10)]
        self.q_ll_rad = [np.pi*self.q_ll_degree[i]/180 for i in range(10)]

    def optimize(self, f, x0, sigma0=None, sigma_reg=None):
        """
        Runs CEM optimization up to convergence or terminates at max_iterations.

        Args:
            f (func): Cost function to optimized.
            x0 (ndarray): Initial distribution mean, shape (n_dims,), defaults to all zeros.
            sigma0 (ndarray): Initial distribution covariance, shape (n_dims, n_dims), defaults
                              to identity matrix.
            sigma_reg (ndarray): Additive regularization covariance to ensure well-conditioned,
                                 shape (n_dims, n_dims), defaults to small-scaled identity matrix.

        """
        print "*** CEM optimizing ... ***"
        if sigma0 is None:
            # sigma0 = np.diag([0.1, 0.1, 0.1, 1, 1, 1, 1, 1, 0.2, 0.8])
            sigma0 = 0.2 * np.eye(10)
        if sigma_reg is None:
            sigma_reg = 1e-5 * np.eye(10)

        mu = torch.Tensor(x0)
        sigma = torch.Tensor(sigma0) + torch.Tensor(sigma_reg)
        # distribution = MultivariateNormal(mu, sigma)

        converged = False
        cost = sys.maxsize
        x = None
        iterates = []
        sample_iterates = []
        found = 0
        while not found:
            distribution = Uniform(torch.tensor(self.q_ll_rad),torch.tensor(self.q_ul_rad))
            samples = distribution.sample((self.n_samples_initial,)).data.numpy()
            sample_iterates.append(samples)
            costs = [f(s) for s in samples]
            num_num = np.isfinite(costs).sum()
            # print("num:", num_num)
            if num_num>=5:
                found = 1


        for i in range(self.max_iterations):
            # Generate samples and evaluate costs to find elite set
            if i>0:
                samples = distribution.sample((self.n_samples,)).data.numpy()
                sample_iterates.append(samples)

                costs = [f(s) for s in samples]
            # print "**************************************"
            # for i in range(len(costs)):
            #     # print(costs[i])
            # print "**************************************"
            sorted_samples = [s for _, s in sorted(zip(costs, samples), key=lambda x: x[0])]
            elite = np.vstack(sorted_samples[:self.n_elite])

            # Re-fit the distribution based on elite set
            prev_distribution = copy.deepcopy(distribution)
            mu = torch.Tensor(np.mean(elite, axis=0))
            sigma = torch.Tensor(np.cov(elite.T)) + torch.Tensor(sigma_reg)
            distribution = MultivariateNormal(mu, sigma)
            # Check convergence based on KL-divergence between previous and current distributions
            if i>0:
                kl = kl_divergence(prev_distribution, distribution).item()
                # print("kl:", kl)

            x = mu.data.numpy()
            # print("cost:", f(x))
            iterates.append(x)

            if i>0:
                if kl < self.kl_epsilon:
                    converged = True
                    cost = f(x)
                    break

        if converged:
            print("\nCEM converged after {} iterations!".format(i))
            print("Solution: {}, Cost: {}\n".format(x, cost))
        else:
            print("\nCEM failed to converge after {} iterations. :(\n"
                  "".format(self.max_iterations))

        return x, cost, converged
