#!/usr/bin/env python

import numpy as np
import random
import rospy
from copy import deepcopy
from kinematic_model import Kinematic_Model
from simulation import sim_teleoperator, sim_follower_robot, sim_leader_robot
import math
from utils.teleop_transformations import teleoperator_to_leader_transform


print "----------------- teleoperator ------------------"
human = sim_teleoperator()
fk_result = human.to_global(human.FK([5.81078944e-17,  3.76262777e-17, -2.84263753e-17,  1.57080000e+00,
   1.48940249e-17,  6.16290861e-17,  1.57080000e+00,  1.77251421e-16,
   0.00000000e+00,  0.00000000e+00]))
print "FK_h:", fk_result
ik_result = human.IK([0.32499989695284626, -0.17626299390881678, 0.9512612985335654, -0.0008000000011753518, 0.0007999999999944426, 2.9385646476667883e-09])
print ik_result
print "----------------- follower ------------------"
follower = sim_follower_robot()
fk_result = follower.to_global(follower.FK([-0.7271562544611375, 0.9622021145904277, 1.4615113000364754, -1.8547917687761624, -0.7767329677576102, 1.0759053910297414, -2.8789999999999947]))
print "FK_f:", fk_result
ik_result = follower.IK(follower.to_local([2.71625574e+00, -5.94855498e-01,  1.37256331e+00,  -4.14150961e+00,
  -3.90966330e-01,  1.19292123e-01]))
print "Follower_IK", ik_result.tolist()
print "----------------- leader ------------------"
leader = sim_leader_robot()
fk_result = leader.to_global(leader.FK([0.31429287258222344, 0.5714291318475408, -0.006198889044449161, -1.445410205307476, 0.003717355352669824, 1.124768244909242, 0.3074714837228943]))
print "FK:", fk_result
# print "transformed" , teleoperator_to_leader_transform([0.25999999999824597, -0.1749987878458239, 1.140000955035553, 1.5708, 0.0, 1.5708])
# ik_result = leader.IK(leader.to_local(teleoperator_to_leader_transform([0.25999999999824597, -0.1749987878458239, 1.140000955035553, 1.5708, 0.0, 1.5708])))
ik_result = leader.IK(leader.to_local([2.87502095e-01, -1.29274698e-01,  1.13363150e+00, -3.17508564e+00,
   2.72702782e-04,  3.13722761e+00]))

print ik_result.tolist()
