#!/usr/bin/env python
import rospy
from std_msgs.msg import Char
from sensor_msgs.msg import JointState
import numpy as np


class JointTester:

    def __init__(self):
        self.leader_arm_name = rospy.get_param("leader_arm_name", "lbr4")
        self.follower_arm_name = rospy.get_param("follower_arm_name", "lbr4")
        # print ("leader_arm_name", self.leader_arm_name)
        # print ("follower_arm_name", self.follower_arm_name)

        self.leader_arm_type = rospy.get_param("/{}/robot_type".format(self.leader_arm_name))
        self.follower_arm_type = rospy.get_param("/{}/robot_type".format(self.follower_arm_name))
        # print ("leader_arm_type", self.leader_arm_type)
        # print ("follower_arm_type", self.follower_arm_type)

        self.leader_hand_name = rospy.get_param("leader_hand_name", "")
        self.follower_hand_name = rospy.get_param("follower_hand_name", "")

        leader_arm_cmd_topic = rospy.get_param("/{}/joint_command_topic".format(self.leader_arm_name))
        follower_arm_cmd_topic = rospy.get_param("/{}/joint_command_topic".format(self.follower_arm_name))
        # print ("leader_arm_cmd_topic", leader_arm_cmd_topic)
        # print ("follower_arm_cmd_topic", follower_arm_cmd_topic)

        self.leader_arm_cmd_pub = rospy.Publisher(leader_arm_cmd_topic, JointState, queue_size=1)
        self.follower_arm_cmd_pub = rospy.Publisher(follower_arm_cmd_topic, JointState, queue_size=1)

        if self.leader_hand_name!='none':
            self.leader_hand_type = rospy.get_param("/{}/robot_type".format(self.leader_hand_name))
            leader_hand_cmd_topic = rospy.get_param("/{}/joint_command_topic".format(self.leader_hand_name), "")
            self.leader_hand_cmd_pub = rospy.Publisher(leader_hand_cmd_topic, JointState, queue_size=1)
        self.rate=rospy.Rate(100)

        if self.follower_hand_name!='none':
            self.follower_hand_type = rospy.get_param("/{}/robot_type".format(self.follower_hand_name))
            follower_hand_cmd_topic = rospy.get_param("/{}/joint_command_topic".format(self.follower_hand_name), "")
            self.follower_hand_cmd_pub = rospy.Publisher(follower_hand_cmd_topic, JointState, queue_size=1)
        self.rate=rospy.Rate(100)

    def run_lbr4_test(self):
        follower_command = JointState()
        follower_command.name = ['follower_lbr4_j{}'.format(i) for i in range(7)]
        follower_command.position = [0.0] * len(follower_command.name)
        follower_command.velocity = [0.0] * len(follower_command.name)
        follower_command.effort = [0.0] * len(follower_command.name)

        leader_command = JointState()
        leader_command.name = ['leader_lbr4_j{}'.format(i) for i in range(7)]
        leader_command.position = [0.0] * len(leader_command.name)
        leader_command.velocity = [0.0] * len(leader_command.name)
        leader_command.effort = [0.0] * len(leader_command.name)

        for i in range(300):
            self.follower_arm_cmd_pub.publish(follower_command)
            for j in range(len(follower_command.name)):
                follower_command.position[j]+=0.005
            self.rate.sleep()

        rospy.sleep(2.0)

        for i in range(300):
            self.follower_arm_cmd_pub.publish(follower_command)
            for j in range(len(follower_command.name)):
                follower_command.position[j]-=0.005
            self.rate.sleep()


        for i in range(300):
            self.leader_arm_cmd_pub.publish(leader_command)
            for j in range(len(leader_command.name)):
                leader_command.position[j]+=0.005
            self.rate.sleep()

        rospy.sleep(2.0)

        for i in range(300):
            self.leader_arm_cmd_pub.publish(leader_command)
            for j in range(len(leader_command.name)):
                leader_command.position[j]-=0.005
            self.rate.sleep()


    def run_leader_lbr4_motion_test(self):

        leader_command = JointState()
        leader_command.name = ['leader_lbr4_j{}'.format(i) for i in range(7)]
        leader_command.position = [0.0] * len(leader_command.name)
        leader_command.velocity = [0.0] * len(leader_command.name)
        leader_command.effort = [0.0] * len(leader_command.name)

        for i in range(300):
            self.leader_arm_cmd_pub.publish(leader_command)
            for j in range(len(leader_command.name)):
                leader_command.position[j]+=0.005
            self.rate.sleep()

        rospy.sleep(2.0)

        for i in range(300):
            self.leader_arm_cmd_pub.publish(leader_command)
            for j in range(len(leader_command.name)):
                leader_command.position[j]-=0.005
            self.rate.sleep()

    def run_leader_lbr4_joint_test(self):

        leader_command = JointState()
        leader_command.name = ['leader_lbr4_j{}'.format(i) for i in range(7)]
        leader_command.position = [0.0] * len(leader_command.name)
        leader_command.velocity = [0.0] * len(leader_command.name)
        leader_command.effort = [0.0] * len(leader_command.name)

        # leader_command.position = [0.0, 0.0, 0, 0, 0, 0, 0]  # initial pose for leader


        # leader_command.position = [0.08337205652301091, 0.7127066699013566, 1.0241284858614608, -1.305990141806959, 0.5226458178820909, 0.7336219693343172, -2.3780060679632746]  # initial pose for leader
        # leader_command.position = [-0.1791247450493644, 0.42206403843824125, 0.7114859694775043, -1.5573693014755021, -0.28401436252318196, 1.2682152179494504, -2.5675213307819833]  # goal pose for leader when human @ neutral pose
        leader_command.position = [0.022712994388582004, 0.635018237551346, 0.7335849357344068, -1.538368084452975, -0.34428363196734, 1.060201923272224, -2.477942379882445]  # goal pose for leader for initial #2

        for i in range(300):
            self.leader_arm_cmd_pub.publish(leader_command)

            self.rate.sleep()

        # rospy.sleep(2.0)


    def run_follower_lbr4_motion_test(self):
        follower_command = JointState()
        follower_command.name = ['follower_lbr4_j{}'.format(i) for i in range(7)]
        follower_command.position = [0.0] * len(follower_command.name)
        follower_command.velocity = [0.0] * len(follower_command.name)
        follower_command.effort = [0.0] * len(follower_command.name)

        for i in range(300):
            self.follower_arm_cmd_pub.publish(follower_command)
            for j in range(len(follower_command.name)):
                follower_command.position[j]+=0.005
            self.rate.sleep()

        rospy.sleep(2.0)

        for i in range(300):
            self.follower_arm_cmd_pub.publish(follower_command)
            for j in range(len(follower_command.name)):
                follower_command.position[j]-=0.005
            self.rate.sleep()

    def run_follower_lbr4_joint_test(self):
        follower_command = JointState()
        follower_command.name = ['follower_lbr4_j{}'.format(i) for i in range(7)]
        follower_command.position = [0.0] * len(follower_command.name)
        follower_command.velocity = [0.0] * len(follower_command.name)
        follower_command.effort = [0.0] * len(follower_command.name)

        # follower_command.position = [0.31429287258222344, 0.5714291318475408, -0.006198889044449161, -1.445410205307476, 0.003717355352669824, 1.124768244909242, 0.3074714837228943] # initial pose for follower
        # follower_command.position = [-0.41429287258222344, 0.4514291318475408, -0.006198889044449161, -1.7445410205307476, 0.003717355352669824, 1.124768244909242, 0.3074714837228943] # goal pose for follower
        follower_command.position = [0.022712994388582004, 0.635018237551346, 0.7335849357344068, -1.538368084452975, -0.34428363196734, 1.060201923272224, -2.477942379882445]  # goal pose for follower for initial #2

        for i in range(300):
            self.follower_arm_cmd_pub.publish(follower_command)
            self.rate.sleep()
        # rospy.sleep(2.0)

    # def run_allegro_test(self):
    #     command = JointState()
    #     command.name = []
    #     command.name += ['index_joint_{}'.format(i) for i in range(4)]
    #     command.name += ['middle_joint_{}'.format(i) for i in range(4)]
    #     command.name += ['ring_joint_{}'.format(i) for i in range(4)]
    #     command.name += ['thumb_joint_{}'.format(i) for i in range(4)]
    #     command.position = [0.0] * len(command.name)
    #     command.velocity = [0.0] * len(command.name)
    #     command.effort = [0.0] * len(command.name)
    #
    #     delta_position = 0.015
    #
    #     for i in range(100):
    #         self.hand_cmd_pub.publish(command)
    #         for j in range(len(command.name)):
    #             command.position[j] += delta_position
    #         self.rate.sleep()
    #
    #     for i in range(100):
    #         self.hand_cmd_pub.publish(command)
    #         for j in range(len(command.name)):
    #             command.position[j] -= delta_position
    #         self.rate.sleep()
    #
    # def run_reflex_test(self):
    #     command = JointState()
    #     command.name = [f'{self.hand_name}_preshape_joint_{i}' for i in [1,2]]
    #     command.name += [f'{self.hand_name}_proximal_joint_{i}' for i in [1,2,3]]
    #     command.position = [0.0] * len(command.name)
    #
    #     delta_position = 0.01
    #
    #     for i in range(120):
    #         self.hand_cmd_pub.publish(command)
    #         for j in range(len(command.name)):
    #             if j > 1:
    #                 command.position[j] += delta_position
    #         self.rate.sleep()
    #
    #     for i in range(120):
    #         self.hand_cmd_pub.publish(command)
    #         for j in range(len(command.name)):
    #             if j <= 1:
    #                 command.position[j] += delta_position
    #         self.rate.sleep()
    #
    #     rospy.sleep(1.0)
    #
    #     for i in range(120):
    #         self.hand_cmd_pub.publish(command)
    #         for j in range(len(command.name)):
    #             if j <= 1:
    #                 command.position[j] -= delta_position
    #         self.rate.sleep()
    #
    #     for i in range(120):
    #         self.hand_cmd_pub.publish(command)
    #         for j in range(len(command.name)):
    #             if j > 1:
    #                 command.position[j] -= delta_position
    #         self.rate.sleep()


if __name__=='__main__':
    rospy.init_node("joint_tester")
    joint_tester = JointTester()
    if joint_tester.follower_arm_type == "lbr4":
        # joint_tester.run_lbr4_test()
        joint_tester.run_leader_lbr4_joint_test()
        joint_tester.run_follower_lbr4_joint_test()
    # if joint_tester.hand_name and joint_tester.hand_type == "allegro":
    #     rospy.sleep(1.0)
    #     joint_tester.run_allegro_test()
    # elif joint_tester.hand_name and joint_tester.hand_type == "reflex":
    #     rospy.sleep(1.0)
    #     joint_tester.run_reflex_test()
