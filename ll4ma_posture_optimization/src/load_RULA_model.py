import torch
from torch.autograd import Variable

class Gradient():
    def __init__(self, model):
        self.model = model

    def get_gradient(self, x):
        x_tensor = Variable(torch.FloatTensor(x), requires_grad=True)
        cost = self.model(x_tensor)
        grad = torch.autograd.grad(cost, x_tensor, retain_graph=True, allow_unused=True)

        return grad



if __name__=='__main__':

    device = torch.device('cpu')

    PATH = "/home/amir/catkin_ws/src/ergo-hri/ll4ma_posture_optimization/src/rula/RULA_model_regression_1200000_1000E_98-56per.pt"
    model = torch.load(PATH, map_location=device)
    model.eval()
    grad = Gradient(model)


    test_input = torch.tensor([0,0,0,1.57,0.5,0,1.8,0,0.5,0.5]).reshape(1,10)
    predict = model(test_input)
    print ("predict: ", predict)
    print ("gradient: ", grad.get_gradient(test_input))
