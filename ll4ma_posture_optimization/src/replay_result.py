#!/usr/bin/env python

import numpy as np
from rula import *
from posture_optimization_problem import *
from solvers import *
from simulation import *
from motion_generation import *
from move_to_home import CommandTeleoperator, CommandOptimalHuman, CommandLeaderRobot, CommandFollowerRobot
from std_msgs.msg import Char
from sensor_msgs.msg import JointState
from utils.teleop_transformations import teleoperator_to_leader_transform
import pickle
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d

class Results():

	def __init__(self):
		self.smoothing_resolution = rospy.get_param("smoothing_resolution")
		self.angular_v_max_human = rospy.get_param("angular_v_max_human")
		self.experiment = rospy.get_param("experiment")
		self.teleoperator = sim_teleoperator()
		self.leader = sim_leader_robot()
		self.follower = sim_follower_robot()
		self.task = Task()
		self.RULA = RULA_assessment()
		self.problem = Posture_Optimization(self.teleoperator, self.RULA)
		self.teleoperator_command = CommandTeleoperator()
		self.optimal_human_command = CommandOptimalHuman()
		self.leader_command = CommandLeaderRobot()
		self.follower_command = CommandFollowerRobot()
		self.human_left_command = [0, 1.5708, 0, 0, 0, 0, 0]


	def load_results(self):
		'''

		'''
		result_with_correction_pickle = open("/home/amir/posture_optimization_results/exp_{0}_correction_True_angV_teleoperator_{1}.pickle".format(self.experiment,  self.angular_v_max_human),"rb")
		result_no_correction_pickle = open("/home/amir/posture_optimization_results/exp_{0}_correction_False_angV_teleoperator_{1}.pickle".format(self.experiment, self.angular_v_max_human),"rb")

		[self.wc_traj_teleoperator_ee, self.wc_traj_teleoperator_joint, self.wc_traj_RULA_teleoperator,
		self.wc_traj_optimal_posture, self.wc_traj_RULA_optimal, self.wc_traj_leader_ee,
		self.wc_traj_leader_joint, self.wc_traj_follower_ee, self.wc_traj_follower_joint] = pickle.load(result_with_correction_pickle)

		[self.nc_traj_teleoperator_ee, self.nc_traj_teleoperator_joint, self.nc_traj_RULA_teleoperator,
		self.nc_traj_optimal_posture, self.nc_traj_RULA_optimal, self.nc_traj_leader_ee,
		self.nc_traj_leader_joint, self.nc_traj_follower_ee, self.nc_traj_follower_joint] = pickle.load(result_no_correction_pickle)

		for i in range(len(self.wc_traj_teleoperator_joint)):
			if type(self.wc_traj_teleoperator_joint[i]) != list:
				self.wc_traj_teleoperator_joint[i] = self.wc_traj_teleoperator_joint[i].tolist()

		for i in range(len(self.nc_traj_optimal_posture)):
			if type(self.nc_traj_optimal_posture[i]) != list:
				self.nc_traj_optimal_posture[i] = self.nc_traj_optimal_posture[i].tolist()

		for i in range(len(self.nc_traj_teleoperator_joint)):
			if type(self.nc_traj_teleoperator_joint[i]) != list:
				self.nc_traj_teleoperator_joint[i] = self.nc_traj_teleoperator_joint[i].tolist()

		for i in range(len(self.nc_traj_optimal_posture)):
			if type(self.nc_traj_optimal_posture[i]) != list:
				self.nc_traj_optimal_posture[i] = self.nc_traj_optimal_posture[i].tolist()

		# closing the pickle files
		result_with_correction_pickle.close()
		result_no_correction_pickle.close()

	def smooth_results(self):
		'''

		'''

		def smooth_traj(joint_traj, smoothing_resolution):
		    '''
		    joint traj: 10 DOF human trajectory

		    '''
		    smoothed_traj = []
		    for i in range(len(joint_traj)-1):
		            smoothed_traj += np.linspace(np.array(joint_traj[i]), np.array(joint_traj[i+1]),smoothing_resolution+1, endpoint=False).tolist()
		    smoothed_traj.append(joint_traj[-1])

		    return smoothed_traj

		def smooth_traj_optimal_posture(optimal_posture_traj, smoothing_resolution):
			'''
			joint traj: 10 DOF human trajectory

			'''
			smoothed_traj = []
			for i in range(len(optimal_posture_traj)-1):
			    for j in range(smoothing_resolution+1):
					smoothed_traj.append(optimal_posture_traj[i])
					smoothed_traj.append(optimal_posture_traj[-1])

			return smoothed_traj



		wc_q0_leader = self.wc_traj_leader_joint[0]
		nc_q0_leader = self.nc_traj_leader_joint[0]
		#initial config of follower for IK solver
		wc_q0_follower = self.wc_traj_follower_joint[0]
		nc_q0_follower = self.nc_traj_follower_joint[0]

		# smoothing the joint trajectories of teleoperator
		self.wc_traj_teleoperator_joint_smooth = smooth_traj(self.wc_traj_teleoperator_joint, self.smoothing_resolution)
		self.nc_traj_teleoperator_joint_smooth = smooth_traj(self.nc_traj_teleoperator_joint, self.smoothing_resolution)


		# smoothing the joint trajectories of teleoperator
		self.wc_traj_optimal_posture_smooth = smooth_traj_optimal_posture(self.wc_traj_optimal_posture, self.smoothing_resolution)
		self.nc_traj_optimal_posture_smooth = smooth_traj_optimal_posture(self.nc_traj_optimal_posture, self.smoothing_resolution)

		# declaring and initializing new variables
		self.wc_traj_teleoperator_ee_smooth = []
		self.nc_traj_teleoperator_ee_smooth = []

		self.wc_traj_RULA_teleoperator_smooth = []
		self.nc_traj_RULA_teleoperator_smooth = []

		self.wc_traj_leader_ee_smooth = []
		self.nc_traj_leader_ee_smooth = []

		self.wc_traj_leader_joint_smooth = []
		self.nc_traj_leader_joint_smooth = []

		self.wc_traj_follower_ee_smooth = [self.wc_traj_follower_ee[0]]
		self.nc_traj_follower_ee_smooth = [self.nc_traj_follower_ee[0]]

		self.wc_traj_follower_joint_smooth = [self.wc_traj_follower_joint[0]]
		self.nc_traj_follower_joint_smooth = [self.nc_traj_follower_joint[0]]

		self.wc_traj_RULA_optimal_smooth = []
		self.nc_traj_RULA_optimal_smooth = []
		# calculating EE trajectories
		for i in range(len(self.wc_traj_teleoperator_joint_smooth)):
			self.wc_traj_teleoperator_ee_smooth.append(self.teleoperator.to_global(self.teleoperator.FK(self.wc_traj_teleoperator_joint_smooth[i])))
			self.wc_traj_RULA_teleoperator_smooth.append(self.RULA.rula_score(self.wc_traj_teleoperator_joint_smooth[i]))

			wc_leader_pos, wc_leader_ori = teleoperator_to_leader_transform(self.wc_traj_teleoperator_ee_smooth[i]) #leader_ori is in 4x4 matrix rotatin
			wc_leader_ori = euler_from_matrix(wc_leader_ori, 'sxyz') #returns ori in euler angles
			self.wc_traj_leader_ee_smooth.append([wc_leader_pos[0], wc_leader_pos[1], wc_leader_pos[2], wc_leader_ori[0], wc_leader_ori[1], wc_leader_ori[2]])


			self.wc_traj_leader_joint_smooth.append(self.leader.IK_manual_update(self.leader.to_local(self.wc_traj_leader_ee_smooth[i]), wc_q0_leader))

			self.wc_traj_RULA_optimal_smooth.append(self.RULA.rula_score(self.wc_traj_optimal_posture_smooth[i]))
			# print len(self.wc_traj_follower_ee_smooth)
			if i>0:
				wc_follower_ee_data = []
				for j in range(3):
				# print i, j, len(self.wc_traj_leader_ee_smooth), len(self.wc_traj_follower_ee_smooth)
				# print self.wc_traj_follower_ee_smooth[i-1]

					wc_follower_ee_data.append(self.wc_traj_follower_ee_smooth[i-1][j] + self.leader.teleop_scale[j] * (self.wc_traj_leader_ee_smooth[i][j] - self.wc_traj_leader_ee_smooth[i-1][j]))
				for j in range(3):
					wc_follower_ee_data.append(self.wc_traj_leader_ee_smooth[i][j])




				self.wc_traj_follower_ee_smooth.append(wc_follower_ee_data)

				self.wc_traj_follower_joint_smooth.append(self.follower.IK_manual_update(self.follower.to_local(self.wc_traj_follower_ee_smooth[i]), wc_q0_follower))

				#initial config of leader for IK solver
				wc_q0_leader = self.wc_traj_leader_joint_smooth[-1]
				#initial config of follower for IK solver
				wc_q0_follower = self.wc_traj_follower_joint_smooth[-1]

		for i in range(len(self.nc_traj_teleoperator_joint_smooth)):
			self.nc_traj_teleoperator_ee_smooth.append(self.teleoperator.to_global(self.teleoperator.FK(self.nc_traj_teleoperator_joint_smooth[i])))
			self.nc_traj_RULA_teleoperator_smooth.append(self.RULA.rula_score(self.nc_traj_teleoperator_joint_smooth[i]))

			nc_leader_pos, nc_leader_ori = teleoperator_to_leader_transform(self.nc_traj_teleoperator_ee_smooth[i]) #leader_ori is in 4x4 matrix rotatin
			nc_leader_ori = euler_from_matrix(nc_leader_ori, 'sxyz') #returns ori in euler angles
			self.nc_traj_leader_ee_smooth.append([nc_leader_pos[0], nc_leader_pos[1], nc_leader_pos[2], nc_leader_ori[0], nc_leader_ori[1], nc_leader_ori[2]])

			self.nc_traj_leader_joint_smooth.append(self.leader.IK_manual_update(self.leader.to_local(self.nc_traj_leader_ee_smooth[i]),nc_q0_leader))

			self.nc_traj_RULA_optimal_smooth.append(self.RULA.rula_score(self.nc_traj_optimal_posture_smooth[i]))

			if i > 0:
				nc_follower_ee_data = []
				for j in range(3):
					nc_follower_ee_data.append(self.nc_traj_follower_ee_smooth [i-1][j] + self.leader.teleop_scale[j] * (self.nc_traj_leader_ee_smooth[i][j] - self.nc_traj_leader_ee_smooth[i-1][j]))
				for j in range(3):
					nc_follower_ee_data.append(self.nc_traj_leader_ee_smooth[i][j]) # TODO: this might cause some problem because it is different than the planner result

				self.nc_traj_follower_ee_smooth.append(nc_follower_ee_data)

				self.nc_traj_follower_joint_smooth.append(self.follower.IK_manual_update(self.follower.to_local(self.nc_traj_follower_ee_smooth[i]), nc_q0_follower))

				#initial config of leader for IK solver
				nc_q0_leader = self.nc_traj_leader_joint_smooth[-1]
				#initial config of follower for IK solver
				nc_q0_follower = self.nc_traj_follower_joint_smooth[-1]



	def replay_result(self):
		rate_1 = rospy.Rate(100)
		rate_2 = rospy.Rate(0.5)
		rate_3 = rospy.Rate(0.2)

		#feed the results
		for i in range(len(self.wc_traj_teleoperator_joint)):
			for j in range (300):
				#updating postures
				# print self.wc_traj_teleoperator_joint, self.human_left_command
				self.teleoperator_command.update_posture(self.wc_traj_teleoperator_joint[i] + self.human_left_command)
				if i<=(len(self.wc_traj_optimal_posture)-1):
					self.optimal_human_command.update_posture(self.wc_traj_optimal_posture[i].tolist() + self.human_left_command)
				self.leader_command.update_config(self.wc_traj_leader_joint[i])
				# print i, self.wc_traj_follower_joint[i]
				self.follower_command.update_config(self.wc_traj_follower_joint[i])

				# publishing the commands
				self.optimal_human_command.left_arm_publisher.publish(self.optimal_human_command.left_command)
				self.optimal_human_command.right_arm_publisher.publish(self.optimal_human_command.right_command)
				self.optimal_human_command.trunk_publisher.publish(self.optimal_human_command.trunk_command)

				self.teleoperator_command.left_publisher.publish(self.teleoperator_command.left_command)
				self.teleoperator_command.right_publisher.publish(self.teleoperator_command.right_command)
				self.teleoperator_command.trunk_publisher.publish(self.teleoperator_command.trunk_command)

				self.leader_command.arm_cmd_pub.publish(self.leader_command.command)
				self.follower_command.arm_cmd_pub.publish(self.follower_command.command)

			rate_2.sleep()

		rate_3.sleep()
		# move_to_home()

		self.teleoperator_command.update_posture(self.teleoperator_command.initial_posture_rad)
		self.optimal_human_command.update_posture(self.teleoperator_command.initial_posture_rad)
		self.leader_command.update_config(self.leader_command.initial_config)
		self.follower_command.update_config(self.follower_command.initial_config)

		# publishing the commands
		self.optimal_human_command.left_arm_publisher.publish(self.optimal_human_command.left_command)
		self.optimal_human_command.right_arm_publisher.publish(self.optimal_human_command.right_command)
		self.optimal_human_command.trunk_publisher.publish(self.optimal_human_command.trunk_command)

		self.teleoperator_command.left_publisher.publish(self.teleoperator_command.left_command)
		self.teleoperator_command.right_publisher.publish(self.teleoperator_command.right_command)
		self.teleoperator_command.trunk_publisher.publish(self.teleoperator_command.trunk_command)

		self.leader_command.arm_cmd_pub.publish(self.leader_command.command)
		self.follower_command.arm_cmd_pub.publish(self.follower_command.command)

		rate_3.sleep()

		for i in range(len(self.nc_traj_teleoperator_joint)):
			for j in range (300):
				#updating postures
				self.teleoperator_command.update_posture(self.nc_traj_teleoperator_joint[i] + self.human_left_command)
				if i<=(len(self.nc_traj_optimal_posture)-1):
					self.optimal_human_command.update_posture(self.nc_traj_optimal_posture[i] + self.human_left_command)
				self.leader_command.update_config(self.nc_traj_leader_joint[i])
				# print i, self.nc_traj_follower_joint[i]
				self.follower_command.update_config(self.nc_traj_follower_joint[i])

				# publishing the commands
				self.optimal_human_command.left_arm_publisher.publish(self.optimal_human_command.left_command)
				self.optimal_human_command.right_arm_publisher.publish(self.optimal_human_command.right_command)
				self.optimal_human_command.trunk_publisher.publish(self.optimal_human_command.trunk_command)

				self.teleoperator_command.left_publisher.publish(self.teleoperator_command.left_command)
				self.teleoperator_command.right_publisher.publish(self.teleoperator_command.right_command)
				self.teleoperator_command.trunk_publisher.publish(self.teleoperator_command.trunk_command)

				self.leader_command.arm_cmd_pub.publish(self.leader_command.command)
				self.follower_command.arm_cmd_pub.publish(self.follower_command.command)

			rate_2.sleep()









if __name__ == '__main__':

	rospy.init_node('result_player', anonymous=True)
	results = Results()
	results.load_results()
	results.smooth_results()
	results.replay_result()
