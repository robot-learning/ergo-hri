#!/usr/bin/env python

import numpy as np
import random
import rospy
from copy import deepcopy
from kinematic_model import Kinematic_Model
import math
from tf.transformations import euler_matrix, euler_from_matrix


class Task():
    def __init__(self):

        self.type = None
        self.goal = None
        self.path = None
        self.traj = None
        self.complete = False
        self.epsilon_g_position = 0.0027
        self.epsilon_g_orientation = 0.5  #TODO: tune this parameter
        self.sigma_pose =   np.diag([1, 1, 1, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03])     # covariance matrix for position and orintation

    def check_complete(self, follower_pose):

        if self.type == "goal-constrained":
            follower_orientation = euler_matrix(follower_pose[3],follower_pose[4],follower_pose[5], 'sxyz')
            task_orientation = euler_matrix(self.goal[3],self.goal[4],self.goal[5], 'sxyz')
            diff_pos = [follower_pose[i]-self.goal[i] for i in range(3)]
            error_pos = np.dot(diff_pos,diff_pos) #position
            diff_ori = []
            for i in range(3):
                for j in range(3):
                    diff_ori.append(abs(follower_orientation[i,j] - task_orientation[i,j]))
            if error_pos < self.epsilon_g_position:
                for i in range(9):
                    if diff_ori[i] > self.epsilon_g_orientation:
                        return False
                return True

        return False



class sim_teleoperator():

    def __init__(self):

        self.neutral_posture = [0, 0, 0, 1.5708, 0, 0, 1.5708, 0, 0, 0]
        self.previous_posture = [0, 0, 0, 1.5708, 0, 0, 1.5708, 0, 0, 0]
        self.current_posture = [0, 0, 0, 1.5708, 0, 0, 1.5708, 0, 0, 0]

        self.q_ul_degree = [ 30,  10,  10, 135, 90, 135, 150, 70, 20, 80]
        self.q_ll_degree = [-10, -10, -10, -90, -90, -45, 0, -70, -30, -70]
        self.q_ul_rad = [np.pi*self.q_ul_degree[i]/180 for i in range(10)]
        self.q_ll_rad = [np.pi*self.q_ll_degree[i]/180 for i in range(10)]
        self.n_h = 10 #  numbe of joints
        self.max_v = float(8)/180*np.pi


        # _EE_NAME ='right_human_arm_hand'  # for using the KDL up tp the wrist
        _EE_NAME ='right_human_arm_ee'
        _BASE_LINK = 'world'
        rob_description = "/human/robot_description"
        self.human = Kinematic_Model(_EE_NAME, _BASE_LINK, rob_description)

        self.current_ee_pose = self.to_global(self.FK(self.current_posture))

    def FK(self, posture, output="pose"):
        joint_angles = list(posture)

        T = self.human.FK(joint_angles)
        orientation=[[T[0,0],T[0,1],T[0,2], 0],
                     [T[1,0],T[1,1],T[1,2], 0],
                     [T[2,0],T[2,1],T[2,2], 0],
                     [0, 0, 0, 1]]

        ee_orientation = euler_from_matrix(orientation, 'sxyz')
        x= [T[0,3], T[1,3], T[2,3], ee_orientation[0], ee_orientation[1], ee_orientation[2]]

        if output=="T_matrix":
            x=T

        return x

    def to_global(self,pose):
        if len(pose) == 6:
            return [pose[0] , pose[1], pose[2]+0.60, pose[3], pose[4], pose[5]] #TODO: get the constant values from parameter server
        elif len(pose) == 4:
            pose[2,3] = pose[2,3] + 0.6
            return pose

    def to_local(self,pose):
        if len(pose) == 6:
            return [pose[0] , pose[1], pose[2] - 0.60, pose[3], pose[4], pose[5]] #TODO: get the constant values from parameter server
        elif len(pose) == 4:
            pose[2,3] = pose[2,3] - 0.6
            return pose

    def IK(self,pose):
        R = euler_matrix(pose[3], pose[4], pose[5], 'sxyz')
        T = np.matrix([[R[0,0], R[0,1], R[0,2], pose[0]],
             [R[1,0], R[1,1], R[1,2], pose[1]],
             [R[2,0], R[2,1], R[2,2], pose[2]],
             [0,0,0,1]])
        q_0 = self.current_posture
        joint_angles = self.human.IK(T, q_0)

        return joint_angles

    def update(self, plan):
        '''
        update current posture
        '''
        self.previous_posture = deepcopy(self.current_posture)
        self.current_posture = plan[1]
        self.current_ee_pose = self.to_global(self.FK(self.current_posture))


class sim_leader_robot():

    def __init__(self):

        self.n_l = 7
        _EE_NAME ='leader_lbr4_7_link'
        _BASE_LINK = 'world'
        rob_description = "/leader_lbr4/robot_description"
        self.leader_robot = Kinematic_Model(_EE_NAME, _BASE_LINK, rob_description)
        self.q_ul, self.q_ll = self.leader_robot.get_joint_limits()

        self.current_config = [-0.10648912074911933, 0.6475657656209903, 0.6342947275676271, -1.4484104911497313, -0.39773585828439184, 1.1756873870652647, -2.5571232600991514]
        self.current_ee_pose = self.to_global(self.FK(self.current_config))

        self.teleop_scale = [1.2, 1.2, 1.2, 1, 1, 1]  #follower_vel = teleop_scale * leader_vel

    def FK(self, q, output="pose"):
        joint_angles = list(q)

        T = self.leader_robot.FK(joint_angles)
        orientation=[[T[0,0],T[0,1],T[0,2], 0],
                           [T[1,0],T[1,1],T[1,2], 0],
                           [T[2,0],T[2,1],T[2,2],0],
                           [0, 0, 0, 1]]

        ee_orientation = euler_from_matrix(orientation, 'sxyz')
        x= [T[0,3], T[1,3], T[2,3], ee_orientation[0], ee_orientation[1], ee_orientation[2]]

        if output=="T_matrix":
            x=T

        return x

    def to_global(self, pose):
        if len(pose) == 6:
            return [pose[0] + 0.9 , pose[1], pose[2]+ 0.76, pose[3], pose[4], pose[5]] #TODO: get the constant values from parameter server
        elif len(pose) == 4:
            pose[0,3] = pose[0,3] + 0.9
            pose[2,3] = pose[2,3] + 0.76
            return pose

    def to_local(self, pose):
        if len(pose) == 6:
            return [pose[0] - 0.9 , pose[1], pose[2] - 0.76, pose[3], pose[4], pose[5]] #TODO: get the constant values from parameter server
        elif len(pose) == 4:
            pose[0,3] = pose[0,3] - 0.9
            pose[2,3] = pose[2,3] - 0.76
            return pose

    def IK(self, pose):
        R = euler_matrix(pose[3], pose[4], pose[5], 'sxyz')
        T = np.matrix([[R[0,0], R[0,1], R[0,2], pose[0]],
             [R[1,0], R[1,1], R[1,2], pose[1]],
             [R[2,0], R[2,1], R[2,2], pose[2]],
             [0,0,0,1]])
        q_0 = self.current_config
        joint_angles = self.leader_robot.IK(T, q_0)

        return joint_angles

    def IK_manual_update(self, pose, q_0):
        R = euler_matrix(pose[3], pose[4], pose[5], 'sxyz')
        T = np.matrix([[R[0,0], R[0,1], R[0,2], pose[0]],
             [R[1,0], R[1,1], R[1,2], pose[1]],
             [R[2,0], R[2,1], R[2,2], pose[2]],
             [0,0,0,1]])

        joint_angles = self.leader_robot.IK(T, q_0)
        return joint_angles

    def update(self, plan):
        '''
        update current joint angles
        '''
        self.current_ee_pose = self.to_global(self.FK(plan[1]))
        self.current_config = plan[1]



class sim_follower_robot():

    def __init__(self):

        # TODO: update the joint limits for robots
        self.n_f = 7
        _EE_NAME ='follower_lbr4_7_link'
        _BASE_LINK = 'world'
        rob_description = "/follower_lbr4/robot_description"

        self.follower_robot = Kinematic_Model(_EE_NAME, _BASE_LINK, rob_description)
        self.q_ul, self.q_ll = self.follower_robot.get_joint_limits()

        self.current_config = [0.31429287258222344, 0.5714291318475408, -0.006198889044449161, -1.445410205307476, 0.003717355352669824, 1.124768244909242, 0.3074714837228943]
        self.current_ee_pose = self.to_global(self.FK(self.current_config)) # in global frame

    def FK(self, q, output="pose"):
        joint_angles = list(q)

        T = self.follower_robot.FK(joint_angles)
        orientation=[[T[0,0],T[0,1],T[0,2], 0],
                           [T[1,0],T[1,1],T[1,2], 0],
                           [T[2,0],T[2,1],T[2,2],0],
                           [0, 0, 0, 1]]

        ee_orientation = euler_from_matrix(orientation, 'sxyz')
        x= [T[0,3], T[1,3], T[2,3], ee_orientation[0], ee_orientation[1], ee_orientation[2]]

        if output=="T_matrix":
            x=T
        return x

    # def to_global(self, pose):
    #     return [pose[0] + 3 , pose[1], pose[2] + 0.92, pose[3], pose[4], pose[5]] #TODO: get the constant values from parameter server
    #
    # def to_local(self, pose):
    #     return [pose[0] - 3 , pose[1], pose[2] - 0.92, pose[3], pose[4], pose[5]] #TODO: get the constant values from parameter server


    def to_global(self, pose):
        if len(pose) == 6:
            return [pose[0] + 3 , pose[1], pose[2]+ 0.92, pose[3], pose[4], pose[5]] #TODO: get the constant values from parameter server
        elif len(pose) == 4:
            pose[0,3] = pose[0,3] + 3
            pose[2,3] = pose[2,3] + 0.92
            return pose

    def to_local(self, pose):
        if len(pose) == 6:
            return [pose[0]- 3 , pose[1], pose[2] - 0.92, pose[3], pose[4], pose[5]] #TODO: get the constant values from parameter server
        elif len(pose) == 4:
            pose[0,3] = pose[0,3] - 3
            pose[2,3] = pose[2,3] - 0.92
            return pose


    def IK(self, pose):
        R = euler_matrix(pose[3], pose[4], pose[5], 'sxyz')
        T = np.matrix([[R[0,0], R[0,1], R[0,2], pose[0]],
             [R[1,0], R[1,1], R[1,2], pose[1]],
             [R[2,0], R[2,1], R[2,2], pose[2]],
             [0,0,0,1]])
        q_0 = self.current_config
        joint_angles = self.follower_robot.IK(T, q_0)

        return joint_angles

    def IK_manual_update(self, pose, q_0):
        R = euler_matrix(pose[3], pose[4], pose[5], 'sxyz')
        T = np.matrix([[R[0,0], R[0,1], R[0,2], pose[0]],
             [R[1,0], R[1,1], R[1,2], pose[1]],
             [R[2,0], R[2,1], R[2,2], pose[2]],
             [0,0,0,1]])
        joint_angles = self.follower_robot.IK(T, q_0)

        return joint_angles

    def update(self, plan):
        '''
        update current joint angles
        '''
        self.current_ee_pose = self.to_global(self.FK(plan[1]))
        self.current_config = plan[1]
