#!/usr/bin/env python
import rospy
from sensor_msgs.msg import JointState
import numpy as np
from simulation import sim_teleoperator,sim_follower_robot, sim_leader_robot
from utils.teleop_transformations import teleoperator_to_leader_transform
from tf.transformations import euler_matrix, euler_from_matrix


if __name__=='__main__':
    rospy.init_node("joint_tester")

    left_publisher = rospy.Publisher("/left_human_arm/joint_cmd", JointState, queue_size=1)
    right_publisher = rospy.Publisher("/right_human_arm/joint_cmd", JointState, queue_size=1)
    trunk_publisher = rospy.Publisher("/human_trunk/joint_cmd", JointState, queue_size=1)

    optimal_human_left_arm_publisher = rospy.Publisher("/left_optimal_human_arm/joint_cmd", JointState, queue_size=1)
    optimal_human_right_arm_publisher = rospy.Publisher("/right_optimal_human_arm/joint_cmd", JointState, queue_size=1)
    optimal_human_trunk_publisher = rospy.Publisher("/optimal_human_trunk/joint_cmd", JointState, queue_size=1)
#########################
    left_command = JointState()
    left_command.name = ['left_human_arm_j{}'.format(i) for i in range(7)]
    left_command.position = [0.0] * len(left_command.name)
    left_command.velocity = [0.0] * len(left_command.name)
    left_command.effort = [0.0] * len(left_command.name)
    right_command = JointState()
    right_command.name = ['right_human_arm_j{}'.format(i) for i in range(7)]
    right_command.position = [0.0] * len(right_command.name)
    right_command.velocity = [0.0] * len(right_command.name)
    right_command.effort = [0.0] * len(right_command.name)
    trunk_command = JointState()
    trunk_command.name = ['human_trunk_j{}'.format(i) for i in range(3)]
    trunk_command.position = [0.0] * len(trunk_command.name)
    trunk_command.velocity = [0.0] * len(trunk_command.name)
    trunk_command.effort = [0.0] * len(trunk_command.name)
#############################
    left_optimal_human_command = JointState()
    left_optimal_human_command.name = ['left_optimal_human_arm_j{}'.format(i) for i in range(7)]
    left_optimal_human_command.position = [0.0] * len(left_optimal_human_command.name)
    left_optimal_human_command.velocity = [0.0] * len(left_optimal_human_command.name)
    left_optimal_human_command.effort = [0.0] * len(left_optimal_human_command.name)
    right_optimal_human_command = JointState()
    right_optimal_human_command.name = ['right_optimal_human_arm_j{}'.format(i) for i in range(7)]
    right_optimal_human_command.position = [0.0] * len(right_optimal_human_command.name)
    right_optimal_human_command.velocity = [0.0] * len(right_optimal_human_command.name)
    right_optimal_human_command.effort = [0.0] * len(right_optimal_human_command.name)
    optimal_human_trunk_command = JointState()
    optimal_human_trunk_command.name = ['optimal_human_trunk_j{}'.format(i) for i in range(3)]
    optimal_human_trunk_command.position = [0.0] * len(optimal_human_trunk_command.name)
    optimal_human_trunk_command.velocity = [0.0] * len(optimal_human_trunk_command.name)
    optimal_human_trunk_command.effort = [0.0] * len(optimal_human_trunk_command.name)


    rate = rospy.Rate(100)

    for i in range(200):
        optimal_human_left_arm_publisher.publish(left_optimal_human_command)
        optimal_human_right_arm_publisher.publish(right_optimal_human_command)
        optimal_human_trunk_publisher.publish(optimal_human_trunk_command)

        left_publisher.publish(left_command)
        right_publisher.publish(right_command)
        trunk_publisher.publish(trunk_command)

        for j in range(0,1):

            # initial experiment 2

            # posture_deg = [10, 0, 0, 0*57.29582/4, 0*57.29582/4, 7*57.29582/4, 0*57.29582/4, 0*57.29582/4, 0*57.29582/4, 0, -90, 0, 0, 0, 0, 0, 0]
            # # posture_deg = [0, 0, 0, 90, 0, 0, 45, 0, 0, 57.2958, -0, 0, 0, 0, 0, 0, 0] # neutral posture


            # posture_deg = [0, 0, 0, 0, 0, 0, 0, 70, 0, 80, 0, 0, 0, 0, 0, 0, 0]
            # posture_deg = [0, 0, 0, 90, 0, 0, 90, 0, 0, 0, -90, 0, 0, 0, 0, 0, 0] # neutral posture
            # posture_deg = [0, 0, 0, 90, 45, 0, 90, 45, 0, 45, 90, 0, 0, 0, 0, 0, 0]
            # posture_deg = [19, 3, 8, -10, 20, -10, 130, -70, 0, 0, -90, 0, 0, 0, 0, 0, 0] # initial #2
            # posture_deg = [25, 5, 5, 50, 20, -15, 140, -30, 0, 0, -90, 0, 0, 0, 0, 0, 0] #
            posture_deg = [20, 7, 0,
                            25, 0, 20,
                            130, -50,
                            -20, -40,
                            -90, 0, 0, 0, 0, 0, 0] # initial #2
            posture_rad = [posture_deg[i]*np.pi/180 for i in range(17)]

            # posture_rad = [0.04890851, -0.05913604, -0.01685379,  1.5059509,   0.5738285 , -0.10335205,
            #                     1.4528768,   0.05133178,  0.03992346,  0.03283259 , 0, 0, 0, 0, 0, 0, 0]

            trunk_command.position[0] = posture_rad[0]
            trunk_command.position[1] = posture_rad[1]
            trunk_command.position[2] = posture_rad[2]

            right_command.position[0]= posture_rad[3]
            right_command.position[1]= posture_rad[4]
            right_command.position[2]= posture_rad[5]
            right_command.position[3]= posture_rad[6]
            right_command.position[4]= posture_rad[7]
            right_command.position[5]= posture_rad[8]
            right_command.position[6]= posture_rad[9]

            left_command.position[0]= posture_rad[10]
            left_command.position[1]= posture_rad[11]
            left_command.position[2]= posture_rad[12]
            left_command.position[3]= posture_rad[13]
            left_command.position[4]= posture_rad[14]
            left_command.position[5]= posture_rad[15]
            left_command.position[6]= posture_rad[16]

            optimal_human_trunk_command.position[0] = posture_rad[0]
            optimal_human_trunk_command.position[1] = posture_rad[1]
            optimal_human_trunk_command.position[2] = posture_rad[2]

            right_optimal_human_command.position[0]= posture_rad[3]
            right_optimal_human_command.position[1]= posture_rad[4]
            right_optimal_human_command.position[2]= posture_rad[5]
            right_optimal_human_command.position[3]= posture_rad[6]
            right_optimal_human_command.position[4]= posture_rad[7]
            right_optimal_human_command.position[5]= posture_rad[8]
            right_optimal_human_command.position[6]= posture_rad[9]

            left_optimal_human_command.position[0]= posture_rad[10]
            left_optimal_human_command.position[1]= posture_rad[11]
            left_optimal_human_command.position[2]= posture_rad[12]
            left_optimal_human_command.position[3]= posture_rad[13]
            left_optimal_human_command.position[4]= posture_rad[14]
            left_optimal_human_command.position[5]= posture_rad[15]
            left_optimal_human_command.position[6]= posture_rad[16]
        rate.sleep()
    # print posture_rad
    print posture_rad

    human = sim_teleoperator()
    fk_result_h = human.to_global(human.FK(posture_rad[0:10]))
    # print "FK_global_human:", fk_result_h


    leader = sim_leader_robot()
    follower = sim_follower_robot()

    desired_l_pose, desired_l_orientation = teleoperator_to_leader_transform(fk_result_h)
    desired_l_orientation_euler = list(euler_from_matrix(desired_l_orientation, 'sxyz'))
    print "leader initial pose _ global: " , desired_l_pose+desired_l_orientation_euler
    print "leader initial pose _ local: ", leader.to_local(desired_l_pose+desired_l_orientation_euler)

    ik_result_l = leader.IK(leader.to_local(desired_l_pose+desired_l_orientation_euler))
    ik_result_l = ik_result_l.tolist()
    print "leader_IK_result: ", ik_result_l
    # print "Leader at goal : ", leader.IK([0.45897139602639286, 0, 1.106959490782692, 2.866753208177198, 0.16949914938527663, 0.6518933184492914])

    fk_result_f_global = follower.to_global(follower.FK(ik_result_l))
    print "follower initial pose _ global:", fk_result_f_global
    fk_result_f_local = follower.FK(ik_result_l)
    print "follower initial pose _ local:", fk_result_f_local

    follower_goal_global =  [2.6293795834515223, -0.1839642316095713, 1.3320823754722355, 3.0147159302823354, 0.003613651370247501, 0.136306240839664]
    follower_goal_local = follower.to_local(follower_goal_global)
    print "follower goal _ local" , follower_goal_local
    follower_ik_at_goal = follower.IK(follower_goal_local)
    print "follower IK at goal" , follower_ik_at_goal.tolist()
