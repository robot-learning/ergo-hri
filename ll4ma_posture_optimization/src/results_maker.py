import numpy as np
from rula import *
from posture_optimization_problem import *
from solvers import *
from simulation import *
from motion_generation import *
from std_msgs.msg import Char
from sensor_msgs.msg import JointState
from utils.teleop_transformations import teleoperator_to_leader_transform
import pickle
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d

def setBoxColors(bp, edge_color, fill_color, mid_color):
	for element in ['boxes', 'whiskers', 'fliers', 'caps']:
		plt.setp(bp[element], color=edge_color)
	plt.setp(bp['medians'], color=mid_color)
	for patch in bp['boxes']:
		patch.set(facecolor=fill_color)

def smooth_traj(joint_traj, smoothing_resolution):
    '''
    joint traj: 10 DOF human trajectory

    '''
    smoothed_traj = []
    for i in range(len(joint_traj)-1):
            smoothed_traj += np.linspace(np.array(joint_traj[i]), np.array(joint_traj[i+1]),smoothing_resolution+1, endpoint=False).tolist()
    smoothed_traj.append(joint_traj[-1])

    return smoothed_traj

def smooth_traj_optimal_posture(optimal_posture_traj, smoothing_resolution):
    '''
    joint traj: 10 DOF human trajectory

    '''
    smoothed_traj = []
    for i in range(len(optimal_posture_traj)-1):
            for j in range(smoothing_resolution+1):
                smoothed_traj.append(optimal_posture_traj[i])
                smoothed_traj.append(optimal_posture_traj[-1])

    return smoothed_traj

def plot_results(wc_traj_teleoperator_joint_smooth, nc_traj_teleoperator_joint_smooth, wc_traj_optimal_posture_smooth, nc_traj_optimal_posture_smooth,
             wc_traj_teleoperator_ee_smooth, nc_traj_teleoperator_ee_smooth, wc_traj_RULA_teleoperator_smooth, nc_traj_RULA_teleoperator_smooth,
             wc_traj_leader_ee_smooth, nc_traj_leader_ee_smooth, wc_traj_leader_joint_smooth, nc_traj_leader_joint_smooth,
             wc_traj_RULA_optimal_smooth, nc_traj_RULA_optimal_smooth, wc_traj_follower_ee_smooth, nc_traj_follower_ee_smooth,
             wc_traj_follower_joint_smooth, nc_traj_follower_joint_smooth, wc_traj_RULA_teleoperator, nc_traj_RULA_teleoperator):

    plt.rcParams.update({'font.size':14})
    plt.rcParams['text.usetex'] = True
    plt.rcParams['text.latex.preamble'] = [r'\usepackage{amsmath}']

    blue_circle = dict(markerfacecolor='deepskyblue', marker='o') ## TODO: move to above initialization part of the function
    peru_circle = dict(markerfacecolor='olivedrab', marker='o')
    red_circle = dict(markerfacecolor='red', marker='o')
    #############################################################################3
    #Plot (1): RULA of wc, nc before smoothing
    fig, ax = plt.subplots()
    ax.plot(wc_traj_RULA_teleoperator)
    ax.plot(nc_traj_RULA_teleoperator)
    ax.plot(wc_traj_RULA_optimal)

    ax.set_ylabel(r'$\mathrm{RULA~Score}$', fontsize=20)
    ax.set_xlabel(r'$\mathrm{Time~(s)}$', fontsize=20)
    ax.set_yticklabels([r'$\mathrm{1}$', r'$\mathrm{2}$', r'$\mathrm{3}$', r'$\mathrm{4}$', r'$\mathrm{5}$', r'$\mathrm{6}$', r'$\mathrm{7}$'])
    ax.set_yticks([1, 2, 3, 4, 5, 6, 7])
    ax.legend([r'$\mathrm{With~Postural~Correction}$', r'$\mathrm{Without~Postural~Correction}$', r'$\mathrm{Optimal~Posture}$' ], loc='upper right', ncol=1, fontsize=14)
    ax.set_ylim((1,8))


    ############################################################################
    # # Plot (2): RULA of wc, nc after smoothing
    # fig, ax = plt.subplots()
    # ax.plot(wc_traj_RULA_teleoperator_smooth)
    # ax.plot(nc_traj_RULA_teleoperator_smooth)
    #
    # ax.set_ylabel(r'$\mathrm{RULA~Score}$', fontsize=20)
    # ax.set_xlabel(r'$\mathrm{Time~(s)}$', fontsize=20)
    # ax.set_yticklabels([r'$\mathrm{1}$', r'$\mathrm{2}$', r'$\mathrm{3}$', r'$\mathrm{4}$', r'$\mathrm{5}$', r'$\mathrm{6}$', r'$\mathrm{7}$'])
    # ax.set_yticks([1, 2, 3, 4, 5, 6, 7])
    # ax.legend([r'$\mathrm{Withour~Postural~Correction}$', r'$\mathrm{With~Postural~Correction}$' ], loc='upper right', ncol=1, fontsize=14)
    # ax.set_ylim((1,8))

    ###########################################################################
    # Plot (3): traj of wc, nc after smoothing
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    # ax.set_aspect('equal')

    wc_x = np.array([wc_traj_teleoperator_ee[t][0] for t in range(1,len(wc_traj_teleoperator_ee))])
    wc_y = np.array([wc_traj_teleoperator_ee[t][1] for t in range(1,len(wc_traj_teleoperator_ee))])
    wc_z = np.array([wc_traj_teleoperator_ee[t][2] for t in range(1,len(wc_traj_teleoperator_ee))])
    nc_x = np.array([nc_traj_teleoperator_ee[t][0] for t in range(1,len(nc_traj_teleoperator_ee))])
    nc_y = np.array([nc_traj_teleoperator_ee[t][1] for t in range(1,len(nc_traj_teleoperator_ee))])
    nc_z = np.array([nc_traj_teleoperator_ee[t][2] for t in range(1,len(nc_traj_teleoperator_ee))])
    ax.plot(wc_x,wc_y,wc_z, zdir='z')
    ax.plot(nc_x,nc_y,nc_z, zdir='z' )
    ax.set_xlabel(r'$\mathrm{X}$', fontsize=14)
    ax.set_ylabel(r'$\mathrm{Y}$', fontsize=14)
    ax.set_zlabel(r'$\mathrm{Z}$', fontsize=14)

    max_range = np.array([wc_x.max()-wc_x.min(), wc_y.max()-wc_y.min(), wc_z.max()-wc_z.min()]).max() / 2.0

    mid_x = (wc_x.max()+wc_x.min()) * 0.5
    mid_y = (wc_y.max()+wc_y.min()) * 0.5
    mid_z = (wc_z.max()+wc_z.min()) * 0.5
    ax.set_xlim(mid_x - max_range, mid_x + max_range)
    ax.set_ylim(mid_y - max_range, mid_y + max_range)
    ax.set_zlim(mid_z - max_range, mid_z + max_range)
    ax.view_init(30,-160)
    ax.legend([r'$\mathrm{With~Postural~Correction}$', r'$\mathrm{Without~Postural~Correction}$' ], loc='upper right', ncol=1, fontsize=14)

    ###########################################################################
    # Plot (4): bar plot of RULA scores

    fig,ax = plt.subplots()
    width = 0.15

    # adding points to the box plot
    print np.mean(wc_traj_RULA_teleoperator), np.std(wc_traj_RULA_teleoperator)

    bp1= ax.boxplot(wc_traj_RULA_teleoperator, positions = [0.1],patch_artist=True, flierprops=blue_circle, showfliers=True)
    setBoxColors(bp1, 'black', 'deepskyblue', 'red')

    bp2= ax.boxplot(nc_traj_RULA_teleoperator, positions = [0.4], patch_artist=True, flierprops=blue_circle, showfliers=True)
    setBoxColors(bp2, 'black', 'coral', 'red')

    # setting the axes names, lables and numbers
    ax.set_xticklabels([r'$\mathrm{With~Postural}$'+ '\n' + r'$\mathrm{~Correction}$', r'$\mathrm{Without~Postural}$'+ '\n' + r'$\mathrm{~Correction}$'], fontsize=14)
    ax.set_xticks([0.1, 0.4])
    ax.set_yticks([2, 3, 4, 5, 6, 7])
    ax.set_xlim(0, 0.8)
    ax.set_ylabel(r'$\mathrm{RULA Score}$')
    # setting the legend
    # ax.legend([r'$\mathrm{With~Postural~Correction}$', r'$\mathrm{Without~Postural~Correction}$' ], loc='upper right', ncol=1, fontsize=14)
    # grid on
    ax.yaxis.grid(True)
    # saving the plot figure
    # plt.savefig("/home/amir/PF_paper_figures/PF_IKvsMocap_error_all_trials_vs_tasks-boxplot_{}.pdf".format(joint_limit_type), dpi =300, bbox_inches='tight')

    plt.show()




if __name__ == '__main__':

    teleoperator = sim_teleoperator()
    leader = sim_leader_robot()
    follower = sim_follower_robot()
    task = Task()
    RULA = RULA_assessment()
    # problem = Posture_Optimization(teleoperator, RULA)
    smoothing_resolution = 50 # number of point to insert linrearly between two setpoints

    experiment = "1"
    # result_with_correction_pickle = open("/home/amir/posture_optimization_results/derivative_free/exp_{0}_correction_True_angV_teleoperator_8.5.pickle".format(experiment),"rb")
    # result_no_correction_pickle = open("/home/amir/posture_optimization_results/derivative_free/exp_{0}_correction_False_angV_teleoperator_8.5.pickle".format(experiment),"rb")

    # result_with_correction_pickle = open("/home/amir/posture_optimization_results/derivative_based/exp_{0}_correction_True_angV_teleoperator_10.pickle".format(experiment),"rb")
    result_with_correction_pickle = open("/home/amir/posture_optimization_results/derivative_based/exp_{0}_correction_True.pickle".format(experiment),"rb")
    result_no_correction_pickle = open("/home/amir/posture_optimization_results/derivative_based/exp_{0}_correction_False.pickle".format(experiment),"rb")

    [wc_traj_teleoperator_ee, wc_traj_teleoperator_joint, wc_traj_RULA_teleoperator,
    wc_traj_optimal_posture, wc_traj_RULA_optimal, wc_traj_leader_ee,
    wc_traj_leader_joint, wc_traj_follower_ee, wc_traj_follower_joint] = pickle.load(result_with_correction_pickle)

    [nc_traj_teleoperator_ee, nc_traj_teleoperator_joint, nc_traj_RULA_teleoperator,
    nc_traj_optimal_posture, nc_traj_RULA_optimal, nc_traj_leader_ee,
    nc_traj_leader_joint, nc_traj_follower_ee, nc_traj_follower_joint] = pickle.load(result_no_correction_pickle)


    # closing the pickle files
    result_with_correction_pickle.close()
    result_no_correction_pickle.close()

    #initial config of leader for IK solver
    wc_q0_leader = wc_traj_leader_joint[0]
    nc_q0_leader = nc_traj_leader_joint[0]
    #initial config of follower for IK solver
    wc_q0_follower = wc_traj_follower_joint[0]
    nc_q0_follower = nc_traj_follower_joint[0]

    # smoothing the joint trajectories of teleoperator
    wc_traj_teleoperator_joint_smooth = smooth_traj(wc_traj_teleoperator_joint, smoothing_resolution)
    nc_traj_teleoperator_joint_smooth = smooth_traj(nc_traj_teleoperator_joint, smoothing_resolution)


    # smoothing the joint trajectories of teleoperator
    wc_traj_optimal_posture_smooth = smooth_traj_optimal_posture(wc_traj_optimal_posture, smoothing_resolution)
    nc_traj_optimal_posture_smooth = smooth_traj_optimal_posture(nc_traj_optimal_posture, smoothing_resolution)

    # declaring and initializing new variables
    wc_traj_teleoperator_ee_smooth = []
    nc_traj_teleoperator_ee_smooth = []

    wc_traj_RULA_teleoperator_smooth = []
    nc_traj_RULA_teleoperator_smooth = []

    wc_traj_leader_ee_smooth = []
    nc_traj_leader_ee_smooth = []

    wc_traj_leader_joint_smooth = []
    nc_traj_leader_joint_smooth = []

    wc_traj_follower_ee_smooth = [wc_traj_follower_ee[0]]
    nc_traj_follower_ee_smooth = [nc_traj_follower_ee[0]]

    wc_traj_follower_joint_smooth = [wc_traj_follower_joint[0]]
    nc_traj_follower_joint_smooth = [nc_traj_follower_joint[0]]

    wc_traj_RULA_optimal_smooth = []
    nc_traj_RULA_optimal_smooth = []

    # calculating EE trajectories
    for i in range(len(wc_traj_teleoperator_joint_smooth)):
        wc_traj_teleoperator_ee_smooth.append(teleoperator.to_global(teleoperator.FK(wc_traj_teleoperator_joint_smooth[i])))

        wc_traj_RULA_teleoperator_smooth.append(RULA.rula_score(wc_traj_teleoperator_joint_smooth[i]))

        wc_leader_pos, wc_leader_ori = teleoperator_to_leader_transform(wc_traj_teleoperator_ee_smooth[i]) #leader_ori is in 4x4 matrix rotatin
        wc_leader_ori = euler_from_matrix(wc_leader_ori, 'sxyz') #returns ori in euler angles
        wc_traj_leader_ee_smooth.append([wc_leader_pos[0], wc_leader_pos[1], wc_leader_pos[2], wc_leader_ori[0], wc_leader_ori[1], wc_leader_ori[2]])

        wc_traj_leader_joint_smooth.append(leader.IK_manual_update(leader.to_local(wc_traj_leader_ee_smooth[i]),wc_q0_leader))

        wc_traj_RULA_optimal_smooth.append(RULA.rula_score(wc_traj_optimal_posture_smooth[i]))

        if i >0:
            wc_follower_ee_data = []
            for j in range(3):
                wc_follower_ee_data.append(wc_traj_follower_ee_smooth [i-1][j] + leader.teleop_scale[j] * (wc_traj_leader_ee_smooth[i][j] - wc_traj_leader_ee_smooth[i-1][j]))
            for j in range(3):
                wc_follower_ee_data.append(wc_traj_leader_ee_smooth[i][j]) # TODO: this might cause some problem because it is different than the planner result

            wc_traj_follower_ee_smooth.append(wc_follower_ee_data)

            wc_traj_follower_joint_smooth.append(follower.IK_manual_update(follower.to_local(wc_traj_follower_ee_smooth[i]),wc_q0_follower))

            #initial config of leader for IK solver
            wc_q0_leader = wc_traj_leader_joint_smooth[-1]
            #initial config of follower for IK solver
            wc_q0_follower = wc_traj_follower_joint_smooth[-1]

    for i in range(len(nc_traj_teleoperator_joint_smooth)):
        nc_traj_teleoperator_ee_smooth.append(teleoperator.to_global(teleoperator.FK(nc_traj_teleoperator_joint_smooth[i])))

        nc_traj_RULA_teleoperator_smooth.append(RULA.rula_score(nc_traj_teleoperator_joint_smooth[i]))

        nc_leader_pos, nc_leader_ori = teleoperator_to_leader_transform(nc_traj_teleoperator_ee_smooth[i]) #leader_ori is in 4x4 matrix rotatin
        nc_leader_ori = euler_from_matrix(nc_leader_ori, 'sxyz') #returns ori in euler angles
        nc_traj_leader_ee_smooth.append([nc_leader_pos[0], nc_leader_pos[1], nc_leader_pos[2], nc_leader_ori[0], nc_leader_ori[1], nc_leader_ori[2]])

        nc_traj_leader_joint_smooth.append(leader.IK_manual_update(leader.to_local(nc_traj_leader_ee_smooth[i]),nc_q0_leader))

        nc_traj_RULA_optimal_smooth.append(RULA.rula_score(nc_traj_optimal_posture_smooth[i]))


        if i > 0:
            nc_follower_ee_data = []
            for j in range(3):
                nc_follower_ee_data.append(nc_traj_follower_ee_smooth [i-1][j] + leader.teleop_scale[j] * (nc_traj_leader_ee_smooth[i][j] - nc_traj_leader_ee_smooth[i-1][j]))
            for j in range(3):
                nc_follower_ee_data.append(nc_traj_leader_ee_smooth[i][j]) # TODO: this might cause some problem because it is different than the planner result

            nc_traj_follower_ee_smooth.append(nc_follower_ee_data)

            nc_traj_follower_joint_smooth.append(follower.IK_manual_update(follower.to_local(nc_traj_follower_ee_smooth[i]),nc_q0_follower))

            #initial config of leader for IK solver
            nc_q0_leader = nc_traj_leader_joint_smooth[-1]
            #initial config of follower for IK solver
            nc_q0_follower = nc_traj_follower_joint_smooth[-1]




    plot_results(wc_traj_teleoperator_joint_smooth, nc_traj_teleoperator_joint_smooth, wc_traj_optimal_posture_smooth, nc_traj_optimal_posture_smooth,
                 wc_traj_teleoperator_ee_smooth, nc_traj_teleoperator_ee_smooth, wc_traj_RULA_teleoperator_smooth, nc_traj_RULA_teleoperator_smooth,
                 wc_traj_leader_ee_smooth, nc_traj_leader_ee_smooth, wc_traj_leader_joint_smooth, nc_traj_leader_joint_smooth,
                 wc_traj_RULA_optimal_smooth, nc_traj_RULA_optimal_smooth, wc_traj_follower_ee_smooth, nc_traj_follower_ee_smooth,
                 wc_traj_follower_joint_smooth, nc_traj_follower_joint_smooth, wc_traj_RULA_teleoperator, nc_traj_RULA_teleoperator)
